package com.grupocassa.agrimobile;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.grupocassa.agrimobile.CASSAUTILS.DBHelper;
import com.grupocassa.agrimobile.CASSAUTILS.NetworkUtility;

import com.grupocassa.agrimobile.CLS.Parametros;
import com.grupocassa.agrimobile.CLS.VolleyCallback;

/*
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;*/
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


//import okhttp3.*;

import static androidx.constraintlayout.widget.Constraints.TAG;
//import static com.grupocassa.agrimobile.CASSAUTILS.EnviarApuntes.JSON;

public class Splash extends Activity {

    DBHelper mydb;
    private static int SPLASH_TIME_OUT = 3000;
    //OkHttpClient client = new OkHttpClient();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        mydb = new DBHelper(this);


        RecuperarEmpresas(new VolleyCallback() {
            @Override
            public boolean onSuccess() {

                return true;
            }

            @Override
            public boolean onError() {

                return false;
            }

            @Override
            public boolean onSuccess(String resp) {

                return false;
            }

            @Override
            public boolean onError(String resp) {


                return false;
            }
        });
        LlamarHttps(new VolleyCallback() {
            @Override
            public boolean onSuccess() {


                Intent i = new Intent(Splash.this, MainActivity.class);
                startActivity(i);
                finish();

                return true;


            }

            @Override
            public boolean onError() {

                Parametros ses = mydb.getSingleParametro(4);
                if (ses != null) {
                    Intent i = new Intent(Splash.this, MainActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    Toast.makeText(Splash.this, "Se cerró la sesión", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(Splash.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
                return false;
            }

            @Override
            public boolean onSuccess(String resp) {

                Intent i = new Intent(Splash.this, MainActivity.class);
                startActivity(i);
                finish();
                return false;
            }

            @Override
            public boolean onError(String resp) {

                Parametros ses = mydb.getSingleParametro(4);
                if (ses != null) {
                    Intent i = new Intent(Splash.this, MainActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    Toast.makeText(Splash.this, "Se cerró la sesión", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(Splash.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }

                return false;
            }
        });
/*
        new Handler().postDelayed(new Runnable() {



            @Override
            public void run() {
                TestTask t = new TestTask();
                t.execute();
            }
        }, SPLASH_TIME_OUT);*/
    }
        private void LlamarHttps( final VolleyCallback callBack) {
            String url = DBHelper.urlServer + "/api/Sesiones/Test";
            //HttpGet httpPost = new HttpGet(DBHelper.urlServer+"/api/Sesiones/Test");
            final boolean[] ok = {false};
            DBHelper mydb = new DBHelper(Splash.this);
            Parametros token = mydb.getSingleParametro(4);
            String sToken = "";
            if (token != null) {
                sToken = token.getValor();
            }
            else {
                callBack.onError();
                return;
            }/*
            HttpClient httpClient = new NetworkUtility().getDefaultHttpClient(Splash.this, DBHelper.urlServer+"/api/Sesiones/Test");
            httpPost.setHeader("token", sToken);*/

            /*
                Request request = new Request.Builder()
                        .url(url)
                        .addHeader("token",sToken)
                        .build();

                Response resp = client.newCall(request).execute();

                String response = resp.body().string();*/


            RequestQueue requestQueue = NetworkUtility.getInstance(getApplicationContext()).getRequestQueue();
            NetworkUtility.nuke();
            final String finalSToken = sToken;

            JsonObjectRequest
                    jsonObjectRequest
                    = new JsonObjectRequest(
                    Request.Method.GET,
                    url,
                    null,
                    new Response.Listener() {
                        @Override
                        public void onResponse(Object response) {
                            //HttpResponse response = httpClient.execute(httpPost);
                            if (response != null) {


                                JSONObject data = (JSONObject) response;
                                try {
                                    ok[0] = data.getBoolean("Key");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                callBack.onSuccess();
                                return;

                            } else {
                                callBack.onError();
                                return;
                            }
                        }

                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                        }
                    }) {

                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", "application/json");
                    headers.put("token", finalSToken);
                    return headers;
                }

            };


            requestQueue.add(jsonObjectRequest);



        }


        private void RecuperarEmpresas(final VolleyCallback callback) {
            //HttpGet httpPost = new HttpGet(DBHelper.urlServer+"/api/DatosMaestros/Empresas");
            String url = DBHelper.urlServer + "/api/DatosMaestros/Empresas";
            final boolean ok = false;
            final DBHelper mydb = new DBHelper(Splash.this);
            String sToken = "";
/*
        HttpClient httpClient = new NetworkUtility().getDefaultHttpClient(Splash.this, DBHelper.urlServer+"/api/Sesiones/Test");
        httpPost.setHeader("token", sToken);*/
            final RequestQueue requestQueue = NetworkUtility.getInstance(getApplicationContext()).getRequestQueue();
            NetworkUtility.nuke();
            final String finalSToken = sToken;

            JsonArrayRequest
                    jsonArrayRequest
                    = new JsonArrayRequest(
                    Request.Method.GET,
                    url,
                    null,
                    new Response.Listener() {
                        @Override
                        public void onResponse(Object response) {
                            //HttpResponse response = httpClient.execute(httpPost);
                            if (response != null) {


                                JSONArray a = null;
                                try {
                                    a = (JSONArray) response;// new JSONArray(response);
                                    if (a.length() > 0) {
                                        mydb.deleteEmpresas();

                                        for (int i = 0; i < a.length(); i++) {
                                            JSONObject hda = a.getJSONObject(i);
                                            int id = hda.getInt("<Sociedad>k__BackingField");
                                            String Ds = hda.getString("<DsEmpresa>k__BackingField");
                                            mydb.insertEmpresa(id, Ds.trim());
                                        }
                                    }
                                    callback.onSuccess();
                                } catch (JSONException e1) {
                                    e1.printStackTrace();
                                    callback.onError();
                                }

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    });


            requestQueue.add(jsonArrayRequest);

            //BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));


            //StringBuffer json = new StringBuffer(1024);
                /*String tmp = "";
                while ((tmp = reader.readLine()) != null)
                    json.append(tmp).append("\n");
                reader.close();
*/
            //JSONObject data = new JSONArray(json.toString());


        }






}
