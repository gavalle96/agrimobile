package com.grupocassa.agrimobile.CASSAUTILS;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.grupocassa.agrimobile.CLS.Haciendas;
import com.grupocassa.agrimobile.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gerardo.valle on 19/7/2017.
 */


public class CustomHdasAdapter extends ArrayAdapter<Haciendas> implements Filterable {
    private List<Haciendas> entries;
    private ArrayList<Haciendas> orig;
    private Activity activity;
    private ArrayFilter myFilter;

    public CustomHdasAdapter(Activity a, int textViewResourceId, ArrayList<Haciendas> entries) {
        super(a, textViewResourceId, entries);
        this.entries = entries;
        this.activity = a;
    }

    public static class ViewHolder{
        public TextView tv_ac_name;
    }

    @Override
    public int getCount(){
        return entries!=null ? entries.size() : 0;
    }

    @Override
    public Haciendas getItem(int index) {
        return entries.get(index);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder holder;
        if (v == null) {
            LayoutInflater vi =
                    (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.autocompleteitem, null);
            holder = new ViewHolder();
            holder.tv_ac_name = (TextView) v.findViewById(R.id.txtautocomplete);
            v.setTag(holder);
        }
        else
            holder=(ViewHolder)v.getTag();

        final Haciendas custom = entries.get(position);
        if (custom != null) {
            holder.tv_ac_name.setText(custom.getBkHacienda()+" - "+custom.getDsHacienda());
        }
        return v;
    }

    @Override
    public Filter getFilter() {
        if (myFilter == null){
            myFilter = new ArrayFilter();
        }
        return myFilter;
    }
//POSIBLE BUG////////////////////////////////////////////////////////////////////7
    @Override
    public String toString() {
        String temp = getClass().getName();
        return temp;
    }
//////////////////////////////////////////////////////////////////////////////7
    private class ArrayFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if (orig == null)
                orig = new ArrayList<Haciendas>(entries);
            if (constraint != null && constraint.length() != 0) {
                ArrayList<Haciendas> resultsSuggestions = new ArrayList<Haciendas>();
                for (int i = 0; i < orig.size(); i++) {
                    if(orig.get(i).getBkHacienda().toLowerCase().startsWith(constraint.toString().toLowerCase()) || orig.get(i).getDsHacienda().toLowerCase().contains(constraint.toString().toLowerCase())){
                        resultsSuggestions.add(orig.get(i));
                    }
                }

                results.values = resultsSuggestions;
                results.count = resultsSuggestions.size();

            }
            else {
                ArrayList <Haciendas> list = new ArrayList <Haciendas>(orig);
                results.values = list;
                results.count = list.size();
            }
            return results;
        }

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            return ((Haciendas) resultValue).getBkHacienda()+" - "+((Haciendas) resultValue).getDsHacienda();
        }
        @Override
        @SuppressWarnings("unchecked")
        protected void publishResults(CharSequence constraint, FilterResults results) {
            clear();
            ArrayList<Haciendas> newValues = (ArrayList<Haciendas>) results.values;
            if(newValues !=null) {
                for (int i = 0; i < newValues.size(); i++) {
                    add(newValues.get(i));
                }
                if(results.count>0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }

        }

    }
}
