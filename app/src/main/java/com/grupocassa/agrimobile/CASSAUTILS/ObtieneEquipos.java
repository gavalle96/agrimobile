package com.grupocassa.agrimobile.CASSAUTILS;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.grupocassa.agrimobile.CLS.Cuadrillas;
import com.grupocassa.agrimobile.CLS.Equipos;
import com.grupocassa.agrimobile.CLS.HuellasTrabajadores;
import com.grupocassa.agrimobile.CLS.Parametros;
import com.grupocassa.agrimobile.CLS.Trabajadores;
import com.grupocassa.agrimobile.CLS.VolleyCallback;
import com.grupocassa.agrimobile.MaestrosActivities.ObtenerEquiposActivity;
/*
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;*/
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
/*
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;*/

/**
 * Created by gerardo.valle on 11/10/2017.
 */

public class ObtieneEquipos extends AsyncTask<Void, Void, Boolean> {
    private ObtenerEquiposActivity activity;
    //OkHttpClient client = new OkHttpClient();
    @Override
    protected Boolean doInBackground(Void... params) {
        ObtenerEquipos(new VolleyCallback() {
            @Override
            public boolean onSuccess() {
                FinalizarBG();
                return false;
            }

            @Override
            public boolean onError() {
                FinalizarBG();
                return false;
            }

            @Override
            public boolean onSuccess(String resp) {
                FinalizarBG();
                return false;
            }

            @Override
            public boolean onError(String resp) {
                FinalizarBG();
                return false;
            }
        });
        return true;
    }

    public ObtieneEquipos(ObtenerEquiposActivity activity1){
        activity = activity1;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);


    }

    private void FinalizarBG(){
        activity.runOnUiThread(new Runnable() {

            @Override
            public void run() {

                activity.oe = null;
                activity.pgProgreso.setVisibility(View.GONE);
                activity.btnConsultar.setVisibility(View.VISIBLE);
                activity.btnDescargar.setVisibility(View.VISIBLE);
                activity.CargarEquipos();
            }
        });
    }

    private void ObtenerEquipos(final VolleyCallback callback){
        final DBHelper mydb = new DBHelper(activity);
        DBExterno dbExterno = null;


        String pendMsg = "";


        String url = DBHelper.urlServer+"/api/DatosMaestros/GetDatosMaestrosFallas?actParo=false&actFlota=false&actEquipo=true&actHaciendas=false&actTrabajadores=false";
        //HttpGet httpGet = new HttpGet(DBHelper.urlServer+"/api/DatosMaestros/GetDatosMaestrosFallas?actParo=false&actFlota=false&actEquipo=true&actHaciendas=false&actTrabajadores=false");

        //httpPost.setEntity(entity);
        //HttpClient httpClient = new NetworkUtility().getDefaultHttpClient(this.activity, DBHelper.urlServer+"/api/DatosMaestros/GetDatosMaestrosFallas?actParo=false&actFlota=false&actEquipo=true&actHaciendas=false&actTrabajadores=false");
        RequestQueue requestQueue = NetworkUtility.getInstance(activity.getApplicationContext()).getRequestQueue();
        NetworkUtility.nuke();


        JsonObjectRequest
                jsonObjectRequest
                = new JsonObjectRequest(
                Request.Method.GET,
                url,
                null,
                new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {
                        //HttpResponse response = httpClient.execute(httpPost);
                        if (response != null) {


                            JSONObject data = (JSONObject) response;
                            try {
                                ArrayList<Cuadrillas> hdasResult = new ArrayList<Cuadrillas>();
                                if (data.getBoolean("<error>k__BackingField") == true) {
                                    String error = data.getString("<msgError>k__BackingField");
                                    callback.onError(error);
                                } else {

                                    ArrayList<Equipos> eqresult = new ArrayList<Equipos>();
                                    JSONArray a = data.getJSONArray("<Equipos>k__BackingField");
                                    for (int i = 0; i < a.length(); i++) {
                                        JSONObject hda = a.getJSONObject(i);
                                        int id = hda.getInt("<IdEquipo>k__BackingField");
                                        String Bk = hda.getString("<CodEquipo>k__BackingField");
                                        String Ds = hda.getString("<NombreEquipo>k__BackingField");
                                        String prop = hda.getString("<PropEquipo>k__BackingField");
                                        int IdSubFlota = hda.getInt("<Id_SubFlota>k__BackingField");
                                        Equipos eq = new Equipos(id, Bk.trim(), Ds.trim(),prop.trim(), IdSubFlota);
                                        eqresult.add(eq);
                                    }

                                    ArrayList<Equipos> existentes = mydb.getAllEquipos();
                                    //if (existentes.size() != eqresult.size()){
                                    mydb.deleteEquipos();
                                    for (Equipos e:
                                            eqresult) {
                                        mydb.insertEquipo(e);
                                    }
                                    //}

                                    mydb.deleteRecursosSubflotas();
                                    JSONArray b = data.getJSONArray("<RecursosSubFlota>k__BackingField");
                                    for (int i = 0; i < b.length(); i++) {
                                        JSONObject hda = b.getJSONObject(i);
                                        int idRecurso = hda.getInt("<IdRecurso>k__BackingField");
                                        int IdSubFlota = hda.getInt("<Id_SubFlota>k__BackingField");
                                        mydb.insertRecursosFlotas(IdSubFlota,idRecurso);
                                    }


                                    callback.onSuccess("Equipos recuperados correctamente");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                callback.onError();
                            }


                        } else {
                            callback.onError();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }) {

            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");

                return headers;
            }

        };


        requestQueue.add(jsonObjectRequest);

    }

}
