package com.grupocassa.agrimobile.CASSAUTILS;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.grupocassa.agrimobile.CLS.Haciendas;
import com.grupocassa.agrimobile.CLS.Parametros;
import com.grupocassa.agrimobile.CLS.VolleyCallback;
import com.grupocassa.agrimobile.LoginActivity;
import com.grupocassa.agrimobile.MainActivity;
/*
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;*/
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
/*
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;*/

/**
 * Created by gerardo.valle on 17/7/2017.
 */


public class RestHttpsClient extends AsyncTask<Void, Void, String> {

    public LoginActivity activity;
    private MainActivity mainActivity;
    private ArrayList<Haciendas> retornoHdas;
    String imei, msgAuth = "", msgDataMaestra = "", msgEnvio = "";
    Boolean autenticado = false, obtenido = false;

 //   OkHttpClient client = new OkHttpClient();

    public RestHttpsClient(MainActivity activity1, String pImei) {
        this.mainActivity = activity1;
        this.imei = pImei;
    }

    public RestHttpsClient(LoginActivity activity1i) {
        this.activity = activity1i;

    }

    protected String doInBackground(Void... params) {

        DBHelper mydb = new DBHelper(activity);
        try {
            LlamarHttps(new VolleyCallback() {
                @Override
                public boolean onSuccess() {
                    return false;
                }

                @Override
                public boolean onError() {
                    return false;
                }

                @Override
                public boolean onSuccess(String resp) {
                    msgAuth = resp ;
                    return false;
                }

                @Override
                public boolean onError(String resp) {
                    msgAuth = resp ;
                    return false;
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
       /* if (autenticado == false) {

            try {
                msgAuth =  LlamarHttps();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }*/
        //msgDataMaestra = getDatosMaestros();

        /*ArrayList<Apuntamientos> apuntesEnvio = mydb.getApuntamientosNoEnviados();
        if(apuntesEnvio != null){
            if(!apuntesEnvio.isEmpty()){
                msgEnvio = EnviarApuntamientos(apuntesEnvio);
            }
        }*/

        return msgDataMaestra;

    }

    @Override
    protected void onPostExecute(String consumer) {

        if (!msgAuth.equals("")) {
            Toast.makeText(this.activity, msgAuth, Toast.LENGTH_SHORT).show();
        }

        if (!msgDataMaestra.equals("")) {

            Toast.makeText(this.activity, msgDataMaestra, Toast.LENGTH_SHORT).show();
        }
        if (!msgEnvio.equals("")) {

            Toast.makeText(this.activity, msgEnvio, Toast.LENGTH_SHORT).show();
        }
        if(activity.logged == true){
            activity.GoToMain();
        }
        //activity.Reiniciar();



    }


    private void LlamarHttps(final VolleyCallback callback) throws IOException {
        String url = DBHelper.urlServer+"/api/Sesiones/Test";
        //HttpGet httpPost = new HttpGet(DBHelper.urlServer+"/api/Sesiones/Test");

        DBHelper mydb = new DBHelper(activity);
        Parametros token = mydb.getSingleParametro(4);
        String sToken = "";
        if (token != null) {
            sToken = token.getValor();
        }
        //HttpClient httpClient = new NetworkUtility().getDefaultHttpClient(this.activity, DBHelper.urlServer+"/api/Sesiones/Test");
        //httpPost.setHeader("token", sToken);
        RequestQueue requestQueue = NetworkUtility.getInstance(activity.getApplicationContext()).getRequestQueue();
        NetworkUtility.nuke();
        final String finalSToken = sToken;

        JsonObjectRequest
                jsonObjectRequest
                = new JsonObjectRequest(
                Request.Method.GET,
                url,
                null,
                new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {
                        //HttpResponse response = httpClient.execute(httpPost);
                        if (response != null) {


                            JSONObject data = (JSONObject) response;
                            try {
                                autenticado = data.getBoolean("Key");

                                if (autenticado == false) {
                                    String responseStr = data.getString("Value");
                                    callback.onSuccess("Respuesta recibida del servidor. Respuesta: " + responseStr);
                                } else {
                                    activity.logged = true;
                                    callback.onSuccess("");

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                callback.onError("Ocurrió un error al recuperar información");
                            }


                        } else {
                            callback.onError("El servidor no devolvió datos");
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }) {

            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("token", finalSToken);
                return headers;
            }

        };


        requestQueue.add(jsonObjectRequest);



    }


    private void Autenticar(final VolleyCallback callback) {

        final DBHelper mydb = new DBHelper(activity);

        Parametros appUsr = mydb.getSingleParametro(1);
        Parametros appPwd = mydb.getSingleParametro(2);

        String sAppUsr = "";
        String sAppPwd = "";

        if (appUsr != null) {
            sAppUsr = appUsr.getValor();
        }

        if (appPwd != null) {
            sAppPwd = appPwd.getValor();
        }




        String url = DBHelper.urlServer+"/api/Sesiones/ImeiLogin?imei=" + imei + "&appUsr=" + sAppUsr + "&appPwd=" + sAppPwd;
       /* List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
        nameValuePairs.add(new BasicNameValuePair("imei", imei));
        nameValuePairs.add(new BasicNameValuePair("appUsr", sAppUsr));
        nameValuePairs.add(new BasicNameValuePair("appPwd", sAppPwd));*/

        //HttpGet httpGet = new HttpGet(DBHelper.urlServer+"/api/Sesiones/ImeiLogin?imei=" + imei + "&appUsr=" + sAppUsr + "&appPwd=" + sAppPwd);
        RequestQueue requestQueue = NetworkUtility.getInstance(activity.getApplicationContext()).getRequestQueue();
        NetworkUtility.nuke();


        JsonObjectRequest
                jsonObjectRequest
                = new JsonObjectRequest(
                Request.Method.GET,
                url,
                null,
                new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {
                        //HttpResponse response = httpClient.execute(httpPost);
                        if (response != null) {


                            JSONObject data = (JSONObject) response;
                            try {
                                String responseStr = "";
                                if (data.getBoolean("Key") == true) {
                                    if (mydb.getSingleParametro(4) == null) {
                                        mydb.insertParametro(4,  data.getString("Value"));
                                    } else {
                                        mydb.updateParametro(4,  data.getString("Value"));
                                    }
                                    callback.onSuccess("Autenticación exitosa");
                                } else {
                                    responseStr = data.getString("Value");//EntityUtils.toString(response.getEntity());
                                    callback.onError("Respuesta recibida del servidor: " + responseStr);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                callback.onError();
                            }


                        } else {
                            callback.onError();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }) {

            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");

                return headers;
            }

        };


        requestQueue.add(jsonObjectRequest);
        //HttpClient httpClient = new NetworkUtility().getDefaultHttpClient(this.activity, DBHelper.urlServer+"/api/Sesiones/ServiceLogin?imei=" + "356605058346996"/*imei*/ + "&appUsr=" + sAppUsr + "&appPwd=" + sAppPwd);

    }


    private void getDatosMaestros(final VolleyCallback callback) {

        final DBHelper mydb = new DBHelper(activity);

        String sAppId;
        Parametros appId = mydb.getSingleParametro(3);
        if (appId != null) {
            sAppId = appId.getValor();
        }

        UUID guid = null;
        String sToken = "";
        Parametros appid = mydb.getSingleParametro(3);
        Parametros token = mydb.getSingleParametro(4);

        if (appid != null) {
            guid = guid.fromString(appid.getValor());
        }
        if (guid == null) callback.onError("Aplicación no autenticada");

        if (token != null) {
            sToken = token.getValor();
        }
        String url = DBHelper.urlServer+"/api/DatosMaestros/GetDatosMaestros?aplicaciones[0]=" + guid.toString();
        //HttpGet httpGet = new HttpGet(DBHelper.urlServer+"/api/DatosMaestros/GetDatosMaestros?aplicaciones[0]=" + guid.toString());
        //httpGet.setHeader("token", sToken);
        //HttpClient httpClient = new NetworkUtility().getDefaultHttpClient(this.activity, DBHelper.urlServer+"/api/DatosMaestros/GetDatosMaestros?aplicaciones[0]=" + guid.toString());
        RequestQueue requestQueue = NetworkUtility.getInstance(activity.getApplicationContext()).getRequestQueue();
        NetworkUtility.nuke();
        final String finalSToken = sToken;

        JsonObjectRequest
                jsonObjectRequest
                = new JsonObjectRequest(
                Request.Method.GET,
                url,
                null,
                new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {
                        //HttpResponse response = httpClient.execute(httpPost);
                        if (response != null) {


                            JSONObject data = (JSONObject) response;
                            try {
                                String responseStr = "";

                                ArrayList<Haciendas> hdasResult = new ArrayList<Haciendas>();
                                if (data.getBoolean("<error>k__BackingField") == true) {
                                    String error = data.getString("<msgError>k__BackingField");
                                    callback.onError(error);
                                } else {
                                    JSONArray a = data.getJSONArray("<haciendas>k__BackingField");
                                    for (int i = 0; i < a.length(); i++) {
                                        JSONObject hda = a.getJSONObject(i);
                                        int id = hda.getInt("<IdHacienda>k__BackingField");
                                        String Bk = hda.getString("<Codigo>k__BackingField");
                                        String Ds = hda.getString("<Nombre>k__BackingField");
                                        boolean bio = hda.getBoolean("<MarcaBiometrico>k__BackingField");
                                        Haciendas iHda = new Haciendas(id, Bk, Ds, bio);
                                        hdasResult.add(iHda);
                                    }

                                    obtenido = true;
                                    if (mydb.ActualizarHaciendas(hdasResult)) {
                                        callback.onSuccess("Datos recuperados correctamente.");
                                    } else {
                                        callback.onSuccess("Datos recuperados.");
                                    }


                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                callback.onError("Error al recuperar datos");
                            }


                        } else {
                            callback.onError();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }) {

            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("token", finalSToken);
                return headers;
            }

        };


        requestQueue.add(jsonObjectRequest);

    }

/*
    private String EnviarApuntamientos(ArrayList<Apuntamientos> apuntesEnvio) {
        DBHelper mydb = new DBHelper(activity);

        String sAppId;
        Parametros appId = mydb.getSingleParametro(3);
        if (appId != null) {
            sAppId = appId.getValor();
        }

        UUID guid = null;
        String sToken = "";
        Parametros appid = mydb.getSingleParametro(3);
        Parametros token = mydb.getSingleParametro(4);
        if (appid != null) {
            guid = guid.fromString(appid.getValor());
        }
        if (guid == null) return "Aplicación no autenticada";

        if (token != null) {
            sToken = token.getValor();
        }




        JSONObject obj = new JSONObject();
        JSONArray arr = new JSONArray();
        if (apuntesEnvio != null) {
            try {
                for (Apuntamientos ap : apuntesEnvio) {
                    obj = new JSONObject();
                    String fa[] = ap.getFecha().split("-");
                    String FechaApunte = fa[0]+fa[1]+fa[2];
                    obj.put("<IdHacienda>k__BackingField", ap.getIdHacienda());
                    obj.put("<precipitacion>k__BackingField", ap.getMmLluvia());
                    obj.put("<IdCondMet>k__BackingField", ap.getIdCondMet());
                    obj.put("<imei>k__BackingField", imei);
                    obj.put("<FechaApuntamientosST>k__BackingField", FechaApunte);
                    obj.put("<FechaHraDigitaST>k__BackingField", ap.getFechaHraDigita());
                    obj.put("<Latitud>k__BackingField",ap.getLatitud());
                    obj.put("<Longitud>k__BackingField",ap.getLongitud());

                    arr.put(obj);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        boolean enviados = false;
        StringEntity entity = null;
        try {
            String arrstr = arr.toString();

            entity = new StringEntity(arr.toString());
            entity.setContentType("application/json");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        if(entity == null) return "No fue posible convertir los datos de envío.";

        HttpPost httpPost = new HttpPost(DBHelper.urlServer+"/api/Apuntamientos/SetApuntesPluviometros");
        httpPost.setEntity(entity);
        httpPost.setHeader("token", sToken);
        HttpClient httpClient = new NetworkUtility().getDefaultHttpClient(this.activity, DBHelper.urlServer+"/api/Apuntamientos/SetApuntesPluviometros");

        try {
            HttpResponse response = httpClient.execute(httpPost);
            if (response != null) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));


                StringBuffer json = new StringBuffer(1024);
                String tmp = "";
                while ((tmp = reader.readLine()) != null)
                    json.append(tmp).append("\n");
                reader.close();
                String jsstr = json.toString();
                JSONObject data = new JSONObject(json.toString());
                enviados = data.getBoolean("Key");
                String responseStr = data.getString("Value");
                if(enviados == false) {
                    return "Respuesta recibida del servidor: " + responseStr;
                }
                else{
                    mydb.setMarcaEnviado(apuntesEnvio); //QUITAR COMENTARIO CUANDO FUNCIONE EL ALMACENAMIENTO EN BD
                    return responseStr;
                }

            } else {
                return "No se obtuvo respuesta del servidor";
            }
        } catch (IOException e) {
            return "Respuesta nula";
        } catch (JSONException ex) {
            ex.printStackTrace();
            return "Error al enviar apuntamientos.";
        }

    }*/


}