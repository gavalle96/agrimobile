package com.grupocassa.agrimobile.CASSAUTILS;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.grupocassa.agrimobile.CLS.Cuadrillas;
import com.grupocassa.agrimobile.CLS.Haciendas;
import com.grupocassa.agrimobile.CLS.HuellasTrabajadores;
import com.grupocassa.agrimobile.CLS.OS;
import com.grupocassa.agrimobile.CLS.OSRecursos;
import com.grupocassa.agrimobile.CLS.Parametros;
import com.grupocassa.agrimobile.CLS.TiposDocumento;
import com.grupocassa.agrimobile.CLS.Trabajadores;
import com.grupocassa.agrimobile.CLS.VolleyCallback;
import com.grupocassa.agrimobile.LoginActivity;
import com.grupocassa.agrimobile.MaestrosActivities.ObtenerMaestrosActivity;
/*
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;*/
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
/*
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
*/
/**
 * Created by gerardo.valle on 19/7/2017.
 */

public class ObtenerMaestros extends AsyncTask<Void, Void, Boolean> {

    private final String BkHacienda;
    private final int IdOS;
    private final String BkCuadrilla;
    private ArrayList<Haciendas> hdas;
    private ArrayList<OS> oeses;
    private ArrayList<OSRecursos> osRecursoses;
    private ArrayList<Cuadrillas> cuadrillasArrayList;
    private ArrayList<Trabajadores> trabajadoresArrayList;
    private ObtenerMaestrosActivity activity;
    String msg;
    public boolean trab,recuperatrab, dm;
    private int EstadoParam, EstadoOS;
    private boolean actualizado;
    //OkHttpClient client = new OkHttpClient();

    public ObtenerMaestros(String BkHacienda, int IdOS,boolean DM, ObtenerMaestrosActivity activity1) {
        this.BkHacienda = BkHacienda;
        this.IdOS = IdOS;
        hdas = new ArrayList<Haciendas>();
        oeses = new ArrayList<OS>();
        osRecursoses = new ArrayList<OSRecursos>();
        activity = activity1;
        trab = false;
        recuperatrab = false;
        BkCuadrilla = "";
        dm =DM;
        activity.pgProgress.setVisibility(View.VISIBLE);
        EstadoParam = activity.txtParametro.getVisibility();
        EstadoOS = activity.txtOS.getVisibility();

        activity.txtOS.setVisibility(View.INVISIBLE);
        activity.txtParametro.setVisibility(View.INVISIBLE);
        actualizado = true;
    }

    public ObtenerMaestros(String cuadrilla, boolean recuperatrab, ObtenerMaestrosActivity activity1) {
        this.BkCuadrilla = cuadrilla;
        this.recuperatrab = recuperatrab;
        hdas = new ArrayList<Haciendas>();
        oeses = new ArrayList<OS>();
        osRecursoses = new ArrayList<OSRecursos>();
        activity = activity1;
        trab = true;
        this.IdOS = 0;
        this.BkHacienda ="";
        dm = false;
        activity.pgProgress.setVisibility(View.VISIBLE);
        EstadoParam = activity.txtParametro.getVisibility();
        EstadoOS = activity.txtOS.getVisibility();

        activity.txtOS.setVisibility(View.INVISIBLE);
        activity.txtParametro.setVisibility(View.INVISIBLE);
        actualizado = true;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        // TODO: attempt authentication against a network service.
        //boolean ok = false;

        msg = msg == null ? "" : msg;
            ObtenerDocumentos();
            if (!trab) {
                ObtenerDatos(BkHacienda, IdOS, new VolleyCallback() {
                    @Override
                    public boolean onSuccess() {


                        msg+="Datos descargados correctamente#";
                        finishBackGround(true);
                        return false;
                    }

                    @Override
                    public boolean onError() {
                        msg += "Error al descargar datos#";
                        finishBackGround(false);
                        return false;
                    }

                    @Override
                    public boolean onSuccess(String resp) {
                        msg += "Datos descargados correctamente. "+resp +"#";
                        finishBackGround(true);
                        return false;
                    }

                    @Override
                    public boolean onError(String resp) {
                        msg += "Error al descargar datos. "+resp +"#";
                        finishBackGround(false);
                        return false;
                    }
                });
            } else {
                ObtenerDatosCuadrilla(new VolleyCallback() {
                    @Override
                    public boolean onSuccess() {


                        msg+="Datos descargados correctamente#";
                        finishBackGround(true);
                        return false;
                    }

                    @Override
                    public boolean onError() {
                        msg += "Error al descargar datos#";
                        finishBackGround(false);
                        return false;
                    }

                    @Override
                    public boolean onSuccess(String resp) {
                        msg += "Datos descargados correctamente. "+resp +"#";
                        finishBackGround(true);
                        return false;
                    }

                    @Override
                    public boolean onError(String resp) {
                        msg += "Error al descargar datos. "+resp +"#";
                        finishBackGround(false);
                        return false;
                    }
                });

            }

        // TODO: register the new account here.
        return actualizado;
    }

    @Override
    protected void onPostExecute(final Boolean success) {

    }

    private void finishBackGround(final Boolean success){
        activity.runOnUiThread(new Runnable() {

            @Override
            public void run() {

                // Stuff that updates the UI
                activity.om = null;
                activity.showProgress(false);

                activity.txtOS.setVisibility(EstadoOS);
                activity.txtParametro.setVisibility(EstadoParam);
                activity.pgProgress.setVisibility(View.INVISIBLE);

                showMsg();
                    cargarListaVista();
                    activity.btnConsultar.performClick();


            }
        });



    }
    @Override
    protected void onCancelled() {
        activity.om = null;
        activity.showProgress(false);
        activity.txtOS.setVisibility(EstadoOS);
        activity.txtParametro.setVisibility(EstadoParam);
        activity.pgProgress.setVisibility(View.INVISIBLE);
    }

    private void showMsg(){
        activity.mProgressView.setVisibility(View.GONE);


        String[] armsg = msg.split("#");
        for (String m: armsg
             ) {
            Toast.makeText(activity, m, Toast.LENGTH_SHORT).show();
        }
    }
    private void cargarListaVista(){
        activity.cargarLista();
    }

    private void ObtenerDatos(final String BkHacienda, final int IdOS, final VolleyCallback callback) {
        final DBHelper mydb = new DBHelper(activity);
        String pendMsg = "";
        String sAppId;



        UUID guid = null;
        String sToken = "";
        //Parametros appid = mydb.getSingleParametro(1);
        Parametros token = mydb.getSingleParametro(4);
/*
        if (appid != null) {
            guid = guid.fromString(appid.getValor());
        }
        if (guid == null) return "Aplicación no autenticada";
*/
        if (token != null) {
            sToken = token.getValor();
        }

        String[] Aplicaciones = mydb.getAllAplicaciones();

        String sApps = "";

        for (int i= 0; i< Aplicaciones.length;i++ ) {
            if (!Aplicaciones[i].toUpperCase().contains("F3F2F71C-BD9D-4797-A4AE-992A4DA3D2C2")) {//EXCLUYE HACIENDAS DE LLUVIAS
                guid = guid.fromString(Aplicaciones[i].toUpperCase());
                sApps += "aplicaciones[" + i + "]=" + guid + "&";
            }
        }

        String url = DBHelper.urlServer+"/api/DatosMaestros/GetDatosMaestros?"+sApps+"hacienda="+BkHacienda+"&os="+IdOS;
        //HttpGet httpGet = new HttpGet(DBHelper.urlServer+"/api/DatosMaestros/GetDatosMaestros?"+sApps+"hacienda="+BkHacienda+"&os="+IdOS);
        //httpGet.setHeader("token", sToken);

        //HttpClient httpClient = new NetworkUtility().getDefaultHttpClient(this.activity, DBHelper.urlServer+"/api/DatosMaestros/GetDatosMaestros?"+sApps+"hacienda="+BkHacienda+"&os="+IdOS);
        RequestQueue requestQueue = NetworkUtility.getInstance(this.activity.getApplicationContext()).getRequestQueue();
        NetworkUtility.nuke();
        final String finalSToken = sToken;

        JsonObjectRequest
                jsonObjectRequest
                = new JsonObjectRequest(
                Request.Method.GET,
                url,
                null,
                new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {
                        //HttpResponse response = httpClient.execute(httpPost);
                        if (response != null) {
                            //BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
                /*StringBuilder builder = new StringBuilder();
                for (String line = null; (line = reader.readLine()) != null;) {
                        builder.append(line).append("\n");
                }
                JSONTokener tokener = new JSONTokener(builder.toString());
                JSONArray finalResult = new JSONArray(tokener);*/

  /*              StringBuffer json = new StringBuffer(1024);
                String tmp = "";
                while ((tmp = reader.readLine()) != null)
                    json.append(tmp).append("\n");
                reader.close();
*/
  try {
      JSONObject data = (JSONObject) response;
      String responseStr = "";

      ArrayList<Haciendas> hdasResult = new ArrayList<Haciendas>();
      oeses = new ArrayList<OS>();
      if (data.getBoolean("<error>k__BackingField") == true) {
          String error = data.getString("<msgError>k__BackingField");
          callback.onError(error);
      } else {
          if (BkHacienda.equals("") && IdOS == 0) {
              JSONArray a = data.getJSONArray("<haciendas>k__BackingField");
              for (int i = 0; i < a.length(); i++) {
                  JSONObject hda = a.getJSONObject(i);
                  int id = hda.getInt("<IdHacienda>k__BackingField");
                  String Bk = hda.getString("<Codigo>k__BackingField");
                  String Ds = hda.getString("<Nombre>k__BackingField");
                  boolean bio = hda.getBoolean("<MarcaBiometrico>k__BackingField");
                  Haciendas iHda = new Haciendas(id, Bk, Ds, bio);
                  hdasResult.add(iHda);
              }

              if (mydb.ActualizarHaciendas(hdasResult)) {
                 // pendMsg += "Haciendas recuperadas correctamente.#";

                  ArrayList<String> hdasStr = new ArrayList<String>();
                  for (Haciendas h : hdasResult
                  ) {
                      hdasStr.add(h.getBkHacienda() + " - " + h.getDsHacienda());
                  }
                  activity.elementos = hdasStr;

              }
          }

          JSONArray b = data.getJSONArray("<ordenesServicio>k__BackingField");
          for (int i = 0; i < b.length(); i++) {
              JSONObject os = b.getJSONObject(i);
              int id = os.getInt("<IdOs>k__BackingField");
              int IdHacienda = os.getInt("<IdHacienda>k__BackingField");
              String DsAct = os.getString("<DsActividad>k__BackingField");
              String BkZafra = os.getString("<BkZafra>k__BackingField");
              String UMP = "";//os.getString("<UMP>k__BackingField");
              float cant = Float.parseFloat("" + os.getDouble("<Cantidad>k__BackingField"));

              if (BkZafra == null) BkZafra = "";

              OS iOS = new OS(id, IdHacienda, DsAct, cant, BkZafra, UMP);
              oeses.add(iOS);
          }

          if (BkHacienda.equals("") && IdOS == 0) {
              if (mydb.ActualizarOS(oeses)) {
                  //pendMsg += "Órdenes de Servicio recuperadas correctamente.#";
                  if (!BkHacienda.equals("") && IdOS == 0) {
                      ArrayList<String> hdasStr = new ArrayList<String>();
                      for (OS h : oeses
                      ) {
                          hdasStr.add("OS " + h.getIdOS() + " - " + h.getDsActividad() + " - Cant: " + h.getCantidad());
                      }
                      activity.elementos = hdasStr;
                  }
              }
          } else if (!BkHacienda.equals("") && IdOS == 0) {
              if (mydb.ActualizarOSByHda(oeses, BkHacienda)) {
                  //pendMsg += "Órdenes de Servicio recuperadas correctamente.#";
                  if (!BkHacienda.equals("") && IdOS == 0) {
                      ArrayList<String> hdasStr = new ArrayList<String>();
                      for (OS h : oeses
                      ) {
                          hdasStr.add("OS " + h.getIdOS() + " - " + h.getDsActividad());
                      }
                      activity.elementos = hdasStr;
                  }
              }
          }


          if (BkHacienda.equals("") && IdOS == 0) {
              JSONArray c = data.getJSONArray("<recursos>k__BackingField");
              for (int i = 0; i < c.length(); i++) {
                  JSONObject os = c.getJSONObject(i);
                  int idos = os.getInt("<IdOs>k__BackingField");
                  int IdRecurso = os.getInt("<IdRecurso>k__BackingField");
                  String BkRecurso = os.getString("<BkRecurso>k__BackingField");
                  String DsRecurso = os.getString("<DsRecurso>k__BackingField");
                  String BkTipRecurso = os.getString("<BkTipRecurso>k__BackingField");
                  String UM = os.getString("<UM>k__BackingField");
                  float cantP = Float.parseFloat("" + os.getDouble("<CantidadPend>k__BackingField"));
                  float cantAp = Float.parseFloat("" + os.getDouble("<CantidadApunt>k__BackingField"));
                  float AreaAp = Float.parseFloat("" + os.getDouble("<AreaApunt>k__BackingField"));
                  Boolean ay = os.getBoolean("<Ayudante>k__BackingField");
                  Boolean imp = os.getBoolean("<requireImplemento>k__BackingField");

                  int idsubflota = 0;
                  if (idos == 748) {
                      if (idos == 748) {
                          int ok = 1;
                      }
                  }
                  OSRecursos iOS = new OSRecursos(idos, IdRecurso, BkRecurso, DsRecurso, BkTipRecurso, UM, cantP, cantAp, AreaAp, ay, imp, idsubflota);
                  osRecursoses.add(iOS);
              }
              if (mydb.ActualizarOSRecursos(osRecursoses)) {
                  //pendMsg += "Recursos recuperados correctamente.#";

              }
          } else if (BkHacienda.equals("") && IdOS > 0) {

              if (dm) {
                  JSONArray x = data.getJSONArray("<haciendas>k__BackingField");
                  for (int i = 0; i < x.length(); i++) {
                      JSONObject hda = x.getJSONObject(i);
                      int id = hda.getInt("<IdHacienda>k__BackingField");
                      String Bk = hda.getString("<Codigo>k__BackingField");
                      String Ds = hda.getString("<Nombre>k__BackingField");
                      Boolean bio = hda.getBoolean("<MarcaBiometrico>k__BackingField");
                      Haciendas iHda = new Haciendas(id, Bk, Ds, bio);
                      hdasResult.add(iHda);
                  }

                  if (mydb.ActualizarHaciendas(hdasResult)) {
                      //pendMsg += "Hacienda recuperada correctamente.#";
                      JSONArray y = data.getJSONArray("<ordenesServicio>k__BackingField");
                      for (int i = 0; i < y.length(); i++) {
                          JSONObject os = y.getJSONObject(i);
                          int id = os.getInt("<IdOs>k__BackingField");
                          int IdHacienda = os.getInt("<IdHacienda>k__BackingField");
                          String DsAct = os.getString("<DsActividad>k__BackingField");
                          String BkZafra = os.getString("<BkZafra>k__BackingField");
                          String UMP = "";// os.getString("<UMP>k__BackingField");
                          float cant = Float.parseFloat("" + os.getDouble("<Cantidad>k__BackingField"));

                          if (BkZafra == null) BkZafra = "";

                          OS iOS = new OS(id, IdHacienda, DsAct, cant, BkZafra, UMP);
                          oeses.add(iOS);
                      }

                      if (mydb.ActualizarOS(oeses)) {

                          //pendMsg += "OS recuperada correctamente.#";
                          JSONArray z = data.getJSONArray("<recursos>k__BackingField");
                          osRecursoses = new ArrayList<OSRecursos>();
                          for (int i = 0; i < z.length(); i++) {
                              JSONObject os = z.getJSONObject(i);
                              int idos = os.getInt("<IdOs>k__BackingField");
                              int IdRecurso = os.getInt("<IdRecurso>k__BackingField");
                              String BkRecurso = os.getString("<BkRecurso>k__BackingField");
                              String DsRecurso = os.getString("<DsRecurso>k__BackingField");
                              String BkTipRecurso = os.getString("<BkTipRecurso>k__BackingField");
                              String UM = os.getString("<UM>k__BackingField");
                              float cantP = Float.parseFloat("" + os.getDouble("<CantidadPend>k__BackingField"));
                              float cantAp = Float.parseFloat("" + os.getDouble("<CantidadApunt>k__BackingField"));
                              float AreaAp = Float.parseFloat("" + os.getDouble("<AreaApunt>k__BackingField"));
                              Boolean ay = os.getBoolean("<Ayudante>k__BackingField");
                              Boolean imp = os.getBoolean("<requireImplemento>k__BackingField");
                              int idsubflota = 0;//os.getInt("<Id_SubFlota>k__BackingField");
                              if (idos == 748) {
                                  if (idos == 748) {
                                      int ok = 1;
                                  }
                              }
                              OSRecursos iOS = new OSRecursos(idos, IdRecurso, BkRecurso, DsRecurso, BkTipRecurso, UM, cantP, cantAp, AreaAp, ay, imp, idsubflota);
                              osRecursoses.add(iOS);
                          }

                          if (mydb.ActualizarOSRecursosByOS(osRecursoses, IdOS)) {
                              //pendMsg += "Recursos recuperados correctamente.#";

                              ArrayList<String> hdasStr = new ArrayList<String>();
                              for (OSRecursos h : osRecursoses
                              ) {
                                  hdasStr.add("- " + h.getDsRecurso() + ": " + h.getCantidadPend() + " " + h.getUM());
                              }
                              activity.elementos = hdasStr;

                          }
                      }
                  }
              } else {
                  JSONArray z = data.getJSONArray("<recursos>k__BackingField");
                  for (int i = 0; i < z.length(); i++) {
                      JSONObject os = z.getJSONObject(i);
                      int idos = os.getInt("<IdOs>k__BackingField");
                      int IdRecurso = os.getInt("<IdRecurso>k__BackingField");
                      String BkRecurso = os.getString("<BkRecurso>k__BackingField");
                      String DsRecurso = os.getString("<DsRecurso>k__BackingField");
                      String BkTipRecurso = os.getString("<BkTipRecurso>k__BackingField");
                      String UM = os.getString("<UM>k__BackingField");
                      float cantP = Float.parseFloat("" + os.getDouble("<CantidadPend>k__BackingField"));
                      float cantAp = Float.parseFloat("" + os.getDouble("<CantidadApunt>k__BackingField"));
                      float AreaAp = Float.parseFloat("" + os.getDouble("<AreaApunt>k__BackingField"));
                      Boolean ay = os.getBoolean("<Ayudante>k__BackingField");
                      Boolean imp = os.getBoolean("<requireImplemento>k__BackingField");
                      int idsubflota = 0;//os.getInt("<Id_SubFlota>k__BackingField");
                      if (idos == 748) {
                          if (idos == 748) {
                              int ok = 1;
                          }
                      }
                      OSRecursos iOS = new OSRecursos(idos, IdRecurso, BkRecurso, DsRecurso, BkTipRecurso, UM, cantP, cantAp, AreaAp, ay, imp, idsubflota);
                      osRecursoses.add(iOS);
                  }

                  if (mydb.ActualizarOSRecursosByOS(osRecursoses, IdOS)) {
                      //pendMsg += "Recursos recuperados correctamente.#";

                      ArrayList<String> hdasStr = new ArrayList<String>();
                      for (OSRecursos h : osRecursoses
                      ) {
                          hdasStr.add("- " + h.getDsRecurso() + ": " + h.getCantidadPend() + " " + h.getUM());
                      }
                      activity.elementos = hdasStr;

                  }
              }


          }

      }
      callback.onSuccess();
  }
  catch (JSONException j){
      callback.onError();
  }

                        } else {
                            //pendMsg += "El servidor no devolvió datos";
                            callback.onError();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }) {

            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("token", finalSToken);
                return headers;
            }

        };


        requestQueue.add(jsonObjectRequest);


    }

    private void ObtenerDatosCuadrilla(final VolleyCallback callback) {
        final DBHelper mydb = new DBHelper(activity);
        String pendMsg = "";
        String sAppId;

        UUID guid = null;
        String sToken = "";
        Parametros token = mydb.getSingleParametro(4);

        if (token != null) {
            sToken = token.getValor();
        }

        String[] Aplicaciones = mydb.getAllAplicaciones();

        String sApps = "";

        for (int i= 0; i< Aplicaciones.length;i++ ) {
            guid = guid.fromString(Aplicaciones[i].toUpperCase());
            sApps += "aplicaciones["+i+"]="+guid+"&";
        }

        String url = DBHelper.urlServer+"/api/DatosMaestros/GetEstructuraOrganizativa?"+sApps+"cuadrilla="+BkCuadrilla+"&recuperaTrabajadores="+recuperatrab;
        //HttpGet httpGet = new HttpGet(DBHelper.urlServer+"/api/DatosMaestros/GetEstructuraOrganizativa?"+sApps+"cuadrilla="+BkCuadrilla+"&recuperaTrabajadores="+recuperatrab);
        //httpGet.setHeader("token", sToken);
        //HttpClient httpClient = new NetworkUtility().getDefaultHttpClient(this.activity, DBHelper.urlServer+"/api/DatosMaestros/GetEstructuraOrganizativa?"+sApps+"cuadrilla="+BkCuadrilla+"&recuperaTrabajadores="+recuperatrab);
        RequestQueue requestQueue = NetworkUtility.getInstance(activity.getApplicationContext()).getRequestQueue();
        NetworkUtility.nuke();
        final String finalSToken = sToken;

        JsonObjectRequest
                jsonObjectRequest
                = new JsonObjectRequest(
                Request.Method.GET,
                url,
                null,
                new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {
                        //HttpResponse response = httpClient.execute(httpPost);
                        if (response != null) {
                            //BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
                /*StringBuilder builder = new StringBuilder();
                for (String line = null; (line = reader.readLine()) != null;) {
                        builder.append(line).append("\n");
                }
                JSONTokener tokener = new JSONTokener(builder.toString());
                JSONArray finalResult = new JSONArray(tokener);*/

  /*              StringBuffer json = new StringBuffer(1024);
                String tmp = "";
                while ((tmp = reader.readLine()) != null)
                    json.append(tmp).append("\n");
                reader.close();
*/
                            JSONObject data = (JSONObject)response;
                            String responseStr = "";

                            ArrayList<Cuadrillas> hdasResult = new ArrayList<Cuadrillas>();
                            oeses = new ArrayList<OS>();
                            try {
                                if (data.getBoolean("<error>k__BackingField") == true) {
                                    String error = data.getString("<msgError>k__BackingField");
                                    callback.onError();
                                } else {
                                    if (BkCuadrilla.equals("")) {
                                        JSONArray a = data.getJSONArray("<cuadrillas>k__BackingField");
                                        for (int i = 0; i < a.length(); i++) {
                                            JSONObject hda = a.getJSONObject(i);
                                            int id = hda.getInt("IdCuadrilla");
                                            String Bk = hda.getString("BkCuadrilla");
                                            String Ds = hda.getString("DsCuadrilla");
                                            Cuadrillas iHda = new Cuadrillas(id, Bk, Ds);
                                            hdasResult.add(iHda);
                                        }

                                        if (mydb.ActualizarCuadrillas(hdasResult)) {


                                            activity.cuadrillasMostrar = hdasResult;

                                        }
                                    }
                                    else if (!BkCuadrilla.equals("")){
                                        Cuadrillas cu = mydb.getSingleCuadrillabyBK(BkCuadrilla);
                                        ArrayList<Trabajadores> tresult = new ArrayList<Trabajadores>();
                                        JSONArray a = data.getJSONArray("<trabajadores>k__BackingField");
                                        for (int i = 0; i < a.length(); i++) {
                                            JSONObject hda = a.getJSONObject(i);
                                            int id = hda.getInt("<IdTrabajador>k__BackingField");
                                            String Bk = hda.getString("<CodTrabajador>k__BackingField");
                                            String Ds = hda.getString("<NomTrabajador>k__BackingField");
                                            int idc = hda.getInt("<IdCuadrilla>k__BackingField");
                                            Trabajadores iHda = new Trabajadores(id, Bk, Ds,idc);
                                            tresult.add(iHda);
                                        }

                                        if (mydb.ActualizarTrabajadoresByCuadrilla(tresult,cu.getIdCuadrilla())) {


                                            ArrayList<String> hdasStr = new ArrayList<String>();
                                            for (Trabajadores h : tresult
                                            ) {
                                                hdasStr.add(h.getCodTrabajador() + " - " + h.getNomTrabajador());
                                            }
                                            activity.elementos = hdasStr;
                                        }
                                    }


                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        } else {
                            callback.onError();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }) {

            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("token", finalSToken);
                return headers;
            }

        };
        requestQueue.add(jsonObjectRequest);
        callback.onSuccess();
    }



    private void ObtenerDocumentos(){
        final DBHelper mydb = new DBHelper(activity);
        DBExterno dbExterno = null;


        String pendMsg = "";


        String url = DBHelper.urlServer+"/api/DatosMaestros/GetTiposDocumento";
        //HttpGet httpGet = new HttpGet(DBHelper.urlServer+"/api/DatosMaestros/GetTiposDocumento");

        //httpPost.setEntity(entity);
        //HttpClient httpClient = new NetworkUtility().getDefaultHttpClient(this.activity, DBHelper.urlServer+"/api/DatosMaestros/GetTiposDocumento");
        final RequestQueue requestQueue = NetworkUtility.getInstance(activity.getApplicationContext()).getRequestQueue();
        NetworkUtility.nuke();

        final JsonArrayRequest
                jsonObjectRequest
                = new JsonArrayRequest(
                Request.Method.GET,
                url,
                null,
                new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {
                        //HttpResponse response = httpClient.execute(httpPost);
                        if (response != null) {

                            JSONArray data = (JSONArray) response;
                            try {
                                ArrayList<TiposDocumento> eqresult = new ArrayList<TiposDocumento>();
                                //JSONArray a = data.getJSONArray("<Equipos>k__BackingField");
                                for (int i = 0; i < data.length(); i++) {
                                    JSONObject hda = null;

                                    hda = data.getJSONObject(i);

                                    int id = hda.getInt("IdTipDocInterno");
                                    String Bk = hda.getString("BkTipDocInterno");
                                    String Ds = hda.getString("DsTipDocInterno");
                                    TiposDocumento eq = new TiposDocumento(id, Bk.trim(), Ds.trim());
                                    eqresult.add(eq);
                                }

                                mydb.deleteAlldocss();
                                ArrayList<TiposDocumento> existentes = mydb.getAllTiposDocumento();
                                if (existentes.size() != eqresult.size()) {
                                    //mydb.deleteEquipos();
                                    for (TiposDocumento e :
                                            eqresult) {
                                        mydb.insertTipoDocumento(e.getIdTipDocInterno(), e.getBkTipDocInterno(), e.getDsTipDocInteno());
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }) {

            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                return headers;
            }

        };


        requestQueue.add(jsonObjectRequest);
    }

}