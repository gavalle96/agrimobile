package com.grupocassa.agrimobile.CASSAUTILS;

/**
 * Created by gerardo.valle on 21/7/2017.
 */

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.grupocassa.agrimobile.CLS.ApuntesArea;
import com.grupocassa.agrimobile.CLS.ApuntesInsumos;
import com.grupocassa.agrimobile.CLS.ApuntesMO;
import com.grupocassa.agrimobile.CLS.ApuntesMaquinaria;
import com.grupocassa.agrimobile.CLS.HuellasTrabajadores;
import com.grupocassa.agrimobile.CLS.OS;
import com.grupocassa.agrimobile.CLS.Parametros;
import com.grupocassa.agrimobile.CLS.VolleyCallback;
import com.grupocassa.agrimobile.MaestrosActivities.EnviarApuntesActivity;
/*
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;*/

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.odata4j.core.Boole;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
/*
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;*/

public class EnviarApuntes extends AsyncTask<Void, Void, Boolean> {

    private EnviarApuntesActivity activity;
    String msg;
    private ArrayList<String> listMsg;
    public ArrayList<HuellasTrabajadores> htrab;
    //private final String urlServer = "https://168.243.156.109:8001";
    //private final String urlServer = "https://192.200.11.10:8001";//"http://10.0.2.2:60940";
    //private final String urlServer = "http://10.0.2.2:60940";;
/*
    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");
    OkHttpClient client = new OkHttpClient();*/
    public EnviarApuntes( EnviarApuntesActivity activity1) {
        activity = activity1;
        activity.flecha.setVisibility(View.INVISIBLE);
        activity.pgProgress.setVisibility(View.VISIBLE);
    }


    @Override
    protected Boolean doInBackground(Void... params) {
        // TODO: attempt authentication against a network service.
        //boolean ok = false;
        final ResponseContainer responseContainer = new ResponseContainer();
        EnviarApuntamientos(new VolleyCallback() {
            @Override
            public boolean onSuccess() {
                msg+="Apuntes enviados correctamente#";
                finishBackGround(true   );
                return false;
            }

            @Override
            public boolean onError() {
                msg+="Ocurrió un problema al enviar apuntes#";
                //finishBackGround(false   );
                return false;
            }

            @Override
            public boolean onSuccess(String resp) {
                msg = resp+"#";
                finishBackGround(true   );
                return true;
            }

            @Override
            public boolean onError(String resp) {
                msg = resp+"#";
                //finishBackGround(false   );
                return false;
            }
        });

        final String msg2 = "#Apuntes de Áreas: ";
        EnviarApuntesArea(new VolleyCallback() {
            @Override
            public boolean onSuccess() {
                finishBackGround(true);
                /*
                msg = "Apuntamientos enviados correctamente#";/*
                Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
                responseContainer.result = true;*/
                return true;
            }

            @Override
            public boolean onError() {
                msg = "Error al enviar apuntes#";
                //finishBackGround(false);
                /*
                responseContainer.result = false;*/
                return false;
            }

            @Override
            public boolean onSuccess(String resp) {
                finishBackGround(true);/*
                Toast.makeText(activity, resp, Toast.LENGTH_SHORT).show();
                responseContainer.result = true;*/
                return false;
            }

            @Override
            public boolean onError(String resp) {
                //finishBackGround(false);
                /*
                Toast.makeText(activity, resp, Toast.LENGTH_SHORT).show();
                responseContainer.result = false;*/
                return false;
            }
        });
        if(msg.contains("No hay")){
            if (!msg2.contains("No hay")) {
                msg = msg2.substring(1);
            }
        }
        else{
            msg += msg2;
        }
        ObtenerApuntes(new VolleyCallback() {
            @Override
            public boolean onSuccess() {
                return false;
            }

            @Override
            public boolean onError() {
                return false;
            }

            @Override
            public boolean onSuccess(String resp) {
                return false;
            }

            @Override
            public boolean onError(String resp) {
                return false;
            }
        });
        //EnviarHuellas();
        // TODO: register the new account here.
        return responseContainer.result;
    }

    @Override
    protected void onPostExecute(final Boolean success) {

    }

    @Override
    protected void onCancelled() {
        activity.om = null;
        activity.showProgress(false);
    }

    private  void finishBackGround(Boolean success){
        activity.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                activity.om = null;
                activity.showProgress(false);
                activity.flecha.setVisibility(View.VISIBLE);
                activity.pgProgress.setVisibility(View.INVISIBLE);

                showMsg();
                cargarListaVista();

            }
        });
    }
    private void showMsg(){
        String[] armsg = msg.split("#");
        for (String m: armsg
                ) {
            Toast.makeText(activity, m, Toast.LENGTH_SHORT).show();
        }
    }
    private void cargarListaVista(){
        activity.cargarLista();
        activity.Refrescar();
    }



    private void EnviarApuntesArea(final VolleyCallback callback){
        final DBHelper mydb = new DBHelper(activity);

        ArrayList<ApuntesArea> arrenviar = mydb.getApuntesAreaNoEnviados();

        if(arrenviar == null){
            arrenviar = new ArrayList<ApuntesArea>();
        }

        if (arrenviar.size() == 0){
            callback.onError();
        }

        UUID guid = null;
        String sToken = "";
        Parametros token = mydb.getSingleParametro(4);
        Parametros usr = mydb.getSingleParametro(5);

        String susr ="";
        if (usr != null){
            susr = usr.getValor();
        }
        if (token != null) {
            sToken = token.getValor();
        }

        JSONArray raiz = new JSONArray();
        JSONObject item = new JSONObject();
        try {
            for (ApuntesArea a :
                    arrenviar) {
                item = new JSONObject();
                String sfecha = a.getFecha();
                String[] arrs = sfecha.split("/");
                if(arrs.length == 3)
                    sfecha = arrs[2]+"-"+arrs[1]+"-"+arrs[0];
                item.put("IdOS",a.getIdOS());
                item.put("FechaApunte", sfecha);
                item.put("Area",a.getArea());
                item.put("UsrDigita",susr);

                raiz.put(item);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String mRequestBody = raiz.toString();
        final boolean[] enviados = {false};
        //StringEntity entity = null;

        //entity = new StringEntity(raiz.toString(),"UTF-8");
        //entity.setContentType("application/json");

        //if(entity == null) return "No fue posible convertir los datos de envío.";



        String url = DBHelper.urlServer+"/api/Apuntamientos/SetApuntesArea?Usuario="+susr;


       /* HttpPost httpPost = new HttpPost(DBHelper.urlServer+"/api/Apuntamientos/SetApuntesArea?Usuario="+susr);
        httpPost.setEntity(entity);
        httpPost.setHeader("token", sToken);
        HttpClient httpClient = new NetworkUtility().getDefaultHttpClient(this.activity, DBHelper.urlServer+"/api/Apuntamientos/SetApuntesArea?Usuario="+susr);
*/
        RequestQueue requestQueue = NetworkUtility.getInstance(activity.getApplicationContext()).getRequestQueue();
        NetworkUtility.nuke();
        final String finalSToken = sToken;

        JsonObjectRequest
                jsonObjectRequest
                = new JsonObjectRequest(
                Request.Method.POST,
                url,
                null,
                new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {
                        //HttpResponse response = httpClient.execute(httpPost);
                        if (response != null) {


                            JSONObject data = (JSONObject) response;
                            try {
                                enviados[0] = data.getBoolean("Key");
                                String responseStr = data.getString("Value");
                                if(enviados[0] == false) {
                                    callback.onError();
                                }
                                else{
                                    mydb.setApAreaEnviados();
                                    callback.onSuccess();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        } else {
                            callback.onError();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }) {

            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("token", finalSToken);
                return headers;
            }
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
                try {
                    return mRequestBody == null ? null : mRequestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", mRequestBody, "utf-8");
                    return null;
                }
            }

        };


        requestQueue.add(jsonObjectRequest);

    }
    private void EnviarApuntamientos(final VolleyCallback callback) {
        final DBHelper mydb = new DBHelper(activity);

        ArrayList<OS> oesesenviar = mydb.getAllOS();



        UUID guid = null;
        String sToken = "";
        Parametros token = mydb.getSingleParametro(4);

        if (token != null) {
            sToken = token.getValor();
        }



        JSONArray raiz = new JSONArray();//Contenedor de OS's/Envíos
        JSONObject os = new JSONObject(); //Objeto OS contenedor de apuntamientos mo ins maq
        JSONArray aptos = new JSONArray(); //array de Apuntes
        JSONObject obj = new JSONObject();//Apuntes
        JSONArray arr = new JSONArray();
        try {


            if(oesesenviar==null){
                listMsg = new ArrayList<String>();
                listMsg.add("No hay datos para envíar");
                callback.onError("No hay datos para envíar");
            }


            ArrayList<OS> osArrayList = new ArrayList<OS>();

            for (OS o: oesesenviar
                    ) {

                ArrayList<ApuntesInsumos> apinsumos = mydb.getApuntesInsumosByOS(o.getIdOS());
                ArrayList<ApuntesMO> apmo = mydb.getApuntesMOByOS(o.getIdOS());
                ArrayList<ApuntesMaquinaria> apmaq = mydb.getApuntesMaquinariaByOS(o.getIdOS());
                if( apinsumos.size() > 0 || apmo.size() > 0  || apmaq.size() > 0){
                    osArrayList.add(o);
                }
            }

            if(osArrayList.size() == 0){
                listMsg = new ArrayList<String>();
                listMsg.add("No hay datos para envíar");
                callback.onError("No hay datos para envíar");
            }

            for (OS o: osArrayList
             ) {

            ArrayList<ApuntesInsumos> apinsumos = mydb.getApuntesInsumosByOS(o.getIdOS());
            ArrayList<ApuntesMO> apmo = mydb.getApuntesMOByOS(o.getIdOS());
            ArrayList<ApuntesMaquinaria> apmaq = mydb.getApuntesMaquinariaByOS(o.getIdOS());
            //    ArrayList<ApuntesMaquinaria> apmaq = mydb.getApuntesMaquinaria();

            os = new JSONObject();
            os.put("IdOS",o.getIdOS());
                aptos = new JSONArray();
            if (apinsumos != null) {
                    for (ApuntesInsumos ap : apinsumos) {
                        obj = new JSONObject();
                        obj.put("CorrelativoMovil",ap.getIdApInsumos());
                        obj.put("FechaApunte", ap.getFechaApunte());

                        obj.put("IdRecurso", ap.getIdRecurso());
                        obj.put("Area", ap.getArea());
                        obj.put("Dosis", ap.getDosis());
                        obj.put("Cantidad",ap.getCantidad());
                        obj.put("FechaDigita", ap.getFechaApunte());
                        obj.put("Observacion", ap.getObservacion());
                        obj.put("FechaIni",ap.getFechaIni());
                        obj.put("FechaFin",ap.getFechaFin());
                        obj.put("Latitud",ap.getLatitud());
                        obj.put("Longitud",ap.getLongitud());

                        aptos.put(obj);



                    }





            }
            os.put("insumos",aptos);

                aptos = new JSONArray();
            if (apmaq != null) {
                    for (ApuntesMaquinaria ap : apmaq) {
                        obj = new JSONObject();
                        String codImplemento =  ap.getCodEquipoImplemento() == null ? "" : (ap.getCodEquipoImplemento().equals("null") ? "" : ap.getCodEquipoImplemento());
                        obj.put("CorrelativoMovil",ap.getIdApMaquinaria());
                        obj.put("FechaApunte", ap.getFechaApunte());

                        obj.put("IdRecurso", ap.getIdRecurso());
                        obj.put("IdTrabajador", ap.getIdTrabajador());
                        obj.put("IdTrabAyudante", ap.getIdTrabAyudante());
                        obj.put("CodEquipo", ap.getCodEquipo());
                        obj.put("CodEquipoImplemento",codImplemento);
                        obj.put("HOR_INI", ap.getHOR_INI());
                        obj.put("HOR_FIN", ap.getHOR_FIN());
                        obj.put("HOR_FALLA", ap.getHOR_FALLA());
                        obj.put("ODO_INI", ap.getODO_INI());
                        obj.put("ODO_FIN", ap.getODO_FIN());
                        obj.put("ODO_FALLA", ap.getODO_FALLA());
                        obj.put("MED_INI", ap.getMED_INI());
                        obj.put("MED_FIN", ap.getMED_FIN());
                        obj.put("MED_FALLA", ap.getMED_FALLA());
                        obj.put("QTD_PAGO", ap.getQTD_PAGO());

                        obj.put("IdTipDocInterno", ap.getIdTipDocInterno());
                        obj.put("NumDocInterno", ap.getNumDocInterno());
                        obj.put("Equipo1", ap.getEquipo1());
                        obj.put("Equipo2", ap.getEquipo2());
                        obj.put("Equipo3", ap.getEquipo3());
                        obj.put("Equipo4", ap.getEquipo4());
                        obj.put("Origen", ap.getOrigen());
                        obj.put("Destino", ap.getDestino());
                        obj.put("Ruta", ap.getRuta());


                        obj.put("Area", ap.getArea());
                        obj.put("FechaDigita", ap.getFechaApunte());
                        obj.put("Observacion", ap.getObservacion());
                        obj.put("FechaIni",ap.getFechaIni());
                        obj.put("FechaFin",ap.getFechaFin());
                        obj.put("Latitud",ap.getLatitud());
                        obj.put("Longitud",ap.getLongitud());

                        aptos.put(obj);
                    }


            }
                os.put("maquinaria",aptos);

                aptos = new JSONArray();
                if (apmo != null) {
                    for (ApuntesMO ap : apmo) {
                        obj = new JSONObject();
                        obj.put("CorrelativoMovil",ap.getIdApMO());
                        obj.put("FechaApunte", ap.getFechaApunte());

                        obj.put("IdRecurso", ap.getIdRecurso());
                        obj.put("IdTrabajador", ap.getIdTrabajador());

                        obj.put("IdCuadrilla", ap.getIdCuadrilla());
                        obj.put("QtdPago", ap.getQTD_PAGO());


                        obj.put("Area", ap.getArea());

                        obj.put("FechaDigita", ap.getFechaApunte());
                        obj.put("Observacion", ap.getObservacion());
                        obj.put("FechaIni",ap.getFechaIni());
                        obj.put("FechaFin",ap.getFechaFin());
                        obj.put("Latitud",ap.getLatitud());
                        obj.put("Longitud",ap.getLongitud());

                        aptos.put(obj);
                    }


                }

                os.put("mo",aptos);

                raiz.put(os);

        }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final String mRequestBody = raiz.toString();


        final boolean[] enviados = {false};/*
        StringEntity entity = null;

        entity = new StringEntity(raiz.toString(),"UTF-8");
        entity.setContentType("application/json");

        if(entity == null) return "No fue posible convertir los datos de envío.";*/

        Parametros usr = mydb.getSingleParametro(5);
        Parametros imei = mydb.getSingleParametro(3);

        String susr ="";
        String simei = "";
        if (usr != null){
            susr = usr.getValor();
        }
        if (imei != null){
            simei = imei.getValor();
        }

        String url = DBHelper.urlServer+"/api/Apuntamientos/SetColectores?Usuario="+susr+"&imei="+simei;
/*
        HttpPost httpPost = new HttpPost(DBHelper.urlServer+"/api/Apuntamientos/SetColectores?Usuario="+susr+"&imei="+simei);
        httpPost.setEntity(entity);
        httpPost.setHeader("token", sToken);
        HttpClient httpClient = new NetworkUtility().getDefaultHttpClient(this.activity, DBHelper.urlServer+"/api/Apuntamientos/SetColectores?Usuario="+susr+"&imei="+simei);
*/
        RequestQueue requestQueue = NetworkUtility.getInstance(activity.getApplicationContext()).getRequestQueue();
        NetworkUtility.nuke();
        final String finalSToken = sToken;

        final String finalSusr = susr;
        JsonObjectRequest
                jsonObjectRequest
                = new JsonObjectRequest(
                Request.Method.POST,
                url,
                null,
                new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {
                        //HttpResponse response = httpClient.execute(httpPost);
                        if (response != null) {


                            JSONObject data = (JSONObject) response;
                            try {
                                enviados[0] = data.getBoolean("Key");
                                String responseStr = data.getString("Value");
                                if(enviados[0] == false) {
                                    callback.onError("Mensaje de server: "+responseStr);
                                }
                                else{
                                    mydb.limpiarTablas(finalSusr);
                                    callback.onSuccess(responseStr);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        } else {
                            callback.onError();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }) {


            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("token", finalSToken);
                return headers;
            }

            @Override
            protected Response parseNetworkResponse(NetworkResponse response) {
                try {
                    String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    return Response.success(new JSONObject(jsonString),HttpHeaderParser.parseCacheHeaders(response));
                } catch (UnsupportedEncodingException e) {
                    return Response.error(new ParseError(e));
                } catch (JSONException je) {
                    return Response.error(new ParseError(je));
                }
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() {
                try {
                    return mRequestBody == null ? null : mRequestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", mRequestBody, "utf-8");
                    return null;
                }
            }

        };

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                150000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);


    }
/*
    private void EnviarHuellas() {
        DBHelper mydb = new DBHelper(activity);
        DBExterno dbExterno = null;
        Context sharedContext = null;
        try {
            sharedContext = activity.createPackageContext("futronictech.com", Context.CONTEXT_INCLUDE_CODE);
            if (sharedContext != null) {
                DBExterno.ctxOriginal = activity;
                dbExterno = new DBExterno(sharedContext);
            }
            else{
                return;
            }
        } catch (Exception e) {
            String error = e.getMessage();
        }

        ArrayList<HuellasTrabajadores> huellasTrabajadores = htrab;//mydb.getHuellas(false); //HUELLAS NO SINCRONIZADAS


        if (huellasTrabajadores == null)
            huellasTrabajadores = new ArrayList<HuellasTrabajadores>();

        if (huellasTrabajadores.size() == 0)
            return;

        UUID guid = null;
        String sToken = "";
        Parametros token = mydb.getSingleParametro(4);

        if (token != null) {
            sToken = token.getValor();
        }



        JSONArray raiz = new JSONArray();//Contenedor de lista de huellas
        JSONObject hu = new JSONObject(); //Objeto con idtrabajador y huella
        try {
            for (HuellasTrabajadores h:
                 huellasTrabajadores) {
                byte[] encoded = Base64.encodeBase64(h.getHuellas());


                hu = new JSONObject();
                hu.put("<CodTrabajador>k__BackingField",h.getCodTrabajador());
                hu.put("<Huella>k__BackingField",new String(encoded));
                raiz.put(hu);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }



        StringEntity entity = null;
        try {

            entity = new StringEntity(raiz.toString());
            entity.setContentType("application/json");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        if(entity == null) return;

        Parametros usr = mydb.getSingleParametro(5);
        Parametros imei = mydb.getSingleParametro(3);

        boolean enviados = false;


        HttpPost httpPost = new HttpPost(DBHelper.urlServer+"/api/Apuntamientos/SetHuellas");
        httpPost.setEntity(entity);
        httpPost.setHeader("token", sToken);
        HttpClient httpClient = new NetworkUtility().getDefaultHttpClient(this.activity, DBHelper.urlServer+"/api/Apuntamientos/SetHuellas");

        try {
            HttpResponse response = httpClient.execute(httpPost);
            if (response != null) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));


                StringBuffer json = new StringBuffer(1024);
                String tmp = "";
                while ((tmp = reader.readLine()) != null)
                    json.append(tmp).append("\n");
                reader.close();
                String jsstr = json.toString();
                JSONObject data = new JSONObject(json.toString());
                enviados = data.getBoolean("Key");
                String responseStr = data.getString("Value");
                if(enviados == false) {
                    return ;
                }
                else{
                    for (HuellasTrabajadores h:
                         huellasTrabajadores) {
                        h.setSincronizada(enviados);
                    }
                    dbExterno.updateHuellas(huellasTrabajadores);
                }

            } else {
                return ;
            }
        } catch (IOException e) {
            return ;//Respuesta nula";
        } catch (JSONException ex) {
            ex.printStackTrace();
            return ;//"Error al enviar apuntamientos.";
        }

    }*/

    private void ObtenerApuntes(final VolleyCallback callback){
        final DBHelper mydb = new DBHelper(activity);
        DBExterno dbExterno = null;

        Parametros usr = mydb.getSingleParametro(5);

        String susr ="";
        if (usr != null){
            susr = usr.getValor();
        }

        String pendMsg = "";


        String url = DBHelper.urlServer+"/api/DatosMaestros/GetApuntesArea?login="+susr;
        //HttpGet httpGet = new HttpGet(DBHelper.urlServer+"/api/DatosMaestros/GetApuntesArea?login="+susr);

        //httpPost.setEntity(entity);
       // HttpClient httpClient = new NetworkUtility().getDefaultHttpClient(this.activity, DBHelper.urlServer+"/api/DatosMaestros/GetApuntesArea?login="+susr);
        RequestQueue requestQueue = NetworkUtility.getInstance(activity.getApplicationContext()).getRequestQueue();
        NetworkUtility.nuke();


        JsonArrayRequest
                jsonObjectRequest
                = new JsonArrayRequest(
                Request.Method.GET,
                url,
                null,
                new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {
                        //HttpResponse response = httpClient.execute(httpPost);
                        if (response != null) {


                            JSONArray data = (JSONArray) response;
                            try {
                                ArrayList<ApuntesArea> eqresult = new ArrayList<ApuntesArea>();
                                //JSONArray a = data.getJSONArray("<Equipos>k__BackingField");
                                for (int i = 0; i < data.length(); i++) {
                                    JSONObject hda = data.getJSONObject(i);
                                    int IdOS = hda.getInt("IdOS");
                                    String fecha = hda.getString("sFechaApunte");
                                    float area = Float.parseFloat(hda.get("Area").toString());
                                    ApuntesArea eq = new ApuntesArea(0,IdOS, fecha,area,true);
                                    eqresult.add(eq);
                                }

                                mydb.deleteAllApunteArea();

                                for (ApuntesArea e:
                                        eqresult) {
                                    mydb.insertApunteAreaIntegrado(e.getIdOS(),e.getFecha(),e.getArea());
                                }

                                callback.onSuccess("Apuntes de área recuperados correctamente");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        } else {
                            callback.onError();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }) {

            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");

                return headers;
            }

        };


        requestQueue.add(jsonObjectRequest);

    }
    private class ResponseContainer {
        public boolean result;
    }
}
