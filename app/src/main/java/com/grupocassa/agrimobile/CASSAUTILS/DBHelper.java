package com.grupocassa.agrimobile.CASSAUTILS;

/**
 * Created by gerardo.valle on 13/7/2017.
 */


import java.lang.reflect.Array;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Objects;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;

import com.grupocassa.agrimobile.CLS.*;

public class DBHelper extends SQLiteOpenHelper {
    //public static String MEDIA_JORNADA = "1";/*CALIDAD*/public static String JORNADA_NOCTURNA = "2";/*CALIDAD*/public static String AMPLIADA = "37";/*CALIDAD*/public static String CAPORAL = "3";/*CALIDAD*/public static String CUIDADOR = "36";/*CALIDAD*/
    public static String MEDIA_JORNADA = "9";/*PRD*/public static String JORNADA_NOCTURNA = "11";/*PRD*/public static String AMPLIADA = "10";/*PRD*/public static String CAPORAL = "13";/*PRD*/public static String CUIDADOR = "12";/*PRD*/
    //public static String urlServer = "https://168.243.156.105:8001";// CALIDAD PUBLIC
    //public static String urlServer = "https://192.168.24.46:8001";//"https://192.200.11.10:8001";// CALIDAD LAN//public static String urlServer = "http://10.0.2.2:60940"; LOCALHOST
   // public static String urlServer = "https://192.168.23.76:8001";//PRD LAN
    public static String urlServer = "https://168.243.156.106:8001";//PRD PUBLICO

    //public static final String fileName = "AGRIMOBILE.apk";
    public static final String fileName = "AGRIMOBILEPRDPUB.apk";
    //public static final String fileName = "AGRIMOBILEPRDLAN.apk";

    public static boolean activo = false;

    public static final String DATABASE_NAME = "AgriMobile.db";
    public static final String HACIENDAS_TABLE_NAME = "Haciendas";
    public static final String HACIENDAS_COLUMN_IDHACIENDA = "IdHacienda";
    public static final String HACIENDAS_COLUMN_BKHACIENDA = "BkHacienda";
    public static final String HACIENDAS_COLUMN_DSHACIENDA = "DsHacienda";
    public static final String HACIENDAS_COLUMN_MARCABIOMETRICO = "MarcaBiometrico";

    public static final String OS_TABLE_NAME = "OS";
    public static final String OS_COLUMN_IDOS = "IdOS";
    public static final String OS_COLUMN_IDHACIENDA = "IdHacienda";
    public static final String OS_COLUMN_BKZAFRA = "BkZafra";
    public static final String OS_COLUMN_DSACTIVIDAD = "DsActividad";//Lote y actividad concatenados
    public static final String OS_COLUMN_CANTIDAD = "Cantidad";

    public static final String OSRecursos_TABLE_NAME = "OSRecursos";
    public static final String OSRecursos_COLUMN_IDOS = "IdOS";
    public static final String OSRecursos_COLUMN_IDRECURSO = "IdRecurso";
    public static final String OSRecursos_COLUMN_BKRECURSO = "BkRecurso";
    public static final String OSRecursos_COLUMN_DSRECURSO = "DsRecurso";
    public static final String OSRecursos_COLUMN_BKTIPRECURSO = "BkTipRecurso";
    public static final String OSRecursos_COLUMN_UM = "UM";
    public static final String OSRecursos_COLUMN_CANTIDADPEND = "CantidadPend";
    public static final String OSRecursos_COLUMN_CANTIDADAPUNT = "CantidadApunt";
    public static final String OSRecursos_COLUMN_REQUIEREIMPLEMENTO = "RequiereImplemento";
    public static final String OSRecursos_COLUMN_IDSUBFLOTA = "IdSubFlota";


    public static final String CUADRILLAS_TABLE_NAME = "Cuadrillas";
    public static final String CUADRILLAS_COLUMN_IDCUADRILLA = "IdCuadrilla";
    public static final String CUADRILLAS_COLUMN_BKCUADRILLA = "BkCuadrilla";
    public static final String CUADRILLAS_COLUMN_DSCUADRILLA = "DsCuadrilla";


    public static final String TRABAJADORES_TABLE_NAME = "Trabajadores";
    public static final String TRABAJADORES_COLUMN_IDTRABAJADOR = "IdTrabajador";
    public static final String TRABAJADORES_COLUMN_CODTRABAJADOR = "CodTrabajador";
    public static final String TRABAJADORES_COLUMN_NOMTRABAJADOR = "NomTrabajador";
    public static final String TRABAJADORES_COLUMN_IDCUADRILLA = "IdCuadrilla";

    public static final String EQUIPOS_TABLE_NAME = "Equipos";
    public static final String EQUIPOS_COLUMN_IDEQUIPO = "IdEquipo";
    public static final String EQUIPOS_COLUMN_CODEQUIPO = "CodEquipo";
    public static final String EQUIPOS_COLUMN_NOMBREEQUIPO = "NombreEquipo";
    public static final String EQUIPOS_COLUMN_PROPEQUIPO = "PropEquipo";
    public static final String EQUIPOS_COLUMN_IDSUBFLOTA = "Id_SubFlota";



    public static final String APUNTESINSUMOS_TABLE_NAME = "ApuntesInsumos";
    public static final String APUNTESINSUMOS_COLUMN_IDAPINSUMOS = "IdApInsumos";
    public static final String APUNTESINSUMOS_COLUMN_IDOS = "IdOS";
    public static final String APUNTESINSUMOS_COLUMN_FECHAAPUNTE = "FechaApunte";
    public static final String APUNTESINSUMOS_COLUMN_IDRECURSO = "IdRecurso";
    public static final String APUNTESINSUMOS_COLUMN_AREA = "Area";
    public static final String APUNTESINSUMOS_COLUMN_DOSIS = "Dosis";
    public static final String APUNTESINSUMOS_COLUMN_USUARIO = "Usuario";
    public static final String APUNTESINSUMOS_COLUMN_FECHADIGITA = "FechaDigita";
    public static final String APUNTESINSUMOS_COLUMN_IDDEV = "IdDev";
    public static final String APUNTESINSUMOS_COLUMN_OBSERVACION = "Observacion";
    public static final String APUNTESINSUMOS_COLUMN_FECHAINI= "FechaIni";
    public static final String APUNTESINSUMOS_COLUMN_FECHAFIN= "FechaFin";
    public static final String APUNTESINSUMOS_COLUMN_LATITUD= "Latitud";
    public static final String APUNTESINSUMOS_COLUMN_LONGITUD= "Longitud";
    public static final String APUNTESINSUMOS_COLUMN_CANTIDAD= "Cantidad";


    public static final String APUNTESMAQUINARIA_TABLE_NAME = "ApuntesMaquinaria";
    public static final String APUNTESMAQUINARIA_COLUMN_IDAPMAQUINARIA = "IdApMaquinaria";
    public static final String APUNTESMAQUINARIA_COLUMN_IDOS = "IdOS";
    public static final String APUNTESMAQUINARIA_COLUMN_FECHAAPUNTE = "FechaApunte";
    public static final String APUNTESMAQUINARIA_COLUMN_IDRECURSO = "IdRecurso";
    public static final String APUNTESMAQUINARIA_COLUMN_IDTRABAJADOR = "IdTrabajador";
    public static final String APUNTESMAQUINARIA_COLUMN_IDTRABAYUDANTE = "IdTrabAyudante";
    public static final String APUNTESMAQUINARIA_COLUMN_CODEQUIPO = "CodEquipo";
    public static final String APUNTESMAQUINARIA_COLUMN_CODEQUIPOIMPLEMENTO = "CodEquipoImplemento";
    public static final String APUNTESMAQUINARIA_COLUMN_AREA = "Area";
    public static final String APUNTESMAQUINARIA_COLUMN_HOR_INI = "HOR_INI";
    public static final String APUNTESMAQUINARIA_COLUMN_HOR_FIN = "HOR_FIN";
    public static final String APUNTESMAQUINARIA_COLUMN_HOR_FALLA= "HOR_FALLA";
    public static final String APUNTESMAQUINARIA_COLUMN_ODO_INI = "ODO_INI";
    public static final String APUNTESMAQUINARIA_COLUMN_ODO_FIN = "ODO_FIN";
    public static final String APUNTESMAQUINARIA_COLUMN_ODO_FALLA = "ODO_FALLA";
    public static final String APUNTESMAQUINARIA_COLUMN_MED_INI = "MED_INI";
    public static final String APUNTESMAQUINARIA_COLUMN_MED_FIN = "MED_FIN";
    public static final String APUNTESMAQUINARIA_COLUMN_MED_FALLA = "MED_FALLA";
    public static final String APUNTESMAQUINARIA_COLUMN_QTD_PAGO = "QTD_PAGO";
    public static final String APUNTESMAQUINARIA_COLUMN_USUARIO = "Usuario";
    public static final String APUNTESMAQUINARIA_COLUMN_FECHADIGITA = "FechaDigita";
    public static final String APUNTESMAQUINARIA_COLUMN_IDDEV = "IdDev";
    public static final String APUNTESMAQUINARIA_COLUMN_OBSERVACION = "Observacion";
    public static final String APUNTESMAQUINARIA_COLUMN_FECHAINI= "FechaIni";
    public static final String APUNTESMAQUINARIA_COLUMN_FECHAFIN= "FechaFin";
    public static final String APUNTESMAQUINARIA_COLUMN_LATITUD= "Latitud";
    public static final String APUNTESMAQUINARIA_COLUMN_LONGITUD= "Longitud";
    public static final String APUNTESMAQUINARIA_COLUMN_EQ1 = "Equipo1";
    public static final String APUNTESMAQUINARIA_COLUMN_EQ2 = "Equipo2";
    public static final String APUNTESMAQUINARIA_COLUMN_EQ3 = "Equipo3";
    public static final String APUNTESMAQUINARIA_COLUMN_EQ4 = "Equipo4";
    public static final String APUNTESMAQUINARIA_COLUMN_ORIGEN = "Origen";
    public static final String APUNTESMAQUINARIA_COLUMN_DESTINO= "Destino";
    public static final String APUNTESMAQUINARIA_COLUMN_RUTA= "Ruta";
    public static final String APUNTESMAQUINARIA_COLUMN_IDTIPDOCINTERNO= "IdTipDocInterno";
    public static final String APUNTESMAQUINARIA_COLUMN_NUMDOCINTERNO= "NumDocInterno";


    public static final String APUNTESMANOOBRA_TABLE_NAME = "ApuntesMO";
    public static final String APUNTESMANOOBRA_COLUMN_IDAPMO = "IdApMO";
    public static final String APUNTESMANOOBRA_COLUMN_IDOS = "IdOS";
    public static final String APUNTESMANOOBRA_COLUMN_FECHAAPUNTE = "FechaApunte";
    public static final String APUNTESMANOOBRA_COLUMN_IDRECURSO = "IdRecurso";
    public static final String APUNTESMANOOBRA_COLUMN_AREA = "Area";
    public static final String APUNTESMANOOBRA_IDCUADRILLA = "IdCuadrilla";
    public static final String APUNTESMANOOBRA_COLUMN_IDTRABAJADOR = "IdTrabajador";
    public static final String APUNTESMANOOBRA_COLUMN_QTD_PAGO = "QTD_PAGO";
    public static final String APUNTESMANOOBRA_COLUMN_USUARIO = "Usuario";
    public static final String APUNTESMANOOBRA_COLUMN_FECHADIGITA = "FechaDigita";
    public static final String APUNTESMANOOBRA_COLUMN_IDDEV = "IdDev";
    public static final String APUNTESMANOOBRA_COLUMN_OBSERVACION = "Observacion";
    public static final String APUNTESMANOOBRA_COLUMN_FECHAINI= "FechaIni";
    public static final String APUNTESMANOOBRA_COLUMN_FECHAFIN= "FechaFin";
    public static final String APUNTESMANOOBRA_COLUMN_LATITUD= "Latitud";
    public static final String APUNTESMANOOBRA_COLUMN_LONGITUD= "Longitud";





    public static final String PARAMETROS_TABLE_NAME = "Parametros";
    public static final String PARAMETROS_COLUMN_IDPARAMETRO = "IdParametro";
    public static final String PARAMETROS_COLUMN_VALOR = "Valor";

    public static final String EMPRESAS_TABLE_NAME = "Empresas";
    public static final String EMPRESAS_COLUMN_SOCIEDAD = "Sociedad";
    public static final String EMPRESAS_COLUMN_DSEMPRESA = "DsEmpresa";

    public static final String RECURSOSSUBFLOTAS_TABLE_NAME = "RecursosSubFlota";
    public static final String RECURSOSSUBFLOTAS_COLUMN_IDRECURSO = "IdRecurso";
    public static final String RECURSOSSUBFLOTAS_COLUMN_IDSUBFLOTA = "IdSubFlota";


    private HashMap hp;
    private String usr;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME , null, 11);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub

        db.execSQL("create table IF NOT EXISTS "+ EMPRESAS_TABLE_NAME +
                "("+EMPRESAS_COLUMN_SOCIEDAD+" integer primary key, "+EMPRESAS_COLUMN_DSEMPRESA+" text)"
        );

        db.execSQL(
                "create table IF NOT EXISTS Haciendas" +
                        "(IdHacienda integer primary key, BkHacienda text, DsHacienda text, "+HACIENDAS_COLUMN_MARCABIOMETRICO+" integer)"
        );

        db.execSQL(
                "create table IF NOT EXISTS "+EQUIPOS_TABLE_NAME +
                        "("+EQUIPOS_COLUMN_IDEQUIPO+" integer primary key, "+EQUIPOS_COLUMN_CODEQUIPO+" text, "+EQUIPOS_COLUMN_NOMBREEQUIPO+" text, "+EQUIPOS_COLUMN_PROPEQUIPO+" text DEFAULT '', "+EQUIPOS_COLUMN_IDSUBFLOTA+" integer)"
        );

        db.execSQL(
                "create table IF NOT EXISTS Aplicaciones" +
                        "(app text)"
        );

        db.execSQL(
                "create table IF NOT EXISTS Parametros" +
                        "(IdParametro integer primary key, Valor text)"
        );



        db.execSQL(
                "create table IF NOT EXISTS OS" +
                        "(IdOS integer primary key, IdHacienda integer,BkZafra text, DsActividad text,Cantidad real, UMP text)"
        );


        db.execSQL(
                "create table IF NOT EXISTS OSRecursos" +
                        "(IdOS integer, IdRecurso integer, BkRecurso text, DsRecurso text,BkTipRecurso text , UM text,CantidadPend real, CantidadApunt real, AreaApunt real,Ayudante integer, "+OSRecursos_COLUMN_REQUIEREIMPLEMENTO+" integer default 0, "+OSRecursos_COLUMN_IDSUBFLOTA+" integer default 0)"
        );


        db.execSQL(
                "create table IF NOT EXISTS Cuadrillas" +
                        "(IdCuadrilla integer primary key,BkCuadrilla text, DsCuadrilla text)"
        );

        db.execSQL(
                "create table IF NOT EXISTS Trabajadores" +
                        "(IdTrabajador integer primary key,  CodTrabajador text, NomTrabajador text, IdCuadrilla integer)"
        );


        db.execSQL(
                "create table IF NOT EXISTS ApuntesInsumos" +
                        "(IdApInsumos integer primary key, IdOS integer, FechaApunte text, IdRecurso integer, Area real, Cantidad real ,Dosis real, Usuario text, FechaDigita text, IdDev integer, Observacion text, FechaIni text, FechaFin text, Latitud real, Longitud real)"
        );

        db.execSQL(
                "create table IF NOT EXISTS ApuntesMaquinaria" +
                        "(IdApMaquinaria integer primary key, IdOS integer, FechaApunte text, IdRecurso integer" +
                        ", IdTrabajador integer,IdTrabAyudante integer, CodEquipo text , CodEquipoImplemento text" +
                        ", Area real, HOR_INI real, HOR_FIN real, HOR_FALLA integer, ODO_INI real, ODO_FIN real" +
                        ", ODO_FALLA integer,MED_INI real, MED_FIN real, MED_FALLA integer, QTD_PAGO real" +
                        ", Equipo1 integer default 0, Equipo2 integer default 0, Equipo3 integer default 0, Equipo4 integer default 0" +
                        ", IdTipDocInterno integer default 0, NumDocInterno text" +
                        ", Origen text, Destino text, Ruta text"+
                        ", Usuario text, FechaDigita text, IdDev integer, Observacion text, FechaIni text, FechaFin text, Latitud real, Longitud real)"
        );

        db.execSQL(
                "create table IF NOT EXISTS ApuntesMO" +
                        "(IdApMO integer primary key, IdOS integer, FechaApunte text, IdRecurso integer, Area real ,IdCuadrilla integer, IdTrabajador integer, QTD_PAGO real, Usuario text, FechaDigita text, IdDev integer, Observacion text, FechaIni text, FechaFin text, Latitud real, Longitud real)"
        );

        db.execSQL(
                "create table IF NOT EXISTS TiposDocumentoInterno" +
                        "(IdTipDocInterno integer primary key, BkTipDocInterno text, DsTipDocInterno)"
        );

        db.execSQL(
                "create table IF NOT EXISTS ApuntesArea" +
                        "(IdApArea integer primary key autoincrement ,IdOS integer, Fecha text, Area real, enviado integer)"
        );

        db.execSQL(
                "create table IF NOT EXISTS " +RECURSOSSUBFLOTAS_TABLE_NAME+
                        " ("+RECURSOSSUBFLOTAS_COLUMN_IDRECURSO+" integer, "+RECURSOSSUBFLOTAS_COLUMN_IDSUBFLOTA+" integer)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        /*db.execSQL("DROP TABLE IF EXISTS Haciendas");
        db.execSQL("DROP TABLE IF EXISTS Parametros");
        db.execSQL("DROP TABLE IF EXISTS OS");
        db.execSQL("DROP TABLE IF EXISTS OSRecursos");
        db.execSQL("DROP TABLE IF EXISTS Cuadrillas");
        db.execSQL("DROP TABLE IF EXISTS Trabajadores");
        db.execSQL("DROP TABLE IF EXISTS ApuntesInsumos");
        db.execSQL("DROP TABLE IF EXISTS ApuntesMaquinaria");
        db.execSQL("DROP TABLE IF EXISTS ApuntesMO");
        onCreate(db);*/
        /*if (newVersion == 4 && oldVersion != newVersion) {//cambios para la VersionCode 9
            db.execSQL("ALTER TABLE OSRecursos ADD COLUMN Ayudante INTEGER DEFAULT 0");
            db.execSQL("ALTER TABLE ApuntesMaquinaria ADD COLUMN IdTrabAyudante INTEGER DEFAULT 0");
        }*/

        if (newVersion == 5) {//cambios para la VersionCode 10
            db.execSQL("ALTER TABLE "+HACIENDAS_TABLE_NAME+" ADD COLUMN "+HACIENDAS_COLUMN_MARCABIOMETRICO+" INTEGER DEFAULT 0");
            db.execSQL(
                    "create table IF NOT EXISTS TiposDocumentoInterno" +
                            "(IdTipDocInterno integer primary key, BkTipDocInterno text, DsTipDocInterno)"
            );

            db.execSQL("ALTER TABLE "+APUNTESMAQUINARIA_TABLE_NAME
                    +" ADD COLUMN Equipo1 INTEGER DEFAULT 0");
            db.execSQL("ALTER TABLE "+APUNTESMAQUINARIA_TABLE_NAME
                    +" ADD COLUMN Equipo2 INTEGER DEFAULT 0");
            db.execSQL("ALTER TABLE "+APUNTESMAQUINARIA_TABLE_NAME
                    +" ADD COLUMN Equipo3 INTEGER DEFAULT 0");
            db.execSQL("ALTER TABLE "+APUNTESMAQUINARIA_TABLE_NAME
                    +" ADD COLUMN Equipo4 INTEGER DEFAULT 0");

            db.execSQL("ALTER TABLE "+APUNTESMAQUINARIA_TABLE_NAME
                    +" ADD COLUMN IdTipDocInterno INTEGER DEFAULT 0");
            db.execSQL("ALTER TABLE "+APUNTESMAQUINARIA_TABLE_NAME
                    +" ADD COLUMN NumDocInterno TEXT");

            db.execSQL("ALTER TABLE "+APUNTESMAQUINARIA_TABLE_NAME
                    +" ADD COLUMN Origen text");
            db.execSQL("ALTER TABLE "+APUNTESMAQUINARIA_TABLE_NAME
                    +" ADD COLUMN Destino text");
            db.execSQL("ALTER TABLE "+APUNTESMAQUINARIA_TABLE_NAME
                    +" ADD COLUMN Ruta text");

            db.execSQL(
                    "create table IF NOT EXISTS "+EQUIPOS_TABLE_NAME +
                            "("+EQUIPOS_COLUMN_IDEQUIPO+" integer primary key, "+EQUIPOS_COLUMN_CODEQUIPO+" text, "+EQUIPOS_COLUMN_NOMBREEQUIPO+" text)"
            );
        }

        if ( newVersion == 6){
            db.execSQL("ALTER TABLE "+EQUIPOS_TABLE_NAME+" ADD COLUMN "+EQUIPOS_COLUMN_PROPEQUIPO+" TEXT DEFAULT ''");
        }

        if( newVersion > 6) {
            db.execSQL("create table IF NOT EXISTS " + EMPRESAS_TABLE_NAME +
                    "(" + EMPRESAS_COLUMN_SOCIEDAD + " integer primary key, " + EMPRESAS_COLUMN_DSEMPRESA + " text)"
            );


        }

        if(newVersion == 8){
            db.execSQL("ALTER TABLE " + OS_TABLE_NAME + " ADD COLUMN " + OS_COLUMN_BKZAFRA + " TEXT DEFAULT ''");
        }

        if(newVersion == 9){
            db.execSQL(
                    "create table IF NOT EXISTS ApuntesArea" +
                            "(IdApArea integer primary key autoincrement, IdOS integer, Fecha text, Area real, enviado integer)"
            );
            db.execSQL("ALTER TABLE " + OS_TABLE_NAME + " ADD COLUMN UMP TEXT DEFAULT ''");
        }

        if ( newVersion == 10){
            db.execSQL("ALTER TABLE "+EQUIPOS_TABLE_NAME+" ADD COLUMN "+EQUIPOS_COLUMN_IDSUBFLOTA+" integer default 0");
            db.execSQL("ALTER TABLE "+OSRecursos_TABLE_NAME+" ADD COLUMN "+OSRecursos_COLUMN_REQUIEREIMPLEMENTO+" integer default 0");
            db.execSQL("ALTER TABLE "+OSRecursos_TABLE_NAME+" ADD COLUMN "+OSRecursos_COLUMN_IDSUBFLOTA+" integer default 0");
        }

        if(newVersion == 11){
            db.execSQL(
                    "create table IF NOT EXISTS " +RECURSOSSUBFLOTAS_TABLE_NAME+
                            " ("+RECURSOSSUBFLOTAS_COLUMN_IDRECURSO+" integer, "+RECURSOSSUBFLOTAS_COLUMN_IDSUBFLOTA+" integer)"
            );
        }
    }

    public boolean insertHda (int IdHacienda, String BkHacienda, String DsHacienda,int IdPluvio, boolean MarcaBiometrico) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("IdHacienda", IdHacienda);
            contentValues.put("BkHacienda", BkHacienda);
            contentValues.put("DsHacienda", DsHacienda);
            contentValues.put(HACIENDAS_COLUMN_MARCABIOMETRICO, MarcaBiometrico);

            return db.insert("Haciendas", null, contentValues) > 0;
        }
        catch (Exception e)
        {
            return false;
        }
    }

    public boolean insertTipoDocumento (int IdTipDocInterno, String BkTipDocInterno, String DsTipDocInterno) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("IdTipDocInterno", IdTipDocInterno);
            contentValues.put("BkTipDocInterno", BkTipDocInterno);
            contentValues.put("DsTipDocInterno", DsTipDocInterno);

            return db.insert("TiposDocumentoInterno", null, contentValues) > 0;
        }
        catch (Exception e)
        {
            return false;
        }
    }

    public boolean insertEmpresa (int Sociedad, String DsEmpresa) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(EMPRESAS_COLUMN_SOCIEDAD, Sociedad);
            contentValues.put(EMPRESAS_COLUMN_DSEMPRESA, DsEmpresa);
            return db.insert(EMPRESAS_TABLE_NAME, null, contentValues) > 0;
        }
        catch (Exception e)
        {
            return false;
        }
    }


    public boolean insertAplicacion(String App) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("app", App);
            return db.insert("Aplicaciones", null, contentValues) > 0;
        }
        catch (Exception e)
        {
            return false;
        }

    }

    public boolean insertEquipo(Equipos e) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(EQUIPOS_COLUMN_IDEQUIPO, e.getIdEquipo());
            contentValues.put(EQUIPOS_COLUMN_CODEQUIPO, e.getCodEquipo());
            contentValues.put(EQUIPOS_COLUMN_NOMBREEQUIPO, e.getNomEquipo());
            contentValues.put(EQUIPOS_COLUMN_PROPEQUIPO, e.getPropEquipo());
            contentValues.put(EQUIPOS_COLUMN_IDSUBFLOTA, e.getId_SubFlota());
            if(e.getCodEquipo().trim().equals("7300000580")){
                if(e.getCodEquipo().trim().equals("7300000580")){
                    int ids = e.getId_SubFlota();
                    int ok = 1;
                }
            }
            return db.insert(EQUIPOS_TABLE_NAME, null, contentValues) > 0;
        }
        catch (Exception ex)
        {
            return false;
        }

    }

    public boolean insertOS (int IdOS, int IdHacienda, String DsActividad, float Cantidad, String bkZafra) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("IdOS", IdOS);
            contentValues.put("IdHacienda", IdHacienda);
            contentValues.put("DsActividad", DsActividad);
            contentValues.put("Cantidad", Cantidad);
            contentValues.put(OS_COLUMN_BKZAFRA,bkZafra);
            return db.insert("OS", null, contentValues) > 0;
        }
        catch (Exception e)
        {
            return false;
        }

    }

    public boolean insertOSRecurso (int IdOS, int IdRecurso, String BkRecurso, String DsRecurso, String BkTipRecurso, String UM, float CantidadPend, float CantidadApunt, float AreaApunt, Boolean Ayudante, Boolean ReqImp, int IdSubFlota) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            if(IdOS == 748){
                if(IdOS == 748) {
                    int ok = 1;
                }
            }
            contentValues.put("IdOS", IdOS);
            contentValues.put("IdRecurso", IdRecurso);
            contentValues.put("BkRecurso", BkRecurso);
            contentValues.put("DsRecurso", DsRecurso);
            contentValues.put("BkTipRecurso", BkTipRecurso);
            contentValues.put("UM", UM);
            contentValues.put("AreaApunt", AreaApunt);
            contentValues.put("Ayudante", Ayudante?1:0);
            contentValues.put(OSRecursos_COLUMN_REQUIEREIMPLEMENTO, ReqImp?1:0);
            contentValues.put(OSRecursos_COLUMN_IDSUBFLOTA, IdSubFlota);
            contentValues.put(OSRecursos_COLUMN_CANTIDADPEND, CantidadPend);
            contentValues.put(OSRecursos_COLUMN_CANTIDADAPUNT, CantidadApunt);
            return db.insert("OSRecursos", null, contentValues) > 0;
        }
        catch (Exception e)
        {
            return false;
        }

    }


    public boolean insertCuadrillas (int IdCuadrilla,  String BkCuadrilla, String DsCuadrilla) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("IdCuadrilla", IdCuadrilla);
            contentValues.put("BkCuadrilla", BkCuadrilla);
            contentValues.put("DsCuadrilla", DsCuadrilla);
            return db.insert("Cuadrillas", null, contentValues) > 0;
        }
        catch (Exception e)
        {
            return false;
        }

    }

    public boolean insertRecursosFlotas (int IdSubFlota,  int IdRecurso) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(RECURSOSSUBFLOTAS_COLUMN_IDRECURSO, IdRecurso);
            contentValues.put(RECURSOSSUBFLOTAS_COLUMN_IDSUBFLOTA, IdSubFlota);
            return db.insert(RECURSOSSUBFLOTAS_TABLE_NAME, null, contentValues) > 0;
        }
        catch (Exception e)
        {
            return false;
        }

    }

    public boolean insertTrabajadores (int IdTrabajador, String CodTrabajador, String NomTabajador, int IdCuadrilla) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("IdTrabajador", IdTrabajador);
            contentValues.put("CodTrabajador", CodTrabajador);
            contentValues.put("NomTrabajador", NomTabajador);
            contentValues.put("IdCuadrilla", IdCuadrilla);
            return db.insert("Trabajadores", null, contentValues) > 0;
        }
        catch (Exception e)
        {
            return false;
        }

    }


    public boolean insertApunteArea (int IdOS,String FechaApunte, float Area) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();


            ContentValues contentValues = new ContentValues();
            contentValues.put("IdOS", IdOS);
            contentValues.put("Fecha", FechaApunte);

            contentValues.put("Area", Area);
            contentValues.put("Enviado", 0);


            int retorno = (int)db.insert("ApuntesArea", null, contentValues) ;
            return  retorno >0;
        }
        catch (Exception e)
        {
            return false;
        }

    }

    public boolean insertApunteAreaIntegrado (int IdOS,String FechaApunte, float Area) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();


            ContentValues contentValues = new ContentValues();
            contentValues.put("IdOS", IdOS);
            contentValues.put("Fecha", FechaApunte);

            contentValues.put("Area", Area);
            contentValues.put("Enviado", 1);


            int retorno = (int)db.insert("ApuntesArea", null, contentValues) ;
            return  retorno >0;
        }
        catch (Exception e)
        {
            return false;
        }

    }

    public boolean insertApuntesInsumos (int IdOS,String FechaApunte,int IdRecurso, float Area, float Dosis, float Cantidad, String Usuario, String FechaDigita, String Observacion, String FechaIni, String FechaFin, float Latitud, float Longitud) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();


            Parametros usr =  getSingleParametro(5);
            if (usr !=null) {
                Usuario = usr.getValor();
            }

            ContentValues contentValues = new ContentValues();
            contentValues.put("IdOS", IdOS);
            contentValues.put("FechaApunte", FechaApunte);
            contentValues.put("IdRecurso", IdRecurso);
            contentValues.put("Area", Area);
            contentValues.put("Dosis", Dosis);

            contentValues.put("Usuario", Usuario);
            contentValues.put(APUNTESINSUMOS_COLUMN_FECHADIGITA, FechaDigita);
            contentValues.put("Observacion",Observacion);
            contentValues.put("FechaIni",FechaIni);
            contentValues.put("FechaFin",FechaFin);
            contentValues.put("Latitud",Latitud);
            contentValues.put("Longitud",Longitud);
            contentValues.put("Cantidad", Cantidad);

            int retorno = (int)db.insert(APUNTESINSUMOS_TABLE_NAME, null, contentValues) ;
            return  retorno >0;
        }
        catch (Exception e)
        {
            return false;
        }

    }

    public boolean insertApuntesMaquinaria (int IdOS,String FechaApunte,int IdRecurso, int IdTrabajador,int IdAyudante
            , String CodEquipo,String CodEquipoImplemento, float Area, float HOR_INI, float HOR_FIN, int HOR_FALLA
            , float ODO_INI, float ODO_FIN, int ODO_FALLA,float MED_INI,float MED_FIN, int MED_FALLA, float QTD_PAGO
            , int Eq1, int Eq2, int Eq3, int Eq4
            , int IdTipDocInterno, String NumDocInterno
            , String Origen, String Destino, String Ruta
            , String Usuario, String FechaDigita, String Observacion, String FechaIni, String FechaFin, float Latitud, float Longitud) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues contentValues = new ContentValues();
            contentValues.put("IdOS", IdOS);
            contentValues.put("FechaApunte", FechaApunte);
            contentValues.put("IdRecurso", IdRecurso);
            contentValues.put("IdTrabajador", IdTrabajador);
            contentValues.put("IdTrabAyudante", IdAyudante);
            contentValues.put("CodEquipo", CodEquipo);
            contentValues.put("CodEquipoImplemento", CodEquipoImplemento);
            contentValues.put("Area", Area);
            contentValues.put("HOR_INI", HOR_INI);
            contentValues.put("HOR_FIN", HOR_FIN);
            contentValues.put("HOR_FALLA", HOR_FALLA);
            contentValues.put("ODO_INI", ODO_INI);
            contentValues.put("ODO_FIN", ODO_FIN);
            contentValues.put("ODO_FALLA", ODO_FALLA);
            contentValues.put("MED_INI", MED_INI);
            contentValues.put("MED_FIN", MED_FIN);
            contentValues.put("MED_FALLA", MED_FALLA);
            contentValues.put("QTD_PAGO", QTD_PAGO);
            contentValues.put("Usuario", Usuario);
            contentValues.put("FechaDigita", FechaDigita);
            contentValues.put("Observacion",Observacion);
            contentValues.put("FechaIni",FechaIni);
            contentValues.put("FechaFin",FechaFin);
            contentValues.put("Latitud",Latitud);
            contentValues.put("Longitud",Longitud);

            contentValues.put("Equipo1", Eq1);
            contentValues.put("Equipo2", Eq2);
            contentValues.put("Equipo3", Eq3);
            contentValues.put("Equipo4", Eq4);

            contentValues.put("IdTipDocInterno",IdTipDocInterno);
            contentValues.put("NumDocInterno",NumDocInterno);
            contentValues.put("Origen",Origen);
            contentValues.put("Destino",Destino);
            contentValues.put("Ruta",Ruta);


            int retorno = (int)db.insert(APUNTESMAQUINARIA_TABLE_NAME, null, contentValues) ;
            return  retorno >0;
        }
        catch (Exception e)
        {
            return false;
        }

    }


    public boolean insertApuntesMO (int IdOS,String FechaApunte,int IdRecurso, float Area, int IdCuadrilla,int IdTrabajador, float QTD_PAGO, String Usuario, String FechaDigita, String Observacion, String FechaIni, String FechaFin, float Latitud, float Longitud){
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Haciendas res = getSingleHacienda(IdOS);

            ContentValues contentValues = new ContentValues();
            contentValues.put("IdOS", IdOS);
            contentValues.put("FechaApunte", FechaApunte);
            contentValues.put("IdRecurso", IdRecurso);
            contentValues.put("Area", Area);
            contentValues.put("IdCuadrilla", IdCuadrilla);
            contentValues.put("IdTrabajador", IdTrabajador);
            contentValues.put("QTD_PAGO", QTD_PAGO);

            contentValues.put("Usuario", getUsr());
            contentValues.put("FechaDigita", FechaDigita);
            contentValues.put("Observacion",Observacion);
            contentValues.put("FechaIni",FechaIni);
            contentValues.put("FechaFin",FechaFin);
            contentValues.put("Latitud",Latitud);
            contentValues.put("Longitud",Longitud);

            int retorno = (int)db.insert(APUNTESMANOOBRA_TABLE_NAME, null, contentValues) ;
            return  retorno > 0;
        }
        catch (Exception e)
        {
            return false;
        }

    }

    public boolean insertParametro (int Id, String Valor) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("IdParametro", Id);

            contentValues.put("Valor", Valor);

            return db.insert(PARAMETROS_TABLE_NAME, null, contentValues) > 0;
        }
        catch (Exception e)
        {
            return false;
        }

    }

    public Cursor getData(int id,String TableName) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from "+TableName+" where id="+id+"", null );
        return res;
    }

    public ApuntesArea getSingleArea(int id){
        ApuntesArea obj = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from ApuntesArea where IdApArea = "+id, null );

        if(res.isAfterLast() == false){
            obj = new ApuntesArea();
            res.moveToFirst();
            int IdApArea = Integer.parseInt(res.getString(res.getColumnIndex("IdApArea")));
            int IdOS = Integer.parseInt(res.getString(res.getColumnIndex("IdOS")));
            String Fecha  = res.getString(res.getColumnIndex("Fecha"));
            float Area = Float.parseFloat(res.getString(res.getColumnIndex("Area")));
            int enviado = Integer.parseInt(res.getString(res.getColumnIndex("enviado")));
            obj = new ApuntesArea(IdApArea,IdOS,Fecha, Area, enviado > 0);
        }


        return  obj;
    }

    public ApuntesArea getSingleArea(int IdOS, String fecha){
        ApuntesArea obj = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from ApuntesArea where IdOS = "+IdOS+" and Fecha = '"+fecha+"'", null );

        if(res.isAfterLast() == false){
            obj = new ApuntesArea();
            res.moveToFirst();
            int IdApArea = Integer.parseInt(res.getString(res.getColumnIndex("IdApArea")));
            String Fecha  = res.getString(res.getColumnIndex("Fecha"));
            float Area = Float.parseFloat(res.getString(res.getColumnIndex("Area")));
            int enviado = Integer.parseInt(res.getString(res.getColumnIndex("enviado")));
            obj = new ApuntesArea(IdApArea,IdOS,Fecha, Area, enviado > 0);
        }


        return  obj;
    }



    public Parametros getSingleParametro(int id){
        Parametros obj = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from Parametros where IdParametro = "+id, null );

        if(res.isAfterLast() == false){
            obj = new Parametros();
            res.moveToFirst();
            int IdParametro = Integer.parseInt(res.getString(res.getColumnIndex(PARAMETROS_COLUMN_IDPARAMETRO)));

            String Valor = res.getString(res.getColumnIndex(PARAMETROS_COLUMN_VALOR));
            obj = new Parametros(IdParametro,Valor);
        }


        return  obj;
    }
    public OS getSingleOS(int id){
        OS obj = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from "+OS_TABLE_NAME+" where "+OS_COLUMN_IDOS+"="+id+"", null );
        System.out.print(res);
        if(res.isAfterLast() == false) {
            obj = new OS();
            obj.setIdOS(id);
            res.moveToFirst();
            int idh = res.getInt(res.getColumnIndex(OS_COLUMN_IDHACIENDA));
            String ds = res.getString(res.getColumnIndex(OS_COLUMN_DSACTIVIDAD));
            float ctd = res.getFloat(res.getColumnIndex(OS_COLUMN_CANTIDAD));

            obj.setIdHacienda(idh);
            obj.setDsActividad(ds);
            obj.setCantidad(ctd);
        }
        return obj;

    }

    public ApuntesMO getSingleApunteManoObra(int id){
        ApuntesMO obj = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select ap.*, osr."+OSRecursos_COLUMN_DSRECURSO+", osr."+OSRecursos_COLUMN_UM+", c."+CUADRILLAS_COLUMN_BKCUADRILLA+" || ' - '||c."+CUADRILLAS_COLUMN_DSCUADRILLA+" as Cuadrilla, tr."+TRABAJADORES_COLUMN_CODTRABAJADOR+"||' - '||"+TRABAJADORES_COLUMN_NOMTRABAJADOR+" as Trabajador from "+APUNTESMANOOBRA_TABLE_NAME+" ap inner join "+OSRecursos_TABLE_NAME+" osr on ap."+APUNTESMANOOBRA_COLUMN_IDOS+" = osr."+OSRecursos_COLUMN_IDOS+" and osr."+OSRecursos_COLUMN_IDRECURSO+"=ap."+APUNTESMANOOBRA_COLUMN_IDRECURSO+" inner join "+CUADRILLAS_TABLE_NAME+" c on ap."+APUNTESMANOOBRA_IDCUADRILLA+" = c."+CUADRILLAS_COLUMN_IDCUADRILLA+" inner join "+TRABAJADORES_TABLE_NAME+" tr on ap."+APUNTESMANOOBRA_COLUMN_IDTRABAJADOR+"  = tr."+TRABAJADORES_COLUMN_IDTRABAJADOR+" where ap."+APUNTESMANOOBRA_COLUMN_IDAPMO+" = "+id+" and ap.Usuario ='"+usr+"'", null );
        System.out.print(res);
        if(res.isAfterLast() == false) {
            res.moveToFirst();
            obj = new ApuntesMO();
            int IdOS =            Integer.parseInt( res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_IDOS)));
            int IdCuadrilla =       Integer.parseInt( res.getString(res.getColumnIndex(APUNTESMANOOBRA_IDCUADRILLA)));
            int IdTrabajador =      Integer.parseInt( res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_IDTRABAJADOR)));

            float QTD_PAGO =        ( res.getFloat(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_QTD_PAGO)));

            String Fecha =          ( res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_FECHAAPUNTE)));
            int IdRecurso =         Integer.parseInt( res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_IDRECURSO)));
            float Area =            res.getFloat(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_AREA));
            String Usuario =        res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_USUARIO));
            String FechaDigita =    res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_FECHADIGITA));
            String IdDev =          res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_IDDEV));
            String Observacion =    res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_OBSERVACION));
            String FechaIni =       res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_FECHAINI));
            String FechaFin =       res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_FECHAFIN));
            String Latitud =        res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_LATITUD));
            String Longitud =       res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_LONGITUD));

            String DsRecurso =      res.getString(res.getColumnIndex(OSRecursos_COLUMN_DSRECURSO));
            String DsCuadrilla =        res.getString(res.getColumnIndex("Cuadrilla"));
            String Trabajador =     res.getString(res.getColumnIndex("Trabajador"));
            String UM = res.getString(res.getColumnIndex(OSRecursos_COLUMN_UM));
            obj = new ApuntesMO(IdOS,Fecha,IdRecurso,Area,Usuario,FechaDigita,IdDev,FechaIni,FechaFin,Latitud,Longitud,Observacion,id,IdCuadrilla,IdTrabajador,QTD_PAGO);
            obj.setDsCuadrilla(DsCuadrilla);
            obj .setTrabajador(Trabajador);
            obj.setDsRecurso(DsRecurso);
            obj.setTrabajador(Trabajador);
            obj.setUM(UM);
        }
        return obj;

    }


    public ApuntesInsumos getSingleApunteInsumos(int id){
        ApuntesInsumos obj = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from "+APUNTESINSUMOS_TABLE_NAME+" where "+APUNTESINSUMOS_COLUMN_IDAPINSUMOS+"="+id+"", null );
        System.out.print(res);
        if(res.isAfterLast() == false) {
            obj = new ApuntesInsumos();
            obj.setIdApInsumos(id);
            res.moveToFirst();
            obj.setDosis(res.getFloat(res.getColumnIndex(APUNTESINSUMOS_COLUMN_DOSIS)));
            obj.setArea(res.getFloat(res.getColumnIndex(APUNTESINSUMOS_COLUMN_AREA)));
            obj.setFechaApunte(res.getString(res.getColumnIndex(APUNTESINSUMOS_COLUMN_FECHAAPUNTE)));
            obj.setIdRecurso( res.getInt(res.getColumnIndex(APUNTESINSUMOS_COLUMN_IDRECURSO)));
            obj.setIdOS(res.getInt(res.getColumnIndex(APUNTESINSUMOS_COLUMN_IDOS)));
            obj.setObservacion(res.getString( res.getColumnIndex(APUNTESINSUMOS_COLUMN_OBSERVACION)));
            obj.setCantidad(res.getFloat(res.getColumnIndex(APUNTESINSUMOS_COLUMN_CANTIDAD)));
        }
        return obj;

    }


    public Haciendas getSingleHacienda(int id){
        Haciendas obj = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from Haciendas where IdHacienda="+id+"", null );
        System.out.print(res);
        if(res.isAfterLast() == false) {
            obj = new Haciendas();
            obj.setIdHacienda(id);
            res.moveToFirst();
            String ds = res.getString(res.getColumnIndex(HACIENDAS_COLUMN_DSHACIENDA));

            obj.setBkHacienda(res.getString(res.getColumnIndex(HACIENDAS_COLUMN_BKHACIENDA)));
            obj.setDsHacienda(ds);
            obj.setIdPluvio(0);
            obj.setMarcaBiometrico(res.getInt(res.getColumnIndex(HACIENDAS_COLUMN_MARCABIOMETRICO))>0);
        }
        return obj;

    }

    public Haciendas getSingleHaciendabyBK(String id){
        Haciendas obj = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from Haciendas where BkHacienda="+id+"", null );
        System.out.print(res);
        if(res.isAfterLast() == false) {
            obj = new Haciendas();

            res.moveToFirst();
            String ds = res.getString(res.getColumnIndex(HACIENDAS_COLUMN_DSHACIENDA));
            obj.setIdHacienda(res.getInt(res.getColumnIndex(HACIENDAS_COLUMN_IDHACIENDA)));
            obj.setBkHacienda(id);
            obj.setDsHacienda(ds);
            obj.setIdPluvio(0);
            obj.setMarcaBiometrico(res.getInt(res.getColumnIndex(HACIENDAS_COLUMN_MARCABIOMETRICO)) > 0);
        }
        return obj;

    }


    public Cuadrillas getSingleCuadrilla(int id){
        Cuadrillas obj = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from Cuadrillas where IdCuadrilla="+id+"", null );
        System.out.print(res);
        if(res.isAfterLast() == false) {
            obj = new Cuadrillas();
            obj.setIdCuadrilla(id);
            res.moveToFirst();
            String ds = res.getString(res.getColumnIndex(CUADRILLAS_COLUMN_DSCUADRILLA));

            obj.setBkCuadrilla(res.getString(res.getColumnIndex(CUADRILLAS_COLUMN_BKCUADRILLA)));
            obj.setDsCuadrilla(ds);
        }
        return obj;

    }

    public Cuadrillas getSingleCuadrillabyBK(String id){
        Cuadrillas obj = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from Cuadrillas where BkCuadrilla="+id+"", null );
        System.out.print(res);
        if(res.isAfterLast() == false) {
            obj = new Cuadrillas();

            res.moveToFirst();
            String ds = res.getString(res.getColumnIndex(CUADRILLAS_COLUMN_DSCUADRILLA));
            obj.setIdCuadrilla(res.getInt(res.getColumnIndex(CUADRILLAS_COLUMN_IDCUADRILLA)));
            obj.setBkCuadrilla(id);
            obj.setDsCuadrilla(ds);
        }
        return obj;

    }

    public Boolean PropietarioIngenio(String Cod){
        Equipos obj = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from "+EQUIPOS_TABLE_NAME+" where trim("+EQUIPOS_COLUMN_CODEQUIPO+")='"+Cod+"' limit 1", null );
        System.out.print(res);
        obj = new Equipos();
        obj.setPropEquipo("");
        if(res.isAfterLast() == false) {

            res.moveToFirst();
            obj.setPropEquipo(res.getString(res.getColumnIndex(EQUIPOS_COLUMN_PROPEQUIPO)));;
        }
        return obj.getPropEquipo().toLowerCase().equals("ip1000") || obj.getPropEquipo().toLowerCase().equals("ip2000");

    }

    public Boolean PropietarioCASSA(String Cod){
        Equipos obj = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from "+EQUIPOS_TABLE_NAME+" where trim("+EQUIPOS_COLUMN_CODEQUIPO+")='"+Cod+"' limit 1", null );
        System.out.print(res);
        obj = new Equipos();
        obj.setPropEquipo("");
        if(res.isAfterLast() == false) {

            res.moveToFirst();
            obj.setPropEquipo(res.getString(res.getColumnIndex(EQUIPOS_COLUMN_PROPEQUIPO)));;
        }
        return obj.getPropEquipo().toLowerCase().equals("ip1000") || obj.getPropEquipo().toLowerCase().equals("ip2000") || obj.getPropEquipo().toLowerCase().equals("ip3000");

    }

    public Equipos getSingleEquipoByCod(String Cod){
        Equipos obj = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from "+EQUIPOS_TABLE_NAME+" where trim("+EQUIPOS_COLUMN_CODEQUIPO+")='"+Cod+"' limit 1", null );
        System.out.print(res);
        if(res.isAfterLast() == false) {
            obj = new Equipos();

            res.moveToFirst();
            String ds = res.getString(res.getColumnIndex(EQUIPOS_COLUMN_NOMBREEQUIPO));
            obj.setIdEquipo(res.getInt(res.getColumnIndex(EQUIPOS_COLUMN_IDEQUIPO)));
            obj.setPropEquipo(res.getString(res.getColumnIndex(EQUIPOS_COLUMN_PROPEQUIPO)));
            obj.setId_SubFlota(res.getInt(res.getColumnIndex(EQUIPOS_COLUMN_IDSUBFLOTA)));
            obj.setCodEquipo(Cod);
            obj.setNomEquipo(ds);
        }
        return obj;

    }

    public Equipos getSingleEquipoById(int Id){
        Equipos obj = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from "+EQUIPOS_TABLE_NAME+" where "+EQUIPOS_COLUMN_IDEQUIPO+"="+Id+" limit 1", null );
        System.out.print(res);
        if(res.isAfterLast() == false) {
            obj = new Equipos();

            res.moveToFirst();
            String ds = res.getString(res.getColumnIndex(EQUIPOS_COLUMN_NOMBREEQUIPO));
            obj.setCodEquipo(res.getString(res.getColumnIndex(EQUIPOS_COLUMN_CODEQUIPO)));
            obj.setPropEquipo(res.getString(res.getColumnIndex(EQUIPOS_COLUMN_PROPEQUIPO)));
            obj.setId_SubFlota(res.getInt(res.getColumnIndex(EQUIPOS_COLUMN_IDSUBFLOTA)));
            obj.setIdEquipo(Id);
            obj.setNomEquipo(ds);
        }
        return obj;

    }

    public ArrayList<Equipos> ConsultaEquipos(String Cod){
        Equipos o = new Equipos();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from "+EQUIPOS_TABLE_NAME+" where trim("+EQUIPOS_COLUMN_CODEQUIPO+") like '%"+Cod+"%' or "+EQUIPOS_COLUMN_NOMBREEQUIPO+" LIKE '%"+Cod+"%' limit 20", null );
        res.moveToFirst();
        ArrayList<Equipos> array_list = new ArrayList<Equipos>();

        String soc = getSociedad();

        while(res.isAfterLast() == false){
            int IdEquipo = Integer.parseInt( res.getString(res.getColumnIndex(EQUIPOS_COLUMN_IDEQUIPO)));
            String CodEquipo = res.getString(res.getColumnIndex(EQUIPOS_COLUMN_CODEQUIPO));
            String NomEquipo = res.getString(res.getColumnIndex(EQUIPOS_COLUMN_NOMBREEQUIPO));
            String PropEquipo = res.getString(res.getColumnIndex(EQUIPOS_COLUMN_PROPEQUIPO));
            int IdSubFlota = res.getInt(res.getColumnIndex(EQUIPOS_COLUMN_IDSUBFLOTA));
            o = new Equipos(IdEquipo,CodEquipo, NomEquipo, PropEquipo, IdSubFlota);


            //if (!soc.equals("3000")){
                array_list.add(o);
            /*}
            else {
                if (!PropietarioIngenio(CodEquipo)){
                    array_list.add(o);
                }
            }*/

            res.moveToNext();
        }
        return array_list;

    }

    public ArrayList<Equipos> ConsultaEquiposSubFlota(int IdRecurso){
        Equipos o = new Equipos();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select A.* from "+EQUIPOS_TABLE_NAME+" as A " +
                "INNER JOIN "+RECURSOSSUBFLOTAS_TABLE_NAME+" as b on a."+EQUIPOS_COLUMN_IDSUBFLOTA+" = b."+RECURSOSSUBFLOTAS_COLUMN_IDSUBFLOTA+" where "+RECURSOSSUBFLOTAS_COLUMN_IDRECURSO+" = "+IdRecurso, null );
        res.moveToFirst();
        ArrayList<Equipos> array_list = new ArrayList<Equipos>();

        String soc = getSociedad();

        while(res.isAfterLast() == false){
            int IdEquipo = Integer.parseInt( res.getString(res.getColumnIndex(EQUIPOS_COLUMN_IDEQUIPO)));
            String CodEquipo = res.getString(res.getColumnIndex(EQUIPOS_COLUMN_CODEQUIPO));
            String NomEquipo = res.getString(res.getColumnIndex(EQUIPOS_COLUMN_NOMBREEQUIPO));
            String PropEquipo = res.getString(res.getColumnIndex(EQUIPOS_COLUMN_PROPEQUIPO));
            int IdSubFlota = Integer.parseInt( res.getString(res.getColumnIndex(EQUIPOS_COLUMN_IDSUBFLOTA)));
            o = new Equipos(IdEquipo,CodEquipo, NomEquipo, PropEquipo, IdSubFlota);


            //if (!soc.equals("3000")){
            array_list.add(o);
            /*}
            else {
                if (!PropietarioIngenio(CodEquipo)){
                    array_list.add(o);
                }
            }*/

            res.moveToNext();
        }
        return array_list;

    }




    public TiposDocumento getSingleTipoDocumento(int Id){
        TiposDocumento obj = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from TiposDocumentoInterno where IdTipDocInterno="+Id+" limit 1", null );
        System.out.print(res);
        if(res.isAfterLast() == false) {
            obj = new TiposDocumento();

            res.moveToFirst();
            String ds = res.getString(res.getColumnIndex("DsTipDocInterno"));
            obj.setBkTipDocInterno(res.getString(res.getColumnIndex("BkTipDocInterno")));
            obj.setIdTipDocInterno(Id);
            obj.setDsTipDocInteno(ds);
        }
        return obj;

    }

    public float getAreaApuntadaIns(int IdOS, int IdRecurso){
        float obj = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select ap."+APUNTESINSUMOS_COLUMN_IDRECURSO+", SUM(ap."+APUNTESINSUMOS_COLUMN_AREA+") as "+APUNTESINSUMOS_COLUMN_AREA+" from "+APUNTESINSUMOS_TABLE_NAME+" ap where IdOS="+IdOS+" and "+APUNTESINSUMOS_COLUMN_IDRECURSO+" = "+IdRecurso+" group by "+APUNTESINSUMOS_COLUMN_IDRECURSO , null );
        System.out.print(res);
        if(res.isAfterLast() == false) {
            res.moveToFirst();

            obj = (res.getFloat(res.getColumnIndex(APUNTESINSUMOS_COLUMN_AREA)));

        }
        return obj;
    }

    public float getAreaApuntada(int IdOS){
        float obj = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select SUM(Area) as Area from ApuntesArea where IdOS="+IdOS , null );
        System.out.print(res);
        if(res.isAfterLast() == false) {
            res.moveToFirst();

            obj = (res.getFloat(res.getColumnIndex("Area")));

        }
        return obj;
    }
    public float getAreaApuntadaMO(int IdOS, int IdRecurso){
        float obj = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "select "+APUNTESMANOOBRA_COLUMN_IDRECURSO+", sum("+APUNTESINSUMOS_COLUMN_AREA+") as "+APUNTESINSUMOS_COLUMN_AREA+" from (" +
                "select "+APUNTESMANOOBRA_COLUMN_FECHAAPUNTE+",ap."+APUNTESMANOOBRA_COLUMN_IDRECURSO+", max(ap."+APUNTESMANOOBRA_COLUMN_AREA+") as "+ APUNTESMANOOBRA_COLUMN_AREA +
                " from "+APUNTESMANOOBRA_TABLE_NAME+" ap " +
                " inner join "+TRABAJADORES_TABLE_NAME+" t ON AP."+APUNTESMANOOBRA_COLUMN_IDTRABAJADOR+" = T."+TRABAJADORES_COLUMN_IDTRABAJADOR +
                " where "+ APUNTESINSUMOS_COLUMN_IDOS +" = " + IdOS +
                " and "+APUNTESMANOOBRA_COLUMN_IDRECURSO+" = " + IdRecurso +
                " group by "+APUNTESMANOOBRA_COLUMN_FECHAAPUNTE+","+APUNTESMANOOBRA_COLUMN_IDRECURSO +", T."+TRABAJADORES_COLUMN_IDCUADRILLA+
                ") a" +
                " group by "+APUNTESMANOOBRA_COLUMN_IDRECURSO;
        /*Cursor res =  db.rawQuery( "select ap."+APUNTESMANOOBRA_COLUMN_IDRECURSO+", SUM(ap."+APUNTESMANOOBRA_COLUMN_AREA+") as "+APUNTESMANOOBRA_COLUMN_AREA+" " +
                "from "+APUNTESMANOOBRA_TABLE_NAME+" ap where IdOS="+IdOS+" and "+APUNTESMANOOBRA_COLUMN_IDRECURSO+" = "+IdRecurso+" group by "+APUNTESMANOOBRA_COLUMN_IDRECURSO , null );
        System.out.print(res);*/
        Cursor res = db.rawQuery(query,null);
        if(res.isAfterLast() == false) {
            res.moveToFirst();

            obj = (res.getFloat(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_AREA)));

        }
        return obj;
    }

    public float getAreaApuntadaMOFecha(int IdOS, int IdRecurso, String Fecha, int IdCuadrilla){
        float obj = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        String query = //"select "+APUNTESMANOOBRA_COLUMN_IDRECURSO+", sum("+APUNTESINSUMOS_COLUMN_AREA+") as "+APUNTESINSUMOS_COLUMN_AREA+" from (" +
                "select "+APUNTESMANOOBRA_COLUMN_FECHAAPUNTE+",ap."+APUNTESMANOOBRA_COLUMN_IDRECURSO+", max(ap."+APUNTESMANOOBRA_COLUMN_AREA+") as "+ APUNTESMANOOBRA_COLUMN_AREA +
                " from "+APUNTESMANOOBRA_TABLE_NAME+" ap " +
                " inner join "+TRABAJADORES_TABLE_NAME+" t ON AP."+APUNTESMANOOBRA_COLUMN_IDTRABAJADOR+" = T."+TRABAJADORES_COLUMN_IDTRABAJADOR + " and T."+TRABAJADORES_COLUMN_IDCUADRILLA +" = "+IdCuadrilla+
                " where "+ APUNTESINSUMOS_COLUMN_IDOS +" = " + IdOS +
                " and "+APUNTESMANOOBRA_COLUMN_IDRECURSO+" = " + IdRecurso +
                " and "+APUNTESMANOOBRA_COLUMN_FECHAAPUNTE+ " = '"+Fecha+"'"+
                " group by "+APUNTESMANOOBRA_COLUMN_FECHAAPUNTE+","+APUNTESMANOOBRA_COLUMN_IDRECURSO +", T."+TRABAJADORES_COLUMN_IDCUADRILLA;
                //") a" +
                //" group by "+APUNTESMANOOBRA_COLUMN_IDRECURSO;
        /*Cursor res =  db.rawQuery( "select ap."+APUNTESMANOOBRA_COLUMN_IDRECURSO+", SUM(ap."+APUNTESMANOOBRA_COLUMN_AREA+") as "+APUNTESMANOOBRA_COLUMN_AREA+" " +
                "from "+APUNTESMANOOBRA_TABLE_NAME+" ap where IdOS="+IdOS+" and "+APUNTESMANOOBRA_COLUMN_IDRECURSO+" = "+IdRecurso+" group by "+APUNTESMANOOBRA_COLUMN_IDRECURSO , null );
        System.out.print(res);*/
        Cursor res = db.rawQuery(query,null);
        if(res.isAfterLast() == false) {
            res.moveToFirst();

            obj = (res.getFloat(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_AREA)));

        }
        return obj;
    }

    public float getCantApuntadaMOTrab(int IdOS, int IdRecurso, String fecha, int IdTrabajador){
        float obj = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select ap."+APUNTESMANOOBRA_COLUMN_IDRECURSO+", SUM(ap."+APUNTESMANOOBRA_COLUMN_QTD_PAGO+") as "+APUNTESMANOOBRA_COLUMN_AREA
                +" from "+APUNTESMANOOBRA_TABLE_NAME+" ap " +
                "where IdOS="+IdOS+" and "+APUNTESMANOOBRA_COLUMN_IDRECURSO+" = "+IdRecurso
                +" and "+APUNTESMANOOBRA_COLUMN_IDTRABAJADOR +" = " + IdTrabajador
                + " and "+APUNTESMANOOBRA_COLUMN_FECHAAPUNTE +" = '" + fecha+"'"
                +" group by "+APUNTESMANOOBRA_COLUMN_IDRECURSO , null );
        System.out.print(res);
        if(res.isAfterLast() == false) {
            res.moveToFirst();

            obj = (res.getFloat(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_AREA)));

        }
        return obj;
    }
    /*
    public ArrayList<OSRecursos> getPendiente(int IdOS){

    }*/

/*
    public int numberOfRows(){
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, CONTACTS_TABLE_NAME);
        return numRows;
    }*/


    public boolean updateApunte (int IdHacienda, String Fecha, float mmLluvia, int IdCondMet, String Longitud, String Latitud) {
        SQLiteDatabase db = this.getWritableDatabase();
        /*ContentValues contentValues = new ContentValues();
        contentValues.put("name", name);
        contentValues.put("phone", phone);
        contentValues.put("email", email);
        contentValues.put("street", street);
        contentValues.put("place", place);
        db.update("contacts", contentValues, "id = ? ", new String[] { Integer.toString(id) } );*/
        String strSQL = "UPDATE Apuntamientos SET Enviado="+0+", IdCondMet= "+IdCondMet+", mmLluvia = "+mmLluvia+", Longitud='"+Longitud+"', Latitud='"+Latitud+"' WHERE IdHacienda = "+ IdHacienda+" AND Fecha like '"+Fecha+"%'";

        db.execSQL(strSQL);

        return true;
    }


    public boolean updateApunteInsumos (int IdApunteInsumos, String FechaApunte, float Area, float Dosis, float Cantidad, String FechaDigita, String Observacion, String FechaIni, String FechaFin, float Latitud, float Longitud) {
        SQLiteDatabase db = this.getWritableDatabase();
        String strSQL = "UPDATE "+APUNTESINSUMOS_TABLE_NAME+" SET "+APUNTESINSUMOS_COLUMN_FECHAAPUNTE+"='"+FechaApunte+"', "+APUNTESINSUMOS_COLUMN_CANTIDAD+"="+Cantidad+", "+APUNTESINSUMOS_COLUMN_AREA+" = "+Area+", "+APUNTESINSUMOS_COLUMN_DOSIS+"="+Dosis+", "+APUNTESINSUMOS_COLUMN_FECHADIGITA+"='"+FechaDigita+"',"+APUNTESINSUMOS_COLUMN_FECHAFIN+"='"+FechaFin+"',"+APUNTESINSUMOS_COLUMN_OBSERVACION+"='"+Observacion+"',"+APUNTESINSUMOS_COLUMN_FECHAINI+"='"+FechaIni+"', Longitud="+Longitud+", Latitud="+Latitud+" WHERE "+APUNTESINSUMOS_COLUMN_IDAPINSUMOS+" = "+ IdApunteInsumos;

        db.execSQL(strSQL);

        return true;
    }

    public boolean updateApunteMO (int IdApMo, String FechaApunte, float Area, float Cantidad,String Observacion,String FechaDigita, String FechaIni, String FechaFin, float Latitud, float Longitud) {
        SQLiteDatabase db = this.getWritableDatabase();
        String strSQL = "UPDATE "+APUNTESMANOOBRA_TABLE_NAME+" SET "+APUNTESMANOOBRA_COLUMN_FECHAAPUNTE+"='"+FechaApunte+"', "+APUNTESMANOOBRA_COLUMN_QTD_PAGO+"="+Cantidad+", "+APUNTESMANOOBRA_COLUMN_AREA+" = "+Area+", "+APUNTESMANOOBRA_COLUMN_FECHADIGITA+"='"+FechaDigita+"',"+APUNTESMANOOBRA_COLUMN_FECHAFIN+"='"+FechaFin+"',"+APUNTESMANOOBRA_COLUMN_OBSERVACION+"='"+Observacion+"',"+APUNTESMANOOBRA_COLUMN_FECHAINI+"='"+FechaIni+"', Longitud="+Longitud+", Latitud="+Latitud+" WHERE "+APUNTESMANOOBRA_COLUMN_IDAPMO+" = "+ IdApMo;

        db.execSQL(strSQL);

        return true;
    }

    public boolean updateApunteMaq (int IdApMq, String FechaApunte, int IdTrabajador,int IdTrabAyudante, String CodEquipo,String CodEquipoImplemento, float Area
            , float HOR_INI, float HOR_FIN, int HOR_FALLA, float ODO_INI, float ODO_FIN, int ODO_FALLA,float MED_INI,float MED_FIN, int MED_FALLA, float QTD_PAGO
            , int Eq1, int Eq2, int Eq3, int Eq4
            , int IdTipDocInterno, String NumDocInterno
            , String Origen, String Destino, String Ruta
            , String Usuario, String FechaDigita, String Observacion, String FechaIni, String FechaFin, float Latitud, float Longitud) {
        SQLiteDatabase db = this.getWritableDatabase();
        String strSQL = "UPDATE "+APUNTESMAQUINARIA_TABLE_NAME+" SET "+APUNTESMAQUINARIA_COLUMN_FECHAAPUNTE+" = '"+FechaApunte+"', "+APUNTESMAQUINARIA_COLUMN_IDTRABAJADOR+"="+IdTrabajador+", "+APUNTESMAQUINARIA_COLUMN_IDTRABAYUDANTE+"= "+IdTrabAyudante+", "+APUNTESMAQUINARIA_COLUMN_CODEQUIPO+"='"+CodEquipo+"', "+APUNTESMAQUINARIA_COLUMN_CODEQUIPOIMPLEMENTO+"='"+CodEquipoImplemento+"', "+APUNTESMANOOBRA_COLUMN_AREA+" = "+Area+", "+APUNTESMAQUINARIA_COLUMN_HOR_INI+"="+HOR_INI+", "+APUNTESMAQUINARIA_COLUMN_HOR_FIN+"="+HOR_FIN+", "+APUNTESMAQUINARIA_COLUMN_HOR_FALLA+"="+HOR_FALLA+", "+APUNTESMAQUINARIA_COLUMN_ODO_INI+"="+ODO_INI+", "+APUNTESMAQUINARIA_COLUMN_ODO_FIN+"="+ODO_FIN +
                ", "+APUNTESMAQUINARIA_COLUMN_ODO_FALLA+"="+ODO_FALLA+", "+APUNTESMAQUINARIA_COLUMN_MED_INI+"="+MED_INI+", "+APUNTESMAQUINARIA_COLUMN_MED_FIN+"="+MED_FIN+", "+APUNTESMAQUINARIA_COLUMN_MED_FALLA+"="+MED_FALLA+", "+APUNTESMAQUINARIA_COLUMN_QTD_PAGO+"="+QTD_PAGO+
                ", "+APUNTESMANOOBRA_COLUMN_FECHADIGITA+"='"+FechaDigita+"',"+APUNTESMANOOBRA_COLUMN_FECHAFIN+"='"+FechaFin+"',"+APUNTESMANOOBRA_COLUMN_OBSERVACION+"='"+Observacion+"',"+APUNTESMANOOBRA_COLUMN_FECHAINI+"='"+FechaIni+"', Longitud="+Longitud+", Latitud="+Latitud+
                ", Equipo1 = "+Eq1+", Equipo2 = "+Eq2+", Equipo3 = "+Eq3+", Equipo4 = "+Eq4+
                ", Origen = '"+Origen+"'"+", Destino = '"+Destino+"'"+", Ruta = '"+Ruta+"'"+
                ", IdTipDocInterno = "+IdTipDocInterno+", NumDocInterno = '"+NumDocInterno+"'"+
                " WHERE "+APUNTESMAQUINARIA_COLUMN_IDAPMAQUINARIA+" = "+ IdApMq;

        db.execSQL(strSQL);

        return true;
    }

    public boolean updateOSRecurso (OSRecursos osRecursos) {
        SQLiteDatabase db = this.getWritableDatabase();
        String strSQL = "UPDATE "+OSRecursos_TABLE_NAME+" SET "+OSRecursos_COLUMN_CANTIDADAPUNT+"="+osRecursos.getCantidadApunt()+",AreaApunt="+osRecursos.getAreaApunt()+" WHERE "+OSRecursos_COLUMN_IDOS+"="+osRecursos.getIdOS()+" AND "+OSRecursos_COLUMN_IDRECURSO+"="+osRecursos.getIdRecurso();

        db.execSQL(strSQL);

        return true;
    }



    public boolean updateParametro (int id, String Valor ) {
        SQLiteDatabase db = this.getWritableDatabase();
        /*ContentValues contentValues = new ContentValues();
        contentValues.put("name", name);
        contentValues.put("phone", phone);
        contentValues.put("email", email);
        contentValues.put("street", street);
        contentValues.put("place", place);
        db.update("contacts", contentValues, "id = ? ", new String[] { Integer.toString(id) } );*/
        String strSQL = "UPDATE Parametros SET Valor = '"+Valor+"' WHERE IdParametro = "+ id;

        db.execSQL(strSQL);

        return true;
    }
    public boolean updateCuadrilla (int id, int idCuadrilla ) {
        SQLiteDatabase db = this.getWritableDatabase();
        /*ContentValues contentValues = new ContentValues();
        contentValues.put("name", name);
        contentValues.put("phone", phone);
        contentValues.put("email", email);
        contentValues.put("street", street);
        contentValues.put("place", place);
        db.update("contacts", contentValues, "id = ? ", new String[] { Integer.toString(id) } );*/
        String strSQL = "UPDATE "+TRABAJADORES_TABLE_NAME+" SET "+TRABAJADORES_COLUMN_IDCUADRILLA+" = '"+idCuadrilla+"' WHERE "+TRABAJADORES_COLUMN_IDTRABAJADOR+" = "+ id;

        db.execSQL(strSQL);

        return true;
    }

    public boolean updateApunteArea (int id, float Area ) {
        SQLiteDatabase db = this.getWritableDatabase();
        /*ContentValues contentValues = new ContentValues();
        contentValues.put("name", name);
        contentValues.put("phone", phone);
        contentValues.put("email", email);
        contentValues.put("street", street);
        contentValues.put("place", place);
        db.update("contacts", contentValues, "id = ? ", new String[] { Integer.toString(id) } );*/
        String strSQL = "UPDATE ApuntesArea SET Area = "+Area+", enviado = 0 WHERE IdApArea = "+ id;

        db.execSQL(strSQL);

        return true;
    }

    public Integer deleteAllApunteArea() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(

                "delete from ApuntesArea"
        );
        return 1;
    }

    public Integer deleteApunteArea(int Id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("ApuntesArea",
                " IdApArea = ? ",
                new String[] { String.valueOf(Id) });
    }

    public Integer deleteApunteMO(int Id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(APUNTESMANOOBRA_TABLE_NAME,
                APUNTESMANOOBRA_COLUMN_IDAPMO+" = ? ",
                new String[] { String.valueOf(Id) });
    }

    public Integer deleteApunteInsumos(int Id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(APUNTESINSUMOS_TABLE_NAME,
                APUNTESINSUMOS_COLUMN_IDAPINSUMOS+" = ? ",
                new String[] { String.valueOf(Id) });
    }

    public Integer deleteApunteMaquinaria(int Id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(APUNTESMAQUINARIA_TABLE_NAME,
                APUNTESMAQUINARIA_COLUMN_IDAPMAQUINARIA+" = ? ",
                new String[] { String.valueOf(Id) });
    }

    public Integer deleteAplicacion () {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(

                "delete from Aplicaciones"
        );
        return 1;
    }

    public Integer deleteEquipos () {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(

                "delete from "+EQUIPOS_TABLE_NAME
        );
        return 1;
    }

    public Integer deleteRecursosSubflotas () {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(

                "delete from "+RECURSOSSUBFLOTAS_TABLE_NAME
        );
        return 1;
    }

    public Integer deleteEmpresas () {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(

                "delete from "+EMPRESAS_TABLE_NAME
        );
        return 1;
    }

    public Integer deleteParametro (int Id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("Parametros",
                "IdParametro = ? ",
                new String[] { Id+"" });
    }

    public Integer deleteHacienda (String bk) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("Haciendas",
                "BkHacienda = ? ",
                new String[] { bk });
    }

    public void deleteAlldocss () {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from TiposDocumentoInterno");

    }
    public void deleteAllHaciendas () {
        SQLiteDatabase db = this.getWritableDatabase();
         db.execSQL("delete from Haciendas");

    }
    public Integer deleteOS (int Id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("OS",
                "IdOS = ? ",
                new String[] { Id+"" });
    }

    public String getUsr(){
        Parametros usrp = getSingleParametro(5);
        usr = "";
        if(usrp != null){
            usr = usrp.getValor();
        }
        return usr;
    }

    public ArrayList<ApuntesMO> getAllApuntesMO() {
        Parametros usrp = getSingleParametro(5);
        usr = "";
        if(usrp != null){
            usr = usrp.getValor();
        }
        ArrayList<ApuntesMO> array_list = new ArrayList<ApuntesMO>();
        ApuntesMO o = new ApuntesMO();
        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select ap.*,r."+OSRecursos_COLUMN_DSRECURSO+", t."+TRABAJADORES_COLUMN_NOMTRABAJADOR+", t."+TRABAJADORES_COLUMN_CODTRABAJADOR+"," +
                "r."+OSRecursos_COLUMN_UM+", c."+CUADRILLAS_COLUMN_DSCUADRILLA+" from "+APUNTESMANOOBRA_TABLE_NAME+" as ap " +
                "INNER JOIN "+OSRecursos_TABLE_NAME+" r ON ap.IdRecurso = r.IdRecurso and r.IdOS = ap.IdOS " +
                "INNER JOIN "+TRABAJADORES_TABLE_NAME+" t ON ap."+APUNTESMANOOBRA_COLUMN_IDTRABAJADOR+" = t."+TRABAJADORES_COLUMN_IDTRABAJADOR+" " +
                "INNER JOIN "+CUADRILLAS_TABLE_NAME+" c ON ap."+APUNTESMANOOBRA_IDCUADRILLA+" = c."+CUADRILLAS_COLUMN_IDCUADRILLA/*+"  where Usuario ='"+usr+"'"*/, null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            int IdApMo = Integer.parseInt( res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_IDAPMO)));
            int IdCuadrilla = Integer.parseInt( res.getString(res.getColumnIndex(APUNTESMANOOBRA_IDCUADRILLA)));
            int IdTrabajador = Integer.parseInt( res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_IDTRABAJADOR)));

            float QTD_PAGO =  ( res.getFloat(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_QTD_PAGO)));

            int IdOS = Integer.parseInt( res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_IDOS)));
            int IdRecurso = Integer.parseInt( res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_IDRECURSO)));
            float Area = res.getFloat(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_AREA));
            String Usuario = res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_USUARIO));
            String Fecha = res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_FECHAAPUNTE));
            String FechaDigita = res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_FECHADIGITA));
            String IdDev = res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_IDDEV));
            String Observacion = res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_OBSERVACION));
            String FechaIni = res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_FECHAINI));
            String FechaFin = res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_FECHAFIN));
            String Latitud = res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_LATITUD));
            String Longitud = res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_LONGITUD));

            String recurso = res.getString(res.getColumnIndex(OSRecursos_COLUMN_DSRECURSO));
            String trab = res.getString(res.getColumnIndex(TRABAJADORES_COLUMN_NOMTRABAJADOR));
            String cod = res.getString(res.getColumnIndex(TRABAJADORES_COLUMN_CODTRABAJADOR));
            String um = res.getString(res.getColumnIndex(OSRecursos_COLUMN_UM));
            String cuad = res.getString(res.getColumnIndex(CUADRILLAS_COLUMN_DSCUADRILLA));


            o = new ApuntesMO(IdOS,Fecha,IdRecurso,Area,Usuario,FechaDigita,IdDev,FechaIni,FechaFin,Latitud,Longitud, Observacion,IdApMo,IdCuadrilla,IdTrabajador,QTD_PAGO);
            o.setDsRecurso(recurso);
            o.setDsCuadrilla(cuad);
            o.setTrabajador(cod+" - "+trab);
            o.setUM(um);
            array_list.add(o);
            res.moveToNext();
        }
        res.close();
        return array_list;
    }
    public ArrayList<ApuntesMO> getApuntesMO() {
        Parametros usrp = getSingleParametro(5);
        usr = "";
        if(usrp != null){
            usr = usrp.getValor();
        }
        ArrayList<ApuntesMO> array_list = new ArrayList<ApuntesMO>();
        ApuntesMO o = new ApuntesMO();
        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select ap.*,r."+OSRecursos_COLUMN_DSRECURSO+", t."+TRABAJADORES_COLUMN_NOMTRABAJADOR+", t."+TRABAJADORES_COLUMN_CODTRABAJADOR+"," +
                "r."+OSRecursos_COLUMN_UM+", c."+CUADRILLAS_COLUMN_DSCUADRILLA+" from "+APUNTESMANOOBRA_TABLE_NAME+" as ap " +
                "INNER JOIN "+OSRecursos_TABLE_NAME+" r ON ap.IdRecurso = r.IdRecurso and r.IdOS = ap.IdOS " +
                "INNER JOIN "+TRABAJADORES_TABLE_NAME+" t ON ap."+APUNTESMANOOBRA_COLUMN_IDTRABAJADOR+" = t."+TRABAJADORES_COLUMN_IDTRABAJADOR+" " +
                "INNER JOIN "+CUADRILLAS_TABLE_NAME+" c ON ap."+APUNTESMANOOBRA_IDCUADRILLA+" = c."+CUADRILLAS_COLUMN_IDCUADRILLA+"  where Usuario ='"+usr+"'", null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            int IdApMo = Integer.parseInt( res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_IDAPMO)));
            int IdCuadrilla = Integer.parseInt( res.getString(res.getColumnIndex(APUNTESMANOOBRA_IDCUADRILLA)));
            int IdTrabajador = Integer.parseInt( res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_IDTRABAJADOR)));

            float QTD_PAGO =  ( res.getFloat(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_QTD_PAGO)));

            int IdOS = Integer.parseInt( res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_IDOS)));
            int IdRecurso = Integer.parseInt( res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_IDRECURSO)));
            float Area = res.getFloat(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_AREA));
            String Usuario = res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_USUARIO));
            String Fecha = res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_FECHAAPUNTE));
            String FechaDigita = res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_FECHADIGITA));
            String IdDev = res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_IDDEV));
            String Observacion = res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_OBSERVACION));
            String FechaIni = res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_FECHAINI));
            String FechaFin = res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_FECHAFIN));
            String Latitud = res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_LATITUD));
            String Longitud = res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_LONGITUD));

            String recurso = res.getString(res.getColumnIndex(OSRecursos_COLUMN_DSRECURSO));
            String trab = res.getString(res.getColumnIndex(TRABAJADORES_COLUMN_NOMTRABAJADOR));
            String cod = res.getString(res.getColumnIndex(TRABAJADORES_COLUMN_CODTRABAJADOR));
            String um = res.getString(res.getColumnIndex(OSRecursos_COLUMN_UM));
            String cuad = res.getString(res.getColumnIndex(CUADRILLAS_COLUMN_DSCUADRILLA));


            o = new ApuntesMO(IdOS,Fecha,IdRecurso,Area,Usuario,FechaDigita,IdDev,FechaIni,FechaFin,Latitud,Longitud, Observacion,IdApMo,IdCuadrilla,IdTrabajador,QTD_PAGO);
            o.setDsRecurso(recurso);
            o.setDsCuadrilla(cuad);
            o.setTrabajador(cod+" - "+trab);
            o.setUM(um);
            array_list.add(o);
            res.moveToNext();
        }
        res.close();
        return array_list;
    }
    public ArrayList<ApuntesMO> getApuntesMOByOS(int IdOS) {
        Parametros usrp = getSingleParametro(5);
        usr = "";
        if(usrp != null){
            usr = usrp.getValor();
        }
        ArrayList<ApuntesMO> array_list = new ArrayList<ApuntesMO>();
        ApuntesMO o = new ApuntesMO();
        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select ap.*, osr."+OSRecursos_COLUMN_DSRECURSO+", osr."+OSRecursos_COLUMN_UM+", c."+CUADRILLAS_COLUMN_BKCUADRILLA+" || ' - '||c."+CUADRILLAS_COLUMN_DSCUADRILLA+" as Cuadrilla, tr."+TRABAJADORES_COLUMN_CODTRABAJADOR+"||' - '||"+TRABAJADORES_COLUMN_NOMTRABAJADOR+" as Trabajador from "+APUNTESMANOOBRA_TABLE_NAME+" ap inner join "+OSRecursos_TABLE_NAME+" osr on ap."+APUNTESMANOOBRA_COLUMN_IDOS+" = osr."+OSRecursos_COLUMN_IDOS+" and osr."+OSRecursos_COLUMN_IDRECURSO+"=ap."+APUNTESMANOOBRA_COLUMN_IDRECURSO+" inner join "+CUADRILLAS_TABLE_NAME+" c on ap."+APUNTESMANOOBRA_IDCUADRILLA+" = c."+CUADRILLAS_COLUMN_IDCUADRILLA+" inner join "+TRABAJADORES_TABLE_NAME+" tr on ap."+APUNTESMANOOBRA_COLUMN_IDTRABAJADOR+"  = tr."+TRABAJADORES_COLUMN_IDTRABAJADOR+" where ap.IdOS = "+IdOS+" and ap.Usuario ='"+usr+"'", null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            int IdApMo = Integer.parseInt( res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_IDAPMO)));
            int IdCuadrilla = Integer.parseInt( res.getString(res.getColumnIndex(APUNTESMANOOBRA_IDCUADRILLA)));
            int IdTrabajador = Integer.parseInt( res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_IDTRABAJADOR)));

            float QTD_PAGO =  ( res.getFloat(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_QTD_PAGO)));

            String Fecha = ( res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_FECHAAPUNTE)));
            int IdRecurso = Integer.parseInt( res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_IDRECURSO)));
            float Area = res.getFloat(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_AREA));
            String Usuario = res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_USUARIO));
            String FechaDigita = res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_FECHADIGITA));
            String IdDev = res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_IDDEV));
            String Observacion = res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_OBSERVACION));
            String FechaIni = res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_FECHAINI));
            String FechaFin = res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_FECHAFIN));
            String Latitud = res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_LATITUD));
            String Longitud = res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_LONGITUD));

            String DsRecurso = res.getString(res.getColumnIndex(OSRecursos_COLUMN_DSRECURSO));
            String DsCuadrilla = res.getString(res.getColumnIndex("Cuadrilla"));
            String Trabajador = res.getString(res.getColumnIndex("Trabajador"));
            String UM = res.getString(res.getColumnIndex(OSRecursos_COLUMN_UM));

            o = new ApuntesMO(IdOS,Fecha,IdRecurso,Area,Usuario,FechaDigita,IdDev,FechaIni,FechaFin,Latitud,Longitud,Observacion,IdApMo,IdCuadrilla,IdTrabajador,QTD_PAGO);
            o.setTrabajador(Trabajador);
            o.setDsRecurso(DsRecurso);
            o.setDsCuadrilla(DsCuadrilla);
            o.setUM(UM);
            array_list.add(o);
            res.moveToNext();
        }
        res.close();
        return array_list;
    }
    public ArrayList<ApuntesMO> getApuntesMOByFecha(String Fecha) {
        Parametros usrp = getSingleParametro(5);
        usr = "";
        if(usrp != null){
            usr = usrp.getValor();
        }
        ArrayList<ApuntesMO> array_list = new ArrayList<ApuntesMO>();
        ApuntesMO o = new ApuntesMO();
        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from "+APUNTESMANOOBRA_TABLE_NAME+" where FechaApunte = "+Fecha+" and Usuario ="+usr, null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            int IdApMo = Integer.parseInt( res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_IDAPMO)));
            int IdCuadrilla = Integer.parseInt( res.getString(res.getColumnIndex(APUNTESMANOOBRA_IDCUADRILLA)));
            int IdTrabajador = Integer.parseInt( res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_IDTRABAJADOR)));

            float QTD_PAGO =  ( res.getFloat(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_QTD_PAGO)));

            int IdOS = Integer.parseInt( res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_IDOS)));
            int IdRecurso = Integer.parseInt( res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_IDRECURSO)));
            float Area = res.getFloat(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_AREA));
            String Usuario = res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_USUARIO));
            String FechaDigita = res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_FECHADIGITA));
            String IdDev = res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_IDDEV));
            String Observacion = res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_OBSERVACION));
            String FechaIni = res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_FECHAINI));
            String FechaFin = res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_FECHAFIN));
            String Latitud = res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_LATITUD));
            String Longitud = res.getString(res.getColumnIndex(APUNTESMANOOBRA_COLUMN_LONGITUD));


            o = new ApuntesMO(IdOS,Fecha,IdRecurso,Area,Usuario,FechaDigita,IdDev,FechaIni,FechaFin,Latitud,Longitud,Observacion,IdApMo,IdCuadrilla,IdTrabajador,QTD_PAGO);
            array_list.add(o);
            res.moveToNext();
        }
        res.close();
        return array_list;
    }

   private String queryMaq = "select a.*,  osr."+OSRecursos_COLUMN_DSRECURSO+", osr."+OSRecursos_COLUMN_UM+
           ", tr."+TRABAJADORES_COLUMN_CODTRABAJADOR+"||' - '|| tr."+TRABAJADORES_COLUMN_NOMTRABAJADOR+" as Trabajador " +
           ", ta."+TRABAJADORES_COLUMN_CODTRABAJADOR+" || ' - ' || ta."+TRABAJADORES_COLUMN_NOMTRABAJADOR+" as Ayudante " +
           ", trim(eq1."+EQUIPOS_COLUMN_CODEQUIPO+")|| ' - ' || trim(eq1."+EQUIPOS_COLUMN_NOMBREEQUIPO+") AS DsEquipo1"+
           ", trim(eq2."+EQUIPOS_COLUMN_CODEQUIPO+")|| ' - ' || trim(eq2."+EQUIPOS_COLUMN_NOMBREEQUIPO+") AS DsEquipo2"+
           ", trim(eq3."+EQUIPOS_COLUMN_CODEQUIPO+")|| ' - ' || trim(eq3."+EQUIPOS_COLUMN_NOMBREEQUIPO+") AS DsEquipo3"+
           ", trim(eq4."+EQUIPOS_COLUMN_CODEQUIPO+")|| ' - ' || trim(eq4."+EQUIPOS_COLUMN_NOMBREEQUIPO+") AS DsEquipo4"+
           " from "+APUNTESMAQUINARIA_TABLE_NAME+" a " +
           " LEFT JOIN "+TRABAJADORES_TABLE_NAME+" tr on a."+APUNTESMAQUINARIA_COLUMN_IDTRABAJADOR+" = tr."+TRABAJADORES_COLUMN_IDTRABAJADOR+" " +
           " INNER JOIN " +OSRecursos_TABLE_NAME+ " osr on osr."+OSRecursos_COLUMN_IDOS+"= a."+APUNTESMAQUINARIA_COLUMN_IDOS+" and osr."+OSRecursos_COLUMN_IDRECURSO+" = a."+APUNTESMAQUINARIA_COLUMN_IDRECURSO+
           " LEFT JOIN  " +EQUIPOS_TABLE_NAME+ " eq1 on a."+APUNTESMAQUINARIA_COLUMN_EQ1+"=eq1."+EQUIPOS_COLUMN_IDEQUIPO+
           " LEFT JOIN  " +EQUIPOS_TABLE_NAME+ " eq2 on a."+APUNTESMAQUINARIA_COLUMN_EQ2+"=eq2."+EQUIPOS_COLUMN_IDEQUIPO+
           " LEFT JOIN  " +EQUIPOS_TABLE_NAME+ " eq3 on a."+APUNTESMAQUINARIA_COLUMN_EQ3+"=eq3."+EQUIPOS_COLUMN_IDEQUIPO+
           " LEFT JOIN  " +EQUIPOS_TABLE_NAME+ " eq4 on a."+APUNTESMAQUINARIA_COLUMN_EQ4+"=eq4."+EQUIPOS_COLUMN_IDEQUIPO+
           " LEFT JOIN  " +TRABAJADORES_TABLE_NAME+" ta ON ta."+TRABAJADORES_COLUMN_IDTRABAJADOR+"=a."+APUNTESMAQUINARIA_COLUMN_IDTRABAYUDANTE;

    public ArrayList<ApuntesMaquinaria> getApuntesMaquinaria() {
        Parametros usrp = getSingleParametro(5);
        usr = "";
        if(usrp != null){
            usr = usrp.getValor();
        }
        ArrayList<ApuntesMaquinaria> array_list = new ArrayList<ApuntesMaquinaria>();
        ApuntesMaquinaria o = new ApuntesMaquinaria();
        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( queryMaq+

                " where a.Usuario ='"+usr+"'", null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            int IdApMaquinaria = Integer.parseInt( res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_IDAPMAQUINARIA)));
            String CodEquipo = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_CODEQUIPO));
            String CodEquipoImplemento = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_CODEQUIPOIMPLEMENTO));
            String trab = res.getString(res.getColumnIndex("Trabajador"));
            String rec = res.getString(res.getColumnIndex(OSRecursos_COLUMN_DSRECURSO));
            String um = res.getString(res.getColumnIndex(OSRecursos_COLUMN_UM));
            int IdTrabajador = Integer.parseInt( res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_IDTRABAJADOR)));
            int IdTrabAyudante = Integer.parseInt( res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_IDTRABAYUDANTE)));
            float HOR_INI =  ( res.getFloat(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_HOR_INI)));
            float HOR_FIN =  ( res.getFloat(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_HOR_FIN)));
            int HOR_FALLA =  res.getInt(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_HOR_FALLA));

            float ODO_INI =  ( res.getFloat(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_ODO_INI)));
            float ODO_FIN =  ( res.getFloat(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_ODO_FIN)));
            int ODO_FALLA =  res.getInt(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_ODO_FALLA));

            float MED_INI =  ( res.getFloat(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_MED_INI)));
            float MED_FIN =  ( res.getFloat(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_MED_FIN)));
            int MED_FALLA =  res.getInt(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_MED_FALLA));
            float QTD_PAGO =  ( res.getFloat(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_QTD_PAGO)));
            String Fecha = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_FECHAAPUNTE));
            int IdOS = Integer.parseInt( res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_IDOS)));
            int IdRecurso = Integer.parseInt( res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_IDRECURSO)));
            float Area = res.getFloat(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_AREA));
            String Usuario = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_USUARIO));
            String FechaDigita = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_FECHADIGITA));
            String IdDev = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_IDDEV));
            String Observacion = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_OBSERVACION));
            String FechaIni = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_FECHAINI));
            String FechaFin = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_FECHAFIN));
            String Latitud = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_LATITUD));
            String Longitud = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_LONGITUD));

            int IdTipDocInterno = res.getInt(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_IDTIPDOCINTERNO));
            String NumDocInt = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_NUMDOCINTERNO));
            int Eq1 = res.getInt(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_EQ1));
            int Eq2 = res.getInt(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_EQ2));
            int Eq3 = res.getInt(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_EQ3));
            int Eq4 = res.getInt(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_EQ4));
            String DsEq1 = res.getString(res.getColumnIndex("DsEquipo1"));
            String DsEq2 = res.getString(res.getColumnIndex("DsEquipo2"));
            String DsEq3 = res.getString(res.getColumnIndex("DsEquipo3"));
            String DsEq4 = res.getString(res.getColumnIndex("DsEquipo4"));
            String Origen = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_ORIGEN));
            String Destino = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_DESTINO));
            String Ruta = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_RUTA));


            String DsRecurso = res.getString(res.getColumnIndex("DsRecurso"));
            String Trabajador = res.getString(res.getColumnIndex("Trabajador"));
            String ayudante = res.getString(res.getColumnIndex("Ayudante"));
            String UM = res.getString(res.getColumnIndex(OSRecursos_COLUMN_UM));


            o = new ApuntesMaquinaria(IdOS,Fecha,IdRecurso,Area,Usuario,FechaDigita,IdDev,FechaIni,FechaFin,Latitud,Longitud,Observacion,IdApMaquinaria,IdTrabajador,IdTrabAyudante,CodEquipo,CodEquipoImplemento,HOR_INI,HOR_FIN,HOR_FALLA,ODO_INI,ODO_FIN,ODO_FALLA,MED_INI,MED_FIN,MED_FALLA,QTD_PAGO);
            o.setDsRecurso(DsRecurso);
            o.setTrabajador(Trabajador);
            o.setAyudante(ayudante);
            o.setUM(UM);

            o.setIdTipDocInterno(IdTipDocInterno);
            o.setNumDocInterno(NumDocInt);
            o.setEquipo1(Eq1);
            o.setEquipo2(Eq2);
            o.setEquipo3(Eq3);
            o.setEquipo4(Eq4);

            o.setEq1(DsEq1);
            o.setEq2(DsEq2);
            o.setEq3(DsEq3);
            o.setEq4(DsEq4);

            o.setOrigen(Origen);
            o.setDestino(Destino);
            o.setRuta(Ruta);

            array_list.add(o);
            res.moveToNext();
        }
        res.close();
        return array_list;
    }
    public ArrayList<ApuntesMaquinaria> getApuntesMaquinariaByOS(int IdOS, String tipo) {
        Parametros usrp = getSingleParametro(5);
        usr = "";
        if(usrp != null){
            usr = usrp.getValor();
        }
        ArrayList<ApuntesMaquinaria> array_list = new ArrayList<ApuntesMaquinaria>();
        ApuntesMaquinaria o = new ApuntesMaquinaria();
        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        String query = queryMaq+" where a.Usuario ='"+usr+"'"+" and a.IdOS = "+IdOS+" and osr."+OSRecursos_COLUMN_BKTIPRECURSO+" = '"+tipo+"' ";
        Cursor res =  db.rawQuery( query,null);// new String[]{String.valueOf(IdOS), String.valueOf(tipo)} );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            int IdApMaquinaria = Integer.parseInt( res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_IDAPMAQUINARIA)));
            String CodEquipo = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_CODEQUIPO));
            String CodEquipoImplemento = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_CODEQUIPOIMPLEMENTO));
            int IdTrabajador = Integer.parseInt( res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_IDTRABAJADOR)));
            int IdTrabAyudante = Integer.parseInt( res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_IDTRABAYUDANTE)));
            float HOR_INI =  ( res.getFloat(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_HOR_INI)));
            float HOR_FIN =  ( res.getFloat(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_HOR_FIN)));
            int HOR_FALLA =  res.getInt(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_HOR_FALLA));

            float ODO_INI =  ( res.getFloat(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_ODO_INI)));
            float ODO_FIN =  ( res.getFloat(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_ODO_FIN)));
            int ODO_FALLA =  res.getInt(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_ODO_FALLA));

            float MED_INI =  ( res.getFloat(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_MED_INI)));
            float MED_FIN =  ( res.getFloat(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_MED_FIN)));
            int MED_FALLA =  res.getInt(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_MED_FALLA));
            float QTD_PAGO =  ( res.getFloat(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_QTD_PAGO)));

            String Fecha = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_FECHAAPUNTE));
            int IdRecurso = Integer.parseInt( res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_IDRECURSO)));
            float Area = res.getFloat(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_AREA));
            String Usuario = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_USUARIO));
            String FechaDigita = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_FECHADIGITA));
            String IdDev = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_IDDEV));
            String Observacion = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_OBSERVACION));
            String FechaIni = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_FECHAINI));
            String FechaFin = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_FECHAFIN));
            String Latitud = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_LATITUD));
            String Longitud = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_LONGITUD));

            int IdTipDocInterno = res.getInt(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_IDTIPDOCINTERNO));
            String NumDocInt = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_NUMDOCINTERNO));
            int Eq1 = res.getInt(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_EQ1));
            int Eq2 = res.getInt(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_EQ2));
            int Eq3 = res.getInt(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_EQ3));
            int Eq4 = res.getInt(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_EQ4));
            String DsEq1 = res.getString(res.getColumnIndex("DsEquipo1"));
            String DsEq2 = res.getString(res.getColumnIndex("DsEquipo2"));
            String DsEq3 = res.getString(res.getColumnIndex("DsEquipo3"));
            String DsEq4 = res.getString(res.getColumnIndex("DsEquipo4"));
            String Origen = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_ORIGEN));
            String Destino = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_DESTINO));
            String Ruta = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_RUTA));


            String DsRecurso = res.getString(res.getColumnIndex("DsRecurso"));
            String Trabajador = res.getString(res.getColumnIndex("Trabajador"));
            String ayudante = res.getString(res.getColumnIndex("Ayudante"));
            String UM = res.getString(res.getColumnIndex(OSRecursos_COLUMN_UM));


            o = new ApuntesMaquinaria(IdOS,Fecha,IdRecurso,Area,Usuario,FechaDigita,IdDev,FechaIni,FechaFin,Latitud,Longitud,Observacion,IdApMaquinaria,IdTrabajador,IdTrabAyudante,CodEquipo,CodEquipoImplemento,HOR_INI,HOR_FIN,HOR_FALLA,ODO_INI,ODO_FIN,ODO_FALLA,MED_INI,MED_FIN,MED_FALLA,QTD_PAGO);
            o.setDsRecurso(DsRecurso);
            o.setTrabajador(Trabajador);
            o.setAyudante(ayudante);
            o.setUM(UM);

            o.setIdTipDocInterno(IdTipDocInterno);
            o.setNumDocInterno(NumDocInt);
            o.setEquipo1(Eq1);
            o.setEquipo2(Eq2);
            o.setEquipo3(Eq3);
            o.setEquipo4(Eq4);

            o.setEq1(DsEq1);
            o.setEq2(DsEq2);
            o.setEq3(DsEq3);
            o.setEq4(DsEq4);

            o.setOrigen(Origen);
            o.setDestino(Destino);
            o.setRuta(Ruta);

            array_list.add(o);
            res.moveToNext();
        }
        res.close();
        return array_list;
    }
    public ArrayList<ApuntesMaquinaria> getApuntesMaquinariaByOS(int IdOS) {
        Parametros usrp = getSingleParametro(5);
        usr = "";
        if(usrp != null){
            usr = usrp.getValor();
        }
        ArrayList<ApuntesMaquinaria> array_list = new ArrayList<ApuntesMaquinaria>();
        ApuntesMaquinaria o = new ApuntesMaquinaria();
        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        String query = queryMaq+
                " where a.Usuario ='"+usr+"' and a.IdOS = "+IdOS;
        Cursor res =  db.rawQuery( query,null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            int IdApMaquinaria = Integer.parseInt( res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_IDAPMAQUINARIA)));
            String CodEquipo = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_CODEQUIPO));
            String CodEquipoImplemento = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_CODEQUIPOIMPLEMENTO));
            int IdTrabajador = Integer.parseInt( res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_IDTRABAJADOR)));
            int IdTrabAyudante = Integer.parseInt( res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_IDTRABAYUDANTE)));
            float HOR_INI =  ( res.getFloat(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_HOR_INI)));
            float HOR_FIN =  ( res.getFloat(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_HOR_FIN)));
            int HOR_FALLA =  res.getInt(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_HOR_FALLA));

            float ODO_INI =  ( res.getFloat(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_ODO_INI)));
            float ODO_FIN =  ( res.getFloat(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_ODO_FIN)));
            int ODO_FALLA =  res.getInt(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_ODO_FALLA));

            float MED_INI =  ( res.getFloat(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_MED_INI)));
            float MED_FIN =  ( res.getFloat(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_MED_FIN)));
            int MED_FALLA =  res.getInt(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_MED_FALLA));
            float QTD_PAGO =  ( res.getFloat(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_QTD_PAGO)));

            String Fecha = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_FECHAAPUNTE));
            int IdRecurso = Integer.parseInt( res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_IDRECURSO)));
            float Area = res.getFloat(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_AREA));
            String Usuario = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_USUARIO));
            String FechaDigita = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_FECHADIGITA));
            String IdDev = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_IDDEV));
            String Observacion = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_OBSERVACION));
            String FechaIni = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_FECHAINI));
            String FechaFin = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_FECHAFIN));
            String Latitud = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_LATITUD));
            String Longitud = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_LONGITUD));

            int IdTipDocInterno = res.getInt(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_IDTIPDOCINTERNO));
            String NumDocInt = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_NUMDOCINTERNO));
            int Eq1 = res.getInt(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_EQ1));
            int Eq2 = res.getInt(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_EQ2));
            int Eq3 = res.getInt(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_EQ3));
            int Eq4 = res.getInt(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_EQ4));
            String DsEq1 = res.getString(res.getColumnIndex("DsEquipo1"));
            String DsEq2 = res.getString(res.getColumnIndex("DsEquipo2"));
            String DsEq3 = res.getString(res.getColumnIndex("DsEquipo3"));
            String DsEq4 = res.getString(res.getColumnIndex("DsEquipo4"));
            String Origen = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_ORIGEN));
            String Destino = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_DESTINO));
            String Ruta = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_RUTA));


            String DsRecurso = res.getString(res.getColumnIndex("DsRecurso"));
            String Trabajador = res.getString(res.getColumnIndex("Trabajador"));
            String ayudante = res.getString(res.getColumnIndex("Ayudante"));
            String UM = res.getString(res.getColumnIndex(OSRecursos_COLUMN_UM));


            o = new ApuntesMaquinaria(IdOS,Fecha,IdRecurso,Area,Usuario,FechaDigita,IdDev,FechaIni,FechaFin,Latitud,Longitud,Observacion,IdApMaquinaria,IdTrabajador,IdTrabAyudante,CodEquipo,CodEquipoImplemento,HOR_INI,HOR_FIN,HOR_FALLA,ODO_INI,ODO_FIN,ODO_FALLA,MED_INI,MED_FIN,MED_FALLA,QTD_PAGO);
            o.setDsRecurso(DsRecurso);
            o.setTrabajador(Trabajador);
            o.setAyudante(ayudante);
            o.setUM(UM);

            o.setIdTipDocInterno(IdTipDocInterno);
            o.setNumDocInterno(NumDocInt);
            o.setEquipo1(Eq1);
            o.setEquipo2(Eq2);
            o.setEquipo3(Eq3);
            o.setEquipo4(Eq4);

            o.setEq1(DsEq1);
            o.setEq2(DsEq2);
            o.setEq3(DsEq3);
            o.setEq4(DsEq4);

            o.setOrigen(Origen);
            o.setDestino(Destino);
            o.setRuta(Ruta);

            array_list.add(o);
            res.moveToNext();
        }
        res.close();
        return array_list;
    }
    public ArrayList<ApuntesMaquinaria> getApuntesMaquinzriaByFecha(String Fecha) {
        Parametros usrp = getSingleParametro(5);
        usr = "";
        if(usrp != null){
            usr = usrp.getValor();
        }
        ArrayList<ApuntesMaquinaria> array_list = new ArrayList<ApuntesMaquinaria>();
        ApuntesMaquinaria o = new ApuntesMaquinaria();
        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from "+APUNTESMAQUINARIA_TABLE_NAME+" where FechaApunte = "+Fecha+" and Usuario ='"+usr+"'", null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            int IdApMaquinaria = Integer.parseInt( res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_IDAPMAQUINARIA)));
            String CodEquipo = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_CODEQUIPO));
            String CodEquipoImplemento = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_CODEQUIPOIMPLEMENTO));
            int IdTrabajador = Integer.parseInt( res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_IDTRABAJADOR)));
            int IdTrabAyudante = Integer.parseInt( res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_IDTRABAYUDANTE)));
            float HOR_INI =  ( res.getFloat(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_HOR_INI)));
            float HOR_FIN =  ( res.getFloat(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_HOR_FIN)));
            int HOR_FALLA =  res.getInt(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_HOR_FALLA));

            float ODO_INI =  ( res.getFloat(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_ODO_INI)));
            float ODO_FIN =  ( res.getFloat(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_ODO_FIN)));
            int ODO_FALLA =  res.getInt(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_ODO_FALLA));

            float MED_INI =  ( res.getFloat(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_MED_INI)));
            float MED_FIN =  ( res.getFloat(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_MED_FIN)));
            int MED_FALLA =  res.getInt(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_MED_FALLA));
            float QTD_PAGO =  ( res.getFloat(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_QTD_PAGO)));

            int IdOS = Integer.parseInt( res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_IDOS)));
            int IdRecurso = Integer.parseInt( res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_IDRECURSO)));
            float Area = res.getFloat(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_AREA));
            String Usuario = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_USUARIO));
            String FechaDigita = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_FECHADIGITA));
            String IdDev = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_IDDEV));
            String Observacion = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_OBSERVACION));
            String FechaIni = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_FECHAINI));
            String FechaFin = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_FECHAFIN));
            String Latitud = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_LATITUD));
            String Longitud = res.getString(res.getColumnIndex(APUNTESMAQUINARIA_COLUMN_LONGITUD));


            o = new ApuntesMaquinaria(IdOS,Fecha,IdRecurso,Area,Usuario,FechaDigita,IdDev,FechaIni,FechaFin,Latitud,Longitud,Observacion,IdApMaquinaria,IdTrabajador, IdTrabAyudante,CodEquipo,CodEquipoImplemento,HOR_INI,HOR_FIN,HOR_FALLA,ODO_INI,ODO_FIN,ODO_FALLA,MED_INI,MED_FIN,MED_FALLA,QTD_PAGO);
            array_list.add(o);
            res.moveToNext();
        }
        res.close();
        return array_list;
    }

    public boolean DocumentoValido(int IdApMaquinaria, int IdTipDoc, String numDoc){
        Parametros usrp = getSingleParametro(5);
        usr = "";
        if(usrp != null){
            usr = usrp.getValor();
        }
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from "+APUNTESMAQUINARIA_TABLE_NAME+" where "+APUNTESMAQUINARIA_COLUMN_IDTIPDOCINTERNO+" = "+IdTipDoc+" and "+APUNTESMAQUINARIA_COLUMN_NUMDOCINTERNO+"='"+numDoc+"' and Usuario ='"+usr+"' and "+APUNTESMAQUINARIA_COLUMN_IDAPMAQUINARIA+" <> "+IdApMaquinaria, null );
        res.moveToFirst();

        return res.isAfterLast();
    }


    public ArrayList<ApuntesInsumos> getAllApuntesInsumos() {
        Parametros usrp = getSingleParametro(5);
        usr = "";
        if(usrp != null){
            usr = usrp.getValor();
        }
        ArrayList<ApuntesInsumos> array_list = new ArrayList<ApuntesInsumos>();
        ApuntesInsumos o = new ApuntesInsumos();
        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select ap.*, orec.DsRecurso, orec."+OSRecursos_COLUMN_UM+" from ApuntesInsumos ap " +
                "inner join "+OSRecursos_TABLE_NAME+" orec on ap.IdOS = orec.IdOs and ap.IdRecurso = orec.IdRecurso" //+
                //" where Usuario ='"+usr+"'"
                , null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            int IdApInsumos = Integer.parseInt( res.getString(res.getColumnIndex(APUNTESINSUMOS_COLUMN_IDAPINSUMOS)));
            int IdOS = Integer.parseInt( res.getString(res.getColumnIndex(APUNTESINSUMOS_COLUMN_IDOS)));
            int IdRecurso = Integer.parseInt( res.getString(res.getColumnIndex(APUNTESINSUMOS_COLUMN_IDRECURSO)));
            float Area =  ( res.getFloat(res.getColumnIndex(APUNTESINSUMOS_COLUMN_AREA)));
            float Dosis =  ( res.getFloat(res.getColumnIndex(APUNTESINSUMOS_COLUMN_DOSIS)));
            String Recurso = res.getString(res.getColumnIndex("DsRecurso"));
            String um = res.getString(res.getColumnIndex(OSRecursos_COLUMN_UM));
            String Fecha = res.getString(res.getColumnIndex(APUNTESINSUMOS_COLUMN_FECHAAPUNTE));
            String Usuario = res.getString(res.getColumnIndex(APUNTESINSUMOS_COLUMN_USUARIO));
            String FechaDigita = res.getString(res.getColumnIndex(APUNTESINSUMOS_COLUMN_FECHADIGITA));
            String IdDev = res.getString(res.getColumnIndex(APUNTESINSUMOS_COLUMN_IDDEV));
            String Observacion = res.getString(res.getColumnIndex(APUNTESINSUMOS_COLUMN_OBSERVACION));
            String FechaIni = res.getString(res.getColumnIndex(APUNTESINSUMOS_COLUMN_FECHAINI));
            String FechaFin = res.getString(res.getColumnIndex(APUNTESINSUMOS_COLUMN_FECHAFIN));
            String Latitud = res.getString(res.getColumnIndex(APUNTESINSUMOS_COLUMN_LATITUD));
            String Longitud = res.getString(res.getColumnIndex(APUNTESINSUMOS_COLUMN_LONGITUD));
            float Cantidad = res.getFloat(res.getColumnIndex(APUNTESINSUMOS_COLUMN_CANTIDAD));


            o = new ApuntesInsumos(IdOS,Fecha,IdRecurso,Recurso,um,Area,Usuario,FechaDigita,IdDev,FechaIni,FechaFin,Latitud,Longitud,Observacion,IdApInsumos,Dosis);
            o.setCantidad(Cantidad);

            array_list.add(o);
            res.moveToNext();
        }
        res.close();
        return array_list;
    }

    public ArrayList<ApuntesInsumos> getApuntesInsumos() {
        Parametros usrp = getSingleParametro(5);
        usr = "";
        if(usrp != null){
            usr = usrp.getValor();
        }
        ArrayList<ApuntesInsumos> array_list = new ArrayList<ApuntesInsumos>();
        ApuntesInsumos o = new ApuntesInsumos();
        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select ap.*, orec.DsRecurso, orec."+OSRecursos_COLUMN_UM+" from ApuntesInsumos ap " +
                "inner join "+OSRecursos_TABLE_NAME+" orec on ap.IdOS = orec.IdOs and ap.IdRecurso = orec.IdRecurso" +
                " where Usuario ='"+usr+"'", null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            int IdApInsumos = Integer.parseInt( res.getString(res.getColumnIndex(APUNTESINSUMOS_COLUMN_IDAPINSUMOS)));
            int IdOS = Integer.parseInt( res.getString(res.getColumnIndex(APUNTESINSUMOS_COLUMN_IDOS)));
            int IdRecurso = Integer.parseInt( res.getString(res.getColumnIndex(APUNTESINSUMOS_COLUMN_IDRECURSO)));
            float Area =  ( res.getFloat(res.getColumnIndex(APUNTESINSUMOS_COLUMN_AREA)));
            float Dosis =  ( res.getFloat(res.getColumnIndex(APUNTESINSUMOS_COLUMN_DOSIS)));
            String Recurso = res.getString(res.getColumnIndex("DsRecurso"));
            String um = res.getString(res.getColumnIndex(OSRecursos_COLUMN_UM));
            String Fecha = res.getString(res.getColumnIndex(APUNTESINSUMOS_COLUMN_FECHAAPUNTE));
            String Usuario = res.getString(res.getColumnIndex(APUNTESINSUMOS_COLUMN_USUARIO));
            String FechaDigita = res.getString(res.getColumnIndex(APUNTESINSUMOS_COLUMN_FECHADIGITA));
            String IdDev = res.getString(res.getColumnIndex(APUNTESINSUMOS_COLUMN_IDDEV));
            String Observacion = res.getString(res.getColumnIndex(APUNTESINSUMOS_COLUMN_OBSERVACION));
            String FechaIni = res.getString(res.getColumnIndex(APUNTESINSUMOS_COLUMN_FECHAINI));
            String FechaFin = res.getString(res.getColumnIndex(APUNTESINSUMOS_COLUMN_FECHAFIN));
            String Latitud = res.getString(res.getColumnIndex(APUNTESINSUMOS_COLUMN_LATITUD));
            String Longitud = res.getString(res.getColumnIndex(APUNTESINSUMOS_COLUMN_LONGITUD));
            float Cantidad = res.getFloat(res.getColumnIndex(APUNTESINSUMOS_COLUMN_CANTIDAD));


            o = new ApuntesInsumos(IdOS,Fecha,IdRecurso,Recurso,um,Area,Usuario,FechaDigita,IdDev,FechaIni,FechaFin,Latitud,Longitud,Observacion,IdApInsumos,Dosis);
            o.setCantidad(Cantidad);

            array_list.add(o);
            res.moveToNext();
        }
        res.close();
        return array_list;
    }


    public ArrayList<ApuntesInsumos> getApuntesInsumosByFecha(String Fecha) {
        Parametros usrp = getSingleParametro(5);
        usr = "";
        if(usrp != null){
            usr = usrp.getValor();
        }
        ArrayList<ApuntesInsumos> array_list = new ArrayList<ApuntesInsumos>();
        ApuntesInsumos o = new ApuntesInsumos();
        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from ApuntesInsumos where FechaApunte = "+Fecha+" and Usuario ='"+usr+"'", null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            int IdApInsumos = Integer.parseInt( res.getString(res.getColumnIndex(APUNTESINSUMOS_COLUMN_IDAPINSUMOS)));
            int IdOS = Integer.parseInt( res.getString(res.getColumnIndex(APUNTESINSUMOS_COLUMN_IDOS)));
            int IdRecurso = Integer.parseInt( res.getString(res.getColumnIndex(APUNTESINSUMOS_COLUMN_IDRECURSO)));
            float Area =  ( res.getFloat(res.getColumnIndex(APUNTESINSUMOS_COLUMN_AREA)));
            float Dosis =  ( res.getFloat(res.getColumnIndex(APUNTESINSUMOS_COLUMN_DOSIS)));

            String Usuario = res.getString(res.getColumnIndex(APUNTESINSUMOS_COLUMN_USUARIO));
            String FechaDigita = res.getString(res.getColumnIndex(APUNTESINSUMOS_COLUMN_FECHADIGITA));
            String IdDev = res.getString(res.getColumnIndex(APUNTESINSUMOS_COLUMN_IDDEV));
            String Observacion = res.getString(res.getColumnIndex(APUNTESINSUMOS_COLUMN_OBSERVACION));
            String FechaIni = res.getString(res.getColumnIndex(APUNTESINSUMOS_COLUMN_FECHAINI));
            String FechaFin = res.getString(res.getColumnIndex(APUNTESINSUMOS_COLUMN_FECHAFIN));
            String Latitud = res.getString(res.getColumnIndex(APUNTESINSUMOS_COLUMN_LATITUD));
            String Longitud = res.getString(res.getColumnIndex(APUNTESINSUMOS_COLUMN_LONGITUD));
            float Cantidad = res.getFloat(res.getColumnIndex(APUNTESINSUMOS_COLUMN_CANTIDAD));


            o = new ApuntesInsumos(IdOS,Fecha,IdRecurso,Area,Usuario,FechaDigita,IdDev,FechaIni,FechaFin,Latitud,Longitud,Observacion,IdApInsumos,Dosis);
            o.setCantidad(Cantidad);
            array_list.add(o);
            res.moveToNext();
        }
        return array_list;
    }

    public ArrayList<ApuntesInsumos> getApuntesInsumosByOS(int IdOS) {
        Parametros usrp = getSingleParametro(5);
        usr = "";
        if(usrp != null){
            usr = usrp.getValor();
        }
        ArrayList<ApuntesInsumos> array_list = new ArrayList<ApuntesInsumos>();
        ApuntesInsumos o = new ApuntesInsumos();
        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select ap.*, osr."+OSRecursos_COLUMN_DSRECURSO+", osr."+OSRecursos_COLUMN_UM+" from ApuntesInsumos ap inner join "+OSRecursos_TABLE_NAME+" osr on ap.IdOS = osr.IdOS and ap.IdRecurso = osr.IdRecurso where ap.IdOS = "+IdOS+" and Usuario ='"+usr+"'", null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            int IdApInsumos = Integer.parseInt( res.getString(res.getColumnIndex(APUNTESINSUMOS_COLUMN_IDAPINSUMOS)));

            int IdRecurso = Integer.parseInt( res.getString(res.getColumnIndex(APUNTESINSUMOS_COLUMN_IDRECURSO)));
            float Area =  ( res.getFloat(res.getColumnIndex(APUNTESINSUMOS_COLUMN_AREA)));
            float Dosis =  ( res.getFloat(res.getColumnIndex(APUNTESINSUMOS_COLUMN_DOSIS)));
            String Fecha = res.getString(res.getColumnIndex(APUNTESINSUMOS_COLUMN_FECHAAPUNTE));
            String Usuario = res.getString(res.getColumnIndex(APUNTESINSUMOS_COLUMN_USUARIO));
            String FechaDigita = res.getString(res.getColumnIndex(APUNTESINSUMOS_COLUMN_FECHADIGITA));
            String IdDev = res.getString(res.getColumnIndex(APUNTESINSUMOS_COLUMN_IDDEV));
            String Observacion = res.getString(res.getColumnIndex(APUNTESINSUMOS_COLUMN_OBSERVACION));
            String FechaIni = res.getString(res.getColumnIndex(APUNTESINSUMOS_COLUMN_FECHAINI));
            String FechaFin = res.getString(res.getColumnIndex(APUNTESINSUMOS_COLUMN_FECHAFIN));
            String Latitud = res.getString(res.getColumnIndex(APUNTESINSUMOS_COLUMN_LATITUD));
            String Longitud = res.getString(res.getColumnIndex(APUNTESINSUMOS_COLUMN_LONGITUD));
            float Cantidad = res.getFloat(res.getColumnIndex(APUNTESINSUMOS_COLUMN_CANTIDAD));

            String DsRecurso = res.getString(res.getColumnIndex(OSRecursos_COLUMN_DSRECURSO));
            String UM = res.getString(res.getColumnIndex(OSRecursos_COLUMN_UM));

            o = new ApuntesInsumos(IdOS,Fecha,IdRecurso,Area,Usuario,FechaDigita,IdDev,FechaIni,FechaFin,Latitud,Longitud,Observacion,IdApInsumos,Dosis);
            o.setDsRecurso(DsRecurso);
            o.setUM(UM);
            o.setCantidad(Cantidad);
            array_list.add(o);
            res.moveToNext();
        }
        res.close();
        return array_list;
    }


    public ArrayList<Trabajadores> getTrabajadoresPorCuadrilla(int IdCuadrilla) {
        ArrayList<Trabajadores> array_list = new ArrayList<Trabajadores>();
        Trabajadores o = new Trabajadores();
        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from Trabajadores where IdCuadrilla = "+IdCuadrilla+" order by "+TRABAJADORES_COLUMN_NOMTRABAJADOR, null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            int IdTrabajador = Integer.parseInt( res.getString(res.getColumnIndex(TRABAJADORES_COLUMN_IDTRABAJADOR)));
            String CodTrabajador = res.getString(res.getColumnIndex(TRABAJADORES_COLUMN_CODTRABAJADOR));
            String NomTrabajador = res.getString(res.getColumnIndex(TRABAJADORES_COLUMN_NOMTRABAJADOR));

            o = new Trabajadores(IdTrabajador,CodTrabajador,NomTrabajador,IdCuadrilla);
            array_list.add(o);
            res.moveToNext();
        }
        return array_list;
    }

    public ArrayList<Trabajadores> getTrabajadores() {
        ArrayList<Trabajadores> array_list = new ArrayList<Trabajadores>();
        Trabajadores o = new Trabajadores();
        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from Trabajadores", null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            int IdTrabajador = Integer.parseInt( res.getString(res.getColumnIndex(TRABAJADORES_COLUMN_IDTRABAJADOR)));
            String CodTrabajador = res.getString(res.getColumnIndex(TRABAJADORES_COLUMN_CODTRABAJADOR));
            String NomTrabajador = res.getString(res.getColumnIndex(TRABAJADORES_COLUMN_NOMTRABAJADOR));
            int IdCuadrilla = res.getInt(res.getColumnIndex(TRABAJADORES_COLUMN_IDCUADRILLA));

            o = new Trabajadores(IdTrabajador,CodTrabajador,NomTrabajador,IdCuadrilla);
            array_list.add(o);
            res.moveToNext();
        }
        return array_list;
    }

    public Trabajadores getSingleTrabajadorByCod(String CodTrabajador) {
        Trabajadores o = new Trabajadores();
        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        int cod = 0;
        try {
            cod = Integer.parseInt(CodTrabajador);
        } catch (NumberFormatException e) {
            cod = 0;
        }

        CodTrabajador = String.valueOf(cod);
        Cursor res =  db.rawQuery( "select * from Trabajadores where "+TRABAJADORES_COLUMN_CODTRABAJADOR+"='"+CodTrabajador+"'", null );
        res.moveToFirst();

        if(res.isAfterLast() == false){
            int IdTrabajador = Integer.parseInt( res.getString(res.getColumnIndex(TRABAJADORES_COLUMN_IDTRABAJADOR)));
            String NomTrabajador = res.getString(res.getColumnIndex(TRABAJADORES_COLUMN_NOMTRABAJADOR));
            int IdCuadrilla = res.getInt(res.getColumnIndex(TRABAJADORES_COLUMN_IDCUADRILLA));

            o = new Trabajadores(IdTrabajador,CodTrabajador,NomTrabajador,IdCuadrilla);

        }
        return o;
    }

    public Trabajadores getSingleTrabajadorById(int IdTrabajador) {
        Trabajadores o = new Trabajadores();
        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from Trabajadores where "+TRABAJADORES_COLUMN_IDTRABAJADOR+"="+IdTrabajador+"", null );
        res.moveToFirst();

        if(res.isAfterLast() == false){
            String CodTrabajador = res.getString(res.getColumnIndex(TRABAJADORES_COLUMN_CODTRABAJADOR));
            String NomTrabajador = res.getString(res.getColumnIndex(TRABAJADORES_COLUMN_NOMTRABAJADOR));
            int IdCuadrilla = res.getInt(res.getColumnIndex(TRABAJADORES_COLUMN_IDCUADRILLA));

            o = new Trabajadores(IdTrabajador,CodTrabajador,NomTrabajador,IdCuadrilla);

        }
        return o;
    }





   /* public ArrayList<Trabajadores> getTrabajadoresSinHuella(){

        ArrayList<Trabajadores> allTrab = getTrabajadores(), arrayList = new ArrayList<Trabajadores>();


        for (  Trabajadores t : allTrab){
            if (!HasHuellas(t.getCodTrabajador()))
                arrayList.add(t);
        }
        return  arrayList;

    }
*/




    public ArrayList<ApuntesArea> getApuntesArea(int IdOS){
        ApuntesArea obj = null;
        ArrayList<ApuntesArea> list = new ArrayList<ApuntesArea>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from ApuntesArea where IdOS = "+IdOS+" order by IdApArea desc", null );
        res.moveToFirst();
        while(res.isAfterLast() == false){
            obj = new ApuntesArea();
            int IdApArea = Integer.parseInt(res.getString(res.getColumnIndex("IdApArea")));
            String Fecha  = res.getString(res.getColumnIndex("Fecha"));
            float Area = Float.parseFloat(res.getString(res.getColumnIndex("Area")));
            int enviado = Integer.parseInt(res.getString(res.getColumnIndex("enviado")));
            obj = new ApuntesArea(IdApArea,IdOS,Fecha, Area, enviado > 0);

            list.add(obj);
            res.moveToNext();

        }


        return  list;
    }

    public ArrayList<ApuntesArea> getApuntesAreaNoEnviados(){
        ApuntesArea obj = null;
        ArrayList<ApuntesArea> list = new ArrayList<ApuntesArea>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from ApuntesArea where enviado = 0", null );
        res.moveToFirst();
        while(res.isAfterLast() == false){
            obj = new ApuntesArea();
            int IdApArea = Integer.parseInt(res.getString(res.getColumnIndex("IdApArea")));
            int IdOS = Integer.parseInt(res.getString(res.getColumnIndex("IdOS")));
            String Fecha  = res.getString(res.getColumnIndex("Fecha"));
            float Area = Float.parseFloat(res.getString(res.getColumnIndex("Area")));
            int enviado = Integer.parseInt(res.getString(res.getColumnIndex("enviado")));
            obj = new ApuntesArea(IdApArea,IdOS,Fecha, Area, enviado > 0);

            list.add(obj);
            res.moveToNext();

        }


        return  list;
    }


    public String[] getAllAplicaciones() {


        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from Aplicaciones", null );
        res.moveToFirst();
        int pos  = 0;

        String[] array_list = new String[res.getCount()];
        while(res.isAfterLast() == false){
            String app = res.getString(res.getColumnIndex("app"));
            array_list[pos] = app ;
            pos = pos+1;
            res.moveToNext();
        }
        return array_list;
    }

    public ArrayList<Cuadrillas> getCuadrillasAutoComplete(String q) {
        ArrayList<Cuadrillas> array_list = new ArrayList<Cuadrillas>();
        Cuadrillas o = new Cuadrillas();
        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select c.* " +
                "from Cuadrillas c" +
                " where TRIM(c."+CUADRILLAS_COLUMN_BKCUADRILLA+") LIKE '%"+q+"%' OR TRIM(c."+CUADRILLAS_COLUMN_DSCUADRILLA+") LIKE '%"+q+"%'" +
                " group by c."+CUADRILLAS_COLUMN_IDCUADRILLA+", c."+CUADRILLAS_COLUMN_BKCUADRILLA+", c."+CUADRILLAS_COLUMN_DSCUADRILLA+" LIMIT 20", null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            int IdCuadrilla = Integer.parseInt( res.getString(res.getColumnIndex(CUADRILLAS_COLUMN_IDCUADRILLA)));
            String BkCuadrilla = res.getString(res.getColumnIndex(CUADRILLAS_COLUMN_BKCUADRILLA));
            String DsCuadrilla = res.getString(res.getColumnIndex(CUADRILLAS_COLUMN_DSCUADRILLA));
            o = new Cuadrillas(IdCuadrilla,BkCuadrilla,DsCuadrilla);
            array_list.add(o);
            res.moveToNext();
        }
        return array_list;
    }

    public ArrayList<Cuadrillas> getAllCuadrillas() {
        ArrayList<Cuadrillas> array_list = new ArrayList<Cuadrillas>();
        Cuadrillas o = new Cuadrillas();
        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from Cuadrillas", null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            int IdCuadrilla = Integer.parseInt( res.getString(res.getColumnIndex(CUADRILLAS_COLUMN_IDCUADRILLA)));
            String BkCuadrilla = res.getString(res.getColumnIndex(CUADRILLAS_COLUMN_BKCUADRILLA));
            String DsCuadrilla = res.getString(res.getColumnIndex(CUADRILLAS_COLUMN_DSCUADRILLA));
            o = new Cuadrillas(IdCuadrilla,BkCuadrilla,DsCuadrilla);
            array_list.add(o);
            res.moveToNext();
        }
        return array_list;
    }

    public ArrayList<Cuadrillas> getAllCuadrillasConTrabajadores() {
        ArrayList<Cuadrillas> array_list = new ArrayList<Cuadrillas>();
        Cuadrillas o = new Cuadrillas();
        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select c."+CUADRILLAS_COLUMN_IDCUADRILLA+", c."+CUADRILLAS_COLUMN_BKCUADRILLA+", c."+CUADRILLAS_COLUMN_DSCUADRILLA+" from Cuadrillas c" +
                                        " inner join "+TRABAJADORES_TABLE_NAME+" t on t."+TRABAJADORES_COLUMN_IDCUADRILLA+" = c."+CUADRILLAS_COLUMN_IDCUADRILLA+" " +
                                        " group by c."+CUADRILLAS_COLUMN_IDCUADRILLA+", c."+CUADRILLAS_COLUMN_BKCUADRILLA+", c."+CUADRILLAS_COLUMN_DSCUADRILLA, null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            int IdCuadrilla = Integer.parseInt( res.getString(res.getColumnIndex(CUADRILLAS_COLUMN_IDCUADRILLA)));
            String BkCuadrilla = res.getString(res.getColumnIndex(CUADRILLAS_COLUMN_BKCUADRILLA));
            String DsCuadrilla = res.getString(res.getColumnIndex(CUADRILLAS_COLUMN_DSCUADRILLA));
            o = new Cuadrillas(IdCuadrilla,BkCuadrilla,DsCuadrilla);
            array_list.add(o);
            res.moveToNext();
        }
        return array_list;
    }

    public ArrayList<Equipos> getAllEquipos() {
        ArrayList<Equipos> array_list = new ArrayList<Equipos>();
        Equipos o = new Equipos();
        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from "+EQUIPOS_TABLE_NAME, null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            int IdEquipo = Integer.parseInt( res.getString(res.getColumnIndex(EQUIPOS_COLUMN_IDEQUIPO)));
            String CodEquipo = res.getString(res.getColumnIndex(EQUIPOS_COLUMN_CODEQUIPO));
            String NomEquipo = res.getString(res.getColumnIndex(EQUIPOS_COLUMN_NOMBREEQUIPO));
            String PropEquipo = res.getString(res.getColumnIndex(EQUIPOS_COLUMN_PROPEQUIPO));
            int idsubflota = res.getInt(res.getColumnIndex(EQUIPOS_COLUMN_IDSUBFLOTA));
            o = new Equipos(IdEquipo,CodEquipo, NomEquipo,PropEquipo, idsubflota);
            array_list.add(o);
            res.moveToNext();
        }
        return array_list;
    }

    public ArrayList<Empresas> getAllEmpresas() {
        ArrayList<Empresas> array_list = new ArrayList<Empresas>();
        Empresas o = new Empresas();
        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from "+EMPRESAS_TABLE_NAME, null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            int Id = Integer.parseInt( res.getString(res.getColumnIndex(EMPRESAS_COLUMN_SOCIEDAD)));
            String Ds = res.getString(res.getColumnIndex(EMPRESAS_COLUMN_DSEMPRESA));

            switch (Id){
                case 1000:
                    Ds = "CENTRAL IZALCO";
                    break;
                case 2000:
                    Ds = "CHAPARRASTIQUE";
                    break;
                case 3000:
                    Ds = "COAGRI";
                    break;
            }

            o = new Empresas(Id,Ds);
            array_list.add(o);
            res.moveToNext();
        }
        return array_list;
    }

    public ArrayList<TiposDocumento> getAllTiposDocumento() {
        ArrayList<TiposDocumento> array_list = new ArrayList<TiposDocumento>();
        TiposDocumento o = new TiposDocumento();
        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from TiposDocumentoInterno", null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            int IdTipDocInterno = Integer.parseInt( res.getString(res.getColumnIndex("IdTipDocInterno")));
            String Bk = res.getString(res.getColumnIndex("BkTipDocInterno"));
            String Ds = res.getString(res.getColumnIndex("DsTipDocInterno"));
            o = new TiposDocumento(IdTipDocInterno,Bk,Ds);
            array_list.add(o);
            res.moveToNext();
        }
        return array_list;
    }

    public ArrayList<OSRecursos> getAllRecursosByOS(int IdOS) {
        ArrayList<OSRecursos> array_list = new ArrayList<OSRecursos>();
        OSRecursos o = new OSRecursos();
        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from OSRecursos where IdOS = "+IdOS, null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            IdOS = Integer.parseInt( res.getString(res.getColumnIndex(OS_COLUMN_IDOS)));
            int IdRecurso = Integer.parseInt( res.getString(res.getColumnIndex(OSRecursos_COLUMN_IDRECURSO)));
            String BkRecurso = res.getString(res.getColumnIndex(OSRecursos_COLUMN_BKRECURSO));
            String DsRecurso = res.getString(res.getColumnIndex(OSRecursos_COLUMN_DSRECURSO));
            String BkTipRecurso = res.getString(res.getColumnIndex(OSRecursos_COLUMN_BKTIPRECURSO));
            String UM = res.getString(res.getColumnIndex(OSRecursos_COLUMN_UM));
            float CantPend = res.getFloat(res.getColumnIndex(OSRecursos_COLUMN_CANTIDADPEND));
            float CantidadAp = res.getFloat(res.getColumnIndex(OSRecursos_COLUMN_CANTIDADAPUNT));
            float AreaApunt = res.getFloat(res.getColumnIndex("AreaApunt"));
            int ay = res.getInt(res.getColumnIndex("Ayudante"));
            boolean impl = res.getInt(res.getColumnIndex(OSRecursos_COLUMN_REQUIEREIMPLEMENTO)) > 0;
            int IdSubFlota = Integer.parseInt( res.getString(res.getColumnIndex(OSRecursos_COLUMN_IDRECURSO)));
            o = new OSRecursos(IdOS,IdRecurso,BkRecurso,DsRecurso,BkTipRecurso, UM,CantPend,CantidadAp,AreaApunt,ay>0, impl, IdSubFlota);
            array_list.add(o);
            res.moveToNext();
        }
        return array_list;
    }

    public OSRecursos getAllRecursosByOSId(int IdOS, int IdRecurso) {
        ArrayList<OSRecursos> array_list = new ArrayList<OSRecursos>();
        OSRecursos o = new OSRecursos();
        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from OSRecursos where IdOS = "+IdOS+" and IdRecurso = "+IdRecurso, null );
        res.moveToFirst();

        if(res.isAfterLast() == false){
            IdOS = Integer.parseInt( res.getString(res.getColumnIndex(OS_COLUMN_IDOS)));
            String BkRecurso = res.getString(res.getColumnIndex(OSRecursos_COLUMN_BKRECURSO));
            String DsRecurso = res.getString(res.getColumnIndex(OSRecursos_COLUMN_DSRECURSO));
            String BkTipRecurso = res.getString(res.getColumnIndex(OSRecursos_COLUMN_BKTIPRECURSO));
            String UM = res.getString(res.getColumnIndex(OSRecursos_COLUMN_UM));
            float CantPend = res.getFloat(res.getColumnIndex(OSRecursos_COLUMN_CANTIDADPEND));
            float CantidadAp = res.getFloat(res.getColumnIndex(OSRecursos_COLUMN_CANTIDADAPUNT));
            float AreaApunt = res.getFloat(res.getColumnIndex("AreaApunt"));
            int ay = res.getInt(res.getColumnIndex("Ayudante"));
            boolean impl = res.getInt(res.getColumnIndex(OSRecursos_COLUMN_REQUIEREIMPLEMENTO)) > 0;
            int IdSubFlota = Integer.parseInt( res.getString(res.getColumnIndex(OSRecursos_COLUMN_IDSUBFLOTA)));
            o = new OSRecursos(IdOS,IdRecurso,BkRecurso,DsRecurso,BkTipRecurso, UM,CantPend,CantidadAp,AreaApunt,ay>0, impl, IdSubFlota);
            array_list.add(o);
            res.moveToNext();
        }
        return o;
    }

    public ArrayList<OS> getAllOS() {
        ArrayList<OS> array_list = new ArrayList<OS>();
        OS o = new OS();
        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from OS", null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            int IdOS = Integer.parseInt( res.getString(res.getColumnIndex(OS_COLUMN_IDOS)));
            int IdHacienda = Integer.parseInt( res.getString(res.getColumnIndex(OS_COLUMN_IDHACIENDA)));
            String DsActividad = res.getString(res.getColumnIndex(OS_COLUMN_DSACTIVIDAD));
            float Cantidad = res.getFloat(res.getColumnIndex(OS_COLUMN_CANTIDAD));
            String bkzafra = res.getString(res.getColumnIndex(OS_COLUMN_BKZAFRA));
            String UMP = res.getString(res.getColumnIndex("UMP"));
            if (bkzafra == null) bkzafra = "";
            o = new OS(IdOS,IdHacienda,DsActividad,Cantidad,bkzafra, UMP);
            array_list.add(o);
            res.moveToNext();
        }
        return array_list;
    }

    public ArrayList<OS> getAllOSByHacienda(int IdHacienda) {
        ArrayList<OS> array_list = new ArrayList<OS>();
        OS o = new OS();
        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from OS where IdHacienda = "+IdHacienda, null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            int IdOS = Integer.parseInt( res.getString(res.getColumnIndex(OS_COLUMN_IDOS)));
            //int IdHacienda = Integer.parseInt( res.getString(res.getColumnIndex(OS_COLUMN_IDHACIENDA)));
            String DsActividad = res.getString(res.getColumnIndex(OS_COLUMN_DSACTIVIDAD));
            float Cantidad = res.getFloat(res.getColumnIndex(OS_COLUMN_CANTIDAD));
            String bkzafra = res.getString(res.getColumnIndex(OS_COLUMN_BKZAFRA));
            String UMP = res.getString(res.getColumnIndex("UMP"));
            if (bkzafra == null ) bkzafra = "";
            o = new OS(IdOS,IdHacienda,DsActividad,Cantidad, bkzafra, UMP);
            array_list.add(o);
            res.moveToNext();
        }
        return array_list;
    }

    public ArrayList<Haciendas> getAllHaciendas() {
        ArrayList<Haciendas> array_list = new ArrayList<Haciendas>();
        Haciendas hda = new Haciendas();
        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from Haciendas", null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            int IdHacienda = Integer.parseInt( res.getString(res.getColumnIndex(HACIENDAS_COLUMN_IDHACIENDA)));
            String BkHacienda = res.getString(res.getColumnIndex(HACIENDAS_COLUMN_BKHACIENDA));
            String DsHacienda = res.getString(res.getColumnIndex(HACIENDAS_COLUMN_DSHACIENDA));
            int bio = res.getInt(res.getColumnIndex(HACIENDAS_COLUMN_MARCABIOMETRICO));
            hda = new Haciendas(IdHacienda,BkHacienda,DsHacienda, bio > 0);
            array_list.add(hda);
            res.moveToNext();
        }
        return array_list;
    }

    public Boolean ActualizarHaciendas(ArrayList<Haciendas> hdasMaestras){

        int recuperadas = 0, insertadas = 0;

        if(hdasMaestras==null)return false;

        recuperadas = hdasMaestras.size();

        //Verificar que las haciendas anteriores sigan existiendo
        ArrayList<Haciendas> exist = getAllHaciendas();
        ArrayList<Haciendas> hdasEliminar = new ArrayList<Haciendas>();


        for (Haciendas h: exist
                ) {
            if(!hdasMaestras.contains(h)){
                hdasEliminar.add(h);
            }

        }
        for (Haciendas h:
                hdasEliminar) {
            deleteHacienda(h.getBkHacienda());
        }

        exist = getAllHaciendas();
        //deleteAllHaciendas();
        for (Haciendas h:
                hdasMaestras) {


                if(insertHda(h.getIdHacienda(),h.getBkHacienda(),h.getDsHacienda(),0, h.isMarcaBiometrico())) {
                    insertadas = insertadas+1;

                }


        }





        return insertadas>0;
    }

    public Boolean ActualizarOS(ArrayList<OS> osMaestras){

        int recuperadas = 0, insertadas = 0;

        if(osMaestras==null)return false;

        recuperadas = osMaestras.size();

        //Verificar que las haciendas anteriores sigan existiendo
        ArrayList<OS> exist = getAllOS();
        ArrayList<OS> osEliminar = new ArrayList<OS>();


        for (OS h: exist
                ) {
            if(!osMaestras.contains(h)){
                osEliminar.add(h);
            }

        }
        for (OS h:
                osEliminar) {
            deleteOS(h.getIdOS());
        }

        exist = getAllOS();

        for (OS h:
                osMaestras) {


            if(!exist.contains(h)){
                if(insertOS(h.getIdOS(),h.getIdHacienda(),h.getDsActividad(),h.getCantidad(),h.getBkZafra())) {
                    insertadas = insertadas+1;

                }
            }

        }





        return insertadas>0;
    }



    public Boolean ActualizarOSRecursos(ArrayList<OSRecursos> osMaestras){

        int recuperadas = 0, insertadas = 0;
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(

                        "delete from "+OSRecursos_TABLE_NAME
        );

        for (OSRecursos o:osMaestras
             ) {
            if(insertOSRecurso(o.getIdOS(),o.getIdRecurso(),o.getBkRecurso(),o.getDsRecurso(),o.getBkTipRecurso(),o.getUM(),o.getCantidadPend(),o.getCantidadApunt(),o.getAreaApunt(),o.getAyudante(), o.getRequiereImplemento(), o.getIdSubFlota())){
                insertadas = insertadas +1;
            }
        }

        return insertadas>0;
    }

    public Boolean ActualizarCuadrillas(ArrayList<Cuadrillas> cuadrillases){

        int recuperadas = 0, insertadas = 0;
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(

                "delete from "+CUADRILLAS_TABLE_NAME
        );

        for (Cuadrillas o:cuadrillases
                ) {
            if(insertCuadrillas(o.getIdCuadrilla(),o.getBkCuadrilla(),o.getDsCuadrilla())){
                insertadas = insertadas +1;
            }
        }

        return insertadas>0;
    }

    public Boolean ActualizarRecursosSubFlotas(ArrayList<Cuadrillas> cuadrillases){

        int recuperadas = 0, insertadas = 0;
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(

                "delete from "+CUADRILLAS_TABLE_NAME
        );

        for (Cuadrillas o:cuadrillases
        ) {
            if(insertCuadrillas(o.getIdCuadrilla(),o.getBkCuadrilla(),o.getDsCuadrilla())){
                insertadas = insertadas +1;
            }
        }

        return insertadas>0;
    }


    public Boolean ActualizarTrabajadoresByCuadrilla(ArrayList<Trabajadores> trabajadores, int IdCuadrilla){

        int recuperadas = 0, insertadas = 0;
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(

                "delete from "+TRABAJADORES_TABLE_NAME +" where IdCuadrilla = "+IdCuadrilla
        );

        for (Trabajadores o:trabajadores
                ) {
            Trabajadores t = getSingleTrabajadorById(o.getIdTrabajador());
            if (t.getIdTrabajador() > 0){
                updateCuadrilla(o.getIdTrabajador(),o.getIdCuadrilla());
            }else if(insertTrabajadores(o.getIdTrabajador(),o.getCodTrabajador(),o.getNomTrabajador(),o.getIdCuadrilla())){
                insertadas = insertadas +1;
            }
        }

        return insertadas>0;
    }

    public Boolean ActualizarOSRecursosByOS(ArrayList<OSRecursos> osMaestras, int IdOS){

        int recuperadas = 0, insertadas = 0;
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(

                "delete from "+OSRecursos_TABLE_NAME +" where IdOS = "+IdOS
        );

        for (OSRecursos o:osMaestras
                ) {
            if(insertOSRecurso(o.getIdOS(),o.getIdRecurso(),o.getBkRecurso(),o.getDsRecurso(),o.getBkTipRecurso(),o.getUM(),o.getCantidadPend(),o.getCantidadApunt(),o.getAreaApunt(),o.getAyudante(), o.getRequiereImplemento(), o.getIdSubFlota())){
                insertadas = insertadas +1;
            }
        }

        return insertadas>0;
    }

    public void limpiarTablas(String usuario) {

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(

                "delete from "+APUNTESMANOOBRA_TABLE_NAME +" where Usuario = '"+usuario+"'"
        );
        db.execSQL(

                "delete from "+APUNTESMAQUINARIA_TABLE_NAME +" where Usuario = '"+usuario+"'"
        );
        db.execSQL(

                "delete from "+APUNTESINSUMOS_TABLE_NAME +" where Usuario = '"+usuario+"'"
        );
    }

    public void setApAreaEnviados() {

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(

                "update ApuntesArea set enviado = 1 where enviado = 0"
        );

    }
    public static String getCurrentTimeStamp() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//dd/MM/yyyy
        Date now = new Date();
        String strDate = sdfDate.format(now);
        return strDate;
    }

    public static String getCurrentDate() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");//dd/MM/yyyy
        Calendar calendar = Calendar.getInstance();
        Date now = calendar.getTime();
        String strDate = sdfDate.format(now);
        return strDate;
    }

    public static String getDate(int year, int month, int day) {
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");//dd/MM/yyyy
        Calendar calendar = Calendar.getInstance();
        calendar.set(year,month,day);
        Date now = calendar.getTime();
        String strDate = sdfDate.format(now);
        return strDate;
    }
    public static String getCurrentDateLA() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy");
        Calendar calendar = Calendar.getInstance();
        Date now = calendar.getTime();
        String strDate = sdfDate.format(now);
        return strDate;
    }

    public static int MargenDiasPermitido(){

        return 3;
    }

    public boolean ActualizarOSByHda(ArrayList<OS> oeses, String bkHacienda) {
        int recuperadas = 0, insertadas = 0;

        Haciendas hda = getSingleHaciendabyBK(bkHacienda);


        if(oeses==null)return false;

        recuperadas = oeses.size();

        //Verificar que las haciendas anteriores sigan existiendo
        ArrayList<OS> exist = getAllOSByHacienda(hda.getIdHacienda());
        ArrayList<OS> osEliminar = new ArrayList<OS>();


        for (OS h: exist
                ) {
            if(!oeses.contains(h)){
                osEliminar.add(h);
            }

        }
        for (OS h:
                osEliminar) {
            deleteOS(h.getIdOS());
        }

        exist = getAllOSByHacienda(hda.getIdHacienda());

        for (OS h:
                oeses) {


            if(!exist.contains(h)){
                if(insertOS(h.getIdOS(),h.getIdHacienda(),h.getDsActividad(),h.getCantidad(),h.getBkZafra())) {
                    insertadas = insertadas+1;

                }
            }

        }

        return insertadas>0;
    }



    public static String getDate(String dtString) {
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");//dd/MM/yyyy
        Calendar calendar = Calendar.getInstance();

        Date now = new Date();
        try {
            now = sdfDate.parse(dtString);
            sdfDate = new SimpleDateFormat("dd/MM/yyyy");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String strDate = sdfDate.format(now);
        return strDate;
    }

    public String getSociedad(){
        Parametros pSociedad = getSingleParametro(8);
        if (pSociedad == null) pSociedad = new Parametros(8,"");

        return  pSociedad.getValor();
    }


}