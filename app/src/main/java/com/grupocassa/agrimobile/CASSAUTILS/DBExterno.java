package com.grupocassa.agrimobile.CASSAUTILS;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.grupocassa.agrimobile.CLS.HuellasTrabajadores;
import com.grupocassa.agrimobile.CLS.Parametros;
import com.grupocassa.agrimobile.CLS.Trabajadores;

import java.util.ArrayList;


/**
 * Created by gerardo.valle on 21/9/2017.
 */

public class DBExterno extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "Huellas.db";
    public static Context ctxOriginal;
    private DBHelper mydb;

    public static final String HUELLASTRABAJADORES_TABLE_NAME = "HuellasTrabajadores";
    public static final String HUELLASTRABAJADORES_COLUMN_IDHUELLA = "IdHuella";
    public static final String HUELLASTRABAJADORES_COLUMN_CODTRABAJADOR = "CodTrabajador";
    public static final String HUELLASTRABAJADORES_COLUMN_HUELLA = "Huella";
    public static final String HUELLASTRABAJADORES_COLUMN_SINCRONIZADA = "Sincronizada";

    public DBExterno(Context context) {
        super(context, DATABASE_NAME , null, 1);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        db.execSQL(
                "create table Personas " +
                        "(Id integer primary key, CodPersona text, Nombre text, Huella1 blob,Huella2 blob, Huella3 blob, UNIQUE(CodPersona))"
        );
        db.execSQL(
                "create table Marcas " +
                        "(IdMarca integer primary key autoincrement, CodTrabajador text, NomTrabajador text, Fecha text, dia text, TipoMarca text, Latitud real, Longitud real, Enviado int)"
        );
        db.execSQL(
                "create table HuellasTrabajadores " +
                        "(IdHuella integer primary key autoincrement, CodTrabajador text, NomTrabajador text, Huella blob, Sincronizada integer)"
        );
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public ArrayList<Trabajadores> ConsultarTrabajadoresConMarca(String dia, String Tipo) {
        SQLiteDatabase db = this.getReadableDatabase();
        mydb = new DBHelper(ctxOriginal);
        ArrayList<Trabajadores> array_list = new ArrayList<Trabajadores>();
        Cursor res =  db.rawQuery( "select distinct CodTrabajador from Marcas where dia = '"+dia+"' and Tipomarca = '"+Tipo+"' ", null );
        Trabajadores  t = new Trabajadores();

        res.moveToFirst();


        while(res.isAfterLast() == false){
            String CodPersona = res.getString(res.getColumnIndex("CodTrabajador"));

            t = mydb.getSingleTrabajadorByCod(CodPersona);
            if (t.getIdTrabajador() != 0)
                array_list.add(t);
            res.moveToNext();
        }
        return array_list;

    }

    public boolean HasHuellas(String CodTrabajador){

        SQLiteDatabase db = this.getReadableDatabase();
        try {
            Cursor res = db.rawQuery("select * from HuellasTrabajadores where " + HUELLASTRABAJADORES_COLUMN_CODTRABAJADOR + " = '" + CodTrabajador + "'", null);

            return !res.isAfterLast();
        }
        catch(Exception e) {
            return false;
        }
    }

    public ArrayList<HuellasTrabajadores> getHuellas(boolean Sincronizadas){
        int sync = Sincronizadas ? 1 : 0;
        ArrayList<HuellasTrabajadores> array_list = new ArrayList<HuellasTrabajadores>();
        HuellasTrabajadores o = new HuellasTrabajadores();
        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from "+HUELLASTRABAJADORES_TABLE_NAME+" WHERE "+HUELLASTRABAJADORES_COLUMN_SINCRONIZADA+ "="+sync, null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            int Id = Integer.parseInt( res.getString(res.getColumnIndex(HUELLASTRABAJADORES_COLUMN_IDHUELLA)));
            String cod = res.getString(res.getColumnIndex(HUELLASTRABAJADORES_COLUMN_CODTRABAJADOR));
            String nom = res.getString(res.getColumnIndex("NomTrabajador"));
            byte[] huella = res.getBlob(res.getColumnIndex(HUELLASTRABAJADORES_COLUMN_HUELLA));

            o = new HuellasTrabajadores(Id,cod,nom,huella,Sincronizadas);
            array_list.add(o);
            res.moveToNext();
        }
        return array_list;

    }

    public ArrayList<Trabajadores> getTrabajadoresConHuellaByCuadrilla(int IdCuadrilla){

        mydb = new DBHelper(ctxOriginal);
        ArrayList<Trabajadores> allTrab = mydb.getTrabajadoresPorCuadrilla(IdCuadrilla), arrayList = new ArrayList<Trabajadores>();


        for (  Trabajadores t : allTrab){
            if (HasHuellas(t.getCodTrabajador()))
                arrayList.add(t);
        }
        return  arrayList;

    }

    public ArrayList<Trabajadores> getTrabajadoresSinHuellaByCuadrilla(int IdCuadrilla){

        mydb = new DBHelper(ctxOriginal);
        ArrayList<Trabajadores> allTrab = mydb.getTrabajadoresPorCuadrilla(IdCuadrilla), arrayList = new ArrayList<Trabajadores>();


        for (  Trabajadores t : allTrab){
            if (!HasHuellas(t.getCodTrabajador()))
                arrayList.add(t);
        }
        return  arrayList;

    }

    public ArrayList<Trabajadores> getTrabajadoresConHuella(){
        mydb = new DBHelper(ctxOriginal);
        ArrayList<Trabajadores> allTrab = mydb.getTrabajadores(), arrayList = new ArrayList<Trabajadores>();


        for (  Trabajadores t : allTrab){
            if (HasHuellas(t.getCodTrabajador()))
                arrayList.add(t);
        }
        return  arrayList;

    }

    public boolean InsertHuellas(ArrayList<HuellasTrabajadores> huellas){
        int cont = 0;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            for (HuellasTrabajadores h:
                    huellas) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(HUELLASTRABAJADORES_COLUMN_CODTRABAJADOR, h.getCodTrabajador());
                contentValues.put("NomTrabajador", h.getNomTrabajador());

                contentValues.put(HUELLASTRABAJADORES_COLUMN_HUELLA, h.getHuellas());
                contentValues.put(HUELLASTRABAJADORES_COLUMN_SINCRONIZADA, h.isSincronizada());

                cont += db.insert(HUELLASTRABAJADORES_TABLE_NAME, null, contentValues);
            }
            return cont > 0;
        }
        catch (Exception e)
        {
            return false;
        }
    }

    public boolean updateHuellas (ArrayList<HuellasTrabajadores> huellas) {
        SQLiteDatabase db = this.getWritableDatabase();
        for (HuellasTrabajadores h: huellas
                ) {

            ContentValues cv = new ContentValues();
            cv.put(HUELLASTRABAJADORES_COLUMN_SINCRONIZADA,h.isSincronizada());
            cv.put(HUELLASTRABAJADORES_COLUMN_CODTRABAJADOR,h.getCodTrabajador());
            cv.put("NomTrabajador", h.getNomTrabajador());
            cv.put(HUELLASTRABAJADORES_COLUMN_HUELLA,h.getHuellas());
            String[] whereArgs = new String[] {String.valueOf(h.getIdHuella())};
            db.update(HUELLASTRABAJADORES_TABLE_NAME,cv,HUELLASTRABAJADORES_COLUMN_IDHUELLA+" = ?", whereArgs);
           /* String strSQL = "UPDATE " + HUELLASTRABAJADORES_TABLE_NAME + " SET " + HUELLASTRABAJADORES_COLUMN_SINCRONIZADA + "=" + h.isSincronizada() + ","+HUELLASTRABAJADORES_COLUMN_HUELLA+"=" + h.getHuellas() + " WHERE " + HUELLASTRABAJADORES_COLUMN_IDHUELLA + "=" + h.getIdHuella();

            db.execSQL(strSQL);*/
        }
        return true;
    }
    /*public ArrayList<HuellasTrabajadores> getTrabajadores() {

        ArrayList<HuellasTrabajadores> array_list = new ArrayList<HuellasTrabajadores>();
        HuellasTrabajadores o = new HuellasTrabajadores();
        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from Hu", null );
        res.moveToFirst();

        DBHelper mydb = new DBHelper(ctxOriginal);


        while(res.isAfterLast() == false){
            String CodTrabajador = res.getString(res.getColumnIndex("CodPersona"));

            byte[] h1 = res.getBlob(res.getColumnIndex("Huella1"));
            byte[] h2 = res.getBlob(res.getColumnIndex("Huella2"));
            byte[] h3 = res.getBlob(res.getColumnIndex("Huella3"));
            Trabajadores t = mydb.getSingleTrabajadorByCod(CodTrabajador);
            o = new HuellasTrabajadores(0,t.getCodTrabajador(),h1,false);
            array_list.add(o);

            o = new HuellasTrabajadores(0,t.getCodTrabajador(),h2,false);
            array_list.add(o);

            o = new HuellasTrabajadores(0,t.getCodTrabajador(),h3,false);
            array_list.add(o);

            res.moveToNext();
        }
        return array_list;
    }*/

}
