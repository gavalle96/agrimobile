package com.grupocassa.agrimobile.CASSAUTILS;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.view.View;

import com.grupocassa.agrimobile.MaestrosActivities.DownloadActivity;
import com.grupocassa.agrimobile.MaestrosActivities.ObtenerMaestrosActivity;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import com.jcraft.jsch.UserInfo;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.URL;
import java.net.URLConnection;
import org.apache.commons.net.ftp.*;

/**
 * Created by gerardo.valle on 23/10/2017.
 */

public class AppUpdater extends AsyncTask<String,String,File> {

    DownloadActivity ctx;
    private static final String SFTPWORKINGDIR = "/";
    private int prgrss;
    public static String user = "";
    public static String pass = "";
    private String host = "ftp.grupocassa.org";//"199.204.248.107";
    private int portNum = 21;
    //private String fileName = "AGRIMOBILEPRDPUB.apk";
    //private String fileName = "AGRIMOBILE.apk";

    public AppUpdater(DownloadActivity activity){
        ctx = activity;

    }

    @Override
    public File doInBackground(String... sUrl) {
        File f = null;
        try {
          f = Downloader(DBHelper.fileName);

        } catch (Exception e) {
            Log.e("YourApp", "Well that didn't work out so well...");
            Log.e("YourApp", e.getMessage());
        }
        return f;
    }

    // begin the installation by opening the resulting file
    @Override
    protected void onPostExecute(File path) {
        ObtenerMaestrosActivity.updater = null;
        ctx.txtMsg.setText("Iniciando Instalación...");
        ctx.pgBar.setVisibility(View.GONE);
        ctx.pgCircular.setVisibility(View.VISIBLE);
        ctx.finish();
        Intent i = new Intent();
        i.setAction(Intent.ACTION_VIEW);
        i.setDataAndType(Uri.fromFile(path), "application/vnd.android.package-archive");
        Log.d("Lofting", "About to install new .apk");
        this.ctx.startActivity(i);
    }

    public File Downloader(String fileName) {

        FTPClient m_client = new FTPClient();
        File newFile = null;
        try {
            String rStr ="";
            m_client = new FTPClient();
            m_client.connect(InetAddress.getByName(host));
            rStr =   m_client.getReplyString();
            System.out.println("Connecting to host " + host);
            m_client.login(user, pass);
            rStr =   m_client.getReplyString();
            System.out.println("User " + user + " login OK");
            //System.out.println(m_client.mess);
           // m_client.changeWorkingDirectory(SFTPWORKINGDIR);
            //System.out.println("Directory: " + sDir);
            m_client.setFileType(FTP.BINARY_FILE_TYPE);
            rStr =   m_client.getReplyString();
            m_client.enterLocalPassiveMode();
            File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);//Environment.getDataDirectory().getAbsoluteFile();//ctx.getFilesDir(); //Environment.getRootDirectory();
            newFile = new File(path, fileName);

            if (newFile.exists()) {
                //newFile.createNewFile();
                newFile.delete();
            }

            FileOutputStream desFileStream = null;
            boolean success = false;
            try {

                long fileSize = 0;
                FTPFile[] files = m_client.listFiles();
                for ( FTPFile ftpFile : files) {
                    if (ftpFile.isFile() && ftpFile.getName().toString().equals(fileName)) {

                        fileSize = ftpFile.getSize();
                    }
                }

                desFileStream = new FileOutputStream(newFile);
                InputStream input = m_client.retrieveFileStream(fileName);
                byte[] data = new byte[1024];
                int count ;
                long total = 0;
                int progreso = 0;
                while ((count = input.read(data)) != -1)
                {
                    total += count;
                    desFileStream.write(data, 0, count);
                    progreso = (int) (total * 100 / fileSize);
                    prgrss = progreso;
                    if (progreso>0) {
                        Log.d("Progreso de descarga: ", String.valueOf(progreso));
                    }
                    ctx.pgBar.setProgress(progreso);
                }




            }catch (Exception e){
                Log.d("Error en FTP: ",e.toString());
            }
            finally {
                if (desFileStream != null) {
                    desFileStream.close();
                }
            }

            //m_client.storeFile(fileName, buffIn)
            m_client.logout();
            m_client.disconnect();

            System.out.println("Success.");
        } catch (Exception ex) {
            System.out.println("Error: " + ex.toString());
        }
        finally {
            if (m_client.isConnected()){
                try {
                    m_client.logout();
                    m_client.disconnect();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return newFile;


    }

    public int ObtenerProgreso(){
        return this.prgrss;
    }


}

