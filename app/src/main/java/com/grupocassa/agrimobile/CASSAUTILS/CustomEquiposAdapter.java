package com.grupocassa.agrimobile.CASSAUTILS;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.grupocassa.agrimobile.CLS.Equipos;
import com.grupocassa.agrimobile.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gerardo.valle on 27/7/2017.
 */

public class CustomEquiposAdapter extends ArrayAdapter<Equipos> implements Filterable {
    private List<Equipos> entries;
    private ArrayList<Equipos> orig;
    private Activity activity;
    ArrayFilter myFilter;
    DBHelper mydb;

    public CustomEquiposAdapter(Activity a, int textViewResourceId, ArrayList<Equipos> entries) {
        super(a, textViewResourceId, entries);
        this.entries = entries;
        this.activity = a;
        mydb = new DBHelper(activity);
    }

    public static class ViewHolder{
        public TextView tv_ac_name;
    }

    @Override
    public int getCount(){
        return entries!=null ? entries.size() : 0;
    }

    @Override
    public Equipos getItem(int index) {
        return entries.get(index);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder holder;
        if (v == null) {
            LayoutInflater vi =
                    (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.autocompleteitem, null);
            holder = new ViewHolder();
            holder.tv_ac_name = (TextView) v.findViewById(R.id.txtautocomplete);
            v.setTag(holder);
        }
        else
            holder=(ViewHolder)v.getTag();

        final Equipos custom = entries.get(position);
        if (custom != null) {
            holder.tv_ac_name.setText(custom.getCodEquipo()+" - "+custom.getNomEquipo());
        }
        return v;
    }

    @Override
    public Filter getFilter() {
        if (myFilter == null){
            myFilter = new ArrayFilter();
        }
        return myFilter;
    }
    //POSIBLE BUG////////////////////////////////////////////////////////////////////7
    @Override
    public String toString() {
        String temp = getClass().getName();
        return temp;
    }
    //////////////////////////////////////////////////////////////////////////////7
    private class ArrayFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            //if(orig == null)  orig = new ArrayList<Equipos>(entries);
            if (constraint != null && constraint.length() >= 3) {
                ArrayList<Equipos> resultsSuggestions = new ArrayList<Equipos>();
                /*for (int i = 0; i < orig.size(); i++) {
                    if(orig.get(i).getCodEquipo().toLowerCase().contains(constraint.toString().toLowerCase()) || orig.get(i).getNomEquipo().toLowerCase().contains(constraint.toString().toLowerCase())){
                        resultsSuggestions.add(orig.get(i));
                    }
                }*/
                resultsSuggestions = mydb.ConsultaEquipos(String.valueOf( constraint));

                if (resultsSuggestions == null) resultsSuggestions = new ArrayList<Equipos>();
                results.values = resultsSuggestions;
                results.count = resultsSuggestions.size();

            }
            else {
                ArrayList <Equipos> list = new ArrayList <Equipos>(orig);
                results.values = list;
                results.count = list.size();
            }
            return results;
        }

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            return ((Equipos) resultValue).getCodEquipo().trim();//+" - "+((Equipos) resultValue).getNomEquipo().trim();
        }
        @Override
        @SuppressWarnings("unchecked")
        protected void publishResults(CharSequence constraint, FilterResults results) {
            clear();
            ArrayList<Equipos> newValues = (ArrayList<Equipos>) results.values;
            if(newValues !=null) {
                for (int i = 0; i < newValues.size(); i++) {
                    add(newValues.get(i));
                }
                if(results.count>0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }

        }

    }

}
