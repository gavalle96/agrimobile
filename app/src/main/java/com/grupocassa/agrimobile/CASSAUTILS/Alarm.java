package com.grupocassa.agrimobile.CASSAUTILS;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Build;
import android.os.PowerManager;
import android.os.Vibrator;
import android.widget.Toast;

import com.grupocassa.agrimobile.CLS.Parametros;
import com.grupocassa.agrimobile.MainActivity;
import com.grupocassa.agrimobile.Splash;

import java.util.Calendar;

/**
 * Created by gerardo.valle on 21/3/2018.
 */

public class Alarm extends BroadcastReceiver
{

    @Override
    public void onReceive(Context context, Intent intent)
    {
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "");
        wl.acquire();


        // Put here YOUR code.
        DBHelper mydb = new DBHelper(context);
        Parametros ps = mydb.getSingleParametro(8);


        if (ps == null)  ps = new Parametros(8,"3000");


        if ( ps.getValor().equals("3000")) {
            if (mydb.deleteParametro(4) > 0) {

                mydb.deleteParametro(8);
                mydb.deleteAplicacion();

  /*              Intent CloseInt = new Intent(context, Splash.class);
                CloseInt.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                CloseInt.putExtra("CloseApp", true);
                startActivity(CloseInt);
*/            }
            Toast.makeText(context, "CERRANDO SESIÓN AGRIMOBILE", Toast.LENGTH_LONG).show();
            restartApp(context);

        }
        /*Intent i = context.getPackageManager()
                .getLaunchIntentForPackage( context.getPackageName() );
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        context.startActivity(i);
        */wl.release();
    }

    public void setAlarm(Context context)
    {
        AlarmManager am =( AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(context, Alarm.class);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, 0);

        Calendar calendar = Calendar.getInstance();
        Calendar current = Calendar.getInstance();

        calendar.add(Calendar.DAY_OF_MONTH,1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);

        if(calendar.getTimeInMillis() < current.getTimeInMillis()){
            calendar.add(Calendar.DAY_OF_MONTH,1);
        }
        am.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY , pi); // Millisec * Second * Minute
    }

    public void cancelAlarm(Context context)
    {
        Intent intent = new Intent(context, Alarm.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
    }

    private void restartApp(Context context) {
        //Intent intent = new Intent(context, Splash.class);
        Intent intent = new Intent(context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("EXIT", true);
        int mPendingIntentId = 1;
        PendingIntent mPendingIntent = PendingIntent.getActivity(context, mPendingIntentId, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager mgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
        System.exit(0);
/*
        Intent intent = new Intent(context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("EXIT", true);*/
    }
}
