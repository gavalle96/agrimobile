package com.grupocassa.agrimobile.CASSAUTILS;

/**
 * Created by gerardo.valle on 19/7/2017.
 */

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.grupocassa.agrimobile.CLS.OS;
import com.grupocassa.agrimobile.R;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by gerardo.valle on 19/7/2017.
 */


public class CustomOSAdapter extends ArrayAdapter<OS> implements Filterable {
    private List<OS> entries;
    private ArrayList<OS> orig;
    private Activity activity;
    private ArrayFilter myFilter;

    public CustomOSAdapter(Activity a, int textViewResourceId, ArrayList<OS> entries) {
        super(a, textViewResourceId, entries);
        this.entries = entries;
        this.activity = a;
    }

    public static class ViewHolder{
        public TextView tv_ac_name;
    }

    @Override
    public int getCount(){
        return entries!=null ? entries.size() : 0;
    }

    @Override
    public OS getItem(int index) {
        return entries.get(index);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder holder;
        if (v == null) {
            LayoutInflater vi =
                    (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.autocompleteitem, null);
            holder = new ViewHolder();
            holder.tv_ac_name = (TextView) v.findViewById(R.id.txtautocomplete);
            v.setTag(holder);
        }
        else
            holder=(ViewHolder)v.getTag();

        final OS custom = entries.get(position);
        if (custom != null) {
            holder.tv_ac_name.setText(custom.getIdOS()+" - "+custom.getDsActividad());
        }
        return v;
    }

    @Override
    public Filter getFilter() {
        if (myFilter == null){
            myFilter = new ArrayFilter();
        }
        return myFilter;
    }
    //POSIBLE BUG////////////////////////////////////////////////////////////////////7
    @Override
    public String toString() {
        String temp = getClass().getName();
        return temp;
    }
    //////////////////////////////////////////////////////////////////////////////7
    private class ArrayFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if (orig == null)
                orig = new ArrayList<OS>(entries);
            if (constraint != null && constraint.length() != 0) {
                ArrayList<OS> resultsSuggestions = new ArrayList<OS>();
                for (int i = 0; i < orig.size(); i++) {
                    String sOS = orig.get(i).getIdOS()+"";
                    if(sOS.toLowerCase().startsWith(constraint.toString().toLowerCase()) || orig.get(i).getDsActividad().toLowerCase().contains(constraint.toString().toLowerCase())){
                        resultsSuggestions.add(orig.get(i));
                    }
                }

                results.values = resultsSuggestions;
                results.count = resultsSuggestions.size();

            }
            else {
                ArrayList <OS> list = new ArrayList <OS>(orig);
                results.values = list;
                results.count = list.size();
            }
            return results;
        }

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            return ((OS) resultValue).getIdOS()+" - "+((OS) resultValue).getDsActividad();
        }
        @Override
        @SuppressWarnings("unchecked")
        protected void publishResults(CharSequence constraint, FilterResults results) {
            clear();
            ArrayList<OS> newValues = (ArrayList<OS>) results.values;
            if(newValues !=null) {
                for (int i = 0; i < newValues.size(); i++) {
                    add(newValues.get(i));
                }
                if(results.count>0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }

        }

    }
}
