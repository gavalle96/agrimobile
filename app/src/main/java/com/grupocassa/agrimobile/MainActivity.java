package com.grupocassa.agrimobile;

import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.google.android.material.navigation.NavigationView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;


import com.grupocassa.agrimobile.ApuntesActivities.MainApuntesActivity;
import com.grupocassa.agrimobile.ApuntesActivities.reporteActivity;
import com.grupocassa.agrimobile.CASSAUTILS.Alarm;
import com.grupocassa.agrimobile.CASSAUTILS.DBHelper;
import com.grupocassa.agrimobile.CLS.Parametros;
import com.grupocassa.agrimobile.MaestrosActivities.DataMaestraMenuActivity;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {


    private DBHelper mydb;
    ImageButton btnDataMaestra, btnApuntes;
    TextView tvusr;
    private PendingIntent pendingIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Alarm alarm = new Alarm();
        alarm.setAlarm(this);

        if (getIntent().getBooleanExtra("EXIT", false)) {
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();



        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        tvusr = (TextView) navigationView.getHeaderView(0).findViewById(R.id.txtUsrShow);
        mydb = new DBHelper(this);

        Parametros usr = mydb.getSingleParametro(5);
        if(usr != null){
            tvusr.setText("Bienvenido "+usr.getValor());
        }
        else{
            tvusr.setText("Bienvenido");
        }


        btnDataMaestra = (ImageButton) findViewById(R.id.btnDataMaestra);
        btnDataMaestra.setOnClickListener(this);

        btnApuntes = (ImageButton) findViewById(R.id.btnApuntes);
        btnApuntes.setOnClickListener(this);
        setTitle(R.string.versionapp);
        //start();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent about = new Intent(MainActivity.this,AboutActivity.class);
            startActivity(about);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_cerrarsesion) {
            // Handle the camera action
            DestroySession();
        } else if (id == R.id.nav_salir) {
            Salir();

        } else if (id == R.id.nav_datamaestra) {
            AbrirDataMaestra();
        } else if (id == R.id.nav_apuntes) {
            AbrirApuntes();
        } else if (id == R.id.nav_ReporteMO) {
            Intent i = new Intent(MainActivity.this,reporteActivity.class);
            startActivity(i);

        }/* else if (id == R.id.nav_send) {

        }*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void DestroySession(){
        Intent i = new Intent(MainActivity.this, LoginActivity.class);
        mydb.deleteParametro(4);
            mydb.deleteParametro(5);
            mydb.deleteParametro(6);
            mydb.deleteAplicacion();
            startActivity(i);
            finish();

    }
    public void Salir(){
        finish();
    }

    public void AbrirDataMaestra(){
        Intent i = new Intent(MainActivity.this, DataMaestraMenuActivity.class);
        startActivity(i);
    }

    public void AbrirApuntes(){
        Intent i = new Intent(MainActivity.this, MainApuntesActivity.class);
        startActivity(i);
    }

    @Override
    public void onClick(View v) {
        if(v==btnDataMaestra){
                AbrirDataMaestra();

        }
        if (v == btnApuntes){
            AbrirApuntes();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        DBHelper.activo = true;
        //Toast.makeText(this,"ACTIVO",Toast.LENGTH_LONG).show();
    }


    @Override
    protected void onDestroy() {
        DBHelper.activo = false;
        //Toast.makeText(this,"STOP",Toast.LENGTH_LONG).show();

        super.onDestroy();
    }
    @Override
    protected void onStop() {
        super.onStop();

    }



}
