package com.grupocassa.agrimobile.CLS;

/**
 * Created by gerardo.valle on 13/1/2018.
 */

public class TiposDocumento {
    private int IdTipDocInterno;
    private String BkTipDocInterno;
    private String DsTipDocInteno;

    public TiposDocumento() {
    }

    public TiposDocumento(int idTipDocInterno, String bkTipDocInterno, String dsTipDocInteno) {
        IdTipDocInterno = idTipDocInterno;
        BkTipDocInterno = bkTipDocInterno;
        DsTipDocInteno = dsTipDocInteno;
    }

    public int getIdTipDocInterno() {
        return IdTipDocInterno;
    }

    public void setIdTipDocInterno(int idTipDocInterno) {
        IdTipDocInterno = idTipDocInterno;
    }

    public String getBkTipDocInterno() {
        return BkTipDocInterno;
    }

    public void setBkTipDocInterno(String bkTipDocInterno) {
        BkTipDocInterno = bkTipDocInterno;
    }

    public String getDsTipDocInteno() {
        return DsTipDocInteno;
    }

    public void setDsTipDocInteno(String dsTipDocInteno) {
        DsTipDocInteno = dsTipDocInteno;
    }

    @Override
    public String toString() {
        return getDsTipDocInteno();
    }
}
