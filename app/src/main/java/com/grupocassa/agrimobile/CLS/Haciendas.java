package com.grupocassa.agrimobile.CLS;

import java.util.ArrayList;

/**
 * Created by gerardo.valle on 13/7/2017.
 */

public class Haciendas {
    private int IdHacienda;
    private String BkHacienda;
    private String DsHacienda;
    private boolean MarcaBiometrico;
    private ArrayList<OS> mOS;

    public int getIdPluvio() {
        return IdPluvio;
    }

    public void setIdPluvio(int idPluvio) {
        IdPluvio = idPluvio;
    }

    private int IdPluvio;

    public Haciendas() {
    }

    public Haciendas(int IdHacienda, String BkHacienda, String DsHacienda, Boolean marcaBiometrico) {
        this.IdHacienda = IdHacienda;
        this.BkHacienda = BkHacienda;
        this.DsHacienda = DsHacienda;
        this.mOS = new ArrayList<OS>();
        this.MarcaBiometrico = marcaBiometrico;

    }

    public int getIdHacienda(){
        return this.IdHacienda;
    }

    public String getBkHacienda(){
        return this.BkHacienda;
    }

    public String getDsHacienda(){
        return this.DsHacienda;
    }

    public void setIdHacienda(int IdHacienda ){
        this.IdHacienda = IdHacienda;
    }

    public void setBkHacienda(String BkHacienda){
        this.BkHacienda = BkHacienda;
    }

    public void setDsHacienda(String DsHacienda){
        this.DsHacienda = DsHacienda;
    }

    public String toString(){
        return  this.getBkHacienda() + " - "+this.getDsHacienda();
    }

    public boolean isMarcaBiometrico() {
        return MarcaBiometrico;
    }

    public void setMarcaBiometrico(boolean marcaBiometrico) {
        MarcaBiometrico = marcaBiometrico;
    }

    public ArrayList<OS> getmOS() {
        return mOS;
    }

    public void setmOS(ArrayList<OS> mOS) {
        this.mOS = mOS;
    }
}