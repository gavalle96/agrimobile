package com.grupocassa.agrimobile.CLS;

/**
 * Created by gerardo.valle on 17/7/2017.
 */

public class ApuntesMO extends Apuntamiento {

    private int IdApMO;
    private int IdCuadrilla;
    private int IdTrabajador;
    private float QTD_PAGO;
    private String DsCuadrilla, Trabajador;

    public ApuntesMO() {
        IdApMO = 0;
        IdCuadrilla = 0;
        IdTrabajador = 0;
        QTD_PAGO = 0;
        DsCuadrilla ="";
        Trabajador ="";
    }

    public ApuntesMO(int idOS, String fechaApunte, int idRecurso, float area, String usuario, String fechaDigita, String idDev, String fechaIni, String fechaFin, String latitud, String longitud,String observacion, int idApMO, int idCuadrilla, int idTrabajador, float QTD_PAGO) {
        super(idOS, fechaApunte, idRecurso, area, usuario, fechaDigita, idDev, fechaIni, fechaFin, latitud, longitud, observacion);
        IdApMO = idApMO;
        IdCuadrilla = idCuadrilla;
        IdTrabajador = idTrabajador;
        this.QTD_PAGO = QTD_PAGO;
        DsCuadrilla = "";
        Trabajador = "";
    }

    public int getIdApMO() {
        return IdApMO;
    }

    public void setIdApMO(int idApMO) {
        IdApMO = idApMO;
    }

    public int getIdCuadrilla() {
        return IdCuadrilla;
    }

    public void setIdCuadrilla(int idCuadrilla) {
        IdCuadrilla = idCuadrilla;
    }

    public int getIdTrabajador() {
        return IdTrabajador;
    }

    public void setIdTrabajador(int idTrabajador) {
        IdTrabajador = idTrabajador;
    }

    public float getQTD_PAGO() {
        return QTD_PAGO;
    }

    public void setQTD_PAGO(float QTD_PAGO) {
        this.QTD_PAGO = QTD_PAGO;
    }

    public String getDsCuadrilla() {
        return DsCuadrilla;
    }

    public void setDsCuadrilla(String dsCuadrilla) {
        DsCuadrilla = dsCuadrilla;
    }

    public String getTrabajador() {
        return Trabajador;
    }

    public void setTrabajador(String trabajador) {
        Trabajador = trabajador;
    }
}
