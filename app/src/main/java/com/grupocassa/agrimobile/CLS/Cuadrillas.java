package com.grupocassa.agrimobile.CLS;

/**
 * Created by gerardo.valle on 17/7/2017.
 */

public class Cuadrillas {
    private int IdCuadrilla;
    private String BkCuadrilla;
    private String DsCuadrilla;

    public Cuadrillas() {
        IdCuadrilla = 0;
        BkCuadrilla = "";
        DsCuadrilla = "";
    }

    public Cuadrillas(int idCuadrilla, String bkCuadrilla, String dsCuadrilla) {
        IdCuadrilla = idCuadrilla;
        BkCuadrilla = bkCuadrilla;
        DsCuadrilla = dsCuadrilla;
    }

    public int getIdCuadrilla() {
        return IdCuadrilla;
    }

    public void setIdCuadrilla(int idRecurso) {
        IdCuadrilla = idRecurso;
    }

    public String getBkCuadrilla() {
        return BkCuadrilla;
    }

    public void setBkCuadrilla(String bkCuadrilla) {
        BkCuadrilla = bkCuadrilla;
    }

    public String getDsCuadrilla() {
        return DsCuadrilla;
    }

    public void setDsCuadrilla(String dsCuadrilla) {
        DsCuadrilla = dsCuadrilla;
    }
}
