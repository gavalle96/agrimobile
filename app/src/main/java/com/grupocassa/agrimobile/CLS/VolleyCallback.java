package com.grupocassa.agrimobile.CLS;

import org.json.JSONObject;

public interface VolleyCallback {

    boolean onSuccess();
    boolean onError();
    boolean onSuccess(String resp);
    boolean onError(String resp);
}
