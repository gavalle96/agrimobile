package com.grupocassa.agrimobile.CLS;

/**
 * Created by gerardo.valle on 17/7/2017.
 */


public class Parametros {
    private int IdParametro;
    private String Valor;

    public Parametros() {
    }

    public Parametros(int idParametro, String valor) {
        IdParametro = idParametro;
        Valor = valor;
    }

    public int getIdParametro() {
        return IdParametro;
    }

    public void setIdParametro(int idParametro) {
        IdParametro = idParametro;
    }

    public String getValor() {
        return Valor;
    }

    public void setValor(String valor) {
        Valor = valor;
    }
}
