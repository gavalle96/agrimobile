package com.grupocassa.agrimobile.CLS;

/**
 * Created by gerardo.valle on 17/7/2017.
 */

public class ApuntesMaquinaria extends Apuntamiento {
    private int IdApMaquinaria;
    private int IdTrabajador;
    private int IdTrabAyudante;
    private String CodEquipo;
    private String CodEquipoImplemento;
    private float HOR_INI;
    private float HOR_FIN;
    private int HOR_FALLA;
    private float ODO_INI;
    private float ODO_FIN;
    private int ODO_FALLA;
    private float MED_INI;
    private float MED_FIN;
    private int MED_FALLA;
    private float QTD_PAGO;
    private String Trabajador;
    private String Ayudante;
    private int Equipo1;
    private int Equipo2;
    private int Equipo3;
    private int Equipo4;

    private int IdTipDocInterno;
    private String NumDocInterno;
    private String eq1, eq2, eq3, eq4;

    private String Origen;
    private String Destino;
    private String Ruta;

    public ApuntesMaquinaria() {
        IdApMaquinaria = 0;
        IdTrabajador = 0;
        IdTrabAyudante = 0;
        CodEquipo = "";
        CodEquipoImplemento = "";
        HOR_INI = 0;
        HOR_FIN = 0;
        HOR_FALLA = 0;
        ODO_INI = 0;
        ODO_FIN = 0;
        ODO_FALLA = 0;
        MED_INI = 0;
        MED_FIN = 0;
        MED_FALLA = 0;
        QTD_PAGO = 0;
        Trabajador ="";

    }

    public ApuntesMaquinaria(int idOS, String fechaApunte, int idRecurso, float area, String usuario, String fechaDigita, String idDev, String fechaIni, String fechaFin, String latitud, String longitud,String observacion, int idApMaquinaria, int idTrabajador, int idTrabAyudante, String codEquipo, String codEquipoImplemento, float HOR_INI, float HOR_FIN, int HOR_FALLA, float ODO_INI, float ODO_FIN, int ODO_FALLA,float MED_INI,float MED_FIN,int MED_FALLA, float QTD_PAGO) {
        super(idOS, fechaApunte, idRecurso, area, usuario, fechaDigita, idDev, fechaIni, fechaFin, latitud, longitud, observacion);
        IdApMaquinaria = idApMaquinaria;
        IdTrabajador = idTrabajador;
        IdTrabAyudante  = idTrabAyudante;
        CodEquipo = codEquipo;
        CodEquipoImplemento = codEquipoImplemento;
        this.HOR_INI = HOR_INI;
        this.HOR_FIN = HOR_FIN;
        this.HOR_FALLA = HOR_FALLA;
        this.ODO_INI = ODO_INI;
        this.ODO_FIN = ODO_FIN;
        this.ODO_FALLA = ODO_FALLA;
        this.MED_INI =  MED_INI;
        this.MED_FIN =  MED_FIN;
        this.MED_FALLA = MED_FALLA;
        this.QTD_PAGO = QTD_PAGO;
        Trabajador = "";
    }

    public int getIdApMaquinaria() {
        return IdApMaquinaria;
    }

    public void setIdApMaquinaria(int idApMaquinaria) {
        IdApMaquinaria = idApMaquinaria;
    }

    public int getIdTrabajador() {
        return IdTrabajador;
    }

    public void setIdTrabajador(int idTrabajador) {
        IdTrabajador = idTrabajador;
    }

    public String getCodEquipo() {
        return CodEquipo;
    }

    public void setCodEquipo(String codEquipo) {
        CodEquipo = codEquipo;
    }

    public String getCodEquipoImplemento() {
        return CodEquipoImplemento;
    }

    public void setCodEquipoImplemento(String codEquipoImplemento) {
        CodEquipoImplemento = codEquipoImplemento;
    }

    public float getHOR_INI() {
        return HOR_INI;
    }

    public void setHOR_INI(float HOR_INI) {
        this.HOR_INI = HOR_INI;
    }

    public float getHOR_FIN() {
        return HOR_FIN;
    }

    public void setHOR_FIN(float HOR_FIN) {
        this.HOR_FIN = HOR_FIN;
    }

    public int getHOR_FALLA() {
        return HOR_FALLA;
    }

    public void setHOR_FALLA(int HOR_FALLA) {
        this.HOR_FALLA = HOR_FALLA;
    }

    public float getODO_INI() {
        return ODO_INI;
    }

    public void setODO_INI(float ODO_INI) {
        this.ODO_INI = ODO_INI;
    }

    public float getODO_FIN() {
        return ODO_FIN;
    }

    public void setODO_FIN(float ODO_FIN) {
        this.ODO_FIN = ODO_FIN;
    }

    public int getODO_FALLA() {
        return ODO_FALLA;
    }

    public void setODO_FALLA(int ODO_FALLA) {
        this.ODO_FALLA = ODO_FALLA;
    }

    public float getQTD_PAGO() {
        return QTD_PAGO;
    }

    public void setQTD_PAGO(float QTD_PAGO) {
        this.QTD_PAGO = QTD_PAGO;
    }

    public float getMED_INI() {
        return MED_INI;
    }

    public void setMED_INI(float MED_INI) {
        this.MED_INI = MED_INI;
    }

    public float getMED_FIN() {
        return MED_FIN;
    }

    public void setMED_FIN(float MED_FIN) {
        this.MED_FIN = MED_FIN;
    }

    public int getMED_FALLA() {
        return MED_FALLA;
    }

    public void setMED_FALLA(int MED_FALLA) {
        this.MED_FALLA = MED_FALLA;
    }

    public String getTrabajador() {
        return Trabajador;
    }

    public void setTrabajador(String trabajador) {
        Trabajador = trabajador;
    }

    public int getIdTrabAyudante() {
        return IdTrabAyudante;
    }

    public void setIdTrabAyudante(int idTrabAyudante) {
        IdTrabAyudante = idTrabAyudante;
    }

    public String getAyudante() {
        return Ayudante;
    }

    public void setAyudante(String ayudante) {
        Ayudante = ayudante;
    }

    public int getEquipo1() {
        return Equipo1;
    }

    public void setEquipo1(int equipo1) {
        Equipo1 = equipo1;
    }

    public int getEquipo2() {
        return Equipo2;
    }

    public void setEquipo2(int equipo2) {
        Equipo2 = equipo2;
    }

    public int getEquipo3() {
        return Equipo3;
    }

    public void setEquipo3(int equipo3) {
        Equipo3 = equipo3;
    }

    public int getEquipo4() {
        return Equipo4;
    }

    public void setEquipo4(int equipo4) {
        Equipo4 = equipo4;
    }

    public int getIdTipDocInterno() {
        return IdTipDocInterno;
    }

    public void setIdTipDocInterno(int idTipDocInterno) {
        IdTipDocInterno = idTipDocInterno;
    }

    public String getNumDocInterno() {
        return NumDocInterno;
    }

    public void setNumDocInterno(String numDocInterno) {
        NumDocInterno = numDocInterno;
    }

    public String getEq1() {
        return eq1;
    }

    public void setEq1(String eq1) {
        this.eq1 = eq1;
    }

    public String getEq2() {
        return eq2;
    }

    public void setEq2(String eq2) {
        this.eq2 = eq2;
    }

    public String getEq3() {
        return eq3;
    }

    public void setEq3(String eq3) {
        this.eq3 = eq3;
    }

    public String getEq4() {
        return eq4;
    }

    public void setEq4(String eq4) {
        this.eq4 = eq4;
    }

    public String getOrigen() {
        return Origen;
    }

    public void setOrigen(String origen) {
        Origen = origen;
    }

    public String getDestino() {
        return Destino;
    }

    public void setDestino(String destino) {
        Destino = destino;
    }

    public String getRuta() {
        return Ruta;
    }

    public void setRuta(String ruta) {
        Ruta = ruta;
    }
}
