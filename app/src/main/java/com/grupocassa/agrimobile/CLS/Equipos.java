package com.grupocassa.agrimobile.CLS;

/**
 * Created by gerardo.valle on 13/1/2018.
 */

public class Equipos {
    private int IdEquipo;
    private String CodEquipo;
    private String NomEquipo;
    private String PropEquipo;
    private int Id_SubFlota;

    public int getId_SubFlota() {
        return Id_SubFlota;
    }

    public void setId_SubFlota(int id_SubFlota) {
        Id_SubFlota = id_SubFlota;
    }

    public Equipos() {
    }

    public Equipos(int idEquipo, String codEquipo, String nomEquipo, String propEquipo, int id_SubFlota) {
        IdEquipo = idEquipo;
        CodEquipo = codEquipo;
        NomEquipo = nomEquipo;
        PropEquipo = propEquipo;
        Id_SubFlota = id_SubFlota;
    }

    public int getIdEquipo() {
        return IdEquipo;
    }

    public void setIdEquipo(int idEquipo) {
        IdEquipo = idEquipo;
    }

    public String getCodEquipo() {
        return CodEquipo;
    }

    public void setCodEquipo(String codEquipo) {
        CodEquipo = codEquipo;
    }

    public String getNomEquipo() {
        return NomEquipo;
    }

    public void setNomEquipo(String nomEquipo) {
        NomEquipo = nomEquipo;
    }

    public String getPropEquipo() {
        return PropEquipo;
    }

    public void setPropEquipo(String propEquipo) {
        PropEquipo = propEquipo;
    }
}
