package com.grupocassa.agrimobile.CLS;

/**
 * Created by gerardo.valle on 16/01/2019.
 */

public class ApuntesArea {
    int IdApArea;
    int IdOS;
    String Fecha;
    float Area;
    boolean Integrado;

    public ApuntesArea() {
    }

    public ApuntesArea(int idApArea, int idOS, String fecha, float area, boolean integrado) {
        IdApArea = idApArea;
        IdOS = idOS;
        Fecha = fecha;
        Area = area;
        Integrado = integrado;
    }

    public int getIdApArea() {
        return IdApArea;
    }

    public void setIdApArea(int idApArea) {
        IdApArea = idApArea;
    }

    public int getIdOS() {
        return IdOS;
    }

    public void setIdOS(int idOS) {
        IdOS = idOS;
    }

    public String getFecha() {
        return Fecha;
    }

    public void setFecha(String fecha) {
        Fecha = fecha;
    }

    public float getArea() {
        return Area;
    }

    public void setArea(float area) {
        Area = area;
    }

    public boolean isIntegrado() {
        return Integrado;
    }

    public void setIntegrado(boolean integrado) {
        Integrado = integrado;
    }
}
