package com.grupocassa.agrimobile.CLS;

/**
 * Created by gerardo.valle on 14/7/2017.
 */

public class OSRecursos {
    private int IdOS;
    private int IdRecurso;
    private String BkRecurso;
    private String DsRecurso;
    private String BkTipRecurso;
    private String UM;
    private float CantidadPend;
    private float CantidadApunt;
    private float AreaApunt;
    private Boolean Ayudante;

    public Boolean getRequiereImplemento() {
        return RequiereImplemento;
    }

    public void setRequiereImplemento(Boolean requiereImplemento) {
        RequiereImplemento = requiereImplemento;
    }

    public int getIdSubFlota() {
        return IdSubFlota;
    }

    public void setIdSubFlota(int idSubFlota) {
        IdSubFlota = idSubFlota;
    }

    private Boolean RequiereImplemento;
    private int IdSubFlota;

    public OSRecursos() {
        IdOS = 0;
        IdRecurso = 0;
        BkRecurso = "";
        DsRecurso = "";
        BkTipRecurso = "";
        CantidadApunt =0;
        CantidadPend = 0;
        AreaApunt = 0;
        UM ="";
        Ayudante = false;
        RequiereImplemento = false;
        IdSubFlota = 0;
    }

    public OSRecursos(int idOS, int idRecurso, String bkRecurso, String dsRecurso, String bkTipRecurso, String uM, float cantidadPend, float cantidadApunt, float areaApunt, Boolean ayudante
    , Boolean requiereImplemento, int idSubFlota) {
        IdOS = idOS;
        IdRecurso = idRecurso;
        BkRecurso = bkRecurso;
        DsRecurso = dsRecurso;
        BkTipRecurso = bkTipRecurso;
        UM = uM;
        CantidadPend = cantidadPend;
        CantidadApunt = cantidadApunt;
        AreaApunt = areaApunt;
        Ayudante = ayudante;
        RequiereImplemento = requiereImplemento;
        IdSubFlota = idSubFlota;
    }

    public int getIdOS() {
        return IdOS;
    }

    public void setIdOS(int idOS) {
        IdOS = idOS;
    }

    public int getIdRecurso() {
        return IdRecurso;
    }

    public void setIdRecurso(int idRecurso) {
        IdRecurso = idRecurso;
    }

    public String getBkRecurso() {
        return BkRecurso;
    }

    public void setBkRecurso(String bkRecurso) {
        BkRecurso = bkRecurso;
    }

    public String getDsRecurso() {
        return DsRecurso;
    }

    public void setDsRecurso(String dsRecurso) {
        DsRecurso = dsRecurso;
    }

    public String getBkTipRecurso() {
        return BkTipRecurso;
    }

    public void setBkTipRecurso(String bkTipRecurso) {
        BkTipRecurso = bkTipRecurso;
    }

    public String getUM() {

        return UM;
    }

    public void setUM(String UM) {
        this.UM = UM;
    }

    public float getCantidadPend() {
        return CantidadPend;
    }

    public void setCantidadPend(float cantidadPend) {
        CantidadPend = cantidadPend;
    }

    public float getCantidadApunt() {
        return CantidadApunt;
    }

    public void setCantidadApunt(float cantidadApunt) {
        CantidadApunt = cantidadApunt;
    }

    public float getAreaApunt() {
        return AreaApunt;
    }

    public void setAreaApunt(float areaApunt) {
        AreaApunt = areaApunt;
    }

    public Boolean getAyudante() {
        return Ayudante;
    }

    public void setAyudante(Boolean ayudante) {
        Ayudante = ayudante;
    }

    @Override
    public String toString() {
        return getDsRecurso();
    }
}
