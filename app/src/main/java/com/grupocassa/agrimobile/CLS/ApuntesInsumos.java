package com.grupocassa.agrimobile.CLS;

/**
 * Created by gerardo.valle on 17/7/2017.
 */

public class ApuntesInsumos extends Apuntamiento {

    private int IdApInsumos;
    private float Dosis;
    private float Cantidad;

    public ApuntesInsumos(int idApInsumos, float dosis) {
        IdApInsumos = idApInsumos;
        Dosis = dosis;
    }

    public ApuntesInsumos(int idOS, String fechaApunte, int idRecurso, float area, String usuario, String fechaDigita, String idDev, String fechaIni, String fechaFin, String latitud, String longitud, String observacion, int idApInsumos, float dosis) {
        super(idOS, fechaApunte, idRecurso, area, usuario, fechaDigita, idDev, fechaIni, fechaFin, latitud, longitud, observacion);
        IdApInsumos = idApInsumos;
        Dosis = dosis;
        Cantidad = 0;
    }

    public ApuntesInsumos(int idOS, String fechaApunte, int idRecurso, String dsRecurso, String um, float area, String usuario, String fechaDigita, String idDev, String fechaIni, String fechaFin, String latitud, String longitud, String observacion, int idApInsumos, float dosis) {
        super(idOS, fechaApunte, idRecurso, area, usuario, fechaDigita, idDev, fechaIni, fechaFin, latitud, longitud, observacion);
        IdApInsumos = idApInsumos;
        Dosis = dosis;
        Cantidad = 0;
        super.setDsRecurso(dsRecurso);
        super.setUM(um);
    }

    public ApuntesInsumos() {
        IdApInsumos = 0;
        Dosis = 0;
        Cantidad = 0;
    }

    public int getIdApInsumos() {
        return IdApInsumos;
    }

    public void setIdApInsumos(int idApInsumos) {
        IdApInsumos = idApInsumos;
    }

    public float getDosis() {
        return Dosis;
    }

    public void setDosis(float dosis) {
        Dosis = dosis;
    }

    public float getCantidad() {
        return Cantidad;
    }

    public void setCantidad(float cantidad) {
        Cantidad = cantidad;
    }




}
