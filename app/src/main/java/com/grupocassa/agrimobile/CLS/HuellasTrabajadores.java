package com.grupocassa.agrimobile.CLS;

/**
 * Created by gerardo.valle on 29/9/2017.
 */

public class HuellasTrabajadores {

    private int IdHuella;
    private String CodTrabajador;
    private String NomTrabajador;
    private byte[] Huellas;
    private boolean Sincronizada;

    public HuellasTrabajadores() {
    }

    public HuellasTrabajadores(int idHuella, String codTrabajador, String nomTrabajador, byte[] huellas, boolean sincronizada) {
        IdHuella = idHuella;
        CodTrabajador = codTrabajador;
        Huellas = huellas;
        Sincronizada = sincronizada;
        NomTrabajador = nomTrabajador;
    }

    public int getIdHuella() {
        return IdHuella;
    }

    public void setIdHuella(int idHuella) {
        IdHuella = idHuella;
    }

    public String getCodTrabajador() {
        return CodTrabajador;
    }

    public void setCodTrabajador(String codTrabajador) {
        CodTrabajador = codTrabajador;
    }

    public String getNomTrabajador() {
        return NomTrabajador;
    }

    public void setNomTrabajador(String nomTrabajador) {
        NomTrabajador = nomTrabajador;
    }

    public byte[] getHuellas() {
        return Huellas;
    }

    public void setHuellas(byte[] huellas) {
        Huellas = huellas;
    }

    public boolean isSincronizada() {
        return Sincronizada;
    }

    public void setSincronizada(boolean sincronizada) {
        Sincronizada = sincronizada;
    }
}
