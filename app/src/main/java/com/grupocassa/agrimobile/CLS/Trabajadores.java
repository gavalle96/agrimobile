package com.grupocassa.agrimobile.CLS;

/**
 * Created by gerardo.valle on 17/7/2017.
 */

public class Trabajadores {
    private int IdTrabajador;
    private String CodTrabajador;
    private String NomTrabajador;
    private int IdCuadrilla;
    private boolean Checked=false;

    public Trabajadores() {
        IdTrabajador = 0;
        CodTrabajador = "";
        NomTrabajador = "";
        IdCuadrilla = 0;
    }

    public Trabajadores(int idTrabajador, String codTrabajador, String nomTrabajador, int idCuadrilla) {
        IdTrabajador = idTrabajador;
        CodTrabajador = codTrabajador;
        NomTrabajador = nomTrabajador;
        IdCuadrilla = idCuadrilla;
    }

    public int getIdTrabajador() {
        return IdTrabajador;
    }

    public void setIdTrabajador(int idTrabajador) {
        IdTrabajador = idTrabajador;
    }

    public String getCodTrabajador() {
        return CodTrabajador;
    }

    public void setCodTrabajador(String codTrabajador) {
        CodTrabajador = codTrabajador;
    }

    public String getNomTrabajador() {
        return NomTrabajador;
    }

    public void setNomTrabajador(String nomTrabajador) {
        NomTrabajador = nomTrabajador;
    }

    public int getIdCuadrilla() {
        return IdCuadrilla;
    }

    public void setIdCuadrilla(int idCuadrilla) {
        IdCuadrilla = idCuadrilla;
    }

    public boolean isChecked() {
        return Checked;
    }

    public void setChecked(boolean checked) {
        Checked = checked;
    }
    public void toggleCheck(){
        this.Checked = !Checked;
    }
}
