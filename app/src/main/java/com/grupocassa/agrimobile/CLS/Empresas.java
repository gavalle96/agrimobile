package com.grupocassa.agrimobile.CLS;

/**
 * Created by gerardo.valle on 5/4/2018.
 */

public class Empresas {
    private int Sociedad;
    private String DsEmpresa;

    public Empresas() {
    }

    public Empresas(int sociedad, String dsEmpresa) {
        Sociedad = sociedad;
        DsEmpresa = dsEmpresa;
    }

    public int getSociedad() {
        return Sociedad;
    }

    public void setSociedad(int sociedad) {
        Sociedad = sociedad;
    }

    public String getDsEmpresa() {
        return DsEmpresa;
    }

    public void setDsEmpresa(String dsEmpresa) {
        DsEmpresa = dsEmpresa;
    }

    @Override
    public String toString() {
        return getSociedad() + " - " + getDsEmpresa();
    }
}
