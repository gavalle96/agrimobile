package com.grupocassa.agrimobile.CLS;

/**
 * Created by gerardo.valle on 17/7/2017.
 */

public class Apuntamiento {
    protected int IdOS;
    protected String FechaApunte;
    protected int IdRecurso;
    protected float Area;
    protected String Usuario;
    protected String FechaDigita;
    protected String IdDev;
    protected String FechaIni;
    protected String FechaFin;
    protected String Latitud;
    protected String Longitud;
    protected String Observacion;
    protected String DsRecurso;
    protected String UM;


    public Apuntamiento() {
        IdOS = 0;
        FechaApunte  ="";
        IdRecurso = 0;
        Area = 0;
        Usuario = "";
        FechaDigita = "";
        IdDev = "";
        FechaIni = "";
        FechaFin = "";
        Latitud = "";
        Longitud ="";
        Observacion ="";
        DsRecurso = "";
        UM ="";
    }

    public Apuntamiento(int idOS, String fechaApunte, int idRecurso, float area, String usuario, String fechaDigita, String idDev, String fechaIni, String fechaFin, String latitud, String longitud, String observacion) {
        IdOS = idOS;
        FechaApunte = fechaApunte;
        IdRecurso = idRecurso;
        Area = area;
        Usuario = usuario;
        FechaDigita = fechaDigita;
        IdDev = idDev;
        FechaIni = fechaIni;
        FechaFin = fechaFin;
        Latitud = latitud;
        Longitud = longitud;
        Observacion = observacion;
        DsRecurso = "";
        UM ="";
    }

    public int getIdOS() {
        return IdOS;
    }

    public void setIdOS(int idOS) {
        IdOS = idOS;
    }

    public String getFechaApunte() {
        return FechaApunte;
    }

    public void setFechaApunte(String fechaApunte) {
        FechaApunte = fechaApunte;
    }

    public int getIdRecurso() {
        return IdRecurso;
    }

    public void setIdRecurso(int idRecurso) {
        IdRecurso = idRecurso;
    }

    public float getArea() {
        return Area;
    }

    public void setArea(float area) {
        Area = area;
    }

    public String getUsuario() {
        return Usuario;
    }

    public void setUsuario(String usuario) {
        Usuario = usuario;
    }

    public String getFechaDigita() {
        return FechaDigita;
    }

    public void setFechaDigita(String fechaDigita) {
        FechaDigita = fechaDigita;
    }

    public String getIdDev() {
        return IdDev;
    }

    public void setIdDev(String idDev) {
        IdDev = idDev;
    }

    public String getFechaIni() {
        return FechaIni;
    }

    public void setFechaIni(String fechaIni) {
        FechaIni = fechaIni;
    }

    public String getFechaFin() {
        return FechaFin;
    }

    public void setFechaFin(String fechaFin) {
        FechaFin = fechaFin;
    }

    public String getLatitud() {
        return Latitud;
    }

    public void setLatitud(String latitud) {
        Latitud = latitud;
    }

    public String getLongitud() {
        return Longitud;
    }

    public void setLongitud(String longitud) {
        Longitud = longitud;
    }

    public String getObservacion() {
        return Observacion;
    }

    public void setObservacion(String observacion) {
        Observacion = observacion;
    }

    public String getDsRecurso() {
        return DsRecurso;
    }

    public void setDsRecurso(String dsRecurso) {
        DsRecurso = dsRecurso;
    }

    public String getUM() {
        return UM;
    }

    public void setUM(String UM) {
        this.UM = UM;
    }
}
