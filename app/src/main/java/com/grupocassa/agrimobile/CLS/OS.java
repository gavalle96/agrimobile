package com.grupocassa.agrimobile.CLS;

import java.util.ArrayList;

/**
 * Created by gerardo.valle on 14/7/2017.
 */

public class OS {
    private int IdOS;
    private int IdHacienda;
    private String BkZafra;
    private String DsActividad;
    private float Cantidad;
    private ArrayList<OSRecursos> recursos;
    private String UMP;

    public OS() {
        IdOS = 0;
        IdHacienda = 0;
        DsActividad = "";
        BkZafra = "";
        Cantidad = 0;
        UMP = "";
    }

    public OS(int idOS, int idHacienda, String dsActividad, float cantidad, String bkzafra,String UMP) {
        IdOS = idOS;
        IdHacienda = idHacienda;
        DsActividad = dsActividad;
        BkZafra = bkzafra;
        Cantidad = cantidad;
        this.recursos = new ArrayList<OSRecursos>();
        this.UMP = UMP;
    }

    public int getIdOS() {
        return IdOS;
    }

    public void setIdOS(int idOS) {
        IdOS = idOS;
    }

    public int getIdHacienda() {
        return IdHacienda;
    }

    public void setIdHacienda(int idHacienda) {
        IdHacienda = idHacienda;
    }

    public String getDsActividad() {
        return DsActividad;
    }

    public void setDsActividad(String dsActividad) {
        DsActividad = dsActividad;
    }

    public float getCantidad() {
        return Cantidad;
    }

    public void setCantidad(float cantidad) {
        Cantidad = cantidad;
    }

    public ArrayList<OSRecursos> getRecursos() {
        return recursos;
    }

    public void setRecursos(ArrayList<OSRecursos> recursos) {
        this.recursos = recursos;
    }

    public String getBkZafra() {
        return BkZafra;
    }

    public void setBkZafra(String bkZafra) {
        BkZafra = bkZafra;
    }

    public String getUMP() {
        return UMP;
    }

    public void setUMP(String UMP) {
        this.UMP = UMP;
    }
}
