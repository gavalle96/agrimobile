package com.grupocassa.agrimobile.ApuntesActivities;

import android.content.Context;
import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.grupocassa.agrimobile.CASSAUTILS.CustomHdasAdapter;
import com.grupocassa.agrimobile.CASSAUTILS.CustomOSAdapter;
import com.grupocassa.agrimobile.CASSAUTILS.DBHelper;
import com.grupocassa.agrimobile.CLS.Haciendas;
import com.grupocassa.agrimobile.CLS.OS;
import com.grupocassa.agrimobile.CLS.OSRecursos;
import com.grupocassa.agrimobile.R;

import java.util.ArrayList;

public class MainApuntesActivity extends AppCompatActivity implements View.OnClickListener {

    private AutoCompleteTextView txtHaciendas, txtOS;
    private int IdHacienda, IdOS;
    public DBHelper mydb;
    private ImageButton btnins, btnmo, btnmq, btnsa, btnClear;
    private TextView tvinsumos, tvmanoobra,tvmaquinaria, tvservicios, txtZafra;
    private ArrayList<OS> lOS;
    private Button btnApunteArea;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_apuntes);
        setTitle("Apuntamientos");

        mydb = new DBHelper(this);

        lOS = mydb.getAllOS();

        txtHaciendas = (AutoCompleteTextView) findViewById(R.id.txtHaciendas);
        txtOS = (AutoCompleteTextView) findViewById(R.id.txtOS);

        btnApunteArea = (Button) findViewById(R.id.btnApunteArea);
        btnins = (ImageButton) findViewById(R.id.btnins);
        btnmo  = (ImageButton) findViewById(R.id.btnmo);
        btnmq  = (ImageButton) findViewById(R.id.btnmq);
        btnsa  = (ImageButton) findViewById(R.id.btnsa);
        btnClear = (ImageButton) findViewById(R.id.btnClear);
        txtZafra = (TextView) findViewById(R.id.txtZafra);

        tvinsumos = (TextView) findViewById(R.id.tinsumos);
        tvmanoobra = (TextView) findViewById(R.id.tmanoobra);
        tvmaquinaria = (TextView) findViewById(R.id.tmaquinaria);
        tvservicios = (TextView) findViewById(R.id.tServiciosAgricolas);

        btnins.setVisibility(View.GONE);
        btnmo.setVisibility(View.GONE);
        btnmq.setVisibility(View.GONE);
        btnsa.setVisibility(View.GONE);

        tvinsumos.setVisibility(View.GONE);
        tvmanoobra.setVisibility(View.GONE);
        tvmaquinaria.setVisibility(View.GONE);
        tvservicios.setVisibility(View.GONE);

        btnApunteArea.setVisibility(View.GONE);

        btnApunteArea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(MainApuntesActivity.this, ApuntesAreaActivity.class);
                in.putExtra("IdOS",IdOS);
                startActivity(in);
            }
        });
        btnins.setOnClickListener(this);
        btnmo.setOnClickListener(this);
        btnmq.setOnClickListener(this);
        btnsa.setOnClickListener(this);
        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    btnins.setVisibility(View.GONE);
                    btnmo.setVisibility(View.GONE);
                    btnmq.setVisibility(View.GONE);
                    btnsa.setVisibility(View.GONE);

                    tvinsumos.setVisibility(View.GONE);
                    tvmanoobra.setVisibility(View.GONE);
                    tvmaquinaria.setVisibility(View.GONE);
                    tvservicios.setVisibility(View.GONE);
                    btnApunteArea.setVisibility(View.GONE);
                    txtHaciendas.setText("");
                    txtOS.setText("");
                    IdHacienda = 0;
                    IdOS = 0;
                    txtZafra.setText("");
                    DBHelper mdb = new DBHelper(MainApuntesActivity.this);
                    ArrayList<OS> alOS = mdb.getAllOS();
                    CustomOSAdapter adapterOS = new CustomOSAdapter(MainApuntesActivity.this, android.R.layout.simple_dropdown_item_1line, alOS);
                    txtOS.setAdapter(adapterOS);

            }
        });



        //autocomplete de haciendas
        ArrayList<Haciendas> hdas = mydb.getAllHaciendas();

        CustomHdasAdapter adapter = new CustomHdasAdapter(
                this, android.R.layout.simple_dropdown_item_1line, hdas);
        txtHaciendas.setAdapter(adapter);


        txtHaciendas.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                Haciendas selected = (Haciendas) arg0.getAdapter().getItem(arg2);
                IdHacienda = selected.getIdHacienda();
                IdOS = 0;
                txtOS.setText("");
                PreparaAutocompleteOS();

            }

        });


        CustomOSAdapter adapterOS = new CustomOSAdapter(this, android.R.layout.simple_dropdown_item_1line, lOS);
        txtOS.setAdapter(adapterOS);

        txtOS.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                OS selected = (OS) arg0.getAdapter().getItem(arg2);
                IdOS = selected.getIdOS();
                IdHacienda = selected.getIdHacienda();

                Haciendas h = mydb.getSingleHacienda(IdHacienda);
                txtHaciendas.setText(h.getBkHacienda()+" - "+h.getDsHacienda());
                AdaptApuntamientos();

                InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                in.hideSoftInputFromWindow(arg1.getWindowToken(), 0);

                txtZafra.setText("Zafra: "+selected.getBkZafra());
            }

        });


    }

    private void PreparaAutocompleteOS(){
        //autocomplete de OS
        ArrayList<OS> lOS = mydb.getAllOSByHacienda(IdHacienda);

        CustomOSAdapter adapterOS = new CustomOSAdapter(this, android.R.layout.simple_dropdown_item_1line, lOS);
        txtOS.setAdapter(adapterOS);

        txtOS.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                OS selected = (OS) arg0.getAdapter().getItem(arg2);
                IdOS = selected.getIdOS();

                IdHacienda = selected.getIdHacienda();

                Haciendas h = mydb.getSingleHacienda(IdHacienda);
                txtHaciendas.setText(h.getBkHacienda()+" - "+h.getDsHacienda());

                AdaptApuntamientos();
            }

        });
    }
    private void AdaptApuntamientos(){
        String sociedad = mydb.getSociedad();
        if (txtOS.getText().toString().contains("UM: HA") && sociedad.equals("3000")){
            btnApunteArea.setVisibility(View.VISIBLE);
        }
        else{
            btnApunteArea.setVisibility(View.INVISIBLE);
        }

        ArrayList<OSRecursos> recs = mydb.getAllRecursosByOS(IdOS);
        boolean ins=false, mo=false, mq=false,serv=false;
        for(OSRecursos d : recs){
            if(d.getBkTipRecurso() != null && d.getBkTipRecurso().contains("1")){
                mo =true;
            }
            if(d.getBkTipRecurso() != null && d.getBkTipRecurso().contains("2")){
                ins =true;
            }
            if(d.getBkTipRecurso() != null && d.getBkTipRecurso().contains("3")){
                mq =true;
            }
            if(d.getBkTipRecurso() != null && d.getBkTipRecurso().contains("5")){
                serv =true;
            }
        }

        if (mo == true){
            btnmo.setVisibility(View.VISIBLE);
            tvmanoobra.setVisibility(View.VISIBLE);

        }
        else {
            btnmo.setVisibility(View.GONE);
            tvmanoobra.setVisibility(View.GONE);
        }

        if (ins == true){
            btnins.setVisibility(View.VISIBLE);
            tvinsumos.setVisibility(View.VISIBLE);
        }
        else {
            btnins.setVisibility(View.GONE);
            tvinsumos.setVisibility(View.GONE);
        }

        if (mq == true){
            btnmq.setVisibility(View.VISIBLE);
            tvmaquinaria.setVisibility(View.VISIBLE);
        }
        else {
            btnmq.setVisibility(View.GONE);
            tvmaquinaria.setVisibility(View.GONE);
        }

        if (serv == true){
            btnsa.setVisibility(View.VISIBLE);
            tvservicios.setVisibility(View.VISIBLE);
        }
        else {
            btnsa.setVisibility(View.GONE);
            tvservicios.setVisibility(View.GONE);
        }

    }

    @Override
    public void onClick(View v) {
        Intent i = new Intent(this,ApuntesActivity.class);
        i.putExtra("IdOS",IdOS);
        if (v == btnins){
            i.putExtra("tipo","2");

        } else if (v == btnmo)
        {
            i.putExtra("tipo", "1");
        } else if (v == btnmq)
        {
            i.putExtra("tipo", "3");
        }
        else if (v == btnsa)
        {
            i.putExtra("tipo", "5" +
                    "");
        }
        startActivity(i);
    }
}
