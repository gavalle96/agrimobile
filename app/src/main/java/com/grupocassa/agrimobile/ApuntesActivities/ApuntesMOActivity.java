package com.grupocassa.agrimobile.ApuntesActivities;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;
import androidx.annotation.Nullable;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.grupocassa.agrimobile.CASSAUTILS.CustomCuadrillasAdapter;
import com.grupocassa.agrimobile.CASSAUTILS.DBExterno;
import com.grupocassa.agrimobile.CASSAUTILS.DBHelper;
import com.grupocassa.agrimobile.CASSAUTILS.GPSTracker;
import com.grupocassa.agrimobile.CLS.Cuadrillas;
import com.grupocassa.agrimobile.CLS.Haciendas;
import com.grupocassa.agrimobile.CLS.OS;
import com.grupocassa.agrimobile.CLS.OSRecursos;
import com.grupocassa.agrimobile.CLS.Parametros;
import com.grupocassa.agrimobile.CLS.Trabajadores;
import com.grupocassa.agrimobile.MyDialogFragment;
import com.grupocassa.agrimobile.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ApuntesMOActivity extends AppCompatActivity implements View.OnClickListener {

    private float Area, Cantidad, Latitud,Longitud;
    private int IdRecurso, IdOS, IdCuadrilla;
    private static int checkeds;
    OSRecursos psdet;
    ArrayList<OSRecursos> recursoses;
    private Spinner spRecursos;
    private String Operacion, Titulo, TituloChk;
    private Button btnGuardar;
    private TextView um;
    private EditText txtArea, txtCantidad,txtObservacion, dtpFechaapunte;
    private AutoCompleteTextView txtCuadrilla;

    private String FechaIni;
    private ListView lvTrabajadores;
    private DBHelper mydb;
    private TrabajadoresArrayAdapter adaptrab;
    private DBExterno dbExterno;
    //float AreaApuntada;
    int medidaAltura;
    ScrollView scrollView;
    EditText inputSearch;

    FloatingActionButton fabInfo;
    Boolean expandido;
    FloatingActionButton fabresize;
    GPSTracker gpsTracker;
    private int year, month, day;
    private Calendar calendar;
    private DatePickerDialog.OnDateSetListener date;
    private String FechaApunte;
    private boolean UsaBiometrico = false;
    private Haciendas hda;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apuntes_mo);
        mydb = new DBHelper(this);
        FechaIni = mydb.getCurrentTimeStamp();
        expandido = false;
        //Parametros uBiom = mydb.getSingleParametro(7);

        //String sUsaBioM = uBiom.getValor();

        UsaBiometrico = false;//Boolean.parseBoolean(sUsaBioM);
        TituloChk = "Seleccionados: 0";
        checkeds = 0;
        inputSearch = (EditText) findViewById(R.id.inputSearch);

        inputSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                //Toast.makeText(ApuntesMOActivity.this, cs, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                ApuntesMOActivity.this.adaptrab.getFilter().filter(arg0);
            }
        });

        scrollView =(ScrollView) findViewById(R.id.scvmo);
        spRecursos = (Spinner) findViewById(R.id.spRecursos);
        txtArea = (EditText) findViewById(R.id.txtArea);
        txtCantidad = (EditText) findViewById(R.id.txtCantidad);
        um = (TextView) findViewById(R.id.um);
        txtObservacion = (EditText) findViewById(R.id.txtObservacion);

        dtpFechaapunte = (EditText) findViewById(R.id.dtpFechaApunte);

        fabresize = (FloatingActionButton) findViewById(R.id.resizefab);

        fabresize.setOnClickListener(this);



        btnGuardar = (Button) findViewById(R.id.btnGuardar);
        btnGuardar.setOnClickListener(this);
        lvTrabajadores = (ListView) findViewById(R.id.lvTrab);
        txtCuadrilla = (AutoCompleteTextView) findViewById(R.id.txtCuadrilla);

        fabInfo = (FloatingActionButton) findViewById(R.id.floatingActionButton);

        fabInfo.setOnClickListener(this);

        Bundle extras = getIntent().getExtras();


        IdOS = extras.getInt("IdOS");
        Operacion = extras.getString("Operacion");

        OS o = mydb.getSingleOS(IdOS);
        hda = mydb.getSingleHacienda(o.getIdHacienda());

        Titulo = "OS: "+o.getIdOS()+". "+o.getDsActividad();
        setTitle(Titulo);

        if(UsaBiometrico && hda.isMarcaBiometrico()) {
            Context sharedContext = null;
            try {
                sharedContext = this.createPackageContext("futronictech.com", Context.CONTEXT_INCLUDE_CODE);
                if (sharedContext != null) {
                    DBExterno.ctxOriginal = this;
                    dbExterno = new DBExterno(sharedContext);
                } else {
                    Toast.makeText(this, "Debe haber instalado la APLICACION DE MARCAS", Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                String error = e.getMessage();
            }
            txtCuadrilla.setVisibility(View.GONE);
        }
        else{
            txtCuadrilla.setVisibility(View.VISIBLE);
        }

        ArrayList<OSRecursos> osRecursoses = mydb.getAllRecursosByOS(IdOS);
        recursoses = new ArrayList<OSRecursos>();
        recursoses.add(new OSRecursos(IdOS,0,"","Selec. Recurso","","",0,0,0,false, false, 0));
        for (OSRecursos or: osRecursoses
                ) {
            if(or.getBkTipRecurso().equals("1")){
                recursoses.add(or);
            }
        }


        //OS o = mydb.getSingleOS(IdOS);
        txtArea.setText("0");//String.valueOf(o.getCantidad()));

        MySimpleArrayAdapter adapterins = new MySimpleArrayAdapter(this,recursoses);
        adapterins.setDropDownViewResource(R.layout.itemsimple);
        spRecursos.setAdapter(adapterins);

        spRecursos.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
                OSRecursos p = (OSRecursos) parentView.getItemAtPosition(position);

                IdRecurso = p.getIdRecurso();


                if (IdRecurso > 0)
                //AreaApuntada = mydb.getAreaApuntadaMO(IdOS,IdRecurso);
                psdet = p;
                um.setText(p.getUM());
                if(recursoBiometrico())
                    CargarTrabajadores();


            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });


        ArrayList<Cuadrillas> lOS = mydb.getAllCuadrillasConTrabajadores();

        txtCuadrilla.setHint("Cuadrilla");
        CustomCuadrillasAdapter adapter = new CustomCuadrillasAdapter(this, android.R.layout.simple_dropdown_item_1line, lOS, false);
        txtCuadrilla.setAdapter(adapter);

        txtCuadrilla.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                Cuadrillas selected = (Cuadrillas) arg0.getAdapter().getItem(arg2);
                IdCuadrilla = selected.getIdCuadrilla();
                CargarTrabajadores();

            }

        });



        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);

        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        showDate(year, month, day);
        CargarTrabajadores();
        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                Calendar diasAnteriores =  Calendar.getInstance();
                diasAnteriores.add(Calendar.DAY_OF_MONTH,-1*DBHelper.MargenDiasPermitido());

                if (calendar.getTimeInMillis() > Calendar.getInstance().getTimeInMillis()){
                    calendar = Calendar.getInstance();
                    year = calendar.get(Calendar.YEAR);

                    month = calendar.get(Calendar.MONTH);
                    day = calendar.get(Calendar.DAY_OF_MONTH);
                    showDate(year, month, day);
                    dtpFechaapunte.setError("No es posible seleccionar una fecha futura");
                    Toast.makeText(ApuntesMOActivity.this, "No es posible seleccionar una fecha futura", Toast.LENGTH_SHORT).show();
                }else if(calendar.getTimeInMillis() < diasAnteriores.getTimeInMillis()){
                    calendar = Calendar.getInstance();
                    year = calendar.get(Calendar.YEAR);

                    month = calendar.get(Calendar.MONTH);
                    day = calendar.get(Calendar.DAY_OF_MONTH);
                    showDate(year, month, day);
                    dtpFechaapunte.setError("No puede apuntar más de "+DBHelper.MargenDiasPermitido()+" días atrás.");
                    Toast.makeText(ApuntesMOActivity.this, "No puede apuntar más de "+DBHelper.MargenDiasPermitido()+" días atrás.", Toast.LENGTH_SHORT).show();
                }
                else{
                    dtpFechaapunte.setError(null);
                    showDate(year, monthOfYear, dayOfMonth);
                }
            }

        };

        dtpFechaapunte.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(ApuntesMOActivity.this, date, calendar
                        .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();


            }
        });




    }






    private void showDate(int year, int month, int day) {
        dtpFechaapunte.setText(new StringBuilder().append(day).append("/")
                .append(month+1).append("/").append(year));
        FechaApunte = mydb.getDate(year,month, day);
        if (recursoBiometrico())
        CargarTrabajadores();

    }

    private void CargarTrabajadores(){
        //CARGAMOS LOS TRABAJADORES QUE MARCARON ENTRADA ESE DIA
        ArrayList<Trabajadores> trabxcuad  =  new ArrayList<Trabajadores>();
        trabxcuad = mydb.getTrabajadoresPorCuadrilla(IdCuadrilla);
        if(dbExterno == null ){
            if(UsaBiometrico && hda.isMarcaBiometrico()) {
                Toast.makeText(this, "Debe instalar la aplicación de marcas para ver trabajadores", Toast.LENGTH_LONG).show();
                return;
            }
        }
        else if(UsaBiometrico && hda.isMarcaBiometrico() && recursoBiometrico()) {

            trabxcuad = dbExterno.ConsultarTrabajadoresConMarca(FechaApunte,"P10");
        }


        if (trabxcuad == null){
            trabxcuad = new ArrayList<Trabajadores>();
        }

        adaptrab = new TrabajadoresArrayAdapter(ApuntesMOActivity.this,trabxcuad);


        lvTrabajadores.setAdapter(adaptrab);


        lvTrabajadores.setItemsCanFocus(true);

        lvTrabajadores.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(ApuntesMOActivity.this, "Test", Toast.LENGTH_SHORT).show();
                adaptrab.getItem(position).toggleCheck();
                TrabViewHolder viewHolder = (TrabViewHolder) view.getTag();
                viewHolder.getCheckBox().setChecked(adaptrab.getItem(position).isChecked());

                if (adaptrab.getItem(position).isChecked()){
                    checkeds+=1;

                }
                else {
                    checkeds-=1;
                }
                Showchecked();
            }

        });
    }

    private Boolean recursoBiometrico(){
        if (psdet!=null){
        if ( psdet.getBkRecurso().equals(DBHelper.JORNADA_NOCTURNA) || psdet.getBkRecurso().equals(DBHelper.AMPLIADA) || psdet.getBkRecurso().equals(DBHelper.CUIDADOR)){
            txtCuadrilla.setVisibility(View.VISIBLE);
            return false;
        }
        }
        if(hda.isMarcaBiometrico() && UsaBiometrico) {
            txtCuadrilla.setVisibility(View.GONE);
            return true;
        }
        else {
            return false;
        }
    }

    private boolean ValidaApunte(){
        if (psdet.getIdRecurso()==0)
        {
            Toast.makeText(this, "Seleccione un recurso", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (txtArea.getText().toString().equals("")){
            txtArea.setError("El campo es obligatorio");
            return false;
        }
        if(txtCantidad.getText().equals("")){
            txtCantidad.setError("El campo es obligatorio");
            return false;
        }
        int checkeados = 0;

        if (lvTrabajadores.getAdapter() == null){
            return  false;
        }
        String sctd = txtCantidad.getText().toString().equals("")?"0":txtCantidad.getText().toString();

        Cantidad = Float.parseFloat(sctd);
        for (int i = 0; i < lvTrabajadores.getAdapter().getCount(); i++) {

            Trabajadores t = (Trabajadores) lvTrabajadores.getAdapter().getItem(i);
            if (t.isChecked()) {
                checkeados +=1;
                float ctdJornalAcum = mydb.getCantApuntadaMOTrab(IdOS,IdRecurso,FechaApunte,t.getIdTrabajador());

                if (ctdJornalAcum + Cantidad > 2 && psdet.getBkRecurso().equals(DBHelper.MEDIA_JORNADA) && Cantidad > 0){

                    new AlertDialog.Builder(ApuntesMOActivity.this)
                            .setTitle("Entrada Errónea")
                            .setMessage("No puede apuntar más de DOS jornadas por día a "+t.getCodTrabajador()+" - "+t.getNomTrabajador())
                            .setCancelable(false)
                            .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // Whatever...
                                }
                            }).show();
                    return false;
                }

                if (ctdJornalAcum + Cantidad > 1 && (psdet.getBkRecurso().equals(DBHelper.AMPLIADA) || psdet.getBkRecurso().equals(DBHelper.JORNADA_NOCTURNA) || psdet.getBkRecurso().equals(DBHelper.CAPORAL) || psdet.getBkRecurso().equals(DBHelper.CUIDADOR)) && Cantidad > 0){

                    new AlertDialog.Builder(ApuntesMOActivity.this)
                            .setTitle("Entrada Errónea")
                            .setMessage("No puede apuntar más de UNA jornada por día a "+t.getCodTrabajador()+" - "+t.getNomTrabajador())
                            .setCancelable(false)
                            .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // Whatever...
                                }
                            }).show();
                    return false;
                }

            }
        };

        if (checkeados == 0){
            Toast.makeText(this, "Debe seleccionar al menos un trabajador", Toast.LENGTH_LONG).show();
            return false;
        }
        float pendiente= 0;
        pendiente = psdet.getCantidadPend()-psdet.getCantidadApunt();
        pendiente = pendiente<=1? 1 : Math.round(pendiente);


        Cantidad = Cantidad*checkeados;
        if (pendiente<=0){
            Toast.makeText(this, "Ya ha utilizado la totalidad del recurso", Toast.LENGTH_LONG).show();
            return false;
        }
        else if(pendiente - Cantidad < 0){
            Toast.makeText(this, "Solo puede apuntar "+pendiente+" "+psdet.getUM(), Toast.LENGTH_LONG).show();
            return false;
        }


        if (Cantidad <= 0){
            Toast.makeText(this,"Debe apuntar una cantidad mayor a cero",Toast.LENGTH_LONG).show();
            return false;
        }




        float area = Float.parseFloat(txtArea.getText().toString());
        OS plan = mydb.getSingleOS(IdOS);

        float areaFecha = mydb.getAreaApuntadaMOFecha(IdOS,IdRecurso,FechaApunte,IdCuadrilla);

        if (areaFecha != area) {

            float areapendiente = plan.getCantidad() - psdet.getAreaApunt();
            double newarea = Math.round(areapendiente * 100.0) / 100.0;
            areapendiente = Float.parseFloat(String.valueOf(newarea));
            if (areapendiente <= 0) {
                Toast.makeText(this, "Ya se ha utilizado la totalidad del área planificada", Toast.LENGTH_LONG).show();
                return false;
            }
            if (area <= 0) {
                Toast.makeText(this, "El área debe ser mayor o igual a cero", Toast.LENGTH_LONG).show();
                return false;
            }
            if (areapendiente - area < 0) {
                Toast.makeText(this, "Solo puede apuntar un máximo " + areapendiente + " Ha.", Toast.LENGTH_LONG).show();
                return false;
            }
        }
        return true;
    }

    @Override
    public void onResume(){
        super.onResume();
        //Toast.makeText(this, "Actividad en resumen", Toast.LENGTH_SHORT).show();
        gpsTracker  = new GPSTracker(this);
        checkGPS();
    }
    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        //Setting Dialog Title
        alertDialog.setTitle("Ubicación desactivada");

        //Setting Dialog Message
        alertDialog.setMessage("Active la ubicación GPS para poder seguir apuntando");

        //On Pressing Setting button
        alertDialog.setPositiveButton("Ajustes", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });

        //On pressing cancel button
        alertDialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.cancel();
                finish();
            }
        });

        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    private void checkGPS(){
        // check if GPS enabled

        if (gpsTracker.getIsGPSTrackingEnabled())
        {
            Latitud = Float.parseFloat(String.valueOf(gpsTracker.getLatitude()));
            Longitud = Float.parseFloat(String.valueOf(gpsTracker.getLongitude()));

        }
        else
        {
            showSettingsAlert();

        }
    }
    @Override
    public void onClick(View v) {
        if (v == btnGuardar && ValidaApunte()){


            int checkeados = 0, apuntados = 0;
            if (lvTrabajadores.getAdapter() == null){
                return;
            }
            for (int i = 0; i < lvTrabajadores.getAdapter().getCount(); i++) {

                Trabajadores t = (Trabajadores) lvTrabajadores.getAdapter().getItem(i);
                if (t.isChecked()) {
                    checkeados +=1;

                }
            };

            float AreaApunt = 1;

            String sarea = txtArea.getText().toString().equals("")?"0":txtArea.getText().toString();

            Area = Float.parseFloat(sarea);
            AreaApunt = Area;
            //AreaApunt = Area / checkeados;
            float areaFecha = mydb.getAreaApuntadaMOFecha(IdOS,IdRecurso,FechaApunte,IdCuadrilla);



            String sctd = txtCantidad.getText().toString().equals("")?"0":txtCantidad.getText().toString();

            Cantidad = Float.parseFloat(sctd);

            float areap = mydb.getAreaApuntadaMO(psdet.getIdOS(),psdet.getIdRecurso());
            psdet.setAreaApunt(psdet.getAreaApunt()-areap);
            for (int i = 0; i < lvTrabajadores.getAdapter().getCount(); i++) {

                Trabajadores t = (Trabajadores) lvTrabajadores.getAdapter().getItem(i);
                if (t.isChecked()) {
                    // Do something
                    //Toast.makeText(this,t.getNomTrabajador() , Toast.LENGTH_SHORT).show();
                    if (mydb.insertApuntesMO(IdOS,FechaApunte,IdRecurso,AreaApunt,((Trabajadores) lvTrabajadores.getAdapter().getItem(i)).getIdCuadrilla(),t.getIdTrabajador(),Cantidad,mydb.getUsr(),mydb.getCurrentTimeStamp(),txtObservacion.getText().toString(),FechaIni,mydb.getCurrentTimeStamp(),Latitud,Longitud)){
                        apuntados+=1;
                    }
                }
            };

            psdet.setAreaApunt(psdet.getAreaApunt() + mydb.getAreaApuntadaMO(psdet.getIdOS(),psdet.getIdRecurso()));

            psdet.setCantidadApunt(psdet.getCantidadApunt()+(Cantidad*checkeados));
            mydb.updateOSRecurso(psdet);
            if (checkeados == apuntados){
                Toast.makeText(this, "Apuntamientos realizados correctamente", Toast.LENGTH_SHORT).show();
                Reset();
                //recreate();
            }
            else {
                Toast.makeText(this, "Se apuntaron "+apuntados+" de "+checkeados+" elementos seleccionados", Toast.LENGTH_LONG).show();
            }
        }
        else if (v == fabInfo){
            DialogFragment newFragment = MyDialogFragment.newInstance(IdOS,"1");
            newFragment.show(getSupportFragmentManager(), "Recursos");
        }
        else if (v == fabresize){

            ValueAnimator anim = ValueAnimator.ofInt(scrollView.getMeasuredHeight(), -100);
            anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    int val = (Integer) valueAnimator.getAnimatedValue();

                    ViewGroup.LayoutParams layoutParams = lvTrabajadores.getLayoutParams();
                    ViewGroup.LayoutParams layoutParams2 = scrollView.getLayoutParams();
                    if (!expandido) {
                        medidaAltura = layoutParams.height;
                        layoutParams.height = AppBarLayout.LayoutParams.WRAP_CONTENT;
                        fabresize.setImageResource(R.drawable.arrdown);

                        layoutParams2.height = 0;
                        scrollView.setVisibility(View.GONE);
                        //inputSearch.setVisibility(View.VISIBLE);



                    }
                    else {
                        layoutParams.height = 0;
                        fabresize.setImageResource(R.drawable.arrup);
                        scrollView.setVisibility(View.VISIBLE);
                        layoutParams2.height = AppBarLayout.LayoutParams.WRAP_CONTENT;
                        //inputSearch.setVisibility(View.GONE);
                        //inputSearch.setText("");
                        ApuntesMOActivity.this.adaptrab.getFilter().filter("");
                    }

                    lvTrabajadores.setLayoutParams(layoutParams);
                }
            });
            anim.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {

                    expandido = !expandido;
                    Showchecked();

                    //Toast.makeText(ApuntesMOActivity.this, ""+expandido, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
            anim.setDuration(0);
            anim.start();


        }

    }

    private void Reset(){
        scrollView.fullScroll(ScrollView.FOCUS_UP);
        txtArea.setText("");
        txtObservacion.setText("");
        txtCantidad.setText("");
        psdet = new OSRecursos();

        txtCuadrilla.setText("");

        IdRecurso = 0;
        IdCuadrilla = 0;
        FechaIni = mydb.getCurrentTimeStamp();
        lvTrabajadores.setAdapter(null);
        spRecursos.setSelection(0);
        // checkGPS();
    }

    @Override
    public void onBackPressed() {
        if (expandido){
            Contraer();
        }
        else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && expandido) {
            Contraer();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void Showchecked(){
        if (expandido) {


            TituloChk = "Seleccionados: " + checkeds;
            setTitle(TituloChk);
        }
        else{
            setTitle(Titulo);
        }
    }

    private void Contraer(){

        ValueAnimator anim = ValueAnimator.ofInt(scrollView.getMeasuredHeight(), -100);
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int val = (Integer) valueAnimator.getAnimatedValue();

                ViewGroup.LayoutParams layoutParams = lvTrabajadores.getLayoutParams();
                ViewGroup.LayoutParams layoutParams2 = scrollView.getLayoutParams();

                    layoutParams.height = 0;
                    fabresize.setImageResource(R.drawable.arrup);
                    scrollView.setVisibility(View.VISIBLE);
                    layoutParams2.height = AppBarLayout.LayoutParams.WRAP_CONTENT;

                //inputSearch.setVisibility(View.GONE);
                //inputSearch.setText("");
                ApuntesMOActivity.this.adaptrab.getFilter().filter("");

                lvTrabajadores.setLayoutParams(layoutParams);
            }
        });
        anim.setDuration(0);
        anim.start();
        expandido = !expandido;
        Showchecked();
    }

    private class MySimpleArrayAdapter extends ArrayAdapter<OSRecursos> implements View.OnClickListener {
        private final Context context;
        private final List<OSRecursos> values;

        public MySimpleArrayAdapter(Context context, List<OSRecursos> values) {
            super(context,R.layout.itemsimple,R.id.txtItem, values);
            this.context = context;
            this.values = values;
        }

        @Override
        public OSRecursos getItem(int position) {
            return values.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.itemsimple, parent, false);
            TextView insumo = (TextView) rowView.findViewById(R.id.txtItem);

            insumo.setText(values.get(position).getDsRecurso());;
            return rowView;
        }
        @Override
        public void onClick(View v) {

        }
    }

    /** Holds child views for one row. */
    private static class TrabViewHolder
    {
        private CheckBox checkBox;
        private TextView textView;

        public TrabViewHolder()
        {
        }

        public TrabViewHolder(TextView textView, CheckBox checkBox)
        {
            this.checkBox = checkBox;
            this.textView = textView;
        }

        public CheckBox getCheckBox()
        {
            return checkBox;
        }

        public void setCheckBox(CheckBox checkBox)
        {
            this.checkBox = checkBox;
        }

        public TextView getTextView()
        {
            return textView;
        }

        public void setTextView(TextView textView)
        {
            this.textView = textView;
        }



    }

    /** Custom adapter for displaying an array of Planet objects. */
    private class TrabajadoresArrayAdapter extends BaseAdapter implements Filterable, View.OnClickListener//extends ArrayAdapter<Trabajadores>
    {
        private ApuntesMOActivity activity;
        private LayoutInflater inflater;
        //public ArrayList<Trabajadores> values;
        private ArrayList<Trabajadores> mOriginalValues; // Original Values
        private ArrayList<Trabajadores> mDisplayedValues;    // Values to be displayed
        public TrabajadoresArrayAdapter(ApuntesMOActivity a, ArrayList<Trabajadores> planetList)
        {
            activity = a;
            inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //super(context, R.layout.itemchk, R.id.txtNombre, planetList);
            // Cache the LayoutInflate to avoid asking for a new one each time.
            //inflater = LayoutInflater.from(context);
            //values = planetList;
            this.mOriginalValues = planetList;
            this.mDisplayedValues = planetList;


        }

        @Override
        public int getCount() {
            return mDisplayedValues.size();
        }

        @Nullable
        @Override
        public Trabajadores getItem(int position) {
            return mDisplayedValues.get(position);
        }

        @Override
        public long getItemId(int position) {
            return mDisplayedValues.get(position).getIdTrabajador();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            // Trabajador to display
            Trabajadores trabajador = (Trabajadores) this.getItem(position);

            // The child views in each row.
            CheckBox checkBox;
            TextView textView;


            // Create a new row view
            if (convertView == null)
            {
                convertView = inflater.inflate(R.layout.itemchk, null);

                // Find the child views.
                textView = (TextView) convertView
                        .findViewById(R.id.txtNombre);
                checkBox = (CheckBox) convertView.findViewById(R.id.chk);

                // Optimization: Tag the row with it's child views, so we don't
                // have to
                // call findViewById() later when we reuse the row.
                convertView.setTag(new TrabViewHolder(textView, checkBox));

                // If CheckBox is toggled, update the planet it is tagged with.

                checkBox.setOnClickListener(new View.OnClickListener()
                {
                    public void onClick(View v)
                    {
                        CheckBox cb = (CheckBox) v;
                        Trabajadores t = (Trabajadores) cb.getTag();
                        t.setChecked(cb.isChecked());
                        if (cb.isChecked()){
                            checkeds+=1;

                        }
                        else {
                            checkeds-=1;
                        }
                        Showchecked();
                    }


                });


            }
            // Reuse existing row view
            else
            {
                // Because we use a ViewHolder, we avoid having to call
                // findViewById().
                TrabViewHolder viewHolder = (TrabViewHolder) convertView
                        .getTag();
                checkBox = viewHolder.getCheckBox();
                textView = viewHolder.getTextView();
            }

            // Tag the CheckBox with the Planet it is displaying, so that we can
            // access the planet in onClick() when the CheckBox is toggled.
            checkBox.setTag(trabajador);

            // Display planet data
            checkBox.setChecked(trabajador.isChecked());
            textView.setText(trabajador.getCodTrabajador()+" - "+trabajador.getNomTrabajador());

            return convertView;
        }





        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {

                @SuppressWarnings("unchecked")
                @Override
                protected void publishResults(CharSequence constraint,FilterResults results) {

                    mDisplayedValues = (ArrayList<Trabajadores>) results.values; // has the filtered values
                    notifyDataSetChanged();  // notifies the data with new filtered values
                }

                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults results = new FilterResults();        // Holds the results of a filtering operation in values
                    ArrayList<Trabajadores> FilteredArrList = new ArrayList<Trabajadores>();

                    if (mOriginalValues == null) {
                        mOriginalValues = new ArrayList<Trabajadores>(mDisplayedValues); // saves the original data in mOriginalValues
                    }

                    /********
                     *
                     *  If constraint(CharSequence that is received) is null returns the mOriginalValues(Original) values
                     *  else does the Filtering and returns FilteredArrList(Filtered)
                     *
                     ********/
                    if (constraint == null || constraint.length() == 0) {

                        // set the Original result to return
                        results.count = mOriginalValues.size();
                        results.values = mOriginalValues;
                    } else {
                        constraint = constraint.toString().toLowerCase();
                        for (int i = 0; i < mOriginalValues.size(); i++) {
                            String data = mOriginalValues.get(i).getNomTrabajador();
                            String cod = mOriginalValues.get(i).getCodTrabajador();
                            if (data.toLowerCase().contains(constraint.toString()) || cod.toLowerCase().contains(constraint.toString())) {
                                FilteredArrList.add(mOriginalValues.get(i));
                            }
                        }
                        // set the Filtered result to return
                        results.count = FilteredArrList.size();
                        results.values = FilteredArrList;
                    }
                    return results;
                }
            };
            return filter;
        }

        @Override
        public void onClick(View v) {
            Toast.makeText(ApuntesMOActivity.this, "ADAPTADOR", Toast.LENGTH_SHORT).show();
        }
    }
}
