package com.grupocassa.agrimobile.ApuntesActivities;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.grupocassa.agrimobile.CASSAUTILS.DBHelper;
import com.grupocassa.agrimobile.CASSAUTILS.GPSTracker;
import com.grupocassa.agrimobile.CLS.ApuntesMO;
import com.grupocassa.agrimobile.CLS.OS;
import com.grupocassa.agrimobile.CLS.OSRecursos;
import com.grupocassa.agrimobile.CLS.Trabajadores;
import com.grupocassa.agrimobile.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class EditApuntesMO extends AppCompatActivity implements View.OnClickListener {

    OSRecursos recurso;
    ApuntesMO apunte;
    DBHelper mydb;
    TextView tvTrabajador;
    EditText txtArea, txtCantidad, txtObserv;
    Button btnGuardar;
    int IdOS, IdRecurso, IdApunte;
    String FechaIni;
    float Latitud, Longitud;
    GPSTracker gpsTracker;
    float AreaApuntada;

    EditText dtpFechaapunte;
    private int year, month, day;
    private Calendar calendar;
    private DatePickerDialog.OnDateSetListener date;
    private String FechaApunte;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_apuntes_mo);

        mydb =new DBHelper(this);
        tvTrabajador = (TextView) findViewById(R.id.tvtrab);
        txtArea = (EditText) findViewById(R.id.Area);
        txtCantidad =(EditText) findViewById(R.id.Cant);
        txtObserv = (EditText) findViewById(R.id.txtObs);
        btnGuardar = (Button) findViewById(R.id.btnGuardar);


        calendar = Calendar.getInstance();
        btnGuardar.setOnClickListener(this);

        Bundle extras = getIntent().getExtras();

        IdOS = extras.getInt("IdOS",0);
        IdApunte = extras.getInt("IdApunte");

        FechaIni = mydb.getCurrentDate();

        ArrayList<ApuntesMO> aps = mydb.getApuntesMOByOS(IdOS);
        for (ApuntesMO ap: aps
             ) {
            if (ap.getIdApMO() == IdApunte){
                apunte = ap;
            }
        }


        dtpFechaapunte = (EditText) findViewById(R.id.dtpFechaApunte);


        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date fec = format.parse(apunte.getFechaApunte());
            Calendar cld = Calendar.getInstance();
            cld.setTime(fec);

            showDate(cld.get(Calendar.YEAR),cld.get(Calendar.MONTH)+1,cld.get(Calendar.DAY_OF_MONTH));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        //apunte = mydb.getSingleApunteManoObra(IdApunte);
        tvTrabajador.setText(apunte.getTrabajador());
        txtArea.setText(String.valueOf(apunte.getArea()));
        txtCantidad.setText(String.valueOf(apunte.getQTD_PAGO()));
        IdRecurso = apunte.getIdRecurso();
        ArrayList<OSRecursos> recursoses = mydb.getAllRecursosByOS(IdOS);

        for (OSRecursos osr: recursoses) {
            if (osr.getIdRecurso() == IdRecurso) {

                recurso = osr;
                recurso.setCantidadApunt(recurso.getCantidadApunt()-apunte.getQTD_PAGO());
            }
        }

        AreaApuntada = mydb.getAreaApuntadaMO(IdOS,IdRecurso);
        AreaApuntada = AreaApuntada - apunte.getArea();


        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                Calendar diasAnteriores =  Calendar.getInstance();
                diasAnteriores.add(Calendar.DAY_OF_MONTH,-1*DBHelper.MargenDiasPermitido());

                if (calendar.getTimeInMillis() > Calendar.getInstance().getTimeInMillis()){
                    calendar = Calendar.getInstance();
                    year = calendar.get(Calendar.YEAR);

                    month = calendar.get(Calendar.MONTH);
                    day = calendar.get(Calendar.DAY_OF_MONTH);
                    showDate(year, month+1, day);
                    dtpFechaapunte.setError("No es posible seleccionar una fecha futura");
                    Toast.makeText(EditApuntesMO.this, "No es posible seleccionar una fecha futura", Toast.LENGTH_SHORT).show();
                }else if(calendar.getTimeInMillis() < diasAnteriores.getTimeInMillis()){
                    calendar = Calendar.getInstance();
                    year = calendar.get(Calendar.YEAR);

                    month = calendar.get(Calendar.MONTH);
                    day = calendar.get(Calendar.DAY_OF_MONTH);
                    showDate(year, month+1, day);
                    dtpFechaapunte.setError("No puede apuntar más de "+DBHelper.MargenDiasPermitido()+" días atrás.");
                    Toast.makeText(EditApuntesMO.this, "No puede apuntar más de "+DBHelper.MargenDiasPermitido()+" días atrás.", Toast.LENGTH_SHORT).show();
                }
                else{
                    dtpFechaapunte.setError(null);
                    showDate(year, monthOfYear+1, dayOfMonth);
                }
            }

        };

        dtpFechaapunte.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(EditApuntesMO.this, date, calendar
                        .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

    }
    @SuppressWarnings("deprecation")
    public void setDate(View view) {
        showDialog(999);
        Toast.makeText(getApplicationContext(), "ca",
                Toast.LENGTH_SHORT)
                .show();
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        if (id == 999) {
            return new DatePickerDialog(this,
                    myDateListener, year, month, day);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new
            DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker arg0,
                                      int arg1, int arg2, int arg3) {
                    // TODO Auto-generated method stub
                    // arg1 = year
                    // arg2 = month
                    // arg3 = day
                    showDate(arg1, arg2+1, arg3);
                }
            };

    private void showDate(int year, int month, int day) {
        dtpFechaapunte.setText(new StringBuilder().append(day).append("/")
                .append(month).append("/").append(year));
        FechaApunte = mydb.getDate(year,month-1, day);
    }
    @Override
    protected void onResume() {
        super.onResume();
        gpsTracker  = new GPSTracker(this);
        checkGPS();
    }

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        //Setting Dialog Title
        alertDialog.setTitle("Ubicación desactivada");

        //Setting Dialog Message
        alertDialog.setMessage("Active la ubicación GPS para poder seguir apuntando");

        //On Pressing Setting button
        alertDialog.setPositiveButton("Ajustes", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });

        //On pressing cancel button
        alertDialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.cancel();
                finish();
            }
        });

        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    private void checkGPS(){
        // check if GPS enabled

        if (gpsTracker.getIsGPSTrackingEnabled())
        {
            Latitud = Float.parseFloat(String.valueOf(gpsTracker.getLatitude()));
            Longitud = Float.parseFloat(String.valueOf(gpsTracker.getLongitude()));

        }
        else
        {
            showSettingsAlert();

        }
    }

    private boolean ValidarApunte(){
        if (txtArea.getText().toString().equals("")){
            txtArea.setError("El campo es obligatorio");
            return false;
        }
        if (txtCantidad.getText().toString().equals("")){
            txtCantidad.setError("El campo es obligatorio");
            return false;
        }
        float area = Float.parseFloat(txtArea.getText().toString());
        float ctd = Float.parseFloat(txtCantidad.getText().toString());

        OS plan = mydb.getSingleOS(IdOS);
        float areapendiente = plan.getCantidad() - (recurso.getAreaApunt() - apunte.getArea());

        double newarea = Math.round(areapendiente*100.0)/100.0;
        areapendiente = Float.parseFloat(String.valueOf(newarea));
        if (area<=0){
            Toast.makeText(this, "El área debe ser mayor o igual a cero", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (areapendiente <= 0){
            Toast.makeText(this, "Ya se ha utilizado la totalidad del área planificada", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (areapendiente -area <0){
            Toast.makeText(this, "Solo puede apuntar "+areapendiente+" Ha.", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (ctd<=0){
            Toast.makeText(this, "La cantidad debe ser mayor o igual a cero", Toast.LENGTH_SHORT).show();
            return false;
        }

        float pendiente = recurso.getCantidadPend()-recurso.getCantidadApunt();
        pendiente = pendiente<=1? 1 : Math.round(pendiente);

        if (pendiente-ctd < 0){
            Toast.makeText(this, "No puede apuntar más de "+pendiente+" "+recurso.getUM(), Toast.LENGTH_SHORT).show();
            return false;
        }

        float ctdJornalAcum = mydb.getCantApuntadaMOTrab(IdOS,IdRecurso,FechaApunte,apunte.getIdTrabajador());
        Trabajadores t = mydb.getSingleTrabajadorById(apunte.getIdTrabajador());
        ctdJornalAcum = ctdJornalAcum - apunte.getQTD_PAGO();
        if (ctdJornalAcum + ctd > 2 && recurso.getBkRecurso().equals(DBHelper.MEDIA_JORNADA) && ctd > 0){

            new AlertDialog.Builder(EditApuntesMO.this)
                    .setTitle("Entrada Errónea")
                    .setMessage("No puede apuntar más de DOS jornadas por día a "+t.getCodTrabajador()+" - "+t.getNomTrabajador())
                    .setCancelable(false)
                    .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // Whatever...
                        }
                    }).show();
            return false;
        }

        if (ctdJornalAcum + ctd > 1 && (recurso.getBkRecurso().equals(DBHelper.AMPLIADA) || recurso.getBkRecurso().equals(DBHelper.JORNADA_NOCTURNA) || recurso.getBkRecurso().equals(DBHelper.CAPORAL) || recurso.getBkRecurso().equals(DBHelper.CUIDADOR)) && ctd > 0){
        //if (ctdJornalAcum + ctd >= 1 && (recurso.getBkRecurso().equals(DBHelper.AMPLIADA) || recurso.getBkRecurso().equals(DBHelper.JORNADA_NOCTURNA) || recurso.getBkRecurso().equals(DBHelper.CAPORAL) || recurso.getBkRecurso().equals(DBHelper.CUIDADOR)) && ctd > 0){

            new AlertDialog.Builder(EditApuntesMO.this)
                    .setTitle("Entrada Errónea")
                    .setMessage("No puede apuntar más de UNA jornada por día a "+t.getCodTrabajador()+" - "+t.getNomTrabajador())
                    .setCancelable(false)
                    .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // Whatever...
                        }
                    }).show();
            return false;
        }

        return true;
    }

    @Override
    public void onClick(View v) {
        float area = Float.parseFloat(txtArea.getText().toString());
        float ctd = Float.parseFloat(txtCantidad.getText().toString());

        if (v == btnGuardar && ValidarApunte()){
            float areap = mydb.getAreaApuntadaMO(recurso.getIdOS(),recurso.getIdRecurso());
            recurso.setAreaApunt(recurso.getAreaApunt()-areap);
            if (mydb.updateApunteMO(IdApunte, FechaApunte,area,ctd,txtObserv.getText().toString(),mydb.getCurrentTimeStamp(),FechaIni,mydb.getCurrentTimeStamp(),Latitud,Longitud)){
                recurso.setCantidadApunt(recurso.getCantidadApunt()+ctd);
                recurso.setAreaApunt(recurso.getAreaApunt() + mydb.getAreaApuntadaMO(recurso.getIdOS(),recurso.getIdRecurso()));


                mydb.updateOSRecurso(recurso);
                Toast.makeText(this, "Apuntamiento guardado correctamente", Toast.LENGTH_SHORT).show();
                Reset();
                finish();
            }
            else{
                Toast.makeText(this, "Ocurrió un problema al guardar el apunte", Toast.LENGTH_SHORT).show();
            }
        }

    }
    private void Reset(){
        recurso = new OSRecursos();
        txtArea.setText("");
        tvTrabajador.setText("");
        txtCantidad.setText("");
        txtArea.setText("");
        txtObserv.setText("");
        IdApunte = 0;

        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);

        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        showDate(year, month+1, day);

        IdOS = 0;
        IdRecurso = 0;
        FechaIni = mydb.getCurrentTimeStamp();
    }
}
