package com.grupocassa.agrimobile.ApuntesActivities;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.AsyncTask;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.grupocassa.agrimobile.CASSAUTILS.DBHelper;
import com.grupocassa.agrimobile.CASSAUTILS.NetworkUtility;
import com.grupocassa.agrimobile.CLS.ApuntesMO;
import com.grupocassa.agrimobile.CLS.Parametros;
import com.grupocassa.agrimobile.CLS.VolleyCallback;
import com.grupocassa.agrimobile.R;
/*
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;*/
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
/*
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;*/

public class reporteActivity extends AppCompatActivity {

    EditText dtpFechaIni, dtpFechaFin;
    ListView lvReporte;
    Calendar calendar;
    Button btnconsultar;
    ProgressBar pgbReporte;
    private DatePickerDialog.OnDateSetListener dateini, datefin;
    private ArrayList<ApuntesMO> elementos;
    //OkHttpClient client = new OkHttpClient();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reporte);
        dtpFechaIni = (EditText) findViewById(R.id.dtpFechaIni);
        dtpFechaFin = (EditText) findViewById(R.id.dtpFechaFin);
        btnconsultar =(Button) findViewById(R.id.btnConsultar);
        pgbReporte = (ProgressBar) findViewById(R.id.pgReporte);
        elementos = new ArrayList<ApuntesMO>();
        lvReporte = (ListView) findViewById(R.id.lvElementos);


        calendar = Calendar.getInstance();

        dateini = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                 showDate(year, monthOfYear+1, dayOfMonth,dtpFechaIni);


            }

        };
        datefin = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                showDate(year, monthOfYear+1, dayOfMonth,dtpFechaFin);


            }

        };

        dtpFechaIni.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(reporteActivity.this, dateini, calendar
                        .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        dtpFechaFin.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(reporteActivity.this, datefin, calendar
                        .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        dtpFechaIni.setText(calendar.get(Calendar.DAY_OF_MONTH)+"/"+(calendar.get(Calendar.MONTH)+1)+"/"+ calendar.get(Calendar.YEAR));
        dtpFechaFin.setText(calendar.get(Calendar.DAY_OF_MONTH)+"/"+(calendar.get(Calendar.MONTH)+1)+"/"+ calendar.get(Calendar.YEAR));

        btnconsultar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pgbReporte.setVisibility(View.VISIBLE);
                btnconsultar.setEnabled(false);
                dtpFechaFin.setEnabled(false);
                dtpFechaIni.setEnabled(false);
                //new ObtenerReporte().execute();
            }
        });
        setTitle("Apuntes de Mano de Obra Enviados");
        LayoutInflater inflater = getLayoutInflater();
        ViewGroup header;
        header = (ViewGroup) inflater.inflate(R.layout.rowmo_reporte, lvReporte, false);

        lvReporte.addHeaderView(header);
    }


    private void showDate(int year, int month, int day, EditText v) {
        v.setText(new StringBuilder().append(day).append("/")
                .append(month).append("/").append(year));
    }

    private void EnableCtrls(){
        pgbReporte.setVisibility(View.INVISIBLE);
        btnconsultar.setEnabled(true);
        dtpFechaFin.setEnabled(true);
        dtpFechaIni.setEnabled(true);
    }

    private void cargarListaVista(){

        ReportAdapter adapter = new ReportAdapter(this,elementos);
        if (elementos.size() == 0){
            Toast.makeText(this, "No hay datos que mostrar", Toast.LENGTH_SHORT).show();
        }
        lvReporte.setAdapter(adapter);
        elementos = new ArrayList<ApuntesMO>();
    }


    private class ObtenerReporte extends AsyncTask<Void, Void, Boolean> {

        String msg ="";
        @Override
        protected Boolean doInBackground(Void... params) {

            ObtenerReporte(new VolleyCallback() {
                @Override
                public boolean onSuccess() {
                    return true;
                }

                @Override
                public boolean onError() {
                    return false;
                }

                @Override
                public boolean onSuccess(String resp) {
                    return false;
                }

                @Override
                public boolean onError(String resp) {
                    return false;
                }
            });
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {

            EnableCtrls();
            showMsg();
            if (success) {
                cargarListaVista();
            }
        }

        private void showMsg(){
            Toast.makeText(reporteActivity.this, msg, Toast.LENGTH_SHORT).show();

        }

        private void ObtenerReporte(final VolleyCallback callBack) {

            DBHelper mydb = new DBHelper(reporteActivity.this);
            UUID guid = null;
            String sToken = "";
            Parametros token = mydb.getSingleParametro(4);

            if (token != null) {
                sToken = token.getValor();
            }


            Parametros usr = mydb.getSingleParametro(5);

            String susr = "";
            if (usr != null) {
                susr = usr.getValor();
            }
            String url = DBHelper.urlServer + "/api/Apuntamientos/GetReporteMO?login=" + susr + "&fechaInicio=" + dtpFechaIni.getText() + "&fechaFin=" + dtpFechaFin.getText();
            //HttpGet httpGet = new HttpGet(DBHelper.urlServer+"/api/Apuntamientos/GetReporteMO?login="+susr+"&fechaInicio="+dtpFechaIni.getText()+"&fechaFin="+dtpFechaFin.getText());
            //httpGet.setHeader("token", sToken);
            //HttpClient httpClient = new NetworkUtility().getDefaultHttpClient(reporteActivity.this, DBHelper.urlServer+"/api/Apuntamientos/GetReporteMO?login="+susr+"&fechaInicio="+dtpFechaIni.getText()+"&fechaFin="+dtpFechaFin.getText());

            RequestQueue requestQueue = NetworkUtility.getInstance(getApplicationContext()).getRequestQueue();
            NetworkUtility.nuke();
            final String finalSToken = sToken;

            JsonObjectRequest
                    jsonObjectRequest
                    = new JsonObjectRequest(
                    Request.Method.GET,
                    url,
                    null,
                    new Response.Listener() {
                        @Override
                        public void onResponse(Object response) {
                            //HttpResponse response = httpClient.execute(httpPost);
                            if (response != null) {


                                JSONObject data = (JSONObject) response;
                                try {
                                    if (response != null) {
                                        //BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));

/*
                    StringBuffer json = new StringBuffer(1024);
                    String tmp = "";
                    while ((tmp = reader.readLine()) != null)
                        json.append(tmp).append("\n");
                    reader.close();*/

                                        boolean error = data.getBoolean("<error>k__BackingField");
                                        String responseStr = data.getString("<msgError>k__BackingField");
                                        if (error) {
                                            callBack.onError();//return "Respuesta recibida del servidor: " + responseStr;
                                        } else {
                                            if (data.length() == 0)
                                                callBack.onError();
                                            else {
                                                JSONArray a = data.getJSONArray("<ResultadoReportes>k__BackingField");
                                                for (int i = 0; i < a.length(); i++) {
                                                    JSONObject hda = a.getJSONObject(i);
                                                    String fecha = hda.getString("FechaApunte");
                                                    String Recurso = hda.getString("DsRecurso");
                                                    String CodTrab = hda.getString("CodTrabajador");
                                                    String cuad = hda.getString("DsCuadrilla");
                                                    String NomTrabajador = hda.getString("NomTrabajador");
                                                    float QtdPago = Float.parseFloat(String.valueOf(hda.getDouble("QtdPago")));
                                                    String um = hda.getString("UM");
                                                    ApuntesMO mo = new ApuntesMO();
                                                    mo.setFechaApunte(fecha);
                                                    mo.setDsRecurso(Recurso);
                                                    mo.setTrabajador(CodTrab + " - " + NomTrabajador);
                                                    mo.setQTD_PAGO(QtdPago);
                                                    mo.setDsCuadrilla(cuad);
                                                    mo.setUM(um);
                                                    elementos.add(mo);
                                                }
                                                callBack.onSuccess();
                                            }
                                        }

                                    } else {
                                        callBack.onError();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                callBack.onSuccess();

                            } else {
                                callBack.onError();
                            }
                        }

                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                        }
                    }) {

                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", "application/json");
                    headers.put("token", finalSToken);
                    return headers;
                }

            };


            requestQueue.add(jsonObjectRequest);


        }
    }

    private class ReportAdapter extends ArrayAdapter<ApuntesMO>{
            private final Context context;
            private final List<ApuntesMO> values;

            public ReportAdapter(Context context, List<ApuntesMO> values) {
                super(context, -1, values);
                this.context = context;
                this.values = values;
            }

            @Override
            public ApuntesMO getItem(int position) {
                return values.get(position);
            }

            @Override
            public long getItemId(int position) {
                return position;
            }
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                LayoutInflater inflater = (LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View rowView = inflater.inflate(R.layout.rowmo_reporte, parent, false);
                TextView fecha = (TextView) rowView.findViewById(R.id.fecha);
                TextView recurso = (TextView) rowView.findViewById(R.id.recurso);
                TextView trab = (TextView) rowView.findViewById(R.id.Trabajador);
                TextView qtd = (TextView) rowView.findViewById(R.id.ctdPago);
                TextView um = (TextView) rowView.findViewById(R.id.um);
                TextView cuad = (TextView) rowView.findViewById(R.id.cuadrilla);

                fecha.setText(DBHelper.getDate( values.get(position).getFechaApunte()));
                recurso.setText(values.get(position).getDsRecurso());
                trab.setText(values.get(position).getTrabajador());
                qtd.setText(String.valueOf(values.get(position).getQTD_PAGO()));
                um.setText(values.get(position).getUM());
                cuad.setText(values.get(position).getDsCuadrilla());

                return rowView;
            }
        }




}
