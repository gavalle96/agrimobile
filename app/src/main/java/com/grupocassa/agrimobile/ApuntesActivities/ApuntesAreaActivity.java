package com.grupocassa.agrimobile.ApuntesActivities;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.grupocassa.agrimobile.CASSAUTILS.DBHelper;
import com.grupocassa.agrimobile.CLS.ApuntesArea;
import com.grupocassa.agrimobile.CLS.OS;
import com.grupocassa.agrimobile.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ApuntesAreaActivity extends AppCompatActivity {


    private Calendar myCalendar;
    EditText dtpFechaapunte;
    DatePickerDialog.OnDateSetListener date;
    DBHelper mydb;
    OS os;
    int IdOS;
    TextView txtAreaPlan;
    TextView txtAreaAcum;
    TextView txtAreaPend;
    EditText txtArea;
    ApAreaAdapter adapter;
    ArrayList<ApuntesArea> list;
    ListView lvItems;
    ImageButton btnGuardar;
    float areaapunt;
    float cantPlan;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apuntes_area);
        myCalendar = Calendar.getInstance();
        mydb = new DBHelper(this);

        dtpFechaapunte = (EditText) findViewById(R.id.dtpFecha);
        updateLabel();
        Bundle extras = getIntent().getExtras();
        IdOS = extras.getInt("IdOS");
        setTitle("OS: "+ IdOS);

        list = mydb.getApuntesArea(IdOS);


        txtAreaAcum = (TextView) findViewById(R.id.txtAreaAcum);
        txtAreaPlan = (TextView) findViewById(R.id.txtAreaPlan);
        txtAreaPend = (TextView) findViewById(R.id.txtAreaPend);
        txtArea = (EditText) findViewById(R.id.txtArea);
        lvItems = (ListView) findViewById(R.id.lvItems);
        btnGuardar = (ImageButton) findViewById(R.id.btnGuardar);
        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                float area = 0;
                if(!txtArea.getText().toString().equals("")) {
                    area = Float.parseFloat(txtArea.getText().toString());
                }
                if(area>0)
                if(mydb.getSingleArea(IdOS, dtpFechaapunte.getText().toString()) == null) {
                    if (areaapunt+area > cantPlan) {
                        AlertDialog alertDialog = new AlertDialog.Builder(ApuntesAreaActivity.this).create();
                        alertDialog.setTitle("Área sobrepasada");
                        alertDialog.setMessage("El área sobrepasa la cantidad planificada");
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        alertDialog.show();
                    }
                    else {
                        mydb.insertApunteArea(IdOS, dtpFechaapunte.getText().toString(), area);
                        list = mydb.getApuntesArea(IdOS);
                        adapter = new ApAreaAdapter(ApuntesAreaActivity.this, list);
                        lvItems.setAdapter(adapter);
                        setValoresOS();
                    }
                }
                else {
                    AlertDialog alertDialog = new AlertDialog.Builder(ApuntesAreaActivity.this).create();
                    alertDialog.setTitle("Apunte ya existe");
                    alertDialog.setMessage("El apunte ya existe para la fecha "+dtpFechaapunte.getText().toString());
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                }
            }
        });
        setValoresOS();

        adapter = new ApAreaAdapter(this,list);
        lvItems.setAdapter(adapter);



        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub

                Calendar tempCalendar = Calendar.getInstance();
                tempCalendar.set(Calendar.YEAR, year);
                tempCalendar.set(Calendar.MONTH, monthOfYear);
                tempCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                if (tempCalendar.getTimeInMillis() > Calendar.getInstance().getTimeInMillis()){


                    dtpFechaapunte.setError("No es posible seleccionar una fecha futura");
                    Toast.makeText(ApuntesAreaActivity.this, "No es posible seleccionar una fecha futura", Toast.LENGTH_SHORT).show();
                }else {
                    myCalendar.set(Calendar.YEAR, year);
                    myCalendar.set(Calendar.MONTH, monthOfYear);
                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    dtpFechaapunte.setError(null);
                }
                updateLabel();
            }

        };

        dtpFechaapunte.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(ApuntesAreaActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    private void setValoresOS(){
        txtArea.setText("");
        os = mydb.getSingleOS(IdOS);
        areaapunt = mydb.getAreaApuntada(IdOS);
        cantPlan = os.getCantidad();
        txtAreaPlan.setText(cantPlan+"");
        txtAreaAcum.setText(areaapunt+"");
        txtAreaPend.setText(os.getCantidad()-areaapunt+"");
    }

    private void updateLabel() {
        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat);

        dtpFechaapunte.setText(sdf.format(myCalendar.getTime()));
    }

    private class ApAreaAdapter extends ArrayAdapter<ApuntesArea> implements View.OnClickListener {
        private final Context context;
        private final List<ApuntesArea> values;

        public ApAreaAdapter(Context context, List<ApuntesArea> values) {
            super(context, -1, values);
            this.context = context;
            this.values = values;
        }

        @Override
        public ApuntesArea getItem(int position) {
            return values.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }
        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.row_area, parent, false);
            TextView dsapunte = (TextView) rowView.findViewById(R.id.textViewItemName);
            TextView dsintegrado = (TextView) rowView.findViewById(R.id.textViewItemPrice);
            ImageButton btneliminar = (ImageButton) rowView.findViewById(R.id.btnEliminar);

            ApuntesArea obj = values.get(position);
            if(obj.isIntegrado())
                btneliminar.setVisibility(View.GONE);
            btneliminar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new AlertDialog.Builder(ApuntesAreaActivity.this)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setTitle("Eliminar apunte")
                            .setMessage("¿Esta seguro de eliminar el apunte?")
                            .setPositiveButton("Si", new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    mydb.deleteApunteArea(values.get(position).getIdApArea());

                                    list = mydb.getApuntesArea(IdOS);
                                    adapter = new ApAreaAdapter(ApuntesAreaActivity.this, list);
                                    lvItems.setAdapter(adapter);
                                    setValoresOS();
                                }

                            })
                            .setNegativeButton("No", null)
                            .show();
                }
            });

            dsapunte.setText(obj.getFecha()+" - "+obj.getArea()+" HA.");
            dsintegrado.setText(obj.isIntegrado() ? "Integrado" : "No Integrado");


            if(!obj.isIntegrado()){
                dsapunte.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        editar(position);
                    }
                });
                dsintegrado.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        editar(position);
                    }
                });
            }

            return rowView;
        }
        @Override
        public void onClick(View v) {


        }

        private void editar(int i){
            final ApuntesArea obj = adapter.values.get(i);
            AlertDialog.Builder builder = new AlertDialog.Builder(ApuntesAreaActivity.this);
            builder.setTitle("Editar Apunte");

// Set up the input
            final EditText input = new EditText(ApuntesAreaActivity.this);
            input.setText(obj.getArea()+"");
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
            input.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_CLASS_NUMBER);
            builder.setView(input);

// Set up the buttons
            builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String sArea = input.getText().toString();
                    float area = 0;
                    if (!(sArea.isEmpty() || sArea.equals(""))) {
                        mydb.updateApunteArea(obj.getIdApArea(), Float.parseFloat(sArea));
                        list = mydb.getApuntesArea(IdOS);
                        adapter = new ApAreaAdapter(ApuntesAreaActivity.this, list);
                        lvItems.setAdapter(adapter);
                        setValoresOS();
                    }


                }
            });
            builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            builder.show();

        }
    }
}
