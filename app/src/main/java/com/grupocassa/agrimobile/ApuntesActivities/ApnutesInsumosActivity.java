package com.grupocassa.agrimobile.ApuntesActivities;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.grupocassa.agrimobile.CASSAUTILS.DBHelper;
import com.grupocassa.agrimobile.CASSAUTILS.GPSTracker;
import com.grupocassa.agrimobile.CLS.ApuntesInsumos;
import com.grupocassa.agrimobile.CLS.OS;
import com.grupocassa.agrimobile.CLS.OSRecursos;
import com.grupocassa.agrimobile.MyDialogFragment;
import com.grupocassa.agrimobile.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ApnutesInsumosActivity extends AppCompatActivity implements View.OnClickListener{

    private int IdOS, IdRecurso;
    private DBHelper mydb;
    private String FechaIni, FechaFin, FechaDigita, FechaApunte;
    private float Latitud, Longitud, Dosis, Area, Cantidad;
    private TextView tvIdOS,tvUM, txtCantidad, txtArea, txtObservacion;
    private Spinner spInsumos;
    private ArrayList<OSRecursos> recinsumos;
    private String Operacion;
    private Button btnGuardar;
    private OSRecursos psdet;
    private ScrollView scv;
    private float AreaApuntada;
    FloatingActionButton fabInfo;
    ApuntesInsumos apinsumos;
    int IdAp;
    EditText dtpFechaapunte;
    private int year, month, day;
    private Calendar calendar;
    private DatePickerDialog.OnDateSetListener date;

    GPSTracker gpsTracker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apnutes_insumos);

        scv = (ScrollView) findViewById(R.id.scvi);




        mydb = new DBHelper(this);
        FechaIni = mydb.getCurrentTimeStamp();

        dtpFechaapunte = (EditText) findViewById(R.id.dtpFechaApunte);




        tvIdOS = (TextView) findViewById(R.id.tvOSinsumos);
        spInsumos = (Spinner) findViewById(R.id.spInsumos);
        tvUM = (TextView) findViewById(R.id.txtUM);
        txtCantidad = (EditText) findViewById(R.id.txtCantidad);
        txtArea = (EditText) findViewById(R.id.txtArea);
        txtObservacion = (EditText) findViewById(R.id.txtObserv);

        btnGuardar = (Button) findViewById(R.id.btnGuardar);
        btnGuardar.setOnClickListener(this);
        fabInfo = (FloatingActionButton) findViewById(R.id.floatingActionButton);

        fabInfo.setOnClickListener(this);

        Bundle extras = getIntent().getExtras();

        IdAp = extras.getInt("IdApunte",0);


        IdOS = extras.getInt("IdOS");
        Operacion = extras.getString("Operacion");

        OS o = mydb.getSingleOS(IdOS);

        ArrayList<OSRecursos> osRecursoses = mydb.getAllRecursosByOS(IdOS);
        recinsumos = new ArrayList<OSRecursos>();
        recinsumos.add(new OSRecursos(IdOS,0,"","Selec. Recurso","","",0,0,0,false, false, 0));
        for (OSRecursos or: osRecursoses
             ) {
            if(or.getBkTipRecurso().equals("2")){
                recinsumos.add(or);
            }
        }


        MySimpleArrayAdapter adapterins = new MySimpleArrayAdapter(this,recinsumos);
        adapterins.setDropDownViewResource(R.layout.itemsimple);
        spInsumos.setAdapter(adapterins);

        spInsumos.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
                OSRecursos p = (OSRecursos) parentView.getItemAtPosition(position);

                IdRecurso = p.getIdRecurso();
                if (IdRecurso > 0)
                psdet = p;
                AreaApuntada = p.getAreaApunt();
                tvUM.setText(p.getUM());




                if (p.getUM().equals("UN")){
                    txtArea.setVisibility(View.GONE);
                }else{
                    txtArea.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        tvIdOS.setText("OS "+o.getIdOS()+" - "+o.getDsActividad());

        switch (Operacion){
            case "a":
                    setTitle("Nuevo Apuntamiento de Insumos");
                break;
            case "m":

                setTitle("Editar Apuntamiento de Insumos");
                break;
        }

        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);

        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        showDate(year, month, day);

        if (psdet== null) psdet = new OSRecursos();

        AreaApuntada = psdet.getAreaApunt();
        if (!Operacion.equals("a") && IdAp > 0){

            apinsumos = mydb.getSingleApunteInsumos(IdAp);
            for (OSRecursos orec:
                 recinsumos) {
                if (orec.getIdRecurso() == apinsumos.getIdRecurso()){

                    IdRecurso = apinsumos.getIdRecurso();
                    spInsumos.setSelection(adapterins.getPosition(orec),true);
                    psdet = orec;
                    AreaApuntada = psdet.getAreaApunt();
                    AreaApuntada = AreaApuntada - apinsumos.getArea();
                }
            }


            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            try {
                /*Date fec = format.parse(apinsumos.getFechaApunte());
                showDate(fec.getYear(),fec.getMonth(),fec.getDate());*/
                String f = apinsumos.getFechaApunte();
                Date fec = format.parse(f);
                Calendar cale = Calendar.getInstance();
                cale.setTime(fec);
                showDate(cale.get(Calendar.YEAR),cale.get(Calendar.MONTH),cale.get(Calendar.DAY_OF_MONTH));
            } catch (ParseException e) {
                e.printStackTrace();
            }



            psdet.setCantidadApunt(psdet.getCantidadApunt() - (apinsumos.getCantidad()));
            psdet.setAreaApunt(psdet.getAreaApunt() - apinsumos.getArea());

            float ctd = apinsumos.getCantidad(); //apinsumos.getDosis()*apinsumos.getArea();
            txtCantidad.setText(String.valueOf(ctd));
            txtArea.setText(String.valueOf(apinsumos.getArea()));
            txtObservacion.setText(apinsumos.getObservacion());

            spInsumos.setEnabled(false);
        }




        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                Calendar diasAnteriores =  Calendar.getInstance();
                diasAnteriores.add(Calendar.DAY_OF_MONTH,-1*DBHelper.MargenDiasPermitido());

                if (calendar.getTimeInMillis() > Calendar.getInstance().getTimeInMillis()){
                    calendar = Calendar.getInstance();
                    year = calendar.get(Calendar.YEAR);

                    month = calendar.get(Calendar.MONTH);
                    day = calendar.get(Calendar.DAY_OF_MONTH);
                    showDate(year, month, day);
                    dtpFechaapunte.setError("No es posible seleccionar una fecha futura");
                    Toast.makeText(ApnutesInsumosActivity.this, "No es posible seleccionar una fecha futura", Toast.LENGTH_SHORT).show();
                }else if(calendar.getTimeInMillis() < diasAnteriores.getTimeInMillis()){
                    calendar = Calendar.getInstance();
                    year = calendar.get(Calendar.YEAR);

                    month = calendar.get(Calendar.MONTH);
                    day = calendar.get(Calendar.DAY_OF_MONTH);
                    showDate(year, month, day);
                    dtpFechaapunte.setError("No puede apuntar más de "+DBHelper.MargenDiasPermitido()+" días atrás.");
                    Toast.makeText(ApnutesInsumosActivity.this, "No puede apuntar más de "+DBHelper.MargenDiasPermitido()+" días atrás.", Toast.LENGTH_SHORT).show();
                }
                else{
                    dtpFechaapunte.setError(null);
                    showDate(year, monthOfYear, dayOfMonth);
                }

            }

        };

        dtpFechaapunte.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(ApnutesInsumosActivity.this, date, calendar
                        .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

    }

    @SuppressWarnings("deprecation")
    public void setDate(View view) {
        showDialog(999);
        Toast.makeText(getApplicationContext(), "ca",
                Toast.LENGTH_SHORT)
                .show();
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        if (id == 999) {

            return new DatePickerDialog(this,
                    myDateListener, year, month, day);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new
            DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker arg0,
                                      int arg1, int arg2, int arg3) {
                    // TODO Auto-generated method stub
                    // arg1 = year
                    // arg2 = month
                    // arg3 = day
                    showDate(arg1, arg2, arg3);
                }
            };

    private void showDate(int year, int month, int day) {
        dtpFechaapunte.setText(new StringBuilder().append(day).append("/")
                .append(month+1).append("/").append(year));
        FechaApunte = mydb.getDate(year,month, day);
    }

    @Override
    public void onPause(){
        super.onPause();
        //Toast.makeText(this, "Actividad en pausa", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResume(){
        super.onResume();
        //Toast.makeText(this, "Actividad en resumen", Toast.LENGTH_SHORT).show();
        gpsTracker  = new GPSTracker(this);
        checkGPS();
    }


    private void checkGPS(){
        // check if GPS enabled

        if (gpsTracker.getIsGPSTrackingEnabled())
        {
            Latitud = Float.parseFloat(String.valueOf(gpsTracker.getLatitude()));
            Longitud = Float.parseFloat(String.valueOf(gpsTracker.getLongitude()));

        }
        else
        {
            showSettingsAlert();

        }
    }

    private void Reset(){
        scv.fullScroll(ScrollView.FOCUS_UP);
        txtArea.setText("");
        txtObservacion.setText("");
        txtCantidad.setText("");

        IdRecurso = 0;
        FechaIni = mydb.getCurrentTimeStamp();
       // checkGPS();

/*        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);

        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);*/
        //showDate(year, month+1, day);

        spInsumos.setSelection(0,true);
        Operacion = "a";
        apinsumos = new ApuntesInsumos();
    }

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        //Setting Dialog Title
        alertDialog.setTitle("Ubicación desactivada");

        //Setting Dialog Message
        alertDialog.setMessage("Active la ubicación GPS para poder seguir apuntando");

        //On Pressing Setting button
        alertDialog.setPositiveButton("Ajustes", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });

        //On pressing cancel button
        alertDialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.cancel();
                finish();
            }
        });

        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    @Override
    public void onClick(View v) {
        if(v == btnGuardar){
            if (ValidarApunte()){
                if(Guardar()){

                    Toast.makeText(this, "Apuntamiento guardado correctamente", Toast.LENGTH_LONG).show();
                    if (Operacion.equals("a")) {
                        Reset();
                        //recreate();
                    }
                    else
                        finish();
                }
            }
        }
        else if (v == fabInfo){
            DialogFragment newFragment = MyDialogFragment.newInstance(IdOS,"2");
            newFragment.show(getSupportFragmentManager(), "Recursos");
        }
    }

    private boolean ValidarApunte() {
        float pendiente = 0;
        String sCtd = txtCantidad.getText().toString();
        if (sCtd.equals("")) sCtd = "0";
        float Apuntado = Float.parseFloat(sCtd);

        if (psdet.getIdRecurso()==0)
        {
            Toast.makeText(this, "Seleccione un recurso", Toast.LENGTH_SHORT).show();
            return false;
        }



        //if (Operacion.equals("a"))
            pendiente = psdet.getCantidadPend() - psdet.getCantidadApunt();
        //else
          //  pendiente = psdet.getCantidadPend() - (psdet.getCantidadApunt()-Apuntado);
        String sArea = txtArea.getText().toString();
        if (sArea.equals("")){
            sArea = "0";
        }

        Area = Float.parseFloat(sArea);




        //if ( Apuntado > pendiente){
            if (pendiente <= 0){
                Toast.makeText(this, "Ya ha consumido la totalidad del insumo", Toast.LENGTH_SHORT).show();

                return false;
            }
            else if (pendiente-Apuntado < 0){
                Toast.makeText(this, "No puede apuntar más de " + pendiente + " " + psdet.getUM(), Toast.LENGTH_LONG).show();

                return false;
            }
       // }
        OS plan = mydb.getSingleOS(IdOS);
        float AreaPendiente = plan.getCantidad() -AreaApuntada;
        if (AreaPendiente- this.Area < 0 && !psdet.getBkTipRecurso().equals("UN") ){
            Toast.makeText(this, "No puede apuntar un área mayor a "+AreaPendiente+" Ha.", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(AreaPendiente == 0){
            Toast.makeText(this, "Ya ha aplicado en la totalidad de área de la OS", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (Apuntado <= 0){
            Toast.makeText(this,"No puede apuntar una cantidad menor o igual a cero",Toast.LENGTH_LONG).show();
            return false;
        }

        if (Area < 0){
            Toast.makeText(this,"No puede apuntar un área menor a cero",Toast.LENGTH_LONG).show();
            return false;
        }

        return true;

    }

    private boolean Guardar(){
        String sArea = txtArea.getText().toString();
        if (sArea.equals("")){
            sArea = "0";
        }

        Area = Float.parseFloat(sArea);

        String sCtd = txtCantidad.getText().toString();
        if (sCtd.equals("")) sCtd = "0";
        float Apuntado = Float.parseFloat(sCtd);

        if (Area > 0)
            Dosis = Apuntado / Area;
        else
            Dosis = Apuntado;

        psdet.setCantidadApunt(psdet.getCantidadApunt()+Apuntado);
        psdet.setAreaApunt(AreaApuntada+Area);



        mydb.updateOSRecurso(psdet);
        if (Operacion.equals("a"))
            return mydb.insertApuntesInsumos(IdOS,FechaApunte,IdRecurso,Area,Dosis,Apuntado,"",mydb.getCurrentTimeStamp(),txtObservacion.getText().toString(),FechaIni,mydb.getCurrentTimeStamp(),Latitud,Longitud);
        else
            return mydb.updateApunteInsumos(IdAp,FechaApunte,Area,Dosis,Apuntado,mydb.getCurrentTimeStamp(),txtObservacion.getText().toString(),FechaIni,mydb.getCurrentTimeStamp(),Latitud,Longitud);
    }

    private class MySimpleArrayAdapter extends ArrayAdapter<OSRecursos> implements View.OnClickListener {
        private final Context context;
        private final List<OSRecursos> values;

        public MySimpleArrayAdapter(Context context, List<OSRecursos> values) {
            super(context,R.layout.itemsimple,R.id.txtItem, values);
            this.context = context;
            this.values = values;
        }

        @Override
        public OSRecursos getItem(int position) {
            return values.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.itemsimple, parent, false);
            TextView insumo = (TextView) rowView.findViewById(R.id.txtItem);

            insumo.setText(values.get(position).getDsRecurso());;
            return rowView;
        }
        @Override
        public void onClick(View v) {


        }
    }
}
