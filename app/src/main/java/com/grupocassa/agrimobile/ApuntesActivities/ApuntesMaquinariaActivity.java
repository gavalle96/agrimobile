package com.grupocassa.agrimobile.ApuntesActivities;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.grupocassa.agrimobile.CASSAUTILS.CustomEquiposAdapter;
import com.grupocassa.agrimobile.CASSAUTILS.CustomImplementosAdapter;
import com.grupocassa.agrimobile.CASSAUTILS.CustomTrabajadoresAdapter;
import com.grupocassa.agrimobile.CASSAUTILS.DBHelper;
import com.grupocassa.agrimobile.CASSAUTILS.GPSTracker;
import com.grupocassa.agrimobile.CLS.ApuntesMaquinaria;
import com.grupocassa.agrimobile.CLS.Equipos;
import com.grupocassa.agrimobile.CLS.OS;
import com.grupocassa.agrimobile.CLS.OSRecursos;
import com.grupocassa.agrimobile.CLS.TiposDocumento;
import com.grupocassa.agrimobile.CLS.Trabajadores;
import com.grupocassa.agrimobile.MyDialogFragment;
import com.grupocassa.agrimobile.R;
import com.grupocassa.agrimobile.ScanActivity;

import java.security.Key;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ApuntesMaquinariaActivity extends AppCompatActivity implements View.OnClickListener{

    OSRecursos recurso;

    ApuntesMaquinaria apunte;

    DBHelper mydb;
    public AutoCompleteTextView txtTrabajador, txtAyudante, txtEq1, txtEq2, txtEq3, txtEq4;
    TextView txtum, txtHTOTAL, txtOTOTAL, txtMTOTAL;
    public AutoCompleteTextView txtEquipo,txtImplemento;
    EditText txtCantidad, txtObserv,txthini, txthfin, txtoini, txtofin,  txtmini, txtmfin, dtpFechaApunte, txtOrigen, txtDestino, txtRuta, txtNumDocumento, txtdseq1, txtdseq2, txtdseq3, txtdseq4, txtdsimplemento, txtdsequipo;
    Button btnGuardar;
    ImageButton bar1, bar2, btnTrab, btnAyud, btnEq1, btnEq2, btnEq3, btnEq4;
    CheckBox chkhfalla, chkofalla, chkmfalla;
    int IdOS, IdRecurso, IdApunte, IdTipDocInterno;
    public int Eq1, Eq2, Eq3, Eq4, IdTrabajador, IdAyudante;
    String FechaIni, tipo, Operacion, NumDocInterno, Origen, Destino, Ruta, codEquipo, codEquipoImplemento;
    float Latitud, Longitud, Cantidad;
    Spinner spRecursos, spDocumento;
    GPSTracker gpsTracker;
    MySimpleArrayAdapter adapRecursos;
    DocumentosArrayAdapter adapDocumentos;
    FloatingActionButton fabInfo;
    ScrollView scv;

    TextInputLayout tayudante, tImplemento, tDsImplemento,tOrigen, tdestino, truta, teq1, teq2, teq3, teq4, tdseq1, tdseq2, tdseq3, tdseq4,thini,thfin;

    private int year, month, day;
    private Calendar calendar;
    private DatePickerDialog.OnDateSetListener date;
    private String FechaApunte;
    private ArrayList<Equipos> eqs;
    private ArrayList<Equipos> eqsImpl;

    private String UMP;
    private CustomImplementosAdapter adapterImpl;
    private CustomEquiposAdapter adapterEq;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apuntes_maquinaria);
        mydb = new  DBHelper(this);
        eqs = mydb.getAllEquipos();
        eqsImpl = mydb.getAllEquipos();
        if (eqs == null) eqs = new ArrayList<Equipos>();
        if (eqsImpl == null) eqsImpl = new ArrayList<Equipos>();
        FechaIni = mydb.getCurrentTimeStamp();


        IdAyudante = 0;
        Eq1 = 0;
        Eq2 = 0;
        Eq3 = 0;
        Eq4 = 0;

        scv = (ScrollView) findViewById(R.id.scvm);

        spRecursos = (Spinner) findViewById(R.id.spRecursos);
        spDocumento = (Spinner) findViewById(R.id.spDocumento);
        tayudante = (TextInputLayout) findViewById(R.id.txvayud);
        txtEq1 = (AutoCompleteTextView) findViewById(R.id.txtEq1);
        txtEq2 = (AutoCompleteTextView) findViewById(R.id.txtEq2);
        txtEq3 = (AutoCompleteTextView) findViewById(R.id.txtEq3);
        txtEq4 = (AutoCompleteTextView) findViewById(R.id.txtEq4);
        txtOrigen = (EditText) findViewById(R.id.txtOrigen);
        txtDestino = (EditText) findViewById(R.id.txtDestino);
        txtRuta  =  (EditText) findViewById(R.id.txtRuta);
        txtNumDocumento = (EditText) findViewById(R.id.txtDocumento);

        thini = (TextInputLayout) findViewById(R.id.til15);
        thfin = (TextInputLayout) findViewById(R.id.til16);


        txtTrabajador = (AutoCompleteTextView) findViewById(R.id.txtTrabajador);
        txtAyudante = (AutoCompleteTextView) findViewById(R.id.txtAyudante);
        txtEquipo = (AutoCompleteTextView) findViewById(R.id.txtEquipo);
        txthini = (EditText) findViewById(R.id.HOR_INI);
        txthfin = (EditText) findViewById(R.id.HOR_FIN);
        txtoini = (EditText) findViewById(R.id.ODO_INI);
        txtofin  =  (EditText) findViewById(R.id.ODO_FIN);
        txtImplemento = (AutoCompleteTextView) findViewById(R.id.txtImplemento);
        txtmini = (EditText) findViewById(R.id.MED_INI);
        txtmfin = (EditText) findViewById(R.id.MED_FIN);
        txtCantidad = (EditText) findViewById(R.id.txtCantidad);
        txtObserv = (EditText) findViewById(R.id.txtObservacion);
        txtum = (TextView) findViewById(R.id.txtum);
        btnGuardar = (Button) findViewById(R.id.btnGuardar);
        chkhfalla = (CheckBox) findViewById(R.id.chkHorFalla);
        chkofalla = (CheckBox) findViewById(R.id.chkOdoFalla);
        chkmfalla = (CheckBox) findViewById(R.id.chkMedFalla);
        fabInfo = (FloatingActionButton) findViewById(R.id.floatingActionButton);
        txtHTOTAL = (TextView) findViewById(R.id.lblHDURA);
        txtOTOTAL = (TextView) findViewById(R.id.lblODURA);
        txtMTOTAL = (TextView) findViewById(R.id.lblMDURA);

        dtpFechaApunte = (EditText) findViewById(R.id.dtpFechaApunte);

        tImplemento = (TextInputLayout) findViewById(R.id.tilyimplemento);
        tDsImplemento = (TextInputLayout) findViewById(R.id.tilyDsImplemento);
        tOrigen = (TextInputLayout) findViewById(R.id.tlytOrigen);
        tdestino = (TextInputLayout) findViewById(R.id.tilytDestino);
        truta = (TextInputLayout) findViewById(R.id.tlytRuta);
        teq1 = (TextInputLayout) findViewById(R.id.tilyeq1);
        teq2 = (TextInputLayout) findViewById(R.id.tilyeq2);
        teq3 = (TextInputLayout) findViewById(R.id.tilyeq3);
        teq4 = (TextInputLayout) findViewById(R.id.tilyeq4);

        tdseq1 = (TextInputLayout) findViewById(R.id.tilytdseq1);
        tdseq2 = (TextInputLayout) findViewById(R.id.tilytEq2);
        tdseq3 = (TextInputLayout) findViewById(R.id.tilytDsEq3);
        tdseq4 = (TextInputLayout) findViewById(R.id.tilytDsEq4);

        txtdseq1 = (EditText) findViewById(R.id.txtDsEq1);
        txtdseq2 = (EditText) findViewById(R.id.txtDsEq2);
        txtdseq3 = (EditText) findViewById(R.id.txtDsEq3);
        txtdseq4 = (EditText) findViewById(R.id.txtDsEq4);
        txtdsequipo = (EditText) findViewById(R.id.txtDsEquipo);
        txtdsimplemento = (EditText) findViewById(R.id.txtDsImplemento);


        fabInfo.setOnClickListener(this);

        bar1 = (ImageButton) findViewById(R.id.btnlector1);
        bar2 = (ImageButton) findViewById(R.id.btnlector2);

        bar1.setOnClickListener(this);
        bar2.setOnClickListener(this);

        btnTrab = (ImageButton) findViewById(R.id.btnTrab);
        btnAyud= (ImageButton) findViewById(R.id.btnAyud);
        btnTrab.setOnClickListener(this);
        btnAyud.setOnClickListener(this);

        btnEq1 = (ImageButton) findViewById(R.id.btnEq1);
        btnEq2 = (ImageButton) findViewById(R.id.btnEq2);
        btnEq3 = (ImageButton) findViewById(R.id.btnEq3);
        btnEq4 = (ImageButton) findViewById(R.id.btnEq4);

        btnEq1.setOnClickListener(this);
        btnEq2.setOnClickListener(this);
        btnEq3.setOnClickListener(this);
        btnEq4.setOnClickListener(this);

        //EQUIVALENTE ON CHANGE EN JAVASCRIPT
        txthini.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {

                // you can call or do what you want with your EditText here
                float inicio = 0, fin = 0;



                try{
                    inicio = Float.parseFloat(txthini.getText().toString());

                }
                catch (NumberFormatException e){

                }
                try{

                    fin = Float.parseFloat(txthfin.getText().toString());
                }
                catch (NumberFormatException e){

                }

                txtHTOTAL.setText("Total: \n"+String.valueOf( fin-inicio ));

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });

        txthfin.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {

                // you can call or do what you want with your EditText here
                float inicio = 0, fin = 0;



                try{
                    inicio = Float.parseFloat(txthini.getText().toString());

                }
                catch (NumberFormatException e){

                }
                try{

                    fin = Float.parseFloat(txthfin.getText().toString());
                }
                catch (NumberFormatException e){

                }

                txtHTOTAL.setText("Total: \n"+String.valueOf( fin-inicio ));

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });

        txtoini.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {

                // you can call or do what you want with your EditText here
                float inicio = 0, fin = 0;



                try{
                    inicio = Float.parseFloat(txtoini.getText().toString());

                }
                catch (NumberFormatException e){

                }
                try{

                    fin = Float.parseFloat(txtofin.getText().toString());
                }
                catch (NumberFormatException e){

                }

                txtOTOTAL.setText("Total: \n"+String.valueOf( fin-inicio ));

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });

        txtofin.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {

                // you can call or do what you want with your EditText here
                float inicio = 0, fin = 0;



                try{
                    inicio = Float.parseFloat(txtoini.getText().toString());

                }
                catch (NumberFormatException e){

                }
                try{

                    fin = Float.parseFloat(txtofin.getText().toString());
                }
                catch (NumberFormatException e){

                }

                txtOTOTAL.setText("Total: \n"+String.valueOf( fin-inicio ));

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });

        txtmini.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {

                // you can call or do what you want with your EditText here
                float inicio = 0, fin = 0;



                try{
                    inicio = Float.parseFloat(txtmini.getText().toString());

                }
                catch (NumberFormatException e){

                }
                try{

                    fin = Float.parseFloat(txtmfin.getText().toString());
                }
                catch (NumberFormatException e){

                }

                txtMTOTAL.setText("Total: \n"+String.valueOf( fin-inicio ));

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });

        txtmfin.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {

                // you can call or do what you want with your EditText here
                float inicio = 0, fin = 0;



                try{
                    inicio = Float.parseFloat(txtmini.getText().toString());

                }
                catch (NumberFormatException e){

                }
                try{

                    fin = Float.parseFloat(txtmfin.getText().toString());
                }
                catch (NumberFormatException e){

                }

                txtMTOTAL.setText("Total: \n"+String.valueOf( fin-inicio ));

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });
        ///////////////////////////7




        Bundle extras = getIntent().getExtras();

        IdOS = extras.getInt("IdOS");
        OS os = mydb.getSingleOS(IdOS);
        UMP = "";
        //Evalua si contiene la unidad de medida
        String[] DsUM = os.getDsActividad().split("-");
        if(DsUM.length > 0) {
            String UMVal = DsUM[DsUM.length - 1];

            String[] KeyValUM = UMVal.split(":");
            if(KeyValUM.length == 2){
                UMP = KeyValUM[KeyValUM.length-1].trim();
            }
        }
        Operacion = extras.getString("Operacion");
        tipo = extras.getString("tipo");
        IdApunte = extras.getInt("IdApunte",0);

        ArrayList<OSRecursos> recursoses = mydb.getAllRecursosByOS(IdOS);
        ArrayList<OSRecursos> recs = new ArrayList<OSRecursos>();

        recs.add(new OSRecursos(IdOS,0,"","Selecione Recurso","","",0,0,0,false, false, 0));

        for (OSRecursos osrec: recursoses ) {

            if (osrec.getBkTipRecurso().equals(tipo)){
                recs.add(osrec);
            }
        }

        adapRecursos = new MySimpleArrayAdapter(this,recs);
        adapRecursos.setDropDownViewResource(R.layout.itemsimple);
        spRecursos.setAdapter(adapRecursos);

        spRecursos.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
                OSRecursos p = (OSRecursos) parentView.getItemAtPosition(position);

                IdRecurso = p.getIdRecurso();

                recurso = p;
                txtum.setText(p.getUM());
                if(!p.getAyudante()){
                    tayudante.setVisibility(View.GONE);
                    txtAyudante.setVisibility(View.GONE);
                    btnAyud.setVisibility(View.GONE);
                    TextInputLayout txvayud = (TextInputLayout) findViewById(R.id.txvayud);
                    if(txvayud!=null)txvayud.setVisibility(View.GONE);
                }
                else{
                    tayudante.setVisibility(View.VISIBLE);
                    txtAyudante.setVisibility(View.VISIBLE);
                    btnAyud.setVisibility(View.VISIBLE);
                    TextInputLayout txvayud = (TextInputLayout) findViewById(R.id.txvayud);
                    if(txvayud!=null)txvayud.setVisibility(View.VISIBLE);
                }


                ShowHideLowBoy(p.getBkRecurso().equals("42004"));
                ConfiguracionSociedad();

                if(codEquipo != null && codEquipo != ""){
                    OSRecursos rec = mydb.getAllRecursosByOSId(IdOS, IdRecurso);
                    //Si el recurso requiere implemento, se coloca un adaptador con la subFlota de equipos asignada
                    boolean escassa = mydb.PropietarioCASSA(codEquipo);
                    boolean reqimpl = rec.getRequiereImplemento();
                    /*if(escassa && reqimpl){

                        eqsImpl = mydb.ConsultaEquiposSubFlota(rec.getIdRecurso());
                        adapterImpl = new CustomImplementosAdapter(ApuntesMaquinariaActivity.this, android.R.layout.simple_dropdown_item_1line, eqsImpl);

                        txtImplemento.setAdapter(adapterImpl);

                        txtImplemento.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                            @Override
                            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                                    long arg3) {
                                Equipos selected = (Equipos) arg0.getAdapter().getItem(arg2);
                                codEquipoImplemento = selected.getCodEquipo();
                                txtdsimplemento.setText(selected.getNomEquipo());

                            }

                        });
                    }
                    else{*/

                        adapterImpl.clear();

                       adapterImpl.notifyDataSetChanged();

                        txtImplemento.setAdapter(ApuntesMaquinariaActivity.this.adapterEq);
                        txtImplemento.setText("");
                        txtdsimplemento.setText("");
                    //}
                    //txtImplemento.setText("");
                    //txtdsimplemento.setText("");
                }




            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });


        ArrayList<TiposDocumento> documents = new ArrayList<TiposDocumento>();
        documents.add(new TiposDocumento(0,"","Selec. Tipo Documento"));
        ArrayList<TiposDocumento> docu = mydb.getAllTiposDocumento();

        for (TiposDocumento t: docu
             ) {
            documents.add(t);
        }

        adapDocumentos = new DocumentosArrayAdapter(this,documents);
        adapRecursos.setDropDownViewResource(R.layout.itemsimple);
        spRecursos.setAdapter(adapRecursos);

        spDocumento.setAdapter(adapDocumentos);

        spDocumento.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
                TiposDocumento p = (TiposDocumento) parentView.getItemAtPosition(position);

                IdTipDocInterno = p.getIdTipDocInterno();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        ////////////////

        setTitle("Nuevo Apuntamiento de Maquinaria");
        if(tipo.equals("3")) {
            txtoini.setVisibility(View.GONE);
            txtofin.setVisibility(View.GONE);
            txtOTOTAL.setVisibility(View.GONE);
            chkofalla.setVisibility(View.GONE);
            chkofalla.setChecked(true);
            setTitle("Nuevo Apuntamiento de Maquinaria");
        }
        else {
            txtmini.setVisibility(View.GONE);
            txtmfin.setVisibility(View.GONE);
            txtMTOTAL.setVisibility(View.GONE);
            chkmfalla.setVisibility(View.GONE);
            chkmfalla.setChecked(true);
            txthini.setVisibility(View.GONE);
            txthfin.setVisibility(View.GONE);
            txtHTOTAL.setVisibility(View.GONE);
            chkhfalla.setVisibility(View.GONE);
            chkhfalla.setVisibility(View.GONE);
            setTitle("Nuevo Apuntamiento de Servicios Agrícolas");
        }

        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);

        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        showDate(year, month+1, day);
        ShowHideLowBoy(false);
        ConfiguracionSociedad();
        Reset();
        if (IdApunte > 0 && Operacion.equals("m")){
            if(tipo.equals("3"))
                setTitle("Editar Apuntamiento de Maquinaria");
            else
                setTitle("Editar Apuntamiento de Servicios Agrícolas");

            ArrayList<ApuntesMaquinaria> apuntes = mydb.getApuntesMaquinariaByOS(IdOS,tipo);
            ApuntesMaquinaria apunte = new ApuntesMaquinaria();



            for (ApuntesMaquinaria ap: apuntes
                    ) {
                if (ap.getIdApMaquinaria() == IdApunte){
                    apunte = ap;
                }
            }
            if (apunte.getIdTipDocInterno()>0){
                int idtdi = apunte.getIdTipDocInterno();

                for (TiposDocumento td:
                        documents) {
                    if (td.getIdTipDocInterno() == apunte.getIdTipDocInterno()){
                        spDocumento.setSelection(adapDocumentos.getPosition(td));//setSelection(adapDocumentos.getPosition(td),true);

                    }
                }


            }
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            try {
                String f = apunte.getFechaApunte();
                Date fec = format.parse(f);
                Calendar cale = Calendar.getInstance();
                cale.setTime(fec);
                showDate(cale.get(Calendar.YEAR),cale.get(Calendar.MONTH)+1,cale.get(Calendar.DAY_OF_MONTH));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            for (OSRecursos orec:
                    recs) {
                if (orec.getIdRecurso() == apunte.getIdRecurso()){

                    IdRecurso = apunte.getIdRecurso();
                    spRecursos.setSelection(adapRecursos.getPosition(orec));//,true);
                    recurso = orec;
                    recurso.setCantidadApunt(orec.getCantidadApunt()-apunte.getQTD_PAGO());
                }
            }

            Equipos e = new Equipos();
            txtTrabajador.setText(apunte.getTrabajador());
            IdTrabajador = apunte.getIdTrabajador();
            codEquipo = String.valueOf( apunte.getCodEquipo());
            codEquipoImplemento = String.valueOf( apunte.getCodEquipoImplemento());
            if (!codEquipo.equals("")) {
                e = mydb.getSingleEquipoByCod(codEquipo);
                if (e!=null)
                txtEquipo.setText(e.getCodEquipo()+" - "+e.getNomEquipo());
            }

            txthini.setText(String.valueOf( apunte.getHOR_INI()));
            txthfin.setText(String.valueOf( apunte.getHOR_FIN()));
            txtoini.setText(String.valueOf( apunte.getODO_INI()));
            txtofin.setText(String.valueOf( apunte.getODO_FIN()));
            if (!codEquipoImplemento.equals("")) {
                e = mydb.getSingleEquipoByCod(codEquipoImplemento);
                if (e!=null)
                txtImplemento.setText(e.getCodEquipo()+" - "+e.getNomEquipo());
            }
            txtmini.setText(String.valueOf(apunte.getMED_INI()));
            txtmfin.setText(String.valueOf(apunte.getMED_FIN()));
            txtCantidad.setText(String.valueOf(apunte.getQTD_PAGO()));
            txtObserv.setText(String.valueOf(apunte.getObservacion()));
            txtAyudante.setText(String.valueOf(apunte.getAyudante()));


            Eq1 = apunte.getEquipo1();
            Eq2 = apunte.getEquipo2();
            Eq3 = apunte.getEquipo3();
            Eq4 = apunte.getEquipo4();

            if (Eq1 > 0) {
                e = mydb.getSingleEquipoById(Eq1);
                if (e!=null)
                txtEq1.setText(e.getCodEquipo()+" - "+e.getNomEquipo());
            }

            if (Eq2 > 0) {
                e = mydb.getSingleEquipoById(Eq2);
                if (e!=null)
                txtEq2.setText(e.getCodEquipo()+" - "+e.getNomEquipo());
            }

            if (Eq3 > 0) {
                e = mydb.getSingleEquipoById(Eq3);
                if (e!=null)
                txtEq3.setText(e.getCodEquipo()+" - "+e.getNomEquipo());
            }

            if (Eq4 > 0) {
                e = mydb.getSingleEquipoById(Eq4);
                if (e!=null)
                txtEq4.setText(e.getCodEquipo()+" - "+e.getNomEquipo());
            }

            IdTipDocInterno = apunte.getIdTipDocInterno();
            NumDocInterno = apunte.getNumDocInterno();

            txtNumDocumento.setText(NumDocInterno);

            Origen = apunte.getOrigen();
            Destino = apunte.getDestino();
            Ruta = apunte.getRuta();

            txtOrigen.setText(Origen);
            txtDestino.setText(Destino);
            txtRuta.setText(Ruta);




            if (apunte.getHOR_FALLA()>0){
                chkhfalla.setChecked(true);
            }
            if (apunte.getODO_FALLA()>0){
                chkofalla.setChecked(true);
            }
            if (apunte.getMED_FALLA()>0){
                chkmfalla.setChecked(true);
            }


        }

        ArrayList<Trabajadores> lOS = mydb.getTrabajadores();
        CustomTrabajadoresAdapter adapter = new CustomTrabajadoresAdapter(this, android.R.layout.simple_dropdown_item_1line, lOS);
        txtTrabajador.setAdapter(adapter);

        txtTrabajador.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                Trabajadores selected = (Trabajadores) arg0.getAdapter().getItem(arg2);
                IdTrabajador = selected.getIdTrabajador();

            }

        });
        txtAyudante.setAdapter(adapter);
        txtAyudante.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                Trabajadores selected = (Trabajadores) arg0.getAdapter().getItem(arg2);
                IdAyudante = selected.getIdTrabajador();

            }

        });



        adapterEq = new CustomEquiposAdapter(this, android.R.layout.simple_dropdown_item_1line, eqs);
       adapterImpl = new CustomImplementosAdapter(this, android.R.layout.simple_dropdown_item_1line, eqsImpl);

        txtEquipo.setAdapter(adapterEq);

        txtEquipo.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                Equipos selected = (Equipos) arg0.getAdapter().getItem(arg2);
                codEquipo = selected.getCodEquipo();
                txtdsequipo.setText(selected.getNomEquipo());
                txtImplemento.setText("");
                txtdsimplemento.setText("");
                if(IdRecurso > 0){
                    OSRecursos rec = mydb.getAllRecursosByOSId(IdOS, IdRecurso);
                    //Si el recurso requiere implemento, se coloca un adaptador con la subFlota de equipos asignada
                    boolean escassa = mydb.PropietarioCASSA(codEquipo);
                    boolean reqimpl = rec.getRequiereImplemento();
                    if(codEquipo != null && codEquipo != ""){
                       /* if(escassa && reqimpl ){

                            eqsImpl = mydb.ConsultaEquiposSubFlota(rec.getIdRecurso());
                            adapterImpl = new CustomImplementosAdapter(ApuntesMaquinariaActivity.this, android.R.layout.simple_dropdown_item_1line, eqsImpl);

                            txtImplemento.setAdapter(adapterImpl);

                            txtImplemento.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                                @Override
                                public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                                        long arg3) {
                                    Equipos selected = (Equipos) arg0.getAdapter().getItem(arg2);
                                    codEquipoImplemento = selected.getCodEquipo();
                                    txtdsimplemento.setText(selected.getNomEquipo());

                                }

                            });
                        }
                        else{
*/
                            adapterImpl.clear();

                            adapterImpl.notifyDataSetChanged();

                            txtImplemento.setAdapter(adapterEq);

                        //}
                    }


                }


            }

        });

        //txtImplemento.setAdapter(adapterImpl);

        txtImplemento.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                Equipos selected = (Equipos) arg0.getAdapter().getItem(arg2);
                codEquipoImplemento = selected.getCodEquipo();
                txtdsimplemento.setText(selected.getNomEquipo());

            }

        });

        txtEq1.setAdapter(adapterEq);

        txtEq1.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                Equipos selected = (Equipos) arg0.getAdapter().getItem(arg2);
                Eq1 = selected.getIdEquipo();
                txtdseq1.setText(selected.getNomEquipo());

                if(!ValidarEquipos()){
                    Eq1 = 0;
                    txtEq1.setText("");
                    txtdseq1.setText("");
                }

            }

        });


        txtEq2.setAdapter(adapterEq);

        txtEq2.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                Equipos selected = (Equipos) arg0.getAdapter().getItem(arg2);
                Eq2 = selected.getIdEquipo();
                txtdseq2.setText(selected.getNomEquipo());
                if(!ValidarEquipos()){
                    Eq2 = 0;
                    txtEq2.setText("");
                    txtdseq2.setText("");
                }

            }

        });

        txtEq3.setAdapter(adapterEq);

        txtEq3.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                Equipos selected = (Equipos) arg0.getAdapter().getItem(arg2);
                Eq3 = selected.getIdEquipo();
                txtdseq3.setText(selected.getNomEquipo());
                if(!ValidarEquipos()){
                    Eq3 = 0;
                    txtEq3.setText("");
                    txtdseq3.setText("");
                }

            }

        });

        txtEq4.setAdapter(adapterEq);

        txtEq4.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                Equipos selected = (Equipos) arg0.getAdapter().getItem(arg2);
                Eq4 = selected.getIdEquipo();
                txtdseq4.setText(selected.getNomEquipo());
                if(!ValidarEquipos()){
                    Eq4 = 0;
                    txtEq4.setText("");
                    txtdseq4.setText("");
                }
            }

        });


        btnGuardar.setOnClickListener(this);
        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                Calendar diasAnteriores =  Calendar.getInstance();
                diasAnteriores.add(Calendar.DAY_OF_MONTH,-1*DBHelper.MargenDiasPermitido());

                if (calendar.getTimeInMillis() > Calendar.getInstance().getTimeInMillis()){
                    calendar = Calendar.getInstance();
                    year = calendar.get(Calendar.YEAR);

                    month = calendar.get(Calendar.MONTH);
                    day = calendar.get(Calendar.DAY_OF_MONTH);
                    showDate(year, month+1, day);
                    dtpFechaApunte.setError("No es posible seleccionar una fecha futura");
                    Toast.makeText(ApuntesMaquinariaActivity.this, "No es posible seleccionar una fecha futura", Toast.LENGTH_SHORT).show();
                }else if(calendar.getTimeInMillis() < diasAnteriores.getTimeInMillis()){
                    calendar = Calendar.getInstance();
                    year = calendar.get(Calendar.YEAR);

                    month = calendar.get(Calendar.MONTH);
                    day = calendar.get(Calendar.DAY_OF_MONTH);
                    showDate(year, month+1, day);
                    dtpFechaApunte.setError("No puede apuntar más de "+DBHelper.MargenDiasPermitido()+" días atrás.");
                    Toast.makeText(ApuntesMaquinariaActivity.this, "No puede apuntar más de "+DBHelper.MargenDiasPermitido()+" días atrás.", Toast.LENGTH_SHORT).show();
                }
                else{
                    dtpFechaApunte.setError(null);
                    showDate(year, monthOfYear+1, dayOfMonth);
                }
            }

        };

        dtpFechaApunte.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(ApuntesMaquinariaActivity.this, date, calendar
                        .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });


    }

    private void showDate(int year, int month, int day) {
        dtpFechaApunte.setText(new StringBuilder().append(day).append("/")
                .append(month).append("/").append(year));
        FechaApunte = mydb.getDate(year,month-1, day);
    }
    @Override
    public void onResume(){
        super.onResume();
        //Toast.makeText(this, "Actividad en resumen", Toast.LENGTH_SHORT).show();
        gpsTracker  = new GPSTracker(this);
        checkGPS();
    }

    private void ShowHideLowBoy(boolean EsLowBoy){

        if (EsLowBoy){
            btnAyud.setVisibility(View.GONE);
            txtImplemento.setHint("LowBoy Utilizado");
            txtImplemento.setVisibility(View.VISIBLE);
            txtdsimplemento.setVisibility(View.VISIBLE);
            tImplemento.setVisibility(View.VISIBLE);
            tOrigen.setVisibility(View.VISIBLE);
            tdestino.setVisibility(View.VISIBLE);
            truta.setVisibility(View.VISIBLE);
            teq1.setVisibility(View.VISIBLE);
            teq2.setVisibility(View.VISIBLE);
            teq3.setVisibility(View.VISIBLE);
            teq4.setVisibility(View.VISIBLE);
            btnEq1.setVisibility(View.VISIBLE);
            btnEq2.setVisibility(View.VISIBLE);
            btnEq3.setVisibility(View.VISIBLE);
            btnEq4.setVisibility(View.VISIBLE);
            
            txtOrigen.setVisibility(View.VISIBLE);
            txtDestino.setVisibility(View.VISIBLE);
            txtRuta.setVisibility(View.VISIBLE);
            txtEq1.setVisibility(View.VISIBLE);
            txtEq2.setVisibility(View.VISIBLE);
            txtEq3.setVisibility(View.VISIBLE);
            txtEq4.setVisibility(View.VISIBLE);

            tdseq1.setVisibility(View.VISIBLE);
            tdseq2.setVisibility(View.VISIBLE);
            tdseq3.setVisibility(View.VISIBLE);
            tdseq4.setVisibility(View.VISIBLE);


            txtImplemento.setVisibility(View.VISIBLE);
            txtdsimplemento.setVisibility(View.VISIBLE);
            bar2.setVisibility(View.VISIBLE);
            txtdsimplemento.setVisibility(View.VISIBLE);
            tDsImplemento.setVisibility(View.VISIBLE);


        }
        else {
            txtImplemento.setHint("Implemento");
            txtImplemento.setVisibility(View.VISIBLE);
            txtdsimplemento.setVisibility(View.VISIBLE);
            bar2.setVisibility(View.VISIBLE);
            txtdsimplemento.setVisibility(View.VISIBLE);
            tDsImplemento.setVisibility(View.VISIBLE);
            if(tipo.equals("5")){
                txtImplemento.setVisibility(View.GONE);
                txtdsimplemento.setVisibility(View.GONE);
                bar2.setVisibility(View.GONE);
                txtdsimplemento.setVisibility(View.GONE);
                tDsImplemento.setVisibility(View.GONE);
            }
            tOrigen.setVisibility(View.GONE);
            tdestino.setVisibility(View.GONE);
            truta.setVisibility(View.GONE);
            teq1.setVisibility(View.GONE);
            teq2.setVisibility(View.GONE);
            teq3.setVisibility(View.GONE);
            teq4.setVisibility(View.GONE);
            btnEq1.setVisibility(View.GONE);
            btnEq2.setVisibility(View.GONE);
            btnEq3.setVisibility(View.GONE);
            btnEq4.setVisibility(View.GONE);

            txtOrigen.setVisibility(View.GONE);
            txtDestino.setVisibility(View.GONE);
            txtRuta.setVisibility(View.GONE);
            txtEq1.setVisibility(View.GONE);
            txtEq2.setVisibility(View.GONE);
            txtEq3.setVisibility(View.GONE);
            txtEq4.setVisibility(View.GONE);
            tdseq1.setVisibility(View.GONE);
            tdseq2.setVisibility(View.GONE);
            tdseq3.setVisibility(View.GONE);
            tdseq4.setVisibility(View.GONE);

            tdseq1.setVisibility(View.GONE);
            tdseq2.setVisibility(View.GONE);
            tdseq3.setVisibility(View.GONE);
            tdseq4.setVisibility(View.GONE);
        }
    }

    private void checkGPS(){
        // check if GPS enabled

        if (gpsTracker.getIsGPSTrackingEnabled())
        {
            Latitud = Float.parseFloat(String.valueOf(gpsTracker.getLatitude()));
            Longitud = Float.parseFloat(String.valueOf(gpsTracker.getLongitude()));

        }
        else
        {
            showSettingsAlert();

        }
    }
    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        //Setting Dialog Title
        alertDialog.setTitle("Ubicación desactivada");

        //Setting Dialog Message
        alertDialog.setMessage("Active la ubicación GPS para poder seguir apuntando");

        //On Pressing Setting button
        alertDialog.setPositiveButton("Ajustes", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });

        //On pressing cancel button
        alertDialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.cancel();
                finish();
            }
        });

        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    private boolean ValidaApuntamiento(){
        if (recurso.getIdRecurso()==0)
        {
            Toast.makeText(this, "Seleccione un recurso", Toast.LENGTH_SHORT).show();
            return false;
        }

        if(txtEquipo.getText().toString().equals("") || txtEquipo.getText().toString().equals("0") || txtEquipo.getText().toString().trim().equals("") || txtEquipo.getText().toString().toLowerCase().trim().equals("null")){
            txtEquipo.setError("El campo es obligatorio");
            Toast.makeText(this, "El campo equipo es obligatorio", Toast.LENGTH_LONG).show();
            return false;
        }/*
        if (txtTrabajador.getText().toString().equals("")){
            txtTrabajador.setError("El campo es obligatorio");
            return false;
        }*/
        if(txtCantidad.getText().toString().equals("")){
            txtCantidad.setError("El campo es obligatorio");
            return false;
        }
        if(txtEquipo.getText().toString().equals("")){
            txtEquipo.setError("El campo es obligatorio");
            return false;
        }
        if(txthini.getText().toString().equals("") && !chkhfalla.isChecked() && tipo.equals("3")){
            txthini.setError("El campo es obligatorio si no se ha marcado como dañado");
            return false;
        }
        if(txthfin.getText().toString().equals("") && !chkhfalla.isChecked() && tipo.equals("3")){
            txthfin.setError("El campo es obligatorio si no se ha marcado como dañado");
            return false;
        }

        if (!chkhfalla.isChecked() && tipo.equals("3")){
            if(Float.parseFloat(txthfin.getText().toString().equals("")?"0":txthfin.getText().toString()) <= Float.parseFloat(txthini.getText().toString().equals("")?"0":txthini.getText().toString())){
                Toast.makeText(this, "Horómetro final no puede ser mayor al inicial", Toast.LENGTH_LONG).show();
                return false;
            }
        }

        if(txtoini.getText().toString().equals("") && !chkofalla.isChecked() && tipo.equals("5")){
            txtoini.setError("El campo es obligatorio si no se ha marcado como dañado");
            return false;
        }
        if(txtofin.getText().toString().equals("") && !chkofalla.isChecked() && tipo.equals("5")){
            txtofin.setError("El campo es obligatorio si no se ha marcado como dañado");
            return false;
        }

        if (!chkofalla.isChecked() && tipo.equals("5")){
            if(Float.parseFloat(txtofin.getText().toString().equals("")? "0": txtofin.getText().toString()) <= Float.parseFloat(txtoini.getText().toString().equals("")? "0" : txtoini.getText().toString())){
                Toast.makeText(this, "Odómetro final no puede ser mayor al inicial", Toast.LENGTH_LONG).show();
                return false;
            }
        }

        /*
        if(txtmini.getText().toString().equals("") && !chkmfalla.isChecked() && !txtImplemento.getText().toString().equals("") && tipo.equals("3")){
            txtmini.setError("El campo es obligatorio si no se ha marcado como dañado");
            return false;
        }
        if(txtmfin.getText().toString().equals("") && !chkmfalla.isChecked() && !txtImplemento.getText().toString().equals("") && tipo.equals("3")){
            txtmfin.setError("El campo es obligatorio si no se ha marcado como dañado");
            return false;
        }*/

        if (!chkmfalla.isChecked() && tipo.equals("3") && !txtImplemento.getText().toString().equals("")){
            if(Float.parseFloat((txtofin.getText().toString().equals("")?"0":txtofin.getText().toString())) <= Float.parseFloat(txtoini.getText().toString().equals("")?"0":txtoini.getText().toString())){
                Toast.makeText(this, "Medidor final no puede ser mayor al inicial", Toast.LENGTH_LONG).show();
                return false;
            }
        }

        if(txtCantidad.getText().toString().equals("") ){
            txtCantidad.setError("El campo es obligatorio");
            Toast.makeText(this,"Digite cantidad",Toast.LENGTH_SHORT).show();
            return false;
        }

        float pendiente= 0;
        pendiente = recurso.getCantidadPend()-recurso.getCantidadApunt();

        String sctd = txtCantidad.getText().toString().equals("")?"0":txtCantidad.getText().toString();

        Cantidad = Float.parseFloat(sctd);
        if (pendiente<=0){
            Toast.makeText(this, "Ya ha utilizado la totalidad del recurso", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(pendiente - Cantidad < 0){
            Toast.makeText(this, "Solo puede apuntar "+pendiente+" "+recurso.getUM(), Toast.LENGTH_SHORT).show();
            return false;
        }

        if (Cantidad <= 0){
            Toast.makeText(this,"Debe apuntar una cantidad mayor a cero",Toast.LENGTH_LONG).show();
            return false;
        }

        if (( Eq1 == Eq2 || Eq1 == Eq3 || Eq1 == Eq4) && Eq1 >0 ){
            Toast.makeText(this,"Equipo 1 Repetido",Toast.LENGTH_LONG).show();
            return false;
        }

        if (( Eq1 == Eq2 || Eq2 == Eq3 || Eq2 == Eq4) &&  Eq2 > 0){
            Toast.makeText(this,"Equipo 2 Repetido",Toast.LENGTH_LONG).show();
            return false;
        }

        if (( Eq3 == Eq2 || Eq1 == Eq3 || Eq3 == Eq4) && Eq3 > 0){
            Toast.makeText(this,"Equipo 3 Repetido",Toast.LENGTH_LONG).show();
            return false;
        }

        if (( Eq1 == Eq4 || Eq2 == Eq4 || Eq3 == Eq4) && Eq4 > 0) {
            Toast.makeText(this,"Equipo 4 Repetido",Toast.LENGTH_LONG).show();
            return false;
        }

        if ( IdTipDocInterno > 0 && txtNumDocumento.getText().toString().equals("")){
            Toast.makeText(this,"Digite el número de documento",Toast.LENGTH_LONG).show();
            txtNumDocumento.setError("Digite el número de documento");
            return false;
        }

        if ( !(IdTipDocInterno > 0) && !txtNumDocumento.getText().toString().equals("")){
            Toast.makeText(this,"Seleccione Tipo de Documento",Toast.LENGTH_LONG).show();

            return false;
        }

        if ( IdTipDocInterno > 0 && !txtNumDocumento.getText().toString().equals("") ){
            if(!mydb.DocumentoValido(IdApunte,IdTipDocInterno,txtNumDocumento.getText().toString())) {
                Toast.makeText(this, "El documento está repetido", Toast.LENGTH_LONG).show();
                return false;
            }
        }

        if (!(IdTrabajador > 0) && mydb.PropietarioIngenio(txtEquipo.getText().toString()) ){
            Toast.makeText(this, "Seleccione Trabajador", Toast.LENGTH_LONG).show();
            txtTrabajador.setError("El campo es obligatorio");
            return false;
        }

        OSRecursos rec = mydb.getAllRecursosByOSId(IdOS, IdRecurso);
        if ( mydb.PropietarioCASSA(txtEquipo.getText().toString()) && rec.getRequiereImplemento() && txtImplemento.getText().toString().isEmpty() ){
            Toast.makeText(this, "Seleccione Implemento", Toast.LENGTH_LONG).show();
            txtImplemento.setError("El campo es obligatorio");
            return false;
        }

        Origen = txtOrigen.getText().toString();
        Destino = txtDestino.getText().toString();
        Ruta = txtRuta.getText().toString();
        NumDocInterno = txtNumDocumento.getText().toString();

        return true;
    }

    @Override
    public void onClick(View v) {

        if (v == btnGuardar && ValidaApuntamiento()){

            if (Guardar()){
                Toast.makeText(this, "Apuntamiento guardado correctamente", Toast.LENGTH_LONG).show();
                Reset();
                //recreate();
            }
            else{
                Toast.makeText(this, "Ocurrió un problema al guardar apuntamiento", Toast.LENGTH_LONG).show();
            }
        }
        else if (v == bar1 ){
            Intent is = new Intent(ApuntesMaquinariaActivity.this,ScanActivity.class);
            int requestCode = 1; // Or some number you choose

            is.putExtra("tipo","equipo");
            startActivityForResult(is, requestCode);
        }
        else if (v == bar2 ){
            Intent is = new Intent(ApuntesMaquinariaActivity.this,ScanActivity.class);
            is.putExtra("tipo","implemento");
            int requestCode = 1; // Or some number you choose
            startActivityForResult(is, requestCode);
        }
        else if (v == btnAyud ){
            Intent is = new Intent(ApuntesMaquinariaActivity.this,ScanActivity.class);
            is.putExtra("tipo","ayudante");
            int requestCode = 1; // Or some number you choose
            startActivityForResult(is, requestCode);
        }
        else if (v == btnTrab ){
            Intent is = new Intent(ApuntesMaquinariaActivity.this,ScanActivity.class);
            is.putExtra("tipo","trabajador");
            int requestCode = 1; // Or some number you choose
            startActivityForResult(is, requestCode);
        }
        else if (v == btnEq1 ){
            Intent is = new Intent(ApuntesMaquinariaActivity.this,ScanActivity.class);
            is.putExtra("tipo","eq1");
            int requestCode = 1; // Or some number you choose
            startActivityForResult(is, requestCode);
        }
        else if (v == btnEq2 ){
            Intent is = new Intent(ApuntesMaquinariaActivity.this,ScanActivity.class);
            is.putExtra("tipo","eq2");
            int requestCode = 1; // Or some number you choose
            startActivityForResult(is, requestCode);
        }
        else if (v == btnEq3 ){
            Intent is = new Intent(ApuntesMaquinariaActivity.this,ScanActivity.class);
            is.putExtra("tipo","eq3");
            int requestCode = 1; // Or some number you choose
            startActivityForResult(is, requestCode);
        }
        else if (v == btnEq4 ){
            Intent is = new Intent(ApuntesMaquinariaActivity.this,ScanActivity.class);
            is.putExtra("tipo","eq4");
            int requestCode = 1; // Or some number you choose
            startActivityForResult(is, requestCode);
        }
        else if (v == fabInfo){
            DialogFragment newFragment = MyDialogFragment.newInstance(IdOS,tipo);
            newFragment.show(getSupportFragmentManager(), "Recursos");
        }
    }
    private boolean ValidarEquipos(){
        if (( Eq1 == Eq2 || Eq1 == Eq3 || Eq1 == Eq4) && Eq1 >0 ){
            Toast.makeText(this,"Equipo 1 Repetido",Toast.LENGTH_LONG).show();
            return false;
        }

        if (( Eq1 == Eq2 || Eq2 == Eq3 || Eq2 == Eq4) &&  Eq2 > 0){
            Toast.makeText(this,"Equipo 2 Repetido",Toast.LENGTH_LONG).show();
            return false;
        }

        if (( Eq3 == Eq2 || Eq1 == Eq3 || Eq3 == Eq4) && Eq3 > 0){
            Toast.makeText(this,"Equipo 3 Repetido",Toast.LENGTH_LONG).show();
            return false;
        }

        if (( Eq1 == Eq4 || Eq2 == Eq4 || Eq3 == Eq4) && Eq4 > 0) {
            Toast.makeText(this,"Equipo 4 Repetido",Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
    protected void onActivityResult (int requestCode, int resultCode, Intent data) {
        // Collect data from the intent and use it
        if(data!=null) {
            if (data.getExtras() != null) {
                String tipocod = data.getExtras().getString("tipo");
                String value = data.getExtras().getString("codigo");
                //String ds = data.getExtras().getString("descripcion");
                //int id = data.getExtras().getInt("id",0);
                Equipos e = new Equipos();
                Trabajadores t = new Trabajadores();
                switch (tipocod) {
                    case "equipo":
                        e = mydb.getSingleEquipoByCod(value);
                        if (e != null){
                            txtEquipo.setText(e.getCodEquipo().trim());
                            txtdsequipo.setText(e.getNomEquipo().trim());
                        }
                        else
                            txtEquipo.setText("DESCRIPCIÓN NO ENCONTRADA");
                        codEquipo = value;
                        break;
                    case "implemento":
                        e = mydb.getSingleEquipoByCod(value);
                        if (e != null){
                            txtImplemento.setText(e.getCodEquipo().trim());
                            txtdsimplemento.setText(e.getNomEquipo().trim());
                        }
                        else
                            txtImplemento.setText("DESCRIPCIÓN NO ENCONTRADA");
                        codEquipoImplemento = value;
                        break;
                    case "trabajador":
                        t = mydb.getSingleTrabajadorByCod(value);
                        if(t==null) t= new Trabajadores();
                        if (!t.getCodTrabajador().equals("")) {

                            txtTrabajador.setText(t.getCodTrabajador() + " - " + t.getNomTrabajador());
                            IdTrabajador = t.getIdTrabajador();
                        }
                        else
                            txtTrabajador.setText("NO ENCONTRADO");
                        break;
                    case "ayudante":
                        t = mydb.getSingleTrabajadorByCod(value);
                        if (t != null) {
                            txtAyudante.setText(t.getCodTrabajador() + " - " + t.getNomTrabajador());
                            IdAyudante = t.getIdTrabajador();
                        }
                        else
                            txtAyudante.setText("NO ENCONTRADO");
                        break;
                    case "eq1":
                        e = mydb.getSingleEquipoByCod(value);
                        if (e != null) {
                            txtEq1.setText(e.getCodEquipo().trim());
                            txtdseq1.setText(e.getNomEquipo().trim());
                            Eq1 = e.getIdEquipo();

                            if( !ValidarEquipos()){
                                txtEq1.setText("");
                                Eq1 = 0;
                                txtdseq1.setText("");
                            }
                        }
                        else
                            txtEq1.setText("NO ENCONTRADO");
                        break;
                    case "eq2":
                        e = mydb.getSingleEquipoByCod(value);
                        if (e != null ) {
                            txtEq2.setText(e.getCodEquipo().trim());
                            txtdseq2.setText(e.getNomEquipo().trim());
                            Eq2 = e.getIdEquipo();

                            if( !ValidarEquipos()){
                                txtEq2.setText("");
                                Eq2 = 0;
                                txtdseq2.setText("");
                            }
                        }
                        else
                            txtEq2.setText("NO ENCONTRADO");
                        break;
                    case "eq3":
                        e = mydb.getSingleEquipoByCod(value);
                        if (e != null) {
                            txtEq3.setText(e.getCodEquipo().trim());
                            txtdseq3.setText(e.getNomEquipo().trim());
                            Eq3 = e.getIdEquipo();

                            if( !ValidarEquipos()){
                                txtEq3.setText("");
                                Eq3 = 0;
                                txtdseq3.setText("");
                            }
                        }
                        else
                            txtEq3.setText("NO ENCONTRADO");
                        break;
                    case "eq4":
                        e = mydb.getSingleEquipoByCod(value);
                        if (e != null ) {
                            txtEq4.setText(e.getCodEquipo().trim());
                            txtdseq4.setText(e.getNomEquipo().trim());
                            Eq4 = e.getIdEquipo();

                            if( !ValidarEquipos()){
                                txtEq4.setText("");
                                Eq4 = 0;
                                txtdseq4.setText("");
                            }
                        }
                        else
                            txtEq4.setText("NO ENCONTRADO");
                        break;
                }
            }
        }
    }
    private void Reset(){
        scv.fullScroll(ScrollView.FOCUS_UP);
        txtTrabajador.setText("");
        txtAyudante.setText("");
        IdAyudante = 0 ;
        txtEquipo.setText("");
        txtdsequipo.setText("");
        txtCantidad.setText("");
        recurso = new OSRecursos();
        txthini.setText("");
        txthfin.setText("");
        txtoini.setText("");
        txtofin.setText("");
        txtmini.setText("");
        txtmfin.setText("");
        chkhfalla.setChecked(false);
        chkofalla.setChecked(false);
        chkmfalla.setChecked(false);
        txtImplemento.setText("");
        txtObserv.setText("");
        txtNumDocumento.setText("");
        txtNumDocumento.setError(null);

/*
        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);

        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        showDate(year, month+1, day);*/

        IdRecurso = 0;
        FechaIni = mydb.getCurrentTimeStamp();
        Cantidad = 0;

        txtAyudante.setVisibility(View.GONE);
        tayudante.setVisibility(View.GONE);
        btnAyud.setVisibility(View.GONE);
        tImplemento.setVisibility(View.GONE);
        tDsImplemento.setVisibility(View.GONE);
        spRecursos.setSelection(0,true);

        spDocumento.setSelection(0,true);
        // checkGPS();
        ShowHideLowBoy(false);

        if (tipo.equals("3")){

            txtmini.setVisibility(View.VISIBLE);
            txtmfin.setVisibility(View.VISIBLE);
            txtMTOTAL.setVisibility(View.VISIBLE);
            chkmfalla.setVisibility(View.VISIBLE);
            tImplemento.setVisibility(View.VISIBLE);
            tDsImplemento.setVisibility(View.VISIBLE);
            bar2.setVisibility(View.VISIBLE);
        }
        else{
            txtmini.setVisibility(View.GONE);
            txtmfin.setVisibility(View.GONE);
            txtMTOTAL.setVisibility(View.GONE);
            chkmfalla.setVisibility(View.GONE);
        }
    }
    private boolean Guardar(){
        //String IdEquipo = "", IdEquipoImplemento = "";
        int  HOR_FALLA = chkhfalla.isChecked()? 1:0,ODO_FALLA=chkofalla.isChecked()? 1:0,MED_FALLA= chkmfalla.isChecked()? 1:0;
        float HOR_INI=0,HOR_FIN=0,ODO_INI=0,ODO_FIN=0,MED_INI=0,MED_FIN=0;

        HOR_INI = txthini.getText().toString().equals("")?0:Float.parseFloat(txthini.getText().toString());
        HOR_FIN = txthfin.getText().toString().equals("")?0:Float.parseFloat(txthfin.getText().toString());
        ODO_INI = txtoini.getText().toString().equals("")?0:Float.parseFloat(txtoini.getText().toString());
        ODO_FIN = txtofin.getText().toString().equals("")?0:Float.parseFloat(txtofin.getText().toString());
        MED_INI = txtmini.getText().toString().equals("")?0:Float.parseFloat(txtmini.getText().toString());
        MED_FIN = txtmfin.getText().toString().equals("")?0:Float.parseFloat(txtmfin.getText().toString());
       /* IdEquipo = txtEquipo.getText().toString();
        IdEquipoImplemento = txtImplemento.getText().toString();*/
        if (txtum.getText().toString().toLowerCase().equals("hma") && Cantidad>24){
            Toast.makeText(this, "No puede apuntar más de 24 Horas máquina al día", Toast.LENGTH_LONG).show();
            return false;
        }

        recurso.setCantidadApunt(recurso.getCantidadApunt()+Cantidad);

        mydb.updateOSRecurso(recurso);
        if (Operacion.equals("a"))
        return  mydb.insertApuntesMaquinaria(IdOS,FechaApunte,IdRecurso,IdTrabajador,IdAyudante,codEquipo,codEquipoImplemento,0,HOR_INI,HOR_FIN, HOR_FALLA,ODO_INI,ODO_FIN,ODO_FALLA,MED_INI,MED_FIN,MED_FALLA,Cantidad
                ,Eq1, Eq2, Eq3, Eq4
                ,IdTipDocInterno, NumDocInterno
                , Origen, Destino, Ruta
                ,mydb.getUsr(),mydb.getCurrentTimeStamp(),txtObserv.getText().toString(),FechaIni,mydb.getCurrentTimeStamp(),Latitud,Longitud);
        else
            return  mydb.updateApunteMaq(IdApunte,FechaApunte,IdTrabajador,IdAyudante,codEquipo,codEquipoImplemento,0,HOR_INI,HOR_FIN,HOR_FALLA,ODO_INI,ODO_FIN,ODO_FALLA,MED_INI,MED_FIN,MED_FALLA,Cantidad
                    ,Eq1, Eq2, Eq3, Eq4
                    ,IdTipDocInterno, NumDocInterno
                    , Origen, Destino, Ruta
                    ,mydb.getUsr(),mydb.getCurrentTimeStamp(),txtObserv.getText().toString(),FechaIni,mydb.getCurrentTimeStamp(),Latitud,Longitud);
    }

    private class MySimpleArrayAdapter extends ArrayAdapter<OSRecursos> implements View.OnClickListener {
        private final Context context;
        private final List<OSRecursos> values;

        public MySimpleArrayAdapter(Context context, List<OSRecursos> values) {
            super(context,R.layout.itemsimple,R.id.txtItem, values);
            this.context = context;
            this.values = values;
        }

        @Override
        public OSRecursos getItem(int position) {
            return values.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.itemsimple, parent, false);
            TextView insumo = (TextView) rowView.findViewById(R.id.txtItem);

            insumo.setText(values.get(position).getDsRecurso());;
            return rowView;
        }
        @Override
        public void onClick(View v) {


        }
    }

    private class DocumentosArrayAdapter extends ArrayAdapter<TiposDocumento> implements View.OnClickListener {
        private final Context context;
        private final List<TiposDocumento> values;

        public DocumentosArrayAdapter(Context context, List<TiposDocumento> values) {
            super(context,R.layout.itemsimple,R.id.txtItem, values);
            this.context = context;
            this.values = values;
        }

        @Override
        public TiposDocumento getItem(int position) {
            return values.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.itemsimple, parent, false);
            TextView insumo = (TextView) rowView.findViewById(R.id.txtItem);

            insumo.setText(values.get(position).getDsTipDocInteno());;
            return rowView;
        }
        @Override
        public void onClick(View v) {
        }
    }

    private void OcultarHorometros(){
        txtHTOTAL.setVisibility(View.GONE);
        txthfin.setVisibility(View.GONE);
        txthini.setVisibility(View.GONE);
        chkhfalla.setVisibility(View.GONE);
        chkhfalla.setChecked(true);
        thini.setVisibility(View.GONE);
        thfin.setVisibility(View.GONE);


    }


    private void MostrarHorometros(){
        txtHTOTAL.setVisibility(View.VISIBLE);
        txthfin.setVisibility(View.VISIBLE);
        txthini.setVisibility(View.VISIBLE);
        chkhfalla.setVisibility(View.VISIBLE);
    }


    private void OcultarOdometros(){

        chkofalla.setChecked(true);
        chkofalla.setVisibility(View.GONE);
        txtoini.setText("0");
        txtoini.setEnabled(false);

    }


    private void MostrarOdometros(){
        txtOTOTAL.setVisibility(View.VISIBLE);
        txtofin.setVisibility(View.VISIBLE);
        txtoini.setVisibility(View.VISIBLE);
        chkofalla.setVisibility(View.VISIBLE);
    }


    private void OcultarMedidores(){

        txtmini.setVisibility(View.GONE);
        txtmfin.setVisibility(View.GONE);
        txtMTOTAL.setVisibility(View.GONE);
        chkmfalla.setVisibility(View.GONE);
    }


    private void MostrarMedidores(){
        txtMTOTAL.setVisibility(View.VISIBLE);
        txtmfin.setVisibility(View.VISIBLE);
        txtmini.setVisibility(View.VISIBLE);
        chkmfalla.setVisibility(View.VISIBLE);
    }

    private void OcultarDocInterno(){
        spDocumento.setVisibility(View.GONE);
        txtNumDocumento.setVisibility(View.GONE);
    }

    private void MostrarDocInterno(){
        spDocumento.setVisibility(View.VISIBLE);
        txtNumDocumento.setVisibility(View.VISIBLE);
    }

    private void OcultarImplementoLowboy(){
        btnAyud.setVisibility(View.GONE);
        txtImplemento.setHint("LowBoy Utilizado");
        txtImplemento.setVisibility(View.GONE);
        txtdsimplemento.setVisibility(View.GONE);
        tImplemento.setVisibility(View.GONE);
        tOrigen.setVisibility(View.GONE);
        tdestino.setVisibility(View.GONE);
        truta.setVisibility(View.GONE);
        teq1.setVisibility(View.GONE);
        teq2.setVisibility(View.GONE);
        teq3.setVisibility(View.GONE);
        teq4.setVisibility(View.GONE);
        btnEq1.setVisibility(View.GONE);
        btnEq2.setVisibility(View.GONE);
        btnEq3.setVisibility(View.GONE);
        btnEq4.setVisibility(View.GONE);

        txtOrigen.setVisibility(View.GONE);
        txtDestino.setVisibility(View.GONE);
        txtRuta.setVisibility(View .GONE);
        txtEq1.setVisibility(View.GONE);
        txtEq2.setVisibility(View.GONE);
        txtEq3.setVisibility(View.GONE);
        txtEq4.setVisibility(View.GONE);

        tdseq1.setVisibility(View.GONE);
        tdseq2.setVisibility(View.GONE);
        tdseq3.setVisibility(View.GONE);
        tdseq4.setVisibility(View.GONE);


        txtImplemento.setVisibility(View.GONE);
        txtdsimplemento.setVisibility(View.GONE);
        bar2.setVisibility(View.GONE);
        txtdsimplemento.setVisibility(View.GONE);
        tDsImplemento.setVisibility(View.GONE);

    }

    private void OcultarMotoristaOperador(){
        txtTrabajador.setVisibility(View.GONE);
        btnTrab.setVisibility(View.GONE);
    }

    private void ConfiguracionSociedad(){
        String sociedad = mydb.getSociedad();

        if (tipo.equals("3")){
            OcultarDocInterno();
        }

        switch (sociedad){
            case "1000":
                MostrarHorometros();
                if(tipo.equals("5"))
                OcultarHorometros();
                break;
            case "2000":
                MostrarHorometros();
                if(tipo.equals("5"))
                OcultarHorometros();
                break;
            case "3000":
               // if(tipo.equals("5")) {
                    OcultarHorometros();
                //}
                if (tipo.equals("3")){
                    OcultarDocInterno();/*
                    txthini.setText("0");
                    txthini.setVisibility(View.GONE);
                    chkhfalla.setChecked(true);
                    chkhfalla.setVisibility(View.GONE);*/
                }
                OcultarMedidores();
                OcultarOdometros();
                OcultarImplementoLowboy();
                OcultarMotoristaOperador();
                break;
            default:
                break;
        }

    }

}
