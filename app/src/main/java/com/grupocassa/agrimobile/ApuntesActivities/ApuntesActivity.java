package com.grupocassa.agrimobile.ApuntesActivities;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.grupocassa.agrimobile.CASSAUTILS.DBHelper;
import com.grupocassa.agrimobile.CLS.ApuntesInsumos;
import com.grupocassa.agrimobile.CLS.ApuntesMO;
import com.grupocassa.agrimobile.CLS.ApuntesMaquinaria;
import com.grupocassa.agrimobile.CLS.OS;
import com.grupocassa.agrimobile.CLS.OSRecursos;
import com.grupocassa.agrimobile.R;

import java.util.ArrayList;
import java.util.List;

public class ApuntesActivity extends AppCompatActivity {

    ListView lv;
    ApinsumosAdapter adapinsumos;
    ApmanoobraAdapter adapmanoobra;
    ApmaqAdapter adapMq;
    String tipo, Titulo;
    int IdOS;
    ArrayList<ApuntesInsumos> apins;
    ArrayList<ApuntesMO> apmo;
    ArrayList<ApuntesMaquinaria> apmq;
    private DBHelper mydb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apuntes);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        lv = (ListView) findViewById(R.id.lvApuntes);

        mydb = new DBHelper(this);

/*
        lv.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                Intent intent = new Intent(MainActivity.this, SendMessage.class);
                String message = "abc";
                intent.putExtra(EXTRA_MESSAGE, message);
                startActivity(intent);
            }
        });*/
        Bundle extras = getIntent().getExtras();
        tipo = extras.getString("tipo");
        IdOS = extras.getInt("IdOS",0);
        IdOS = extras.getInt("IdOS",0);
        LayoutInflater inflater = getLayoutInflater();
        ViewGroup header;




        OS theOS = mydb.getSingleOS(IdOS);
        switch (tipo)
        {
            case "1":
                setTitle("Mano de Obra - OS:  "+theOS.getIdOS()+". "+theOS.getDsActividad());
                header  = (ViewGroup) inflater.inflate(R.layout.rowmo, lv,false);
                lv.addHeaderView(header);

                apmo = mydb.getApuntesMOByOS(IdOS);
                if (apmo == null){
                    apmo = new ArrayList<ApuntesMO>();
                }

                adapmanoobra = new ApmanoobraAdapter(this,apmo);

                lv.setAdapter(adapmanoobra);
                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        ApuntesMO apmobra = (ApuntesMO) parent.getItemAtPosition(position);

                        Toast.makeText(ApuntesActivity.this, "Prueba", Toast.LENGTH_SHORT).show();
                        Intent in = new Intent(ApuntesActivity.this, EditApuntesMO.class);
                        in.putExtra("IdOS",IdOS);
                        in.putExtra("Operacion","m");
                        in.putExtra("IdApunte",apmobra.getIdApMO());
                        startActivity(in);

                    }
                });
                break;
            case "2":
                setTitle("Insumos - "+theOS.getDsActividad());
                header  = (ViewGroup) inflater.inflate(R.layout.rowinsumos, lv,false);
                lv.addHeaderView(header);

                apins = mydb.getApuntesInsumosByOS(IdOS);
                if (apins == null){
                    apins = new ArrayList<ApuntesInsumos>();
                }

                adapinsumos = new ApinsumosAdapter(this,apins);

                lv.setAdapter(adapinsumos);
                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        ApuntesInsumos apunteIns = (ApuntesInsumos) parent.getItemAtPosition(position);

                        Intent in = new Intent(ApuntesActivity.this, ApnutesInsumosActivity.class);
                        in.putExtra("IdOS",IdOS);
                        in.putExtra("Operacion","m");
                        in.putExtra("IdApunte",apunteIns.getIdApInsumos());
                        startActivity(in);

                    }
                });

                break;
            case "3":
                setTitle("Maquinaria - "+theOS.getDsActividad());
                header  = (ViewGroup) inflater.inflate(R.layout.rowmq, lv,false);
                lv.addHeaderView(header);

                apmq = mydb.getApuntesMaquinariaByOS(IdOS,tipo);
                if (apmq == null){
                    apmq = new ArrayList<ApuntesMaquinaria>();
                }

                adapMq = new ApmaqAdapter(this,apmq);

                lv.setAdapter(adapMq);
                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        ApuntesMaquinaria apunteMaquinaria = (ApuntesMaquinaria) parent.getItemAtPosition(position);

                        Intent in = new Intent(ApuntesActivity.this, ApuntesMaquinariaActivity.class);
                        in.putExtra("IdOS",IdOS);
                        in.putExtra("Operacion","m");
                        in.putExtra("IdApunte",apunteMaquinaria.getIdApMaquinaria());
                        in.putExtra("tipo",tipo);
                        startActivity(in);

                    }
                });

                break;
            case "5":
                setTitle("Servicios Agrícolas - "+theOS.getDsActividad());
                header  = (ViewGroup) inflater.inflate(R.layout.rowmq, lv,false);
                lv.addHeaderView(header);

                apmq = mydb.getApuntesMaquinariaByOS(IdOS,tipo);
                if (apmq == null){
                    apmq = new ArrayList<ApuntesMaquinaria>();
                }

                adapMq = new ApmaqAdapter(this,apmq);

                lv.setAdapter(adapMq);
                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        ApuntesMaquinaria apunteMaquinaria = (ApuntesMaquinaria) parent.getItemAtPosition(position);

                        Intent in = new Intent(ApuntesActivity.this, ApuntesMaquinariaActivity.class);
                        in.putExtra("IdOS",IdOS);
                        in.putExtra("Operacion","m");
                        in.putExtra("IdApunte",apunteMaquinaria.getIdApMaquinaria());
                        in.putExtra("tipo",tipo);
                        startActivity(in);

                    }
                });

                break;
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.add);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int permissionCheck = ContextCompat.checkSelfPermission(ApuntesActivity.this,
                        Manifest.permission.ACCESS_COARSE_LOCATION);
                if (!(permissionCheck == PackageManager.PERMISSION_GRANTED)) {

                    // Should we show an explanation?
                    if (ActivityCompat.shouldShowRequestPermissionRationale(ApuntesActivity.this,
                            Manifest.permission.ACCESS_COARSE_LOCATION)) {
                        Toast.makeText(ApuntesActivity.this, "Debe conceder permisos para poder apuntar", Toast.LENGTH_LONG).show();
                    } else {
                        // do request the permission
                        ActivityCompat.requestPermissions(ApuntesActivity.this,
                                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                                8);
                    }
                }
                else{
                    OpenNuevoApunte();
                }



            }
        });

        registerForContextMenu(lv);
    }
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (v == lv) {
            ListView lv = (ListView) v;
            AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) menuInfo;


            menu.add(Menu.NONE, 1, Menu.NONE, "Eliminar Apunte");
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:

                AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
                if (info.position > 0) {
                    if (tipo.equals("1")){
                        ApuntesMO capmo = adapmanoobra.getItem(info.position - 1);
                        int idrec = capmo.getIdRecurso();
                        int idos = capmo.getIdOS();
                        ArrayList<OSRecursos> or = mydb.getAllRecursosByOS(idos);
                        OSRecursos recur = new OSRecursos();
                        for (OSRecursos o : or
                                ) {
                            if (idrec == o.getIdRecurso()) {
                                recur = o;
                            }
                        }

                        float areap = mydb.getAreaApuntadaMO(idos,idrec);
                        recur.setAreaApunt(recur.getAreaApunt() - areap);

                        ShowConfirmDelete(info.position);

                        recur.setAreaApunt(recur.getAreaApunt() + mydb.getAreaApuntadaMO(idos, idrec));

                        mydb.updateOSRecurso(recur);
                }
                else{
                        ShowConfirmDelete(info.position);
                    }

                }
                else
                    Toast.makeText(this, "No seleccionó nada", Toast.LENGTH_LONG).show();

                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    public void DeleteItem(int position){
        boolean exito = false;

        ArrayList<OSRecursos> recursoses = mydb.getAllRecursosByOS(IdOS);
        switch (tipo)
        {
            case "1":
                ApuntesMO capmo = adapmanoobra.getItem(position-1);
                if (capmo != null) {
                    float areaapunt = mydb.getAreaApuntadaMO(IdOS,capmo.getIdRecurso());
                    OSRecursos osrec = new OSRecursos();
                    for (OSRecursos osr:
                            recursoses) {
                        if (osr.getBkTipRecurso().equals(tipo) && osr.getIdRecurso() == capmo.getIdRecurso()){
                            osr.setAreaApunt(osr.getAreaApunt()-areaapunt);
                            //osr.setAreaApunt(osr.getAreaApunt()-capmo.getArea());
                            osrec = osr;
                        }
                    }

                    exito = mydb.deleteApunteMO(capmo.getIdApMO()) > 0;
                    if (exito){

                        areaapunt = mydb.getAreaApuntadaMO(IdOS,capmo.getIdRecurso());

                        osrec.setCantidadApunt(osrec.getCantidadApunt()-capmo.getQTD_PAGO());
                        osrec.setAreaApunt(osrec.getAreaApunt() + areaapunt);

                        mydb.updateOSRecurso(osrec);
                    }
                    cargarLista();
                }

                break;
            case "2":
                ApuntesInsumos ap = adapinsumos.getItem(position-1);
                if (ap != null) {
                    exito = mydb.deleteApunteInsumos(ap.getIdApInsumos()) > 0;
                    if (exito){
                        for (OSRecursos osr:
                             recursoses) {
                            if (osr.getBkTipRecurso().equals(tipo) && osr.getIdRecurso() == ap.getIdRecurso()){
                                osr.setCantidadApunt(osr.getCantidadApunt()-ap.getCantidad());
                                osr.setAreaApunt(osr.getAreaApunt()-ap.getArea());
                                mydb.updateOSRecurso(osr);
                            }
                        }

                    }
                    cargarLista();
                }

                break;
            default:
                ApuntesMaquinaria apmq = adapMq.getItem(position-1);
                if (apmq != null) {
                    exito = mydb.deleteApunteMaquinaria(apmq.getIdApMaquinaria()) > 0;
                    if (exito){
                        for (OSRecursos osr:
                                recursoses) {
                            if (osr.getBkTipRecurso().equals(tipo) && osr.getIdRecurso() == apmq.getIdRecurso()){
                                osr.setCantidadApunt(osr.getCantidadApunt()-apmq.getQTD_PAGO());
                                mydb.updateOSRecurso(osr);
                            }
                        }

                    }
                    cargarLista();
                }
                break;
        }

        if (exito == true){
            Toast.makeText(this, "Apuntamiento eliminado", Toast.LENGTH_SHORT).show();
        }
        else {
            Toast.makeText(this, "Error al eliminar apuntamiento", Toast.LENGTH_SHORT).show();
        }
    }

    public void ShowConfirmDelete(final int position) {



        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        //Setting Dialog Title
        alertDialog.setTitle("Eliminar Apuntamiento");

        //Setting Dialog Message
        alertDialog.setMessage("¿Está seguro de eliminar el apuntamiento seleccionado?");

        //On Pressing Setting button
        alertDialog.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which)
            {

                DeleteItem(position);
            }
        });

        //On pressing cancel button
        alertDialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.cancel();

            }
        });

        alertDialog.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 8: {
                // grantResults[0] = -1
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    OpenNuevoApunte();

                } else {
                    Toast.makeText(this, "Permiso requerido para apuntar", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Toast.makeText(this, "Prueba", Toast.LENGTH_SHORT).show();
        cargarLista();
    }

    private void cargarLista(){
        switch (tipo)
        {
            case "1":

                apmo = mydb.getApuntesMOByOS(IdOS);
                if (apmo == null){
                    apmo = new ArrayList<ApuntesMO>();
                }

                adapmanoobra = new ApmanoobraAdapter(this,apmo);

                lv.setAdapter(adapmanoobra);
                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        ApuntesMO apmobra = (ApuntesMO) parent.getItemAtPosition(position);

                        if (apmobra != null) {
                            Intent in = new Intent(ApuntesActivity.this, EditApuntesMO.class);
                            in.putExtra("IdOS", IdOS);
                            in.putExtra("Operacion", "m");
                            in.putExtra("IdApunte", apmobra.getIdApMO());
                            startActivity(in);
                        }


                    }
                });

                break;
            case "2":

                apins = mydb.getApuntesInsumosByOS(IdOS);
                if (apins == null){
                    apins = new ArrayList<ApuntesInsumos>();
                }

                adapinsumos = new ApinsumosAdapter(this,apins);

                lv.setAdapter(adapinsumos);
                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        ApuntesInsumos apunteIns = (ApuntesInsumos) parent.getItemAtPosition(position);

                        if (apunteIns != null) {
                            Intent in = new Intent(ApuntesActivity.this, ApnutesInsumosActivity.class);
                            in.putExtra("IdOS", IdOS);
                            in.putExtra("Operacion", "m");
                            in.putExtra("IdApunte", apunteIns.getIdApInsumos());
                            startActivity(in);
                        }

                    }
                });

                break;

            case "3":

                apmq = mydb.getApuntesMaquinariaByOS(IdOS,tipo);
                if (apmq == null){
                    apmq = new ArrayList<ApuntesMaquinaria>();
                }

                adapMq = new ApmaqAdapter(this,apmq);

                lv.setAdapter(adapMq);
                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        ApuntesMaquinaria apunteMaquinaria = (ApuntesMaquinaria) parent.getItemAtPosition(position);

                        if (apunteMaquinaria!=null) {
                            Intent in = new Intent(ApuntesActivity.this, ApuntesMaquinariaActivity.class);
                            in.putExtra("IdOS", IdOS);
                            in.putExtra("Operacion", "m");
                            in.putExtra("tipo", tipo);
                            in.putExtra("IdApunte", apunteMaquinaria.getIdApMaquinaria());
                            startActivity(in);
                        }
                    }
                });

                break;

            case "5":

                apmq = mydb.getApuntesMaquinariaByOS(IdOS,tipo);
                if (apmq == null){
                    apmq = new ArrayList<ApuntesMaquinaria>();
                }

                adapMq = new ApmaqAdapter(this,apmq);

                lv.setAdapter(adapMq);
                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        ApuntesMaquinaria apunteMaquinaria = (ApuntesMaquinaria) parent.getItemAtPosition(position);
                        if (apunteMaquinaria != null) {
                            Intent in = new Intent(ApuntesActivity.this, ApuntesMaquinariaActivity.class);
                            in.putExtra("IdOS", IdOS);
                            in.putExtra("Operacion", "m");
                            in.putExtra("tipo", tipo);
                            in.putExtra("IdApunte", apunteMaquinaria.getIdApMaquinaria());
                            startActivity(in);
                        }

                    }
                });

                break;
        }
    }
    private void OpenNuevoApunte(){
        Intent in ;
        switch (tipo) {
            case "1":
                in = new Intent(ApuntesActivity.this, ApuntesMOActivity.class);
                in.putExtra("IdOS",IdOS);
                in.putExtra("Operacion","a");
                startActivity(in);

                break;
            case "2":
                in = new Intent(ApuntesActivity.this, ApnutesInsumosActivity.class);
                in.putExtra("IdOS",IdOS);
                in.putExtra("Operacion","a");
                startActivity(in);

                break;
            case "3":
                in = new Intent(ApuntesActivity.this, ApuntesMaquinariaActivity.class);
                in.putExtra("IdOS",IdOS);
                in.putExtra("Operacion","a");
                in.putExtra("tipo",tipo);

                startActivity(in);
                break;
            case "5":
                in = new Intent(ApuntesActivity.this, ApuntesMaquinariaActivity.class);
                in.putExtra("IdOS",IdOS);
                in.putExtra("Operacion","a");
                in.putExtra("tipo",tipo);

                startActivity(in);
                break;
        }
    }

    private class ApinsumosAdapter extends ArrayAdapter<ApuntesInsumos> implements View.OnClickListener {
        private final Context context;
        private final List<ApuntesInsumos> values;

        public ApinsumosAdapter(Context context, List<ApuntesInsumos> values) {
            super(context, -1, values);
            this.context = context;
            this.values = values;
        }

        @Override
        public ApuntesInsumos getItem(int position) {
            return values.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.itemapinsumos, parent, false);
            TextView fecha = (TextView) rowView.findViewById(R.id.fecha);
            TextView insumo = (TextView) rowView.findViewById(R.id.insumo);
            TextView area = (TextView) rowView.findViewById(R.id.Areansumo);
            TextView cantidad = (TextView) rowView.findViewById(R.id.cantidadinsumo);
            TextView um = (TextView) rowView.findViewById(R.id.uminsumo);
            TextView ob = (TextView) rowView.findViewById(R.id.obsinsumo);

            insumo.setText(values.get(position).getDsRecurso());
            area.setText(String.valueOf( values.get(position).getArea()));
            float ctd = values.get(position).getCantidad();
            cantidad.setText(String.valueOf( ctd));
            um.setText(values.get(position).getUM());
            ob.setText(values.get(position).getObservacion());
            String fec = values.get(position).getFechaApunte();
            String[] f = fec.split("-");
            fecha.setText(f[2]+"-"+f[1]+"-"+f[0]);
            return rowView;
        }
        @Override
        public void onClick(View v) {


        }
    }
    private class ApmanoobraAdapter extends ArrayAdapter<ApuntesMO> implements View.OnClickListener {
        private final Context context;
        private final List<ApuntesMO> values;

        public ApmanoobraAdapter(Context context, List<ApuntesMO> values) {
            super(context, -1, values);
            this.context = context;
            this.values = values;
        }

        @Override
        public ApuntesMO getItem(int position) {
            return values.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.rowmo, parent, false);
            TextView fecha = (TextView) rowView.findViewById(R.id.fecha);
            TextView recurso = (TextView) rowView.findViewById(R.id.recurso);
            TextView trab = (TextView) rowView.findViewById(R.id.Trabajador);
            TextView qtd = (TextView) rowView.findViewById(R.id.ctdPago);
            TextView um = (TextView) rowView.findViewById(R.id.um);
            TextView area = (TextView) rowView.findViewById(R.id.area);
            TextView cuad = (TextView) rowView.findViewById(R.id.cuadrilla);
            TextView obs = (TextView) rowView.findViewById(R.id.obs);

            recurso.setText(values.get(position).getDsRecurso());
            area.setText(String.valueOf( values.get(position).getArea()));
            trab.setText(values.get(position).getTrabajador());
            qtd.setText(String.valueOf(values.get(position).getQTD_PAGO()));
            um.setText(values.get(position).getUM());
            cuad.setText(values.get(position).getDsCuadrilla());
            obs.setText(values.get(position).getObservacion());
            String fec = values.get(position).getFechaApunte();
            String[] f = fec.split("-");
            fecha.setText(f[2]+"-"+f[1]+"-"+f[0]);

            return rowView;
        }
        @Override
        public void onClick(View v) {


        }
    }
    private class ApmaqAdapter extends ArrayAdapter<ApuntesMaquinaria> implements View.OnClickListener {
        private final Context context;
        private final List<ApuntesMaquinaria> values;

        public ApmaqAdapter(Context context, List<ApuntesMaquinaria> values) {
            super(context, -1, values);
            this.context = context;
            this.values = values;
        }

        @Override
        public ApuntesMaquinaria getItem(int position) {
            return values.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.rowmq, parent, false);
            TextView fecha = (TextView) rowView.findViewById(R.id.fecha);
            TextView recurso = (TextView) rowView.findViewById(R.id.recurso);
            TextView trab = (TextView) rowView.findViewById(R.id.codequipo);
            TextView qtd = (TextView) rowView.findViewById(R.id.ctdPago);
            TextView um = (TextView) rowView.findViewById(R.id.um);;
            TextView obs = (TextView) rowView.findViewById(R.id.obs);

            recurso.setText(values.get(position).getDsRecurso());
            trab.setText(String.valueOf( values.get(position).getCodEquipo()));
            qtd.setText(String.valueOf(values.get(position).getQTD_PAGO()));
            um.setText(values.get(position).getUM());
            obs.setText(values.get(position).getObservacion());
            String fec = values.get(position).getFechaApunte();
            String[] f = fec.split("-");
            fecha.setText(f[2]+"-"+f[1]+"-"+f[0]);

            return rowView;
        }
        @Override
        public void onClick(View v) {


        }
    }

}
