package com.grupocassa.agrimobile;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.grupocassa.agrimobile.CASSAUTILS.DBHelper;
import com.grupocassa.agrimobile.CLS.OSRecursos;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gerardo.valle on 18/8/2017.
 */

public class MyDialogFragment extends DialogFragment {

    private ListView lv;
    private String tipo;
    private ArrayList<OSRecursos> orec;
    private DBHelper mydb;


    public static MyDialogFragment newInstance(int IdOS, String tipo) {
        MyDialogFragment f = new MyDialogFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putInt("IdOS", IdOS);
        args.putString("tipo", tipo);
        f.setArguments(args);

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sample_dialog, container, false);
        getDialog().setTitle("Recursos");
        mydb = new DBHelper(getContext());


        Button dismiss = (Button) rootView.findViewById(R.id.dismiss);
        dismiss.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        dismiss.setVisibility(View.GONE);
        orec = new ArrayList<OSRecursos>();

        Bundle extras = getArguments();
        int IdOS = extras.getInt("IdOS");
        tipo = extras.getString("tipo");

        ArrayList<OSRecursos> recs = mydb.getAllRecursosByOS(IdOS);
        for (OSRecursos ore : recs
             ) {
            if (ore.getBkTipRecurso().equals(tipo)){
                orec.add(ore);
            }
        }

        lv = (ListView) rootView.findViewById(R.id.LVPlan);
        ViewGroup header;
        header  = (ViewGroup) inflater.inflate(R.layout.row_itemplan, lv,false);
        if (!tipo.equals("2") && !tipo.equals("1")) {
            header.removeViewInLayout(header.findViewById(R.id.areaplan));
            header.removeViewInLayout(header.findViewById(R.id.areareal));
        }
        lv.addHeaderView(header);

        OsRecursosAdapter adapter = new OsRecursosAdapter(getContext(),orec,tipo);

        lv.setAdapter(adapter);

        return rootView;
    }

    private class OsRecursosAdapter extends ArrayAdapter<OSRecursos> implements View.OnClickListener {
        private final Context context;
        private final List<OSRecursos> values;
        private final float AreaPlan;
        private final String tipo;
        private DBHelper mydb;

        public OsRecursosAdapter(Context context, List<OSRecursos> values, String tipo) {
            super(context, -1, values);
            this.context = context;
            this.values = values;
            this.tipo = tipo;
            this.mydb = new DBHelper(context);


            if (values.size()>0)
                AreaPlan = mydb.getSingleOS(values.get(0).getIdOS()).getCantidad();
            else
                AreaPlan = 0;

        }

        @Override
        public OSRecursos getItem(int position) {
            return values.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.row_itemplan, parent, false);
            TextView recurso = (TextView) rowView.findViewById(R.id.recurso);
            TextView um = (TextView) rowView.findViewById(R.id.um);
            TextView cantplan = (TextView) rowView.findViewById(R.id.cantplan);
            TextView canreal = (TextView) rowView.findViewById(R.id.cantreal);
            TextView areaplan = (TextView) rowView.findViewById(R.id.areaplan);
            TextView areareal = (TextView) rowView.findViewById(R.id.areareal);

            recurso.setText(values.get(position).getDsRecurso());
            cantplan.setText(String.valueOf(values.get(position).getCantidadPend()));
            canreal.setText(String.valueOf(values.get(position).getCantidadApunt()));
            um.setText(values.get(position).getUM());
            float areaApl = 0;
            OSRecursos orec = values.get(position);
            switch (tipo){

                case "2":


                    //areaApl = mydb.getAreaApuntadaIns(orec.getIdOS(),orec.getIdRecurso());
                    areaplan.setText(String.valueOf(AreaPlan));
                    areareal.setText(String.valueOf(orec.getAreaApunt()));

                    break;
                case "1":


                    //areaApl = mydb.getAreaApuntadaIns(orec.getIdOS(),orec.getIdRecurso());
                    areaplan.setText(String.valueOf(AreaPlan));
                    areareal.setText(String.valueOf(orec.getAreaApunt()));
                    areaplan.setVisibility(View.VISIBLE);
                    areareal.setVisibility(View.VISIBLE);

                    break;
                default:
                    areaplan.setVisibility(View.GONE);
                    areareal.setVisibility(View.GONE);
                    break;
            }

            return rowView;
        }

        @Override
        public void onClick(View v) {


        }
    }
    }

