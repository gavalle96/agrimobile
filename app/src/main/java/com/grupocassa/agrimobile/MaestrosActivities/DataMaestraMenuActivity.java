package com.grupocassa.agrimobile.MaestrosActivities;

import android.content.Context;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.grupocassa.agrimobile.R;

import java.util.ArrayList;
import java.util.List;

public class DataMaestraMenuActivity extends AppCompatActivity {


    ListView lv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_maestra_menu);

        setTitle("Datos Maestros y Envíos");
        List<OpcionesDatos> opcionesDatosList = new ArrayList<OpcionesDatos>();
        opcionesDatosList.add(new OpcionesDatos("maestros","Obtener Órdenes de Servicio"));
        opcionesDatosList.add(new OpcionesDatos("ossingle","Obtener Datos de una OS"));
        opcionesDatosList.add(new OpcionesDatos("cuadrillas","Obtener Cuadrillas de Trabajadores"));
        opcionesDatosList.add(new OpcionesDatos("os","Actualizar Recursos de Órdenes de Servicio"));
        opcionesDatosList.add(new OpcionesDatos("enviar","Enviar Apuntamientos al Sistema"));
        opcionesDatosList.add(new OpcionesDatos("equipos","Descargar Equipos"));


        lv = (ListView) findViewById(R.id.lvOpcDM);

        MySimpleArrayAdapter adapter = new MySimpleArrayAdapter(this,opcionesDatosList);

        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i;
                switch (position){
                    case 0:

                        //i= new Intent(DataMaestraMenuActivity.this,MaestrosActivity.class);
                        i= new Intent(DataMaestraMenuActivity.this, ObtenerMaestrosActivity.class);
                        i.putExtra("Titulo","Obtener Todas las Órdenes de Servicio");
                        i.putExtra("tipo","todo");
                        startActivity(i);




                        break;
                    case 1:
                        Intent intt= new Intent(DataMaestraMenuActivity.this, ObtenerMaestrosActivity.class);
                        intt.putExtra("Titulo","Obtener Datos de una Orden de Servicio");
                        intt.putExtra("tipo","os");
                        intt.putExtra("dm",true);
                        startActivity(intt);
                        break;
                    case 2:
                        Intent cu = new Intent(DataMaestraMenuActivity.this,MenuCuadrillasActivity.class);
                        startActivity(cu);
                        break;
                    case 3:
                        Intent in= new Intent(DataMaestraMenuActivity.this, ObtenerMaestrosActivity.class);
                        in.putExtra("Titulo","Recursos por Orden de Servicio");
                        in.putExtra("tipo","os");
                        startActivity(in);
                        break;
                    case 4:
                        Intent env = new Intent(DataMaestraMenuActivity.this,EnviarApuntesActivity.class);
                        startActivity(env);
                        break;
                    case 5:
                        Intent eq = new Intent(DataMaestraMenuActivity.this,ObtenerEquiposActivity.class);
                        startActivity(eq);
                        break;
                }
            }
        });


    }

    private class OpcionesDatos{
        private String Tipo;
        private String Descripcion;

        public OpcionesDatos(String tipo, String descripcion) {
            Tipo = tipo;
            Descripcion = descripcion;
        }

        public String getTipo() {
            return Tipo;
        }

        public void setTipo(String tipo) {
            Tipo = tipo;
        }

        public String getDescripcion() {
            return Descripcion;
        }

        public void setDescripcion(String descripcion) {
            Descripcion = descripcion;
        }
    }

    private class MySimpleArrayAdapter extends ArrayAdapter<OpcionesDatos> implements View.OnClickListener {
        private final Context context;
        private final List<OpcionesDatos> values;

        public MySimpleArrayAdapter(Context context, List<OpcionesDatos> values) {
            super(context, -1, values);
            this.context = context;
            this.values = values;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.itemdm, parent, false);
            TextView textView = (TextView) rowView.findViewById(R.id.firstLine);
            ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
            textView.setText(values.get(position).getDescripcion());
            // change the icon for Windows and iPhone
            String s = values.get(position).getTipo();


            switch (s) {
                case "enviar":
                imageView.setImageResource(R.drawable.enviaricon);
                    break;
                case "cuadrillas":
                    imageView.setImageResource(R.drawable.cuadrillasicon);
                    break;
                case "os":
                    imageView.setImageResource(R.drawable.osrecursosicon);
                    break;
                case "ossingle":
                    imageView.setImageResource(R.drawable.ic_assignment);
                    break;
                case "maestros":
                    imageView.setImageResource(R.drawable.getmaestrosicon);
                    break;
                case "equipos":
                    imageView.setImageResource(R.drawable.maqicon);
                    break;
                default:
                    break;
            }
            return rowView;
        }
        @Override
        public void onClick(View v) {

            int position=(Integer) v.getTag();
            Object object= getItem(position);
            OpcionesDatos dataModel=(OpcionesDatos) object;
            Intent i;
            Toast.makeText(DataMaestraMenuActivity.this, v.getTag()+"", Toast.LENGTH_SHORT).show();
            switch (dataModel.getTipo()){
                case "maestros":
                    i = new Intent(DataMaestraMenuActivity.this,MaestrosActivity.class);
                    startActivity(i);
                    break;

            }
        }
    }
}
