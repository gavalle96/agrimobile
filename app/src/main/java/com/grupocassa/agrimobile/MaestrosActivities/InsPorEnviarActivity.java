package com.grupocassa.agrimobile.MaestrosActivities;

import android.content.Context;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.grupocassa.agrimobile.CASSAUTILS.DBHelper;
import com.grupocassa.agrimobile.CLS.ApuntesInsumos;
import com.grupocassa.agrimobile.R;

import java.util.ArrayList;
import java.util.List;

public class InsPorEnviarActivity extends AppCompatActivity {
    ListView lvElementos;
    ArrayList<ApuntesInsumos> elementos;
    DBHelper mydb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ins_por_enviar);

        lvElementos = (ListView) findViewById(R.id.lvElementos);

        mydb = new DBHelper(this);
        elementos = mydb.getApuntesInsumos();

        if (elementos == null) elementos = new ArrayList<ApuntesInsumos>();

        for (ApuntesInsumos ap: elementos
                ) {

        }

        ReportAdapter adapter = new ReportAdapter(this,elementos);
        lvElementos.setAdapter(adapter);

        if (elementos.size() == 0){
            Toast.makeText(this, "No hay datos que mostrar", Toast.LENGTH_SHORT).show();
        }

        LayoutInflater inflater = getLayoutInflater();
        ViewGroup header;
        header = (ViewGroup) inflater.inflate(R.layout.rowinsumos_reporte, lvElementos, false);

        lvElementos.addHeaderView(header);

    }
    private class ReportAdapter extends ArrayAdapter<ApuntesInsumos> {
        private final Context context;
        private final List<ApuntesInsumos> values;

        public ReportAdapter(Context context, List<ApuntesInsumos> values) {
            super(context, -1, values);
            this.context = context;
            this.values = values;
        }

        @Override
        public ApuntesInsumos getItem(int position) {
            return values.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.rowinsumos_reporte, parent, false);
            TextView fecha = (TextView) rowView.findViewById(R.id.fecha);
            TextView os = (TextView) rowView.findViewById(R.id.os);
            TextView insumo = (TextView) rowView.findViewById(R.id.insumo);
            TextView um = (TextView) rowView.findViewById(R.id.um);
            TextView qtd = (TextView) rowView.findViewById(R.id.cantidad);
            TextView area = (TextView) rowView.findViewById(R.id.area);
            TextView obs = (TextView) rowView.findViewById(R.id.obs);
            ApuntesInsumos ins = values.get(position);

            String fec = values.get(position).getFechaApunte();
            String[] f = fec.split("-");
            fecha.setText(f[2]+"-"+f[1]+"-"+f[0]);
            os.setText(String.valueOf( ins.getIdOS()));
            insumo.setText(ins.getDsRecurso());
            area.setText(String.valueOf( ins.getArea()));
            qtd.setText(String.valueOf(ins.getCantidad()));
            um.setText(ins.getUM());
            obs.setText(ins.getObservacion());

            return rowView;
        }
    }

}
