package com.grupocassa.agrimobile.MaestrosActivities;

import android.content.Context;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.grupocassa.agrimobile.CASSAUTILS.DBHelper;
import com.grupocassa.agrimobile.CLS.ApuntesMaquinaria;
import com.grupocassa.agrimobile.R;

import java.util.ArrayList;
import java.util.List;

public class MaqPorEnviarActivity extends AppCompatActivity {

    ListView lvElementos;
    ArrayList<ApuntesMaquinaria> elementos;
    DBHelper mydb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maq_por_enviar);

        lvElementos = (ListView) findViewById(R.id.lvElementos);

        mydb = new DBHelper(this);
        elementos = mydb.getApuntesMaquinaria();

        if (elementos == null) elementos = new ArrayList<ApuntesMaquinaria>();



        ReportAdapter adapter = new ReportAdapter(this,elementos);
        lvElementos.setAdapter(adapter);

        if (elementos.size() == 0){
            Toast.makeText(this, "No hay datos que mostrar", Toast.LENGTH_SHORT).show();
        }

        LayoutInflater inflater = getLayoutInflater();
        ViewGroup header;
        header = (ViewGroup) inflater.inflate(R.layout.rowmaq_reporte, lvElementos, false);

        lvElementos.addHeaderView(header);
    }
    private class ReportAdapter extends ArrayAdapter<ApuntesMaquinaria> {
        private final Context context;
        private final List<ApuntesMaquinaria> values;

        public ReportAdapter(Context context, List<ApuntesMaquinaria> values) {
            super(context, -1, values);
            this.context = context;
            this.values = values;
        }

        @Override
        public ApuntesMaquinaria getItem(int position) {
            return values.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.rowmaq_reporte, parent, false);
            TextView fecha = (TextView) rowView.findViewById(R.id.fecha);
            TextView os = (TextView) rowView.findViewById(R.id.os);

            TextView recurso = (TextView) rowView.findViewById(R.id.recurso);
            TextView trab = (TextView) rowView.findViewById(R.id.operador);
            TextView qtd = (TextView) rowView.findViewById(R.id.cantidad);
            TextView um = (TextView) rowView.findViewById(R.id.um);
            TextView horas = (TextView) rowView.findViewById(R.id.horas);
            TextView dist = (TextView) rowView.findViewById(R.id.dist);
            TextView obs = (TextView) rowView.findViewById(R.id.obs);

            String fec = values.get(position).getFechaApunte();
            String[] f = fec.split("-");
            fecha.setText(f[2]+"-"+f[1]+"-"+f[0]);
            recurso.setText(values.get(position).getDsRecurso());
            os.setText(String.valueOf( values.get(position).getIdOS()));
            trab.setText(values.get(position).getTrabajador());
            qtd.setText(String.valueOf(values.get(position).getQTD_PAGO()));
            um.setText(values.get(position).getUM());
            horas.setText(String.valueOf( values.get(position).getHOR_FIN() - values.get(position).getHOR_INI()));
            dist.setText(String.valueOf( values.get(position).getODO_FIN() - values.get(position).getODO_INI()));
            obs.setText(values.get(position).getObservacion());

            return rowView;
        }
    }

}
