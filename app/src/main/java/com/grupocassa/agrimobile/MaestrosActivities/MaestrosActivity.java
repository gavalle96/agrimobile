package com.grupocassa.agrimobile.MaestrosActivities;

import android.content.Context;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.grupocassa.agrimobile.R;

import java.util.ArrayList;
import java.util.List;

public class MaestrosActivity extends AppCompatActivity {

    private ListView lv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maestros);

        List<String> opcionesDatosList = new ArrayList<String>();
        opcionesDatosList.add("Obtener Todo (Todas las Órdenes de Servicio)");
        opcionesDatosList.add("Obtener OS por Hacienda");

        lv = (ListView)findViewById(R.id.lvMenuMaestros);

        MySimpleArrayAdapter adapter = new MySimpleArrayAdapter(this,opcionesDatosList);
        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i= new Intent(MaestrosActivity.this, ObtenerMaestrosActivity.class);

                switch(position){
                    case 0:
                        i.putExtra("Titulo","Obtener Todas las Órdenes de Servicio");
                        i.putExtra("tipo","todo");
                        break;
                    case 1:
                        i.putExtra("Titulo","Órdenes de Servicio por Hacienda");
                        i.putExtra("tipo","hacienda");
                        break;

                }

                startActivity(i);
            }
        });
        setTitle("Obtener Órdenes de Servicio");

    }

    private class MySimpleArrayAdapter extends ArrayAdapter<String> {
        private final Context context;
        private final List<String> values;

        public MySimpleArrayAdapter(Context context, List<String> values) {
            super(context, -1, values);
            this.context = context;
            this.values = values;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.itemsimple, parent, false);
            TextView textView = (TextView) rowView.findViewById(R.id.txtItem);
            textView.setText(values.get(position));

            return rowView;
        }
    }

}
