package com.grupocassa.agrimobile.MaestrosActivities;

import android.content.DialogInterface;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.grupocassa.agrimobile.CASSAUTILS.AppUpdater;
import com.grupocassa.agrimobile.R;

import static com.grupocassa.agrimobile.MaestrosActivities.ObtenerMaestrosActivity.updater;

public class DownloadActivity extends AppCompatActivity {

    public ProgressBar pgCircular, pgBar;
    public TextView txtMsg;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download);
        pgCircular = (ProgressBar) findViewById(R.id.pgCircle);
        pgBar = (ProgressBar) findViewById(R.id.pgBar);
        txtMsg = (TextView) findViewById(R.id.txtMensajeDescarga);
        if (ObtenerMaestrosActivity.updater != null){
            pgCircular.setVisibility(View.GONE);
            pgBar.setProgress(updater.ObtenerProgreso());
            txtMsg.setText(txtMsg.getText().toString()+" "+ updater.ObtenerProgreso()+"%");
        }
        else{
            IniciarDescarga();
        }
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Actualización en progreso")
                .setMessage("Por favor espere a que finalice la actualización.")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }

                })
                .show();
    }

    private void IniciarDescarga(){
        pgCircular.setVisibility(View.GONE);
        updater = new AppUpdater(this);
        updater.execute();
    }
}
