package com.grupocassa.agrimobile.MaestrosActivities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.grupocassa.agrimobile.CASSAUTILS.DBHelper;
import com.grupocassa.agrimobile.CASSAUTILS.EnviarApuntes;
import com.grupocassa.agrimobile.CLS.ApuntesArea;
import com.grupocassa.agrimobile.CLS.ApuntesInsumos;
import com.grupocassa.agrimobile.CLS.ApuntesMO;
import com.grupocassa.agrimobile.CLS.ApuntesMaquinaria;
import com.grupocassa.agrimobile.CLS.Haciendas;
import com.grupocassa.agrimobile.R;

import java.util.ArrayList;
import java.util.List;

public class EnviarApuntesActivity extends AppCompatActivity implements View.OnClickListener{

    private Button btnEnviar;
    private ListView lvElementos;
    public ArrayList<String> elementos;
    public ProgressBar pgProgress;
    public EnviarApuntes om;
    public ImageView flecha;

    ArrayList<Haciendas> hdasResult;
    DBHelper mydb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enviar_apuntes);


        btnEnviar = (Button) findViewById(R.id.btnEnviarApuntes);
        lvElementos = (ListView) findViewById(R.id.lvEstados);
        pgProgress = (ProgressBar) findViewById(R.id.pgProgress);
        flecha = (ImageView) findViewById(R.id.flecha);

        mydb = new DBHelper(this);
        setTitle("Enviar Apuntamientos al Sistema");
/*
        ArrayList<ApuntesInsumos> apins = mydb.getApuntesInsumos();
        ArrayList<ApuntesMaquinaria> apmq = mydb.getApuntesMaquinaria();
        ArrayList<ApuntesMO> apmo = mydb.getApuntesMO();

        elementos = new ArrayList<String>();
        if(apins == null)
            apins = new ArrayList<ApuntesInsumos>();
        if (apmq == null)
            apmq = new ArrayList<ApuntesMaquinaria>();
        if (apmo == null)
            apmo = new ArrayList<ApuntesMO>();

        elementos.add("Apuntes de Insumos: "+apins.size());
        elementos.add("Apuntes de Mano de Obra: "+apmo.size());
        elementos.add("Apuntes de Maquinaria y Servicios Agrícolas: "+apmq.size());
        cargarLista();*/

        Refrescar();
        btnEnviar.setOnClickListener(this);

        lvElementos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 2){
                    Intent i = new Intent(EnviarApuntesActivity.this,MaqPorEnviarActivity.class);
                    startActivity(i);
                }

                if (position == 1){
                    Intent i = new Intent(EnviarApuntesActivity.this,MOPorEnviarActivity.class);
                    startActivity(i);
                }

                if (position == 0){
                    Intent i = new Intent(EnviarApuntesActivity.this,InsPorEnviarActivity.class);
                    startActivity(i);
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        boolean es = v.getId() == btnEnviar.getId();
        if(es){
            AttempObtener();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Toast.makeText(this, "Prueba", Toast.LENGTH_SHORT).show();

    }
    public void Refrescar(){
        ArrayList<ApuntesInsumos> apins = mydb.getApuntesInsumos();
        ArrayList<ApuntesMaquinaria> apmq = mydb.getApuntesMaquinaria();
        ArrayList<ApuntesMO> apmo = mydb.getApuntesMO();
        ArrayList<ApuntesArea> apArea = mydb.getApuntesAreaNoEnviados();

        elementos = new ArrayList<String>();
        if(apins == null)
            apins = new ArrayList<ApuntesInsumos>();
        if (apmq == null)
            apmq = new ArrayList<ApuntesMaquinaria>();
        if (apmo == null)
            apmo = new ArrayList<ApuntesMO>();
        if (apArea == null)
            apArea = new ArrayList<ApuntesArea>();

        elementos.add("Apuntes de Insumos: "+apins.size());
        elementos.add("Apuntes de Mano de Obra: "+apmo.size());
        elementos.add("Apuntes de Maquinaria y Servicios Agrícolas: "+apmq.size());
        elementos.add("Apuntes de Área: "+apArea.size());
        cargarLista();
    }

    private void AttempObtener() {
        if (om != null) return;

  /*      Context sharedContext = null;
        try {
            sharedContext = this.createPackageContext("futronictech.com", Context.CONTEXT_INCLUDE_CODE);
            if (sharedContext != null) {

                DBExterno.ctxOriginal = this;
*/
                om = new EnviarApuntes(this);

                om.execute();
            /*    DBExterno dbe = new DBExterno(sharedContext);
                om.htrab = dbe.getHuellas(false); //dbe.getTrabajadores();

            }
            else{
                Toast.makeText(this, "contexto nulo", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            String error = e.getMessage();
            Toast.makeText(this,error, Toast.LENGTH_LONG).show();
        }*/

    }
    public void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);


            pgProgress.setVisibility(show ? View.VISIBLE : View.GONE);
            pgProgress.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    pgProgress.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            pgProgress.setVisibility(show ? View.VISIBLE : View.GONE);
        }
    }

    public void cargarLista() {
        if(elementos!=null) {
            MySimpleArrayAdapter adapter = new MySimpleArrayAdapter(this, elementos);
            lvElementos.setAdapter(adapter);
        }
    }

    private class MySimpleArrayAdapter extends ArrayAdapter<String> {
        private final Context context;
        private final List<String> values;

        public MySimpleArrayAdapter(Context context, List<String> values) {
            super(context, -1, values);
            this.context = context;
            this.values = values;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.itemsimple, parent, false);
            TextView textView = (TextView) rowView.findViewById(R.id.txtItem);
            textView.setText(values.get(position));

            return rowView;
        }
    }
}
