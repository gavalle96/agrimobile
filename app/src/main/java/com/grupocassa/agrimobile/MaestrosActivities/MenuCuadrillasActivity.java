package com.grupocassa.agrimobile.MaestrosActivities;

import android.content.Context;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.grupocassa.agrimobile.R;

import java.util.ArrayList;
import java.util.List;

public class MenuCuadrillasActivity extends AppCompatActivity {
    private ListView lv;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_cuadrillas);
        List<String> opcionesDatosList = new ArrayList<String>();
        opcionesDatosList.add("Cuadrillas");
        opcionesDatosList.add("Trabajadores por Cuadrilla");

        lv = (ListView)findViewById(R.id.lvMenuCuadrillas);

        MySimpleArrayAdapter adapter = new MySimpleArrayAdapter(this,opcionesDatosList);
        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i= new Intent(MenuCuadrillasActivity.this, ObtenerMaestrosActivity.class);

                switch(position){
                    case 0:
                        i.putExtra("Titulo","Obtener Todas las Cuadrillas");
                        i.putExtra("tipo","cuadrillas");
                        break;
                    case 1:
                        i.putExtra("Titulo","Trabajadores por Cuadrilla");
                        i.putExtra("tipo","trabajadores");
                        break;

                }

                startActivity(i);
            }
        });
        setTitle("Obtener Cuadrillas de Trabajadores");

    }

    private class MySimpleArrayAdapter extends ArrayAdapter<String> {
        private final Context context;
        private final List<String> values;

        public MySimpleArrayAdapter(Context context, List<String> values) {
            super(context, -1, values);
            this.context = context;
            this.values = values;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.itemsimple, parent, false);
            TextView textView = (TextView) rowView.findViewById(R.id.txtItem);
            textView.setText(values.get(position));

            return rowView;
        }
    }
}
