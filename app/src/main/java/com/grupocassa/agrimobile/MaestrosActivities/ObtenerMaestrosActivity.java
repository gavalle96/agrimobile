package com.grupocassa.agrimobile.MaestrosActivities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.grupocassa.agrimobile.CASSAUTILS.AppUpdater;
import com.grupocassa.agrimobile.CASSAUTILS.CustomCuadrillasAdapter;
import com.grupocassa.agrimobile.CASSAUTILS.CustomHdasAdapter;
import com.grupocassa.agrimobile.CASSAUTILS.CustomOSAdapter;
import com.grupocassa.agrimobile.CASSAUTILS.DBHelper;
import com.grupocassa.agrimobile.CASSAUTILS.ObtenerMaestros;
import com.grupocassa.agrimobile.CLS.ApuntesInsumos;
import com.grupocassa.agrimobile.CLS.ApuntesMO;
import com.grupocassa.agrimobile.CLS.ApuntesMaquinaria;
import com.grupocassa.agrimobile.CLS.Cuadrillas;
import com.grupocassa.agrimobile.CLS.Haciendas;
import com.grupocassa.agrimobile.CLS.OS;
import com.grupocassa.agrimobile.CLS.OSRecursos;
import com.grupocassa.agrimobile.CLS.Trabajadores;
import com.grupocassa.agrimobile.R;

import java.util.ArrayList;
import java.util.List;

public class ObtenerMaestrosActivity extends AppCompatActivity implements View.OnClickListener {

    public AutoCompleteTextView txtParametro;
    Button btnObtener;
    ListView lvElementos;
    public ArrayList<Cuadrillas> cuadrillasMostrar;
    public ObtenerMaestros om;
    //public ObtieneHuellas oh;
    public View mProgressView;
    public ProgressBar pgProgress;
    private String BkHacienda;
    private String BkCuadrilla;
    private int IdOS;
    public ArrayList<String> elementos;
    private DBHelper mydb;
    public Button btnConsultar, btnClear;
    private String tipo;
    boolean todo,Opselected;
    private ScrollView LinearTree;
    private RelativeLayout relativeLayout;
    ArrayList<Haciendas> hdasResult;
    boolean isFirstViewClick=false;
    boolean isSecondViewClick=false;
    boolean DataMaestra = false;
    public EditText txtOS;
    LinearLayout linearLyt;
    public static AppUpdater updater;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_obtener_maestros);

        cuadrillasMostrar = new ArrayList<Cuadrillas>();
        elementos = new ArrayList<String>();
        mydb = new DBHelper(this);
        txtParametro = (AutoCompleteTextView) findViewById(R.id.txtParametro);
        btnObtener = (Button) findViewById(R.id.btnObtener);
        lvElementos = (ListView) findViewById(R.id.lvElements);
        btnConsultar = (Button) findViewById(R.id.btnConsulta);
        btnClear = (Button) findViewById(R.id.btnClear);
        mProgressView = findViewById(R.id.progress);
        LinearTree = (ScrollView) findViewById(R.id.TreeView);
        linearLyt = (LinearLayout) findViewById(R.id.linear_ListView);
        relativeLayout = (RelativeLayout) findViewById(R.id.LView);
        txtOS = (EditText) findViewById(R.id.txtOS);
        txtOS.setVisibility(View.GONE);
        pgProgress = (ProgressBar) findViewById(R.id.pgProgress);
        IdOS = 0;
        BkHacienda = "";
        todo = false;
        Intent i = getIntent();
        Bundle extras = i.getExtras();
        String Titulo = extras.getString("Titulo");
        Opselected = false;
        if (Titulo != null) {
            setTitle(Titulo);
        }
        DataMaestra = extras.getBoolean("dm",false);
        tipo = extras.getString("tipo");

        if (!tipo.equals("todo")) {
            if (tipo.equals("hacienda")) {

                ArrayList<Haciendas> hdas = mydb.getAllHaciendas();

                CustomHdasAdapter adapter = new CustomHdasAdapter(
                        this, android.R.layout.simple_dropdown_item_1line, hdas);
                txtParametro.setAdapter(adapter);


                txtParametro.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                            long arg3) {
                        Haciendas selected = (Haciendas) arg0.getAdapter().getItem(arg2);
                        BkHacienda = selected.getBkHacienda();
                        Opselected = true;
                    }

                });
            }
            if (tipo.equals("os")) {


                txtParametro.setHint("Orden de Servicio");
                if (!DataMaestra) {
                    ArrayList<OS> lOS = mydb.getAllOS();
                    CustomOSAdapter adapter = new CustomOSAdapter(this, android.R.layout.simple_dropdown_item_1line, lOS);
                    txtParametro.setAdapter(adapter);

                    txtParametro.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                                long arg3) {
                            OS selected = (OS) arg0.getAdapter().getItem(arg2);
                            IdOS = selected.getIdOS();
                            Opselected = true;
                        }

                    });
                }
                else{
                    txtParametro.setVisibility(View.GONE);
                    txtOS.setVisibility(View.VISIBLE);
                }
            }
            if (tipo.equals("cuadrillas")){
                txtParametro.setVisibility(View.GONE);
            }
            if (tipo.equals("trabajadores")){
                ArrayList<Cuadrillas> lOS = mydb.getAllCuadrillas();

                txtParametro.setHint("Cuadrilla");
                CustomCuadrillasAdapter adapter = new CustomCuadrillasAdapter(this, android.R.layout.simple_dropdown_item_1line, lOS,true);
                txtParametro.setAdapter(adapter);

                txtParametro.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                            long arg3) {
                        Cuadrillas selected = (Cuadrillas) arg0.getAdapter().getItem(arg2);
                        BkCuadrilla = selected.getBkCuadrilla();
                        Opselected = true;
                    }

                });
            }

            LinearTree.setVisibility(View.GONE);
            relativeLayout.setVisibility(View.VISIBLE);
        } else {
            txtParametro.setVisibility(View.GONE);
            todo = true;
            relativeLayout.setVisibility(View.GONE);
            //btnConsultar.setVisibility(View.GONE);
        }
        btnConsultar.setOnClickListener(this);
        btnObtener.setOnClickListener(this);

        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtOS.setText("");
                txtParametro.setText("");

                BkCuadrilla = "";
                BkHacienda = "";
                IdOS = 0;
            }
        });

      // AvisoInstalacion();
    }

    public void cargarLista() {

        if(tipo.equals("cuadrillas")){
             CuadrillasAdapter adapter = new CuadrillasAdapter(this, cuadrillasMostrar);
            lvElementos.setAdapter(adapter);
        }
        else if(tipo.equals("trabajadores")) {
           /* CuadrillasAdapter adapter = new CuadrillasAdapter(this, cuadrillasMostrar);
            lvElementos.setAdapter(adapter);*/
            Cuadrillas c = mydb.getSingleCuadrillabyBK(BkCuadrilla);
            ArrayList<Trabajadores> trabajadoresal = mydb.getTrabajadoresPorCuadrilla(c.getIdCuadrilla());
            elementos = new ArrayList<String>();
            for (Trabajadores t: trabajadoresal
                    ) {
                elementos.add(t.getCodTrabajador()+" - "+t.getNomTrabajador() );

            }
            //cargarLista();
            MySimpleArrayAdapter adapter = new MySimpleArrayAdapter(this, elementos);
            lvElementos.setAdapter(adapter);
            /*if(oh == null) {
               // oh = new ObtieneHuellas(this, BkCuadrilla);
                //oh.execute();
            }
*/
        }
        else{
            if (elementos != null) {
                if (tipo.equals("todo")) {

                    hdasResult = mydb.getAllHaciendas();

                    if (hdasResult !=null)

                        cargarTree();
                    else
                        Toast.makeText(this, "No hay datos para mostrar", Toast.LENGTH_SHORT).show();

                }
                else{
                    MySimpleArrayAdapter adapter = new MySimpleArrayAdapter(this, elementos);
                    lvElementos.setAdapter(adapter);

                }

            }
        }

        showProgress(false);
    }

    private void cargarTree(){
        linearLyt.removeAllViewsInLayout();
        //Adds data into first row
        for (int i = 0; i < hdasResult.size(); i++)
        {

            hdasResult.get(i).setmOS(mydb.getAllOSByHacienda(hdasResult.get(i).getIdHacienda()));
            LayoutInflater inflater = null;
            inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View mLinearView = inflater.inflate(R.layout.row_first, null);

            final TextView mProductName = (TextView) mLinearView.findViewById(R.id.textViewName);
            final RelativeLayout mLinearFirstArrow=(RelativeLayout)mLinearView.findViewById(R.id.linearFirst);
            final ImageView mImageArrowFirst=(ImageView)mLinearView.findViewById(R.id.imageFirstArrow);
            final LinearLayout mLinearScrollSecond=(LinearLayout)mLinearView.findViewById(R.id.linear_scroll);

            //checkes if menu is already opened or not
            if(isFirstViewClick==false)
            {
                mLinearScrollSecond.setVisibility(View.GONE);
                mImageArrowFirst.setBackgroundResource(R.drawable.arrright);
            }
            else
            {
                mLinearScrollSecond.setVisibility(View.VISIBLE);
                mImageArrowFirst.setBackgroundResource(R.drawable.arrdown);
            }
            //Handles onclick effect on list item
            mLinearFirstArrow.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v) {
                    if(isFirstViewClick==false)
                    {
                        isFirstViewClick=true;
                        mImageArrowFirst.setBackgroundResource(R.drawable.arrdown);
                        mLinearScrollSecond.setVisibility(View.VISIBLE);

                    }
                    else
                    {
                        isFirstViewClick=false;
                        mImageArrowFirst.setBackgroundResource(R.drawable.arrright);
                        mLinearScrollSecond.setVisibility(View.GONE);
                    }
                }


            });


            final String name = hdasResult.get(i).getBkHacienda()+" - "+hdasResult.get(i).getDsHacienda();
            mProductName.setText(name);

            //Adds data into second row
            for (int j = 0; j < hdasResult.get(i).getmOS().size(); j++)
            {
                LayoutInflater inflater2 = null;
                inflater2 = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View mLinearView2 = inflater2.inflate(R.layout.row_second, null);

                TextView mSubItemName = (TextView) mLinearView2.findViewById(R.id.textViewTitle);
                final RelativeLayout mLinearSecondArrow=(RelativeLayout)mLinearView2.findViewById(R.id.linearSecond);
                final ImageView mImageArrowSecond=(ImageView)mLinearView2.findViewById(R.id.imageSecondArrow);
                final LinearLayout mLinearScrollThird=(LinearLayout)mLinearView2.findViewById(R.id.linear_scroll_third);

                //checkes if menu is already opened or not
                if(isSecondViewClick==false)
                {
                    mLinearScrollThird.setVisibility(View.GONE);
                    mImageArrowSecond.setBackgroundResource(R.drawable.arrright);
                }
                else
                {
                    mLinearScrollThird.setVisibility(View.VISIBLE);
                    mImageArrowSecond.setBackgroundResource(R.drawable.arrdown);
                }
                //Handles onclick effect on list item
                mLinearSecondArrow.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v) {
                        if(isSecondViewClick==false)
                        {
                            isSecondViewClick=true;
                            mImageArrowSecond.setBackgroundResource(R.drawable.arrdown);
                            mLinearScrollThird.setVisibility(View.VISIBLE);

                        }
                        else
                        {
                            isSecondViewClick=false;
                            mImageArrowSecond.setBackgroundResource(R.drawable.arrright);
                            mLinearScrollThird.setVisibility(View.GONE);
                        }
                    }


                });


                OS o = hdasResult.get(i).getmOS().get(j);
                final String catName = "OS "+o.getIdOS()+" - "+o.getDsActividad()+". Cant: "+o.getCantidad();
                mSubItemName.setText(catName);
                hdasResult.get(i).getmOS().get(j).setRecursos(mydb.getAllRecursosByOS(hdasResult.get(i).getmOS().get(j).getIdOS()));

                //Adds items in subcategories
                for (int k = 0; k < hdasResult.get(i).getmOS().get(j).getRecursos().size(); k++)
                {
                    LayoutInflater inflater3 = null;
                    inflater3 = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View mLinearView3 = inflater3.inflate(R.layout.row_third, null);

                    TextView mItemName = (TextView) mLinearView3.findViewById(R.id.textViewItemName);
                    TextView mItemPrice = (TextView) mLinearView3.findViewById(R.id.textViewItemPrice);
                    OSRecursos item = hdasResult.get(i).getmOS().get(j).getRecursos().get(k);
                    final String itemName = item.getDsRecurso();
                    final String itemPrice = item.getCantidadPend()+" "+item.getUM();
                    mItemName.setText(itemName);
                    mItemPrice.setText(itemPrice);

                    mLinearScrollThird.addView(mLinearView3);
                }

                mLinearScrollSecond.addView(mLinearView2);

            }

            linearLyt.addView(mLinearView);
        }

    }
    private boolean validarParametro() {
        String sParam = txtParametro.getText().toString();
        boolean ok = true;
        if(sParam.equals("") && todo != true && !tipo.equals("cuadrillas")){
            txtParametro.setError("El campo es requerido");
            ok =  false;
        }
        else if (Opselected == false && !tipo.equals("os") && !tipo.equals("cuadrillas") && !tipo.equals("todo")){
            ok =false;
            txtParametro.setError("Seleccione una de las sugerencias");
        }
        if (tipo.equals("os") && DataMaestra)
        {

            try {
                IdOS = Integer.parseInt(txtOS.getText().toString());
                if (IdOS <= 0){
                    txtOS.setError("Debe digitar un valor numérico mayor a cero");
                    return false;
                }
                return true;
            } catch (NumberFormatException e) {
                txtOS.setError("Debe digitar un valor numérico entero para la OS");
                return false;
            }
        }
        return ok;
    }

    private void AttempObtener() {

        if (ApuntesPendientes()) {
            ShowWarningApuntesPendientes();
            return;
        }

        if (om != null) return;


        om = new ObtenerMaestros(BkHacienda, IdOS,DataMaestra, this);
        om.execute();
    }



    private void ShowWarningApuntesPendientes(){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setTitle("Obtener datos maestros");
        builder1.setMessage("Debe enviar todos los apuntamientos antes de obtener nuevos datos");
        builder1.setCancelable(true);
        builder1.setNeutralButton(android.R.string.ok,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    private boolean ApuntesPendientes(){
        ArrayList<ApuntesInsumos> apins = mydb.getApuntesInsumos();
        ArrayList<ApuntesMaquinaria> apmq = mydb.getApuntesMaquinaria();
        ArrayList<ApuntesMO> apmo = mydb.getApuntesMO();

        elementos = new ArrayList<String>();
        if(apins == null)
            apins = new ArrayList<ApuntesInsumos>();
        if (apmq == null)
            apmq = new ArrayList<ApuntesMaquinaria>();
        if (apmo == null)
            apmo = new ArrayList<ApuntesMO>();

        return (apins.size() + apmq.size() + apmo.size()) > 0;
    }

    private  void AttempObtenerTrab(){
        if (om != null) return;
        if(tipo.equals("cuadrillas")) {
            om = new ObtenerMaestros("", false, this);
        }
        if (tipo.equals("trabajadores")){
            om = new ObtenerMaestros(BkCuadrilla, true, this);

        }
        om.execute();
    }



    @Override
    protected void onRestart() {
        super.onRestart();
        cargarLista();
    }

    public void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);


            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        if(validarParametro() && v == btnObtener){

            if( !tipo.equals("cuadrillas") && !tipo.equals("trabajadores")){
                AttempObtener();
            }
            else {
                AttempObtenerTrab();
            }

            txtOS.setVisibility(View.INVISIBLE);
            txtParametro.setVisibility(View.INVISIBLE);
            mProgressView.setVisibility(View.VISIBLE);
        }
        else if(v == btnConsultar && validarParametro()){

            switch (tipo){
                case "todo":
                    ArrayList<Haciendas> hdas = mydb.getAllHaciendas();
                    elementos = new ArrayList<String>();

                    for ( Haciendas h: hdas){
                        elementos.add(h.getBkHacienda()+" - "+h.getDsHacienda());
                        ArrayList<OS> oeses = mydb.getAllOSByHacienda(h.getIdHacienda());
                        for (OS o : oeses){
                            ArrayList<OSRecursos> recursoses = mydb.getAllRecursosByOS(IdOS);
                            o.setRecursos(recursoses);
                        }
                        h.setmOS(oeses);
                    }
                    cargarLista();
                    break;
                case "hacienda":
                    Haciendas hs = mydb.getSingleHaciendabyBK(BkHacienda);
                    ArrayList<OS> os = mydb.getAllOSByHacienda(hs.getIdHacienda());
                    elementos = new ArrayList<String>();

                    for ( OS h: os){
                        elementos.add("OS "+h.getIdOS()+" - "+h.getDsActividad()+" - Cant: "+h.getCantidad());
                    }
                    cargarLista();
                    break;
                case "os":
                    ArrayList<OSRecursos> osr = mydb.getAllRecursosByOS(IdOS);
                    elementos = new ArrayList<String>();

                    for ( OSRecursos h: osr){
                        elementos.add(h.getDsRecurso()+": "+h.getCantidadPend()+" "+h.getUM());
                    }
                    cargarLista();
                    break;
                case "cuadrillas":

                    if(lvElementos.getChildCount() == 0) {
                        LayoutInflater inflater = getLayoutInflater();
                        ViewGroup header;
                        header = (ViewGroup) inflater.inflate(R.layout.itemcuadrilla, lvElementos, false);

                        lvElementos.addHeaderView(header);
                    }
                    ArrayList<Cuadrillas> cuadrillasArrayList = mydb.getAllCuadrillas();
                    elementos = new ArrayList<String>();
                    for (Cuadrillas c: cuadrillasArrayList
                         ) {
                        cuadrillasMostrar.add(c);

                    }
                    cargarLista();
                    break;
                case "trabajadores":
                    Cuadrillas c = mydb.getSingleCuadrillabyBK(BkCuadrilla);
                    ArrayList<Trabajadores> trabajadoresal = mydb.getTrabajadoresPorCuadrilla(c.getIdCuadrilla());
                    elementos = new ArrayList<String>();
                    for (Trabajadores t: trabajadoresal
                            ) {
                        elementos.add(t.getCodTrabajador()+" - "+t.getNomTrabajador() );

                    }
                    cargarLista();
                    break;
            }
        }
    }

    private class MySimpleArrayAdapter extends ArrayAdapter<String> {
        private final Context context;
        private final List<String> values;

        public MySimpleArrayAdapter(Context context, List<String> values) {
            super(context, -1, values);
            this.context = context;
            this.values = values;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.itemsimple, parent, false);
            TextView textView = (TextView) rowView.findViewById(R.id.txtItem);
            textView.setText(values.get(position));

            return rowView;
        }
    }

    private class CuadrillasAdapter extends ArrayAdapter<Cuadrillas> {
        private final Context context;
        private final List<Cuadrillas> values;

        public CuadrillasAdapter(Context context, List<Cuadrillas> values) {
            super(context, -1, values);
            this.context = context;
            this.values = values;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.itemcuadrilla, parent, false);
            TextView dscuad = (TextView) rowView.findViewById(R.id.dscuad);
            TextView numtrab = (TextView) rowView.findViewById(R.id.numtrab);

            Cuadrillas obj = values.get(position);
            dscuad.setText(obj.getDsCuadrilla());

            ArrayList<Trabajadores> trabs = mydb.getTrabajadoresPorCuadrilla(obj.getIdCuadrilla());

            if (trabs == null){
                trabs = new ArrayList<Trabajadores>();
            }

            numtrab.setText(String.valueOf(trabs.size()));

            return rowView;
        }
    }
}

