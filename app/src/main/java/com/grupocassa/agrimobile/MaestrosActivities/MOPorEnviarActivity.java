package com.grupocassa.agrimobile.MaestrosActivities;

import android.content.Context;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.grupocassa.agrimobile.CASSAUTILS.DBHelper;
import com.grupocassa.agrimobile.CLS.ApuntesMO;
import com.grupocassa.agrimobile.R;

import java.util.ArrayList;
import java.util.List;

public class MOPorEnviarActivity extends AppCompatActivity {

    ListView lvElementos;
    ArrayList<ApuntesMO> elementos;
    DBHelper mydb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mopor_enviar);

        lvElementos = (ListView) findViewById(R.id.lvElementos);

        mydb = new DBHelper(this);
        elementos = mydb.getApuntesMO();

        if (elementos == null) elementos = new ArrayList<ApuntesMO>();

        for (ApuntesMO ap: elementos
             ) {

        }

        ReportAdapter adapter = new ReportAdapter(this,elementos);
        lvElementos.setAdapter(adapter);

        if (elementos.size() == 0){
            Toast.makeText(this, "No hay datos que mostrar", Toast.LENGTH_SHORT).show();
        }

        LayoutInflater inflater = getLayoutInflater();
        ViewGroup header;
        header = (ViewGroup) inflater.inflate(R.layout.rowmo_reporte, lvElementos, false);

        lvElementos.addHeaderView(header);

    }
    private class ReportAdapter extends ArrayAdapter<ApuntesMO> {
        private final Context context;
        private final List<ApuntesMO> values;

        public ReportAdapter(Context context, List<ApuntesMO> values) {
            super(context, -1, values);
            this.context = context;
            this.values = values;
        }

        @Override
        public ApuntesMO getItem(int position) {
            return values.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.rowmo_reporte, parent, false);
            TextView fecha = (TextView) rowView.findViewById(R.id.fecha);
            TextView recurso = (TextView) rowView.findViewById(R.id.recurso);
            TextView trab = (TextView) rowView.findViewById(R.id.Trabajador);
            TextView qtd = (TextView) rowView.findViewById(R.id.ctdPago);
            TextView um = (TextView) rowView.findViewById(R.id.um);
            TextView cuad = (TextView) rowView.findViewById(R.id.cuadrilla);

            String fec = values.get(position).getFechaApunte();
            String[] f = fec.split("-");
            fecha.setText(f[2]+"-"+f[1]+"-"+f[0]);
            recurso.setText(values.get(position).getDsRecurso());
            trab.setText(values.get(position).getTrabajador());
            qtd.setText(String.valueOf(values.get(position).getQTD_PAGO()));
            um.setText(values.get(position).getUM());
            cuad.setText(values.get(position).getDsCuadrilla());

            return rowView;
        }
    }

}
