package com.grupocassa.agrimobile.MaestrosActivities;

import android.content.Context;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.grupocassa.agrimobile.CASSAUTILS.DBHelper;
import com.grupocassa.agrimobile.CASSAUTILS.ObtieneEquipos;
import com.grupocassa.agrimobile.CLS.Equipos;
import com.grupocassa.agrimobile.R;

import java.util.ArrayList;
import java.util.List;

public class ObtenerEquiposActivity extends AppCompatActivity {

    public static ObtieneEquipos oe;
    public ProgressBar pgProgreso;
    public Button btnConsultar, btnDescargar;
    ListView lvItems;
    ArrayList<Equipos> eqs;
    ArrayList<String> elementos;
    DBHelper mydb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_obtener_equipos);
        mydb = new DBHelper(this);

        eqs = new ArrayList<Equipos>();

        pgProgreso = (ProgressBar) findViewById(R.id.pgprogreso);
        btnConsultar = (Button) findViewById(R.id.btnConsulta);
        btnDescargar = (Button) findViewById(R.id.btnDescarga);
        lvItems = (ListView) findViewById(R.id.lvItems);

        pgProgreso.setVisibility(View.GONE);

        btnConsultar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnDescargar.setEnabled(false);
                CargarEquipos();
                btnDescargar.setEnabled(true);
            }
        });

        btnDescargar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (oe!=null) return;
                oe = new ObtieneEquipos(ObtenerEquiposActivity.this);
                oe.execute();
                btnDescargar.setVisibility(View.INVISIBLE);
                btnConsultar.setVisibility(View.INVISIBLE);
                pgProgreso.setVisibility(View.VISIBLE);
            }
        });
    }
    public void CargarEquipos(){
        eqs = mydb.getAllEquipos();
        elementos = new ArrayList<String>();
        if (eqs == null) eqs = new ArrayList<Equipos>();
        for (Equipos e: eqs
             ) {
            elementos.add(e.getCodEquipo().trim()+" - "+e.getNomEquipo().trim());
        }
        MySimpleArrayAdapter adapter = new MySimpleArrayAdapter(this,elementos);
        lvItems.setAdapter(adapter);
    }

    private class MySimpleArrayAdapter extends ArrayAdapter<String> {
        private final Context context;
        private final List<String> values;

        public MySimpleArrayAdapter(Context context, List<String> values) {
            super(context, -1, values);
            this.context = context;
            this.values = values;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.itemsimple, parent, false);
            TextView textView = (TextView) rowView.findViewById(R.id.txtItem);
            textView.setText(values.get(position));

            return rowView;
        }
    }
}
