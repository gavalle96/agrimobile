package com.grupocassa.agrimobile;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.grupocassa.agrimobile.CASSAUTILS.DBHelper;
import com.grupocassa.agrimobile.CASSAUTILS.NetworkUtility;
import com.grupocassa.agrimobile.CASSAUTILS.RestHttpsClient;
import com.grupocassa.agrimobile.CLS.Empresas;
import com.grupocassa.agrimobile.CLS.Parametros;
import com.grupocassa.agrimobile.CLS.VolleyCallback;
/*
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;*/
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/*
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;*/

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {

    /**
     * Id to identity READ_CONTACTS permission request.
     */
    private static final int REQUEST_PHONE_STATE = 1;
    private static final int REQUEST_READ_CONTACTS = 0;
    private String imei;
    //public static String calipub = "https://168.243.156.109:8001";// CALIDAD PUBLICO
    //public static String calilan = "https://192.200.11.10:8001";// CALIDAD LAN
    //public static String urlServer = "http://10.0.2.2:60940"; LOCALHOST

    Spinner spEmpresas;
    ArrayList<Empresas> empresas;
    int SOCIEDAD;

    /**
     * A dummy authentication store containing known user names and passwords.
     * TODO: remove after connecting to a real authentication system.
     */
    private static final String[] DUMMY_CREDENTIALS = new String[]{
            "admin:admin", "bar@example.com:world"
    };
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;

    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    private DBHelper mydb;
    private String txtFToast;
    private RestHttpsClient rc;
    public boolean logged;
    private CheckBox chkShowPwd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mydb = new DBHelper(this);
        // Set up the login form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        //populateAutoComplete();

        spEmpresas = (Spinner) findViewById(R.id.spEmpresa);


        empresas = mydb.getAllEmpresas();

        MySimpleArrayAdapter adapterSp = new MySimpleArrayAdapter(this, empresas);
        adapterSp.setDropDownViewResource(R.layout.itemsimple);

        spEmpresas.setAdapter(adapterSp);
        spEmpresas.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Empresas p = (Empresas) parent.getItemAtPosition(position);
                SOCIEDAD = p.getSociedad();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (empresas.size() > 0) {
            spEmpresas.setSelection(2);
        }

        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        chkShowPwd = (CheckBox) findViewById(R.id.chkMostrarPwd);

        chkShowPwd.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // Toast.makeText(LoginActivity.this, ""+isChecked, Toast.LENGTH_SHORT).show();
                if (isChecked) {

                    mPasswordView.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                } else //if(showhide.getText().equals("Show"))
                {
                    //showhide.setText("Hide");
                    mPasswordView.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
            }
        });

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
        if (mydb.getSingleParametro(1) == null) {
            Boolean i = mydb.insertParametro(1, "AgricolaMobile");
        }
        if (mydb.getSingleParametro(2) == null) {
            Boolean i = mydb.insertParametro(2, "C4ss4Agr1col_Mobile0");
        }

        Parametros mImei = mydb.getSingleParametro(3);
        if (mImei == null) {
            TelephonyManager mngr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    Activity#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for Activity#requestPermissions for more details.
                    return;
                }
            }
            String imei = mngr.getDeviceId();
            Boolean i = mydb.insertParametro(3, imei);
        }


    }


    private void showMessages() {
        Toast.makeText(this, txtFToast, Toast.LENGTH_LONG).show();
    }
/*
    private void populateAutoComplete() {
        if (!mayRequestContacts()) {
            return;
        }

        getLoaderManager().initLoader(0, null, this);
    }*/
/*
    private boolean mayRequestContacts() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(READ_CONTACTS)) {
            Snackbar.make(mEmailView, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
                        }
                    });
        } else {
            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
        }
        return false;
    }*/

    /**
     * Callback received when a permissions request has been completed.
     *//*
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_READ_CONTACTS) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                populateAutoComplete();
            }
        }
    }*/


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString().toLowerCase();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        /*Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }*/

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            checkForPhoneStatePermission();
            mAuthTask = new UserLoginTask(email, password);
            mAuthTask.execute((Void) null);
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */

    private void checkForPhoneStatePermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (ContextCompat.checkSelfPermission(LoginActivity.this,
                    Manifest.permission.READ_PHONE_STATE)
                    != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(LoginActivity.this,
                        Manifest.permission.READ_PHONE_STATE)) {

                    // Show an explanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.

                    showPermissionMessage();

                } else {

                    // No explanation needed, we can request the permission.
                    ActivityCompat.requestPermissions(LoginActivity.this,
                            new String[]{Manifest.permission.READ_PHONE_STATE},
                            REQUEST_PHONE_STATE);
                }
            } else {
                //... Permission has already been granted, obtain the UUID
                getDeviceUuId(LoginActivity.this);
            }

        } else {
            //... No need to request permission, obtain the UUID
            getDeviceUuId(LoginActivity.this);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case REQUEST_PHONE_STATE:

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // .. Can now obtain the UUID
                    getDeviceUuId(LoginActivity.this);
                } else {
                    Toast.makeText(LoginActivity.this, "La aplicación no funciona si no otorga los permisos.", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public void GoToMain() {
        Intent i = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(i);
        finish();
    }


    private void getDeviceUuId(Context context) {

        TelephonyManager mngr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    Activity#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for Activity#requestPermissions for more details.
                return;
            }
        }
        imei = mngr.getDeviceId();
    }

    private void showPermissionMessage(){
        new AlertDialog.Builder(this)
                .setTitle("Leer el estado del teléfono")
                .setMessage("Esta aplicación necesita leer el estado del teléfono para continuar.")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(LoginActivity.this,
                                new String[]{Manifest.permission.READ_PHONE_STATE},
                                REQUEST_PHONE_STATE);
                    }
                }).create().show();
    }


    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private final String mEmail;
        private final String mPassword;

        UserLoginTask(String email, String password) {
            mEmail = email.trim();
            mPassword = password.trim();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.
            boolean ok = false;
                Autenticar(mEmail, mPassword, new VolleyCallback() {
                    @Override
                    public boolean onSuccess() {

                            mydb.deleteParametro(5);
                            mydb.deleteParametro(6);
                            if(mydb.getSingleParametro(5)==null) {
                                Boolean i = mydb.insertParametro(5, mEmail);
                            }
                            if(mydb.getSingleParametro(6)==null) {
                                Boolean i = mydb.insertParametro(6, mPassword);
                            }
                            ObtenerApps(mEmail, new VolleyCallback() {
                                @Override
                                public boolean onSuccess() {
                                    return false;
                                }

                                @Override
                                public boolean onError() {
                                    return false;
                                }

                                @Override
                                public boolean onSuccess(String resp) {
                                    return false;
                                }

                                @Override
                                public boolean onError(String resp) {
                                    return false;
                                }
                            });

                            mydb.deleteParametro(8);
                            mydb.insertParametro(8,String.valueOf(SOCIEDAD));
                        GoToMain();

                        return false;
                    }

                    @Override
                    public boolean onError() {
                        ObtenerApps(mEmail, new VolleyCallback() {
                            @Override
                            public boolean onSuccess() {
                                return false;
                            }

                            @Override
                            public boolean onError() {
                                return false;
                            }

                            @Override
                            public boolean onSuccess(String resp) {
                                return false;
                            }

                            @Override
                            public boolean onError(String resp) {
                                return false;
                            }
                        });

                        return false;
                    }

                    @Override
                    public boolean onSuccess(String resp) {
                        mydb.deleteParametro(5);
                        mydb.deleteParametro(6);
                        if(mydb.getSingleParametro(5)==null) {
                            Boolean i = mydb.insertParametro(5, mEmail);
                        }
                        if(mydb.getSingleParametro(6)==null) {
                            Boolean i = mydb.insertParametro(6, mPassword);
                        }
                        ObtenerApps(mEmail, new VolleyCallback() {
                            @Override
                            public boolean onSuccess() {
                                return false;
                            }

                            @Override
                            public boolean onError() {
                                return false;
                            }

                            @Override
                            public boolean onSuccess(String resp) {
                                return false;
                            }

                            @Override
                            public boolean onError(String resp) {
                                return false;
                            }
                        });

                        mydb.deleteParametro(8);
                        mydb.insertParametro(8,String.valueOf(SOCIEDAD));
                        GoToMain();
                        return false;
                    }

                    @Override
                    public boolean onError(String resp) {
                        mydb.deleteParametro(5);
                        mydb.deleteParametro(6);
                        if(mydb.getSingleParametro(5)==null) {
                            Boolean i = mydb.insertParametro(5, mEmail);
                        }
                        if(mydb.getSingleParametro(6)==null) {
                            Boolean i = mydb.insertParametro(6, mPassword);
                        }
                        ObtenerApps(mEmail, new VolleyCallback() {
                            @Override
                            public boolean onSuccess() {
                                return false;
                            }

                            @Override
                            public boolean onError() {
                                return false;
                            }

                            @Override
                            public boolean onSuccess(String resp) {
                                return false;
                            }

                            @Override
                            public boolean onError(String resp) {
                                return false;
                            }
                        });

                        mydb.deleteParametro(8);
                        mydb.insertParametro(8,String.valueOf(SOCIEDAD));

                        return false;
                    }
                });
                // Simulate network access.

            // TODO: register the new account here.
            return ok;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);
            showMessages();
            if (success) {

                GoToMain();
            } else {
               /* mPasswordView.setError(getString(R.string.error_incorrect_password));
                mPasswordView.requestFocus();*/
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
        //OkHttpClient client = new OkHttpClient();
        private boolean ObtenerApps(String usrLogin, final VolleyCallback callback){

            Parametros token = mydb.getSingleParametro(4);
            String sToken = "";
            if (token != null) {
                sToken = token.getValor();
            }
            String  url = DBHelper.urlServer+"/api/Sesiones/AplicacionesPorUsuario?login="+usrLogin;
            //HttpGet httpGet = new HttpGet(DBHelper.urlServer+"/api/Sesiones/AplicacionesPorUsuario?login="+usrLogin);
            //httpGet.setHeader("token", sToken);
            //HttpClient httpClient = new NetworkUtility().getDefaultHttpClient(LoginActivity.this, DBHelper.urlServer+"/api/Sesiones/AplicacionesPorUsuario?login="+usrLogin);
            RequestQueue requestQueue = NetworkUtility.getInstance(getApplicationContext()).getRequestQueue();
            NetworkUtility.nuke();
            final String finalSToken = sToken;

            JsonArrayRequest
                    jsonObjectRequest
                    = new JsonArrayRequest(
                    Request.Method.GET,
                    url,
                    null,
                    new Response.Listener() {
                        @Override
                        public void onResponse(Object response) {
                            //HttpResponse response = httpClient.execute(httpPost);
                            if (response != null) {


                                JSONArray data = (JSONArray) response;
                                try {
                                    mydb.deleteAplicacion();
                                    int ctdap = mydb.getAllAplicaciones().length;
                                    for (int i = 0; i < data.length(); i++) {
                                        JSONObject hda = data.getJSONObject(i);
                                        String id = hda.getString("ApplicationId");
                                        mydb.insertAplicacion(id);
                                        String msg ="ok";
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                callback.onSuccess();

                            } else {
                                callback.onError();
                            }
                        }

                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                        }
                    }) {

                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", "application/json");
                    headers.put("token", finalSToken);
                    return headers;
                }

            };


            requestQueue.add(jsonObjectRequest);
           return true;
        }
        private void Autenticar(String usrLogin, String usrPwd, final VolleyCallback callback) {


            Parametros appUsr = mydb.getSingleParametro(1);
            Parametros appPwd = mydb.getSingleParametro(2);

            String sAppUsr = "";
            String sAppPwd = "";

            if (appUsr != null) {
                sAppUsr = appUsr.getValor();
            }

            if (appPwd != null) {
                sAppPwd = appPwd.getValor();
            }





       /* List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
        nameValuePairs.add(new BasicNameValuePair("imei", imei));
        nameValuePairs.add(new BasicNameValuePair("appUsr", sAppUsr));
        nameValuePairs.add(new BasicNameValuePair("appPwd", sAppPwd));*/
            String url =  DBHelper.urlServer+"/api/Sesiones/ServiceLogin?imei=" + imei + "&appUsr=" + sAppUsr + "&appPwd=" + sAppPwd+"&usrLogin="+usrLogin+"&usrPwd="+usrPwd;
            //HttpGet httpGet = new HttpGet(DBHelper.urlServer+"/api/Sesiones/ServiceLogin?imei=" + "356605058346996"/*imei*/ + "&appUsr=" + sAppUsr + "&appPwd=" + sAppPwd+"&usrLogin="+usrLogin+"&usrPwd="+usrPwd);
            RequestQueue requestQueue = NetworkUtility.getInstance(getApplicationContext()).getRequestQueue();
            NetworkUtility.nuke();


            JsonObjectRequest
                    jsonObjectRequest
                    = new JsonObjectRequest(
                    Request.Method.GET,
                    url,
                    null,
                    new Response.Listener() {
                        @Override
                        public void onResponse(Object response) {
                            //HttpResponse response = httpClient.execute(httpPost);
                            if (response != null) {


                                JSONObject data = (JSONObject) response;
                                try {
                                    if (data.getBoolean("Key") == true) {
                                        if (mydb.getSingleParametro(4) == null) {
                                            mydb.insertParametro(4,  data.getString("Value"));
                                        } else {
                                            mydb.updateParametro(4,  data.getString("Value"));
                                        }
                                        //Toast.makeText(LoginActivity.this,"Autenticación exitosa",Toast.LENGTH_LONG).show();
                                        txtFToast = "¡Bienvenido!";
                                        callback.onSuccess(txtFToast);
                                    } else {
                                        String responseStr = data.getString("Value");//EntityUtils.toString(response.getEntity());
                                        txtFToast = "Respuesta recibida del servidor: " + responseStr;
                                        //Toast.makeText(LoginActivity.this,"Respuesta recibida del servidor: " + responseStr,Toast.LENGTH_LONG).show();
                                        callback.onError(txtFToast);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    callback.onError("Ocurrió un error al recibir los datos");
                                }


                            } else {
                                callback.onError("El servidor no devolvió información");
                            }
                        }

                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                        }
                    }) {

                @Override
                public Map getHeaders() throws AuthFailureError {
                    HashMap headers = new HashMap();
                    headers.put("Content-Type", "application/json");

                    return headers;
                }

            };


            requestQueue.add(jsonObjectRequest);
            //HttpClient httpClient = new NetworkUtility().getDefaultHttpClient(LoginActivity.this, DBHelper.urlServer+"/api/Sesiones/ServiceLogin?imei=" + "356605058346996"/*imei*/ + "&appUsr=" + sAppUsr + "&appPwd=" + sAppPwd+"&usrLogin="+usrLogin+"&usrPwd="+usrPwd);

        }
    }

    private class MySimpleArrayAdapter extends ArrayAdapter<Empresas> implements View.OnClickListener {
        private final Context context;
        private final List<Empresas> values;

        public MySimpleArrayAdapter(Context context, List<Empresas> values) {
            super(context,R.layout.itemsimple,R.id.txtItem, values);
            this.context = context;
            this.values = values;
        }

        @Override
        public Empresas getItem(int position) {
            return values.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.itemsimple, parent, false);
            TextView insumo = (TextView) rowView.findViewById(R.id.txtItem);
            Empresas e  = values.get(position);
            insumo.setText(e.getSociedad()+" - "+e.getDsEmpresa());;
            return rowView;
        }
        @Override
        public void onClick(View v) {


        }
    }


}

