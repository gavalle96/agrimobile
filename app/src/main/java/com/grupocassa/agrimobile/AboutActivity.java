package com.grupocassa.agrimobile;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.util.Calendar;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        Calendar cal = Calendar.getInstance();

        int  anio = cal.get(Calendar.YEAR);
        String version = "Version x.x.x";
        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            version = "© "+anio+" - Grupo CASSA -Version "+ pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        setTitle("Acerca de AGRIMOBILE");

        TextView txtVersion = (TextView) findViewById(R.id.txtVersion);
        txtVersion.setText(version);
    }
}
